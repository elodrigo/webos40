var fs = require('fs');
var https = require('https');

var download365careUrl = 'download.365care.lge.com';
var companyInfo = '/info-service/v1/company-info/';
var pathAppList = '/info-service/v1/app-list/webos/';
var pathDownloadUrlPrefix = '/info-service/v1/app-download-url/webos';

function createRequestOptions(host, path) {
    return {
        host: host,
        path: path,
        method: 'GET',
        headers: {
            "Content-Type": "application/json"
        }
    }
}

function searchAccountName(accountNumber, callback){

    var nameData = '';
    var companyInfoAccount = companyInfo + accountNumber;
    var accountNameURL = createRequestOptions(download365careUrl, companyInfoAccount);
    var req = https.request(accountNameURL, function(res) {
        res.on('data', function (chunk) {
            nameData += chunk;
        });

        res.on('end', function() {
            var ret = JSON.parse(nameData);
            callback(ret);
        });

        res.on('error', function(e) {
            console.log('ERROR' + e);
            callback("ERROR");
        });
    });

    req.on('socket', function(socket) {
        socket.setTimeout(5000, function() {
            req.abort();
        });
    });

    req.on('error', function(e) {
        console.log('ERROR' + e);
        if (e == 'Error: socket hang up') {
            callback("ERROR");
        } else {
            callback("TIME");
        }
    });

    req.end();
}

function appDownloadURL(commer365CareServiceMode, callback) {

    var premanData = '';
    var downloadData = '';
    var serviceModePathAppList = pathAppList + commer365CareServiceMode;
    var option = createRequestOptions(download365careUrl, serviceModePathAppList);
    var req = https.request(option, function(res) {
        res.on('data', function (chunk) {
            premanData += chunk;
        });

        res.on('end', function() {
            var ret = JSON.parse(premanData);
            var path = pathDownloadUrlPrefix + '/' + ret.appNames.DownloadablePreman;
            console.log(path);
            var pathOption = createRequestOptions(download365careUrl, path);
            var pathReq = https.request(pathOption, function(pathRes) {
                pathRes.on('data', function (chunk) {
                    downloadData += chunk;
                });

                pathRes.on('end', function() {
                    var pathRet = JSON.parse(downloadData);
                    callback({signature :pathRet.signature, downloadUrl :pathRet.downloadUrl});
                });

                pathRes.on('error', function(e) {
                    callback("ERROR");
                    console.log('downURL_ERROR', e);
                });
            });

            pathReq.on('socket', function(socket) {
                socket.setTimeout(5000, function() {
                    pathReq.abort();
                });
            });

            pathReq.on('error', function(e) {
                callback("ERROR");
                console.log('PATH_ERROR', e);
            });
            pathReq.end();
        });
    });

    req.on('socket', function(socket) {
        socket.setTimeout(5000, function() {
            req.abort();
        });
    });

    req.on('error', function(e) {
        console.log('ERROR' + e);
        if (e == 'Error: socket hang up') {
            callback("ERROR");
        } else {
            callback("TIME");
        }
    });
    req.end();
}

exports.appDownloadURL = appDownloadURL;
exports.searchAccountName = searchAccountName;
