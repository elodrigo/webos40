/**
 * Copyright(c) 2017 by LG Electronics Inc.
 */
var fs = require('fs');
var password = require('./passwd');
var web = require('./web');
var languageManager = require('./LanguageManager');

var FIRST_LOGIN_CHECK_FILE = "/var/luna/preferences/first_login_check.json";
var MAX_LOGIN_TIMES = 5;
var LOGIN_STATUS_RESTRICTED = "restricted";

var captchaStr = "";

function checkLogin(req, res, next) {
    if (!req.session || !req.session.login) {
        if (req.url !== "/main") {
            res.cookie("isLogin", false);
            setRequestParams(req);
            res.redirect("/main");
        } else {
            setRequestParams(req);
            next();
        }
    } else {
        res.cookie("isLogin", true);
        setRequestParams(req);
        next();
    }
}

function setRequestParams(req) {
    req.parameters = {
        languageTable: languageManager.languageTable,
        currentTime: (new Date()).toString()
    };
}

var loginTryTimes = 0;
var loginRestrictedSeconds = 0;
var loginRestrictedInterval = undefined;
var resultData = '';

function reduceLoginRestrictedSeconds() {
    if (loginRestrictedSeconds > 0) {
        --loginRestrictedSeconds;
        resultData = LOGIN_STATUS_RESTRICTED + ':' + loginTryTimes + ':' + loginRestrictedSeconds;
    } else {
        loginTryTimes = 0;
        resultData = '';
        clearInterval(loginRestrictedInterval);
        loginRestrictedInterval = undefined;
    }
}

function checkPassword(req, res) {
    resultData = "success";

    if (loginRestrictedSeconds > 0) {
        resultData = LOGIN_STATUS_RESTRICTED + ':' + MAX_LOGIN_TIMES + ':' + loginRestrictedSeconds;
        res.send(resultData);
        return;
    }

    var isOkPinPwd = password.checkPasswd(req.body.password, web.env.powerOnlyMode);
    var isOkCaptcha = web.env.powerOnlyMode || (req.body.captcha === captchaStr.toString());

    if (isOkPinPwd && isOkCaptcha) {
        // process session
        if (isFirstLogin()) {
            resultData = "first_login";
        } else {
            resultData = "success";
        }
        loginTryTimes = 0;
        loginRestrictedSeconds = 0;
        req.session.login = true;
    } else {
        ++loginTryTimes;
        if (loginTryTimes >= MAX_LOGIN_TIMES) {
            loginRestrictedSeconds = 5 * 60;
            resultData = LOGIN_STATUS_RESTRICTED + ':' + loginTryTimes + ':' + loginRestrictedSeconds;
            if (loginRestrictedInterval == undefined) {
                loginRestrictedInterval = setInterval(reduceLoginRestrictedSeconds, 1000);
            }
        } else {
            resultData = "fail:"+ loginTryTimes + ':0';
        }
    }

    res.send(resultData);
}

function isFirstLogin() {
    if (web.env.powerOnlyMode) {
        return false;
    }
    var retVal = true;
    try {
        fs.readFileSync(FIRST_LOGIN_CHECK_FILE);
        retVal = false;
    } catch (err) {
        console.log("first login is true");
    }
    return retVal;
}

function setFirstLoginDone() {
    if (isFirstLogin()) {
        var information = {
            isFirstLogin: false
        };
        fs.writeFileSync(FIRST_LOGIN_CHECK_FILE, JSON.stringify(information));
    }
}

function setCaptchaStr(value) {
    captchaStr = value;
}

function getCaptchaStr() {
    return captchaStr;
}
function getCurrentStatus() {
    return resultData;
}

exports.checkLogin = checkLogin;
exports.checkPassword = checkPassword;
exports.setCaptchaStr = setCaptchaStr;
exports.getCaptchaStr = getCaptchaStr;
exports.getCurrentStatus = getCurrentStatus;
exports.setFirstLoginDone = setFirstLoginDone;
exports.isFirstLogin = isFirstLogin;
