var fs = require('fs');
var web = require('./web');
var api = require('./api');
var async = require('async');

var luna = undefined;
var remoCode = {};

function getSystemSettings(category, keys, callback) {
    luna.call("luna://com.webos.settingsservice/getSystemSettings", {
        category: category,
        keys: keys
    }, callback);
}

function setSystemSettings(category, settings, callback) {
    luna.call("luna://com.webos.settingsservice/setSystemSettings", {
        category: category,
        settings: settings
    }, callback);
}

function getSystemProperties(keys, callback) {
    luna.call("luna://com.webos.service.tv.systemproperty/getProperties", {
        keys: keys
    }, callback);
}

function setSystemProperties(properties, callback) {
    luna.call("luna://com.webos.service.tv.systemproperty/setProperties",
        properties, callback);
}

function getCurrentTimeLuna(callback) {
    luna.call("luna://com.palm.systemservice/time/getSystemTime", {}, callback);
}

function setCurrentTime(utc, callback) {
    luna.call("luna://com.palm.systemservice/time/setSystemTime", {utc: utc}, callback);
}

function getCurrentTime(callback) {
    getCurrentTimeLuna(function (time) {
        var currentdate = new Date();

        var proc = String(fs.readFileSync('/proc/uptime'));
        proc = proc.substr(0, proc.indexOf(' '));
        var uptime = Number(proc);
        var second = Math.floor(uptime) % 60;
        var minute = Math.floor(uptime / 60) % 60;
        var hours = Math.floor(uptime / 60 / 60) % 24;
        var days = Math.floor(uptime / 60 / 60 / 24);

        var strUpTime = "" + days + " Day " + hours + " H " + minute + " M " + second + " S";

        var localtime = time.payload.localtime;

        var year = localtime.year;
        var month = localtime.month;
        var day = localtime.day;
        var hour = localtime.hour;
        var minute = localtime.minute;
        var second = localtime.second;

        var date = new Date(year, month - 1, day, hour, minute, second);

        var ret = {
            current: date.toString(),
            uptime: strUpTime,
            year: year,
            month: month,
            day: day,
            hour: hour,
            minute: minute
        };

        callback(ret);
    });
}

function getTemperature(callback) {
    luna.call("luna://com.webos.service.tv.outdoor/getTemperature", {}, callback);
}

function getAllTemperature(callback) {
    getTemperature(function (m) {
        var ret = {
            returnValue: m.payload.returnValue
        };

        var types = m.payload.group ? m.payload.group : [];
        types.forEach(function (type) {
            var sensor = m.payload[type];
            var temperature = sensor.celsius;
            ret[type] = temperature
        });

        callback(ret);
    });
}

function getForegroundAppInfo(callback) {
    luna.call('luna://com.webos.applicationManager/getForegroundAppInfo', {}, function (msg) {
        callback(msg);
    });
}

function screenShot(path, height, callback, format) {
    if (!format) {
        format = 'jpg';
    }

    var data = {
        path: path,
        method: 'DISPLAY',
        width: Math.round((height * web.env.screen.width / web.env.screen.height)),
        height: height,
        format: format
    };

    luna.call('luna://com.webos.service.tv.capture/executeOneShot', data, function (msg) {
        callback(msg);
    });
}

function getNetworkStatus(callback) {
    luna.call("luna://com.webos.service.connectionmanager/getstatus", {}, function (net) {
        getMacAddr(function (mac) {
            if (mac.payload.returnValue) {
                net.payload.wired.macAddress = mac.payload.wiredInfo ? mac.payload.wiredInfo.macAddress : 'None';
                net.payload.wifi.macAddress = mac.payload.wifiInfo ? mac.payload.wifiInfo.macAddress : 'None';
            }
            callback(net);
        });
    });
}

function getMacAddr(callback) {
    luna.call("luna://com.webos.service.connectionmanager/getinfo", {}, callback);
}

function getDoorStatus(callback) {
    luna.call("luna://com.webos.service.tv.outdoor/getDoorState", {}, callback);
}

function getFanStatus(callback) {
    luna.call("luna://com.webos.service.tv.outdoor/getFan", {}, callback);
}

function getDowntimeIncident(callback) {
    luna.call("luna://com.webos.service.tv.outdoor/getDowntimeIncident", {}, callback);
}

function getCheckScreenOn(callback) {
    getSystemSettings('commercial', ['checkScreen'], callback);
}

function getCheckScreenInfo(callback) {
    luna.call("luna://com.webos.service.tv.outdoor/getCheckScreen", {}, callback);
}

function calibrateCheckScreenEYEKey(callback) {
    luna.call("luna://com.webos.service.tv.outdoor/calibrateCheckScreenEYEKey", {}, callback);
}

function isNoSignal(callback) {
    luna.call("luna://com.webos.service.tv.signage/isNoSignal", {}, callback);
}

function getVideoSize(callback) {
    luna.call("luna://com.webos.service.tv.broadcast/getPipelineTemporary", {}, function (m) {
        if (m.payload.returnValue) {
            luna.call("luna://com.webos.service.tv.display/getCurrentVideo", {context: m.payload.broadcastId}, callback);
        } else {
            callback({
                payload: {
                    returnValue: false
                }
            });
        }
    });
}

function getVideoStillStatus(callback) {
    luna.call("luna://com.webos.service.tv.outdoor/isStalledImage", {}, callback);
}

function getBasicInfo(callback) {
    var keys = ["firmwareVersion", "micomVersion", "serialNumber",
        "hardwareVersion", "bootLoaderVersion", "sdkVersion", "modelName"];
    getSystemProperties(keys, callback);
}

function getInputList(callback) {
    luna.call('luna://com.webos.service.eim/getAllInputStatus', {}, callback);
}

function getCurrentInput(callback) {
    luna.call("luna://com.webos.service.tv.externaldevice/getCurrentInput", {}, callback);
}

function setCurrentInput(id, callback) {
    sendRemocon(id, callback);
}

function sendRemocon(key, callback) {
    var code = remoCode[key];

    if (!code) {
        return;
    }

    if (code == "signage") {
        var sub = luna.call("luna://com.palm.applicationManager/launch", {
            id: "com.webos.app.installation"
        }, callback);
        return;
    }

    var sub = luna.call("luna://com.webos.service.tv.legacyinput/createKeyEvent", {
        type: "single",
        code: code
    }, callback);
}

function getVolume(callback) {
    luna.call("luna://com.webos.audio/getVolume", {}, callback);
}

function setVolume(volume, callback) {
    luna.call("luna://com.webos.audio/setVolume", {
        volume: volume
    }, callback);
}

function setMuted(muted, callback) {
    luna.call("luna://com.webos.audio/setMuted", {
        muted: muted
    }, callback);
}

function getWebOSInfo(callback) {
    luna.call("luna://com.palm.systemservice/osInfo/query", {
        parameters: ["core_os_release", "core_os_release_codename"]
    }, callback);
}

function getFanMicomInfo(callback) {
    luna.call("luna://com.webos.service.tv.outdoor/getFanMicomVersion", {}, callback);
}

function getTempHistory(param, callback) {
    luna.call("luna://com.webos.service.tv.outdoor/getTemperatureHistory", param, callback);
}

function getFanRpmHistory(param, callback) {
    luna.call("luna://com.webos.service.tv.outdoor/getFanRpmHistory", param, callback);
}

function getBacklightHistory(param, callback) {
    luna.call("luna://com.webos.service.tv.outdoor/getBacklightHistory", param, callback);
}

function getContrastHistory(param, callback) {
    luna.call("luna://com.webos.service.tv.outdoor/getContrastHistory", param, callback);
}

function getEyeQSensorHistory(param, callback) {
    if (!param.id) {
        param.id = 'main';
    }

    luna.call("luna://com.webos.service.tv.outdoor/getEyeQHistory", param, callback);
}

exports.getHumidityHistory = function (param, callback) {
    luna.call('luna://com.webos.service.tv.outdoor/getHumidity', {}, function (m) {
        if (m.payload.returnValue && m.payload.support) {
            param.id = m.payload.group[0];
            luna.call("luna://com.webos.service.tv.outdoor/getHumidityHistory", param, callback);
        }
    });
}

function getPanelErrorOut(callback) {
    luna.call("luna://com.webos.service.tv.outdoor/getPanelErrorOut", {subscribe: true}, callback);
}

function getBacklight(param, callback) {
    luna.call("luna://com.webos.service.tv.outdoor/getBacklight", param, callback);
}

function setBacklight(param, callback) {
    luna.call("luna://com.webos.service.tv.outdoor/setBacklight", param, callback);
}

function getListDevices(param, callback) {
    luna.call("luna://com.webos.service.attachedstoragemanager/listDevices", param, callback);
}

function getUSBfileList(param, callback) {
    luna.call("luna://com.webos.service.attachedstoragemanager/listFolderContents", param, callback);
}

function copyMediaFile(param, callback) {
    luna.call("luna://com.webos.service.attachedstoragemanager/copy", param, callback);
}

function deleteInternalFile(param, callback) {
    luna.call("luna://com.webos.service.attachedstoragemanager/delete", param, callback);
}

function setInputSouce(appId, callback) {
    luna.call("luna://com.palm.applicationManager/launch", {id: appId}, callback);
}

function getAspectRatioDesc(callback) {
    luna.call("luna://com.webos.settingsservice/getSystemSettingDesc", {
        "category": "aspectRatio",
        "keys": ["arcPerApp"],
        "current_app": true
    }, callback);
}

function getAspectRatio(callback) {
    var result = {payload: {aspectRatio: "original"}};
    getAspectRatioDesc(function (m) {
        try {
            result.payload.active = m.payload.results[0].ui.active;
            if (m.payload.results[0].ui.active === false || !m.payload.app_id) {
                result.payload.aspectRatio = m.payload.results[0].values.inactiveValue;
                callback(result);
            } else {
                luna.call("luna://com.webos.settingsservice/getSystemSettings", {
                    category: "aspectRatio",
                    keys: ["arcPerApp"],
                    app_id: m.payload.app_id
                }, function (m2) {
                    if (m2.payload.returnValue === true) {
                        if (m2.payload.settings.arcPerApp === "16x9") {
                            result.payload.aspectRatio = "full";
                        } else {
                            result.payload.aspectRatio = m2.payload.settings.arcPerApp;
                        }
                    }
                    callback(result);
                });
            }
        } catch (e) {
            callback(result);
        }
    });
}

function setAspectRatio(rotationAspectRatio, callback) {
    var tasks = [
        function (callback) {
            getAspectRatioDesc(function (m) {
                callback(null, m.payload.app_id);
            })
        },
        function (arg1, callback) {
            var params = {
                category: 'aspectRatio',
                settings: {
                    arcPerApp: rotationAspectRatio === 'full' ? '16x9' : rotationAspectRatio,
                },
                app_id: arg1
            };
            luna.call("luna://com.webos.settingsservice/setSystemSettings", params, function (m) {
                callback(null, m);
            });
        }
    ];

    async.waterfall(tasks, function (err, result) {
        callback(result)
    });
}

function getHdmiPcMode(callback) {
    luna.call("luna://com.lge.settingsservice/getSystemSettings", {category: "other", keys: ["hdmiPcMode"]}, callback);
}

function listUpdates(callback) {
    luna.call("luna://com.webos.service.update/listUpdatesInUsb", {
		path: '/media/update/'
	}, callback);
}

function createRemoCodeMap() {
    remoCode = JSON.parse(fs.readFileSync(__dirname + '/env/remocode.json'));
}

exports.getOSDLockMode = function (callback) {
    callAPI('getOSDLockMode', {}, 'OSDLockMode', callback);
}

exports.getRecentsAppInfo = function (callback) {
    luna.call("luna://com.webos.surfacemanager/getRecentsAppList", {}, callback);
}

exports.getSDMInfo = function (callback) {
    luna.call("luna://com.webos.service.tv.outdoor/getSDMInfo", {"subscribe":true}, callback);
}

exports.getCurrent = function (callback) {
    luna.call('luna://com.webos.service.tv.outdoor/getCurrent', {}, callback);
}

exports.getBluMaintain = function (callback) {
    luna.call('luna://com.webos.service.tv.outdoor/getBluMaintain', {}, callback);
}

exports.getSolar = function (callback) {
    luna.call('luna://com.webos.service.tv.outdoor/getSolar', {}, callback);
}

exports.getCurrentHistory = function (param, callback) {
    luna.call('luna://com.webos.service.tv.outdoor/getCurrentHistory', param, callback);
}

exports.getBluMaintainHistory = function (param, callback) {
    luna.call('luna://com.webos.service.tv.outdoor/getBluMaintainHistory', param, callback);
}

exports.getSolarHistory = function (param, callback) {
    if (!param.id) {
        param.id = 'main';
    }

    luna.call('luna://com.webos.service.tv.outdoor/getSolarHistory', param, callback);
}

function init(l) {
    luna = l;
}

exports.getSystemSettings = getSystemSettings;
exports.setSystemSettings = setSystemSettings;
exports.getSystemProperties = getSystemProperties;
exports.setSystemProperties = setSystemProperties;

exports.getCurrentTime = getCurrentTime;
exports.setCurrentTime = setCurrentTime;
exports.screenShot = screenShot;
exports.getTemperature = getTemperature;
exports.getAllTemperature = getAllTemperature;
exports.getForegroundAppInfo = getForegroundAppInfo;
exports.getNetworkStatus = getNetworkStatus;
exports.getMacAddr = getMacAddr;
exports.getDoorStatus = getDoorStatus;
exports.getFanStatus = getFanStatus;
exports.getDowntimeIncident = getDowntimeIncident;
exports.getCheckScreenOn = getCheckScreenOn;
exports.getCheckScreenInfo = getCheckScreenInfo;
exports.calibrateCheckScreenEYEKey = calibrateCheckScreenEYEKey;

exports.isNoSignal = isNoSignal;
exports.getVideoSize = getVideoSize;
exports.getVideoStillStatus = getVideoStillStatus;

exports.getBasicInfo = getBasicInfo;

exports.getInputList = getInputList;
exports.getCurrentInput = getCurrentInput;
exports.setCurrentInput = setCurrentInput;

exports.sendRemocon = sendRemocon;

exports.getVolume = getVolume;
exports.setVolume = setVolume;
exports.setMuted = setMuted;

exports.getWebOSInfo = getWebOSInfo;
exports.getFanMicomInfo = getFanMicomInfo;

exports.getTempHistory = getTempHistory;
exports.getFanRpmHistory = getFanRpmHistory;
exports.getBacklightHistory = getBacklightHistory;
exports.getContrastHistory = getContrastHistory;
exports.getEyeQSensorHistory = getEyeQSensorHistory;
exports.getPanelErrorOut = getPanelErrorOut;
exports.getBacklight = getBacklight;
exports.setBacklight = setBacklight;
exports.getListDevices = getListDevices;
exports.getUSBfileList = getUSBfileList;
exports.copyMediaFile = copyMediaFile;
exports.deleteInternalFile = deleteInternalFile;
exports.setInputSouce = setInputSouce;
exports.setAspectRatio = setAspectRatio;
exports.getAspectRatio = getAspectRatio;
exports.getAspectRatioDesc = getAspectRatioDesc;
exports.getHdmiPcMode = getHdmiPcMode;

exports.listUpdates = listUpdates;

exports.init = init;
exports.createRemoCodeMap = createRemoCodeMap;
