#!/bin/bash

if [ -z ${1+x} ]; 
then ip=192.168.1.244; 
else ip=$1;
fi

scp -r views/*.* root@$ip:/media/developer/com.webos.service.outdoorwebcontrol/views/
scp -r public/js/design/*.* root@$ip:/media/developer/com.webos.service.outdoorwebcontrol/public/js/design/
scp views/partials/*.* root@$ip:/media/developer/com.webos.service.outdoorwebcontrol/views/partials/
scp views/ledSignage/*.* root@$ip:/media/developer/com.webos.service.outdoorwebcontrol/views/ledSignage/
scp routes/index.js root@$ip:/media/developer/com.webos.service.outdoorwebcontrol/routes/index.js
scp app.js root@$ip:/media/developer/com.webos.service.outdoorwebcontrol/app.js
scp -r public/js/design/ledSignage root@$ip:/media/developer/com.webos.service.outdoorwebcontrol/public/js/design/
scp -r public/js/design/modal root@$ip:/media/developer/com.webos.service.outdoorwebcontrol/public/js/design/
scp LanguageManager.js root@$ip:/media/developer/com.webos.service.outdoorwebcontrol/LanguageManager.js
scp web.js root@$ip:/media/developer/com.webos.service.outdoorwebcontrol/web.js
scp public/js/page/ledSignage/signage_common.js root@$ip:/media/developer/com.webos.service.outdoorwebcontrol/public/js/page/ledSignage/signage_common.js
scp public/js/page/ledSignage/signage_login.js root@$ip:/media/developer/com.webos.service.outdoorwebcontrol/public/js/page/ledSignage/signage_login.js
scp loginChecker.js root@$ip:/media/developer/com.webos.service.outdoorwebcontrol/