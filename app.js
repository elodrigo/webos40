var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var routes = require('./routes/index');
var factorymenu = require('./routes/factorymenu');
var ltf = require('./routes/ltf');
// var mobile = require('./routes/mobile');
var rest = require('./routes/rest');
var hbs = require('hbs');
var web = require('./web');
var helmet = require('helmet');

var app = express();
hbs.registerPartials(__dirname + '/views/partials');

// view engine setup
app.set('views', [
	path.join(__dirname, 'views'),
	path.join(__dirname, 'views/factorymenu'),
	// path.join(__dirname, 'views/mobile')
]);
app.set('view engine', 'hbs');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
if (app.get('env') === 'development') {
	app.use(logger('dev'));
}
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: false
}));
app.use(cookieParser());

app.use(helmet({frameguard: {action: 'DENY'}}));
app.use(helmet.xssFilter());
app.use(helmet.noSniff());
app.use(helmet.noCache());
app.use(helmet.contentSecurityPolicy({
	directives: {
		defaultSrc: ["'self'"],
		// fontSrc: ["'self'", 'https://fonts.gstatic.com'],
		// styleSrc: ["'self'", "'unsafe-inline'"],
		scriptSrc: ["'self'", "'unsafe-inline'", "'unsafe-eval'"],
		connectSrc: ["'self'", "ws:", "wss:"]
	},
	reportOnly: true
}));
app.use(helmet.hsts({maxAge : 31536000}));

var expressSession = require('express-session');
var FileStore = require('session-file-store')(expressSession);

var session = expressSession({
	store: new FileStore({
		ttl: 15 * 60, // 15 minutes
		path: "/var/run/webgateway",
		encrypt: true
	}),
	secret: 'webgateway',
	resave: false,
	saveUninitialized: true,
	cookie: {
		secure: true,
		httpOnly: true
	}
});

app.use(session);

app.use('/login.html', function (req, res, next) {
    if (web.env.supportLedSignage) {
        if (web.env.fixedModel && !web.env.powerOnlyMode) {
            res.json({err: 'not supported'});
            res.end();
            return;
        }
        next();
    } else {
	    web.getLocalIPAddress(req, function (ipAddress) {
		    console.log("Go to main page: " + 'https://' + ipAddress + ":" + routes.WEBGATEWAY_PORT);
		    res.redirect('https://' + ipAddress + ":" + routes.WEBGATEWAY_PORT);
	    });
    }
});

app.use('/tmp/tnm', express.static(path.join(__dirname, 'public/tmp/tnm'), {
	dotfiles: 'allow'
}));
app.use(express.static(path.join(__dirname, 'public')));

app.set('json spaces', '  ');
app.use('/rest', rest);
app.use('/factorymenu', factorymenu);
app.use('/ltf',ltf);
// app.use('/mobile', mobile);
app.use('/', routes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// error handlers
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
	app.use(function(err, req, res, next) {
		res.status(err.status || 500);
		res.render('error', {
			layout: false,
			message: err.message,
			error: err
		});
	});
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
	res.status(err.status || 500);
	res.render('error', {
		layout: false,
		message: err.message,
		error: {status: err.status}
	});
});

hbs.registerHelper('fileName', function(full) {
	var str = "" + full;
	var split = str.lastIndexOf('.');
	var name = str.substring(0, split);
	var ext = str.substr(split + 1);
	if (name.length > 20) {
		str = name.substr(0, 20) + "...." + ext;
	}
	return str;
});

hbs.registerHelper('logNumber', function(fileName) {
	var str = "" + fileName;
	str = str.substr(3, 5);
	return Number(str);
});

hbs.registerHelper('isCurFile', function(fileName, curFile) {
	return fileName == curFile ? "class='active'" : "";
});

hbs.registerHelper('encodeFilenameURI', function(dir, filename) {
	var uri = dir + '/' + filename;
	uri = encodeURIComponent(uri);
	return uri;
});

hbs.registerHelper('supportMicomFanControl', function(block) {
	if (web.env.outdoor && web.env.fanControl.fanMicom > 0) {
		return block.fn(this);
	} else {
		return block.inverse(this);
	}
});

hbs.registerHelper('supportFan', function(block) {
	if (web.env.outdoor && web.env.fanControl.support) {
		return block.fn(this);
	} else {
		return block.inverse(this);
	}
});

hbs.registerHelper('supportWifi', function(block) {
	if (web.env.disableWifi) {
		return block.inverse(this);
	} else {
		return block.fn(this);
	}
});

hbs.registerHelper('supportLedSignage', function(block) {
	if (web.env.supportLedSignage) {
		return block.fn(this);
	} else {
		return block.inverse(this);
	}
});

hbs.registerHelper('notSupportLedSignage', function(block) {
	if (!web.env.supportLedSignage) {
		return block.fn(this);
	} else {
		return block.inverse(this);
	}
});


hbs.registerHelper('fileSize', function(size) {
	size = Number(size);
	var K = 1024;
	var M = K * K;
	var G = M * K;

	if (size > G) {
		return (size / G).toFixed(2) + " GB";
	}
	if (size > M) {
		return (size / M).toFixed(2) + " MB";
	}
	if (size > K) {
		return (size / K).toFixed(2) + " KB";
	}

	return size + " B";
});

hbs.registerHelper('isNew', function(fileName, newFiles, options) {
	if(newFiles.indexOf(fileName) > -1) {
		return options.fn(this);
	}
	return options.inverse(this);
});

hbs.registerHelper('json', function(context) {
	return JSON.stringify(context);
});

hbs.registerHelper('getLanguageText', function (text) {
	return require("./LanguageManager").getLanguageText(text);
});

hbs.registerHelper('for', function (from, to, incr, block) {
	var retVal = "";
	for (var i = from; i <= to; i += incr) {
		retVal += block.fn(i);
	}
	return retVal;
});

hbs.registerHelper('firstLogin', function (block) {
	if (require("./loginChecker").isFirstLogin()) {
		return block.inverse(this);
	} else {
		return block.fn(this);
	}
});

hbs.registerHelper('notFirstLogin', function (block) {
	if (require("./loginChecker").isFirstLogin()) {
		return block.fn(this);
	} else {
		return block.inverse(this);
	}
});

hbs.registerHelper('notPowerOnlyMode', function (block) {
	if (!web.env.powerOnlyMode) {
		return block.fn(this);
	}
	return block.inverse(this);
});

hbs.registerHelper('checkToSupportChart', function (chart, block) {
	if (chart === 'backlight') {
		var supportBacklightChart = web.env.sensorChart.indexOf('backlight') > -1;
		var supporteyeQChart = web.env.sensorChart.indexOf('eyeQ') > -1;
		if (supportBacklightChart && supporteyeQChart) {
			return block.fn(this);
		} else {
			return block.inverse(this);
		}
	} else {
		if (web.env.sensorChart.indexOf(chart) > -1) {
			return block.fn(this);
		} else {
			return block.inverse(this);
		}
	}
});

hbs.registerHelper('signage365Care', function(block) {
    if (web.env.support365Care) {
        return block.fn(this);
    }
    return block.inverse(this);
});

hbs.registerHelper('cinemaModel', function(block) {
    if (web.env.supportCinemaModel) {
        return block.fn(this);
    }
    return block.inverse(this);
});

hbs.registerHelper('cinemaModelBlock', function(block) {
    if (web.env.supportCinemaModel) {
        return block.inverse(this);
    }
    return block.fn(this);
});

exports.app = app;
exports.session = session;
