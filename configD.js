var env = {};
var service = undefined;

exports.init = function (lunaService, envirment) {
    service = lunaService;
    env = envirment;
    geOledInfo();
    getBackupViaStorageInfo();
    getDPMModeInfo();
    getPmModeInfo();
    getPlayViaUrlInfo();
    getTileModeInfo();
    getScreenFaultDetetactionInfo()
    getHideMenusInfo();
    getNatureModeInfo();
    getPictureMode();
};

function geOledInfo() {
    service.call("luna://com.webos.service.tv.systemproperty/getProperties", {keys: ["OLED"]}, function (m) {
        env.oledModel = m.payload.OLED === "true" ? true : false;
    });
}

function getBackupViaStorageInfo() {
    service.call("luna://com.webos.settingsservice/getSystemSettingDesc", {"category": "commercial", keys: ["backupViaStorage"]}, function (m) {
        env.failover = {
            backupViaStorage : {
                off: { visible: false },
                auto: { visible: false },
                manual: { visible: false },
                supersign: { visible: false },
                siapp: { visible: false }
            }
        };

        if (m.payload.returnValue) {
            var itemList = m.payload.results[0].values.arrayExt;
            for (var i = 0; i < itemList.length; i++) {
                var item = m.payload.results[0].values.arrayExt[i];
                env.failover.backupViaStorage[item.value] = item;
            }
        }
    });
}

function getPmModeInfo() {
    service.call("luna://com.webos.settingsservice/getSystemSettingDesc", {"category": "commercial", keys: ["pmMode"]}, function (m) {
        if (!env.pmMode) {
            env.pmMode = {};
        }

        if (m.payload.returnValue) {
            env.pmMode.itemList = m.payload.results[0].values.arrayExt;
            env.pmMode.visible = m.payload.results[0].ui.visible;
        }
    });
}

function getDPMModeInfo() {
    service.call("luna://com.webos.settingsservice/getSystemSettingDesc", {"category": "commercial", keys: ["dpmMode"]}, function (m) {
        if (!env.dpmMode) {
            env.dpmMode = {};
        }

        if (m.payload.returnValue) {
            env.dpmMode.itemList = m.payload.results[0].values.arrayExt;
            env.dpmMode.visible = m.payload.results[0].ui.visible;
        }
    });
}

function getPlayViaUrlInfo() {
    service.call("luna://com.webos.applicationManager/getAppInfo", {"id": "com.webos.app.browser"}, function (m) {
        if (m.payload.returnValue) {
            env.playViaUrl = true;
        } else {
            env.playViaUrl = false;
        }
    });

}

function getTileModeInfo(){
    service.call("luna://com.webos.settingsservice/getSystemSettingDesc", {"category": "commercial", keys: ["tileMode"]}, function (m) {
        if (!env.tileMode) {
            env.tileMode = {};
        }

        if (m.payload.returnValue) {
            var menuInfo = m.payload.results[0].ui;
            env.tileMode = menuInfo;
        }
    });
}

function getScreenFaultDetetactionInfo() {
    // webos 4.0 not supported
    // outdoor model support
    env.screenFaultDetetactionInfo = {};
    env.screenFaultDetetactionInfo.visible = env.outdoor;
}

function getHideMenusInfo() {
    service.call("luna://com.webos.service.config/getConfigs", {"configNames": ["commercial.com.palm.app.settings.hideMenus"]}, function (m) {
        if (!env.hideMenus) {
            env.hideMenus = {};
        }

        // whether support sound
        env.hideMenus.sound = false;
        // whether support backlight
        env.hideMenus.backlight = false;


        if (m.payload.returnValue) {
            var hideMenus = m.payload.configs["commercial.com.palm.app.settings.hideMenus"];
            for (var i = 0; i < hideMenus.length; i++) {
                if (hideMenus[i] === "Sound") {
                    env.hideMenus.sound = true;
                }
                if (hideMenus[i] === "Picture Mode") {
                    env.hideMenus.backlight = true;
                }
            }
        }

        setSoundSupport();
    });
}

function setSoundSupport() {
    if (!env.hideMenus.sound) {
        if (!env.isHwSpeakerSupported) {
            var extSpkVolumeControl = service.subscribe('luna://com.webos.settingsservice/getSystemSettings', {
                category:"commercial",
                keys: ['extSpkVolumeControl'],
                subscribe: true
            });

            extSpkVolumeControl.on('response', function (m) {
                console.log("extSpkVolumeControl: " + m.payload.settings.extSpkVolumeControl);

                // extSpkVolumeControl -> "variable"
                if (m.payload.settings.extSpkVolumeControl === "1") {
                    env.hideMenus.sound = false;
                } else {
                    env.hideMenus.sound = true;
                }
                var wordsList = require("./wordsSelector").wordsList;
                wordsList.deviceTitle = env.hideMenus.sound ? "Display" : "Display & Sound";
            });
        }
    }
}

function getNatureModeInfo() {
    if (env.naturalMode === undefined) {
        env.naturalMode = {};
    }

    var api = "luna://com.webos.settingsservice/getSystemSettingDesc";
    var params = {
        category: "commercial",
        keys: ["naturalMode"]
    }
    var callback = function (res) {
        if (res.payload.returnValue === true) {
            env.naturalMode = res.payload.results[0].ui;
        }
    }
    service.call(api, params, callback);
}

function getPictureMode() {
    service.call("luna://com.webos.settingsservice/getSystemSettingDesc", {category: 'picture', keys: ['pictureMode']}, function (m) {
        env.pictureModeList = [];
        if (m.payload.returnValue) {
            var pictureModes = m.payload.results[0].values.arrayExt;
            for (var i = 0; i < pictureModes.length; i++) {
                if (pictureModes[i].visible === true) {
                    env.pictureModeList.push(pictureModes[i].value);
                }
            }
        }
    });
}
