var fs = require('fs');
var crypto = require('crypto');

var saveFile = "/var/luna/preferences/webgateway.json";

function checkPasswd(input, powerOnlyMode) {
	var json = ""
    var file = powerOnlyMode ? __dirname + '/env/pass.json' : saveFile;
    console.log(file);
	try {
		json = fs.readFileSync(file);
	} catch(err) {
		json = fs.readFileSync(__dirname + '/env/pass.json');
	}
	var passObj = JSON.parse(json);
	var salt = passObj.salt;
	var hash = passObj.hash;

	var genHash = crypto.createHash('sha512').update(salt + input).digest('hex');

	if (hash == genHash) {
		return true;
	}
	return false;
}

function createPasswd(word, salt, type) {
	if (!type) {
		type = 'sha512';
	}
	var hashpass = crypto.createHash(type).update(salt + word).digest('hex');

	return hashpass
}

function changePasswd(password) {
	var salt = Math.round((new Date().valueOf() * Math.random())) + '';

	var passObj = {};
	passObj.salt = salt;
	passObj.hash = createPasswd(password, salt);

	fs.writeFileSync(saveFile, JSON.stringify(passObj));
}

function isDefaultPasswdGenerated() {
	var json = ""
	try {
		json = fs.readFileSync(saveFile);
        return true;
	} catch(err) {
		json = fs.readFileSync(__dirname + '/env/pass.json');
        return false;
	}
    return false;
}

exports.checkPasswd = checkPasswd;
exports.changePasswd = changePasswd;
exports.createPasswd = createPasswd;
exports.isDefaultPasswdGenerated = isDefaultPasswdGenerated;
