var web = require("./web.js");
var env = web.env;

var wordsList = {};

exports.init = function () {
    setWords()
};

function setWords() {
    wordsList.backlight = "Backlight";
    wordsList.deviceTitle = "Display & Sound";
    try {
        env.oledModel ? wordsList.backlight = "OLED LIGHT" : wordsList.backlight = "Backlight";
        wordsList.deviceTitle = env.hideMenus.sound ? "Display" : "Display & Sound";
    } catch (e) {
        setTimeout(function () {
            setWords();
        }, 1000);
    }
}


exports.wordsList = wordsList;