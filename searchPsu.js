var net = require('net');
var lunaAPI = require('./luna');

var socketArray = [];
var socketCount = 0;
var changeIpCount = 0;
var actualChangeIpCount = 0;
var ipSearchRange = 0;
var psuMacAddrCommand = 'sn 00 ba ff\n';
var psuSetIdCommand = 'jx 00 ff\n';
var psuVersionCommand = 'fz 00 ff\n';
var psuIpSettingCommandHead = 'sn 00 80 01 ';
var psuIpSettingCommandTail = ' 255255255000 ';
var psuIpApplyCommand = 'sn 00 82 80\n';

function searchPsu(ipRange, callback) {
    ipSearchRange = Number(ipRange.endIp) - Number(ipRange.startIp) + 1;

    for(var i = Number(ipRange.startIp); i <= Number(ipRange.endIp); ++i) {
        var currentHost = ipRange.firstIp + '.' + ipRange.secondIp + '.' + ipRange.thirdIp + '.' + i;
        getConnection(currentHost, callback);
    }
}

function getConnection(currentHost, callback) {
    var readCount = 0;
    var macAddr = '';
    var setId = '';

    var socket = net.connect({port : 9761, host:currentHost}, function() {
        writeData(socket, psuMacAddrCommand);
        console.log("Connection! IP : " + currentHost);
    });

    socket.setTimeout(5000);

    socket.on('data', function(data) {
        readCount++;

        if(readCount == 1) {
            macAddr = data.toString().substr(21,17);
            writeData(socket, psuSetIdCommand);
        } else if (readCount == 2) {
            setId = data.toString().substr(7,2);
            writeData(socket, psuVersionCommand);
        } else if (readCount == 3) {
            var cpu = data.toString().substr(7,6);
            var formatCpu = cpu.substr(0,2) + '.' + cpu.substr(2,2) + '.' + cpu.substr(4,2);

            socketArray.push({ip :currentHost, macAddr :macAddr, setId :setId, cpu :formatCpu, micom :data.toString().substr(14,6)});
            socket.end();
        }
    });

    socket.on('error', function(err) {
        socket.destroy();
    });

    socket.on('timeout', function() {
        socket.destroy();
    });

    socket.on('close', function() {
        socketCount++;
        if (socketCount == ipSearchRange) {
            callback(socketArray);
            socketCount = 0;
            socketArray = [];
        }
    });
}

function ipFormatChange(changeIp) {
    var ipArray = changeIp.split('.');
    var preZero = '00';
    var resultIp = '';

    for (var pieceIp in ipArray) {
        var str = preZero + ipArray[pieceIp];
        resultIp += str.slice(-3);
    }
    return resultIp;
}

function changePsuIp(currentIp, changeIp, callback) {
    var socket = net.connect({port : 9761, host:currentIp}, function() {
        var settingCommand = psuIpSettingCommandHead + ipFormatChange(changeIp) + psuIpSettingCommandTail + ipFormatChange(changeIp).substr(0,9) + '001\n';
        writeData(socket, settingCommand);
        writeData(socket, psuIpApplyCommand);
    });

    socket.setTimeout(5000);

    socket.on('error', function(err) {
        socket.destroy();
    });

    socket.on('timeout', function() {
        socket.destroy();
    });

    socket.on('close', function() {
        if (actualChangeIpCount == changeIpCount) {
            callback(true);
        }
    });
}

function savePsuIp(psuIpArray, callback) {
    changeIpCount = 0;
    actualChangeIpCount = psuIpArray.length;

    for (var psuIp in psuIpArray) {
        changeIpCount++;
        changePsuIp(psuIpArray[psuIp].currentIp, psuIpArray[psuIp].changeIp, callback);
    }
}

function writeData(socket, data){
    var success = !socket.write(data);
    if (!success){
        (function(socket, data){
            socket.once('drain', function(){
            writeData(socket, data);
            });
        })(socket, data);
    }
}

exports.savePsuIp = savePsuIp
exports.searchPsu = searchPsu;
