var fs = require('fs');
var web = require('./web');
var luna;
var clientLocale = "en-US";
var languageTable = [];
var languageTableClient = [];

exports.init = function (lunaService) {
    luna = lunaService;

    setLanguageTable("en-US".split("-"));

    var language = luna.subscribe('luna://com.webos.settingsservice/getSystemSettings', {
        keys: ['localeInfo'],
        subscribe: true
    });

    language.on('response', function (ret) {
        var localeInfo = ret.payload.settings.localeInfo;
        var domain = localeInfo.locales.UI.split("-");
        var io = require("./api").io;
        setLanguageTable(domain);
        io.emit("locale", domain);
    });
};

function setLanguageTable(domain) {
    // console.log('here domain: ', domain)
    var fileName = "strings.json";
    var filePath = "/resources/";

    languageTable = [];
    for (var i = 0; i < domain.length; i++) {
        filePath = filePath + domain[i] + "/";
        try {
            languageTable[i] = JSON.parse(fs.readFileSync(__dirname + filePath + fileName));
        } catch (e) {
            languageTable[i] = {};
        }
    }
    exports.languageTable = languageTable;
}

exports.setLanguageTableClient = function (locale) {
    var fileName = "strings.json";
    var filePath = "/resources/";
    var localeArr = locale.split("-");

    languageTableClient = [];
    for (var i = 0; i < localeArr.length; i++) {
        filePath = filePath + localeArr[i] + "/";
        try {
            languageTableClient[i] = JSON.parse(fs.readFileSync(__dirname + filePath + fileName));
        } catch (e) {
            languageTableClient[i] = {};
        }
    }

    exports.clientLocale = locale;
    exports.languageTableClient = languageTableClient;
};

exports.getLanguageText = function (text) {
    var changedText = text + "";
    // var supportBrowserLocale = web.isSupportedBrowserLocale();
    var supportBrowserLocale = false;
    var table = supportBrowserLocale ? languageTableClient : languageTable;

    for (var i = table.length - 1; i >= 0; i--) {
        if (table[i][changedText]) {
            changedText = table[i][changedText];
        }
    }
    return changedText;
};

exports.clientLocale = clientLocale;
exports.languageTableClient = languageTableClient;
exports.languageTable = languageTable;