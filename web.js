var app = require('./app').app;
var session = require('./app').session;
var api = require("./api");
var ledAPI = require('./ledAPI');
var lunaAPI = require("./luna");
var fs = require('fs');
var https = require('https');
var crtpyo = require('crypto');

var passwd = require('./passwd');

var envStr = fs.readFileSync(__dirname + '/env/env.json');
var env = JSON.parse(envStr);

var server;
var Service = require('webos-service');
var service = new Service("com.webos.service.outdoorwebcontrol");

var forceStartStr = '--force-start';
var supportLedSignageStr = '--support-led-signage';
var support365CareStr = '--support-365-care';
var supportCinemaStr = '--support-cinema-model';
var passwdPostfix = 'LGe12#';

var fixedIP = {
    dhcp: 'manual',
    ip: '192.168.0.101',
    netmask: '255.255.255.0',
    gateway: '192.168.0.1',
}

lunaAPI.init(service);

lunaAPI.getWebOSInfo(function (m) {
    var webOSVer = m.payload.core_os_release;
    var version = (webOSVer && webOSVer.substring(0, webOSVer.indexOf("."))) || 0;

    //Convert string to number
    version = +version;

    if (version >= 3) {
        startServerFor_3_0();
    }
    else if (version === 2 && process.argv.indexOf(forceStartStr) >= 0) {
        initEnv('86BH5C');
        lunaAPI.createRemoCodeMap();

        startServer();
        api.init(service, server, session);
    }
    else {
        console.log("This model does not support outdoorwebcontrol");
        process.exit(0);
    }
});

var configNames = [
    'com.webos.service.commercial.ledcontrol',
    'com.webos.service.outdoorwebcontrol',
    'commercial.hw.sensor.bluMaintain',
    'commercial.hw.sensor.current',
    'commercial.hw.sensor.extraInfo',
    // 'commercial.hw.sensor.miscellaneousList',
    'commercial.hw.sensor.rgbPixel',
    'commercial.hw.sensor.temperature',
    'commercial.hw.speaker',
    'commercial.network.disableWifi',
    'commercial.sensor.list',
    'commercial.video.bluControl',
    'tv.model.*'
];

function startServerFor_3_0() {
    service.call('luna://com.webos.service.config/getConfigs', {
        configNames: configNames
    }, function (msg) {
        if (!msg) {
            return;
        }

        env.fixedModel = false;
        var outdoorwebcontrol = msg.payload.configs["com.webos.service.outdoorwebcontrol"];
        if (outdoorwebcontrol && outdoorwebcontrol.support === false) {
            var fixedModel = msg.payload.configs["com.webos.service.commercial.ledcontrol"]["layout"].includes('fixed',0);
            if (fixedModel) {
                env.fixedModel = true;
                console.log('This device is Fixed Model');
            } else {
                console.log('This device is not supported');
                process.exit(0);
            }
        }

        // check whether property exists.
        var modelName = msg.payload.configs["tv.model.commerModelOptType"];
        if (!modelName) {
            console.log("Cannot read Micom model opt type!");
            process.exit(0);
            return;
        }

        var moduleInch = msg.payload.configs['tv.model.moduleInchType'];
        var temperature = msg.payload.configs['commercial.hw.sensor.temperature'];
        var temperatureInfo = getTemperatureInfo(temperature, moduleInch);
        var rgbPixel = msg.payload.configs['commercial.hw.sensor.rgbPixel'];
        var bluMaintain = msg.payload.configs['commercial.hw.sensor.bluMaintain'];
        var current = msg.payload.configs['commercial.hw.sensor.current'];
        // var miscellaneousList = msg.payload.configs['commercial.hw.sensor.miscellaneousList'];

        env.disableWifi = msg.payload.configs["commercial.network.disableWifi"];
        env.isHwSpeakerSupported = msg.payload.configs["commercial.hw.speaker"];
        env.commerAddLanguageType = msg.payload.configs["tv.model.commerAddLanguageType"];
        env.supportLedSignage = msg.payload.configs["com.webos.service.commercial.ledcontrol"]["support"] || process.argv.indexOf(supportLedSignageStr) >= 0;
        env.supportCinemaModel = msg.payload.configs["com.webos.service.commercial.ledcontrol"]["cinema"] || process.argv.indexOf(supportCinemaStr) >= 0;
        env.factorymenuIndex = env.supportCinemaModel ? "factorymenu/remocon" : "factorymenu/index";
        env.supportScreenOnOff = false;
        env.outdoor = outdoorwebcontrol ? outdoorwebcontrol.type === 'outdoor' : false; // Indicating whether outdoor model or not
        env.logList = outdoorwebcontrol ? outdoorwebcontrol.logList : ['time', 'temperature', 'signal'];
        env.sensorChart = outdoorwebcontrol ? outdoorwebcontrol.chartList : ['temperature'];
        env.curTempSensor = temperatureInfo.sensors;
        env.supportHumid = temperatureInfo.humidity;
        env.criticalTemp = temperatureInfo.criticalTemp;
        env.checkscreen = rgbPixel ? rgbPixel : {};
        env.checkscreen.support = env.outdoor && env.checkscreen.support;
        env.supportPanelErrorOut = env.checkscreen.support && msg.payload.configs['tv.model.supportCommerPanelErrorOut'];
        env.supportBrighness = bluMaintain ? bluMaintain.support : false;
        env.supportAcCurrent = current && current.list && current.list.indexOf('power') > -1 ? true : false;
        // env.supportDoor = miscellaneousList && miscellaneousList.indexOf('door') > -1 ? true : false;
        env.supportDoor = false;
        env.supportStalledImage = true;
        env.supportFilterAlarmDeltaTemp = false;
        env.checkscreenPosition = false; // Temporary value. del checkscreen position.

        service.call('luna://com.webos.service.tv.outdoor/getSolar', {}, function(m) {
            env.supportSolarVoltage = env.outdoor && m.payload.support ? m.payload.support : false;
        });

        getFanInfo(function (fanControl) {
            env.fanControl = fanControl;
        });

        service.call('luna://com.webos.service.tv.outdoor/getSDMInfo', {
            subscribe: false
        }, function (m) {
            env.psuNum = m.payload.PSUStatus ? m.payload.PSUStatus.length : 2;
            env.supportPsuStatus = m.payload.PSUStatus && m.payload.PSUStatus.length > 0 ? true : false;
        });

        var warmMode = service.subscribe('luna://com.webos.service.tv.commercial/cwm/getWarmStatus', {
            subscribe: true
        });

        warmMode.on("response", function (m) {
            console.log("[ControlManager] WarmStatus : " + m.payload.operationState);
            if(m.payload.operationState === "powerOff"){
                console.log('WARM MODE, status powerOff');
                process.exit(0);
            }
        });

        lunaAPI.getSystemSettings('commercial', [
            "signage365CareAccountNumber", "signage365CareAccountName", "care365Enable"
        ], function(m) {
            env.support365Care = (env.supportLedSignage && m.payload.returnValue) || process.argv.indexOf(support365CareStr) >=0;
        });

        initEnv(modelName);
        lunaAPI.createRemoCodeMap();

        startServer();
        api.init(service, server, session);
        require("./configD").init(service, env);
        require("./wordsSelector").init();
        // if (!isSupportedBrowserLocale()) {
        //     require("./LanguageManager").init(service);
        // }
        require("./LanguageManager").init(service);
        if (env.supportLedSignage) {
            ledAPI.init();
            if (!passwd.isDefaultPasswdGenerated()) {
                lunaAPI.getSystemProperties(['serialNumber'], function(ret) {
                    passwd.changePasswd(ret.payload.serialNumber + passwdPostfix);
                });
            }
        }
    });
}

function initEnv(modelName) {
    if (!fs.existsSync('/var/outdoorweb')) {
        fs.mkdirSync('/var/outdoorweb');
    }

    env.emergencyTemp = [
        { event: "turnOff", temperature: "80" },
        { event: "turnOn", temperature: "70" },
        { event: "powerOff", temperature: "100" }
    ];

    service.call('luna://com.webos.service.tv.systemproperty/getProperties', {
        keys: ['displayResolution', 'powerOnlyMode', 'commer365CareServiceMode']
    }, function (msg) {
        var resolution = msg.payload.displayResolution;
        if (resolution !== undefined) {
            var resolArray = resolution.split('x');
            env.screen.width = Number(resolArray[0]);
            env.screen.height = Number(resolArray[1]);
        }

        env.powerOnlyMode = msg.payload.powerOnlyMode == 'true';
        env.commer365CareServiceMode = msg.payload.commer365CareServiceMode;
    });

    lunaAPI.getCurrentTime(function(time) {
        env.bootTime = time.current;
    });
}

function startServer() {
    if (env.secured) {
        var ciphers = [
			// refer to : https://www.acunetix.com/blog/articles/tls-ssl-cipher-hardening/
			"ECDHE-ECDSA-AES256-GCM-SHA384",
			"ECDHE-RSA-AES256-GCM-SHA384",
//			"ECDHE-ECDSA-AES128-GCM-SHA256",
//			"ECDHE-RSA-AES128-GCM-SHA256",
			"ECDHE-RSA-AES256-SHA384",
			"ECDHE-RSA-AES256-SHA256",
//			"ECDHE-RSA-AES128-SHA256",
			"!aNULL",
			"!eNULL",
			"!EXPORT",
			"!DES",
			"!RC4",
			"!MD5",
			"!PSK",
			"!SRP",
			"!CAMELLIA"
        ].join(':');

        var options = {
            key: fs.readFileSync('https/key.pem'),
            cert: fs.readFileSync('https/cert.pem'),
            secureOptions: crtpyo.constants.SSL_OP_NO_TLSv1 | crtpyo.constants.SSL_OP_NO_TLSv1_1,
            ciphers: ciphers,
            honorCipherOrder: false
        };
        server = https.createServer(options, app).listen(env.port);
    } else {
        server = app.listen(env.port, function () {
            console.log("server started");
        });
    }

    service.register("serverURL", function (message) {
        service.call("luna://com.palm.connectionmanager/getstatus", {},
            function (m) {
                var url = m.payload.wired.ipAddress;
                url = (env.secured ? "https://" : "http://") + url;
                url += ":" + env.port;

                message.respond({
                    url: url
                });
            });
    });

    if (env.supportCinemaModel) {
    	service.register("marriageButtonStatus", function (message) {
		if (!(message.payload.longPress == undefined || message.payload.longPress == null) && message.payload.longPress) {
            if (ledAPI.isMarriageTimerOn()) {
                message.respond({});
                return;
            }
			console.log("IP RESET");
			var param = {
				method: fixedIP.dhcp,
				address: fixedIP.ip,
				netmask: fixedIP.netmask,
				gateway: fixedIP.gateway
			};

			service.call("luna://com.webos.service.connectionmanager/setipv4", param, function(m) {
			});
	    } else {
			api.io.emit("buttonStatus", true);
	    }
      	message.respond({});
		});
    }
}

function isSupportedBrowserLocale() {
    if (env.supportLedSignage) {
        return false;
    }
    return true;
}

function getLocalIPAddress(req, callback) {
    callback(req.hostname);
}

function getFanInfo(callback) {
    service.call('luna://com.webos.service.tv.outdoor/getFan', {}, function (m) {
        var fanControl = {
            support: false,
            fanMicom: 0,
            numOfFanIndex: 1,
            fans: []
        }

        if (m.payload.returnValue) {
            fanControl.support = m.payload.support && env.outdoor;
            fanControl.fanMicom = m.payload.fanMicom === undefined ? 0 : m.payload.fanMicom;
            fanControl.numOfFanIndex = fanControl.fanMicom > 0 ? env.fanControl.fanMicom : 1;

            if (m.payload.support) {
                var types = m.payload.group;
                types.forEach(function (type) {
                    var fan = {
                        id: type,
                        numberOfFans: m.payload[type].numberOfFans
                    }
                    switch (type) {
                        case 'openLoop':
                            fan.name = 'Open Loop';
                            break;
                        case 'closedLoop':
                            fan.name = 'Closed Loop';
                            break;
                        case 'psu':
                            fan.name = 'PSU';
                            break;
                        case 'normal':
                            fan.name = 'Normal';
                            break;
                        default:
                    }
                    fanControl.fans.push(fan);
                });
            }
        }
        callback(fanControl);
    });
}

function getTemperatureInfo(temperature, moduleInch) {
    var temperatureInfo = {
        criticalTemp: 85,
        humidity: false,
        sensors: []
    }

    if (temperature) {
        var tempType = [];
        if (temperature.inchType) {
            for (var i = 0; i < temperature.inchType.length; i++) {
                if (temperature.inchType[i].inch === parseInt(moduleInch)) {
                    temperatureInfo.criticalTemp = temperature.inchType[i].criticalTemp;
                    tempType = temperature.inchType[i].tempType;
                    break;
                }
            }
        } else {
            temperatureInfo.criticalTemp = temperature.criticalTemp;
            tempType = temperature.tempType;
        }

        for (var i = 0; i < tempType.length; i++) {
            if (tempType[i].humidity) {
                temperatureInfo.humidity = tempType[i].humidity;
                break;
            }
        }

        for (var i = 0; i < tempType.length; i++) {
            var index = tempType[i].priority -1;
            var id = tempType[i].displayName;
            var name = (id === 'psu' || id === 'blu') ? id.toUpperCase() : capitalizeFirstLetterTempSensor(id);
            temperatureInfo.sensors[index] = {
                id: id,
                name: name
            }
        }
    }

    return temperatureInfo;
}

function capitalizeFirstLetterTempSensor(str) {
    var str = str.substring(0, 1).toUpperCase() + str.substring(1, str.length);
    return str;
}

exports.env = env;
// exports.isSupportedBrowserLocale = isSupportedBrowserLocale;
exports.isSupportedBrowserLocale = false;
exports.getLocalIPAddress = getLocalIPAddress;
