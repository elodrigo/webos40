var express = require('express');
var router = express.Router();
var api = require('../api');
var fs = require('fs');
var passwd = require('../passwd');
var web = require('../web');
var lunaAPI = require('../luna');
var sequence = require('../sequence');
var defaultRouter = require('./index');

/* GET users listing. */
router.all('*', function(req, res, next) {
	if (!req.session || ! req.session.login) {
        if (web.env.supportLedSignage) {
		    res.redirect('/loginLed');
        } else {
		    res.redirect('/login.html');
        }
	} else {
		req.parameters = {
			env: web.env,
            languageTable: require("../LanguageManager").languageTable,
			layout: 'factorymenu/factorymenu_layout'
		};
		next();
	}
});

router.get('/', function(req, res, next) {
    res.render(web.env.factorymenuIndex, req.parameters);
    return;
});

router.get('/remocon', function(req, res, next) {
    if (web.env.supportLedSignage) {
	    res.render('factorymenu/remocon', req.parameters);
    } else {
	    res.redirect(web.env.factorymenuIndex);
    }
    return;
});

router.get('/testLedAPI', function(req, res, next) {
    if (web.env.supportLedSignage) {
	    res.render('factorymenu/testLedAPI', req.parameters);
    } else {
        res.redirect(web.env.factorymenuIndex);
    }
    return;
});

router.get('/testPsuAPI', function(req, res, next) {
    if (web.env.supportLedSignage) {
	    res.render('factorymenu/testPsuAPI', req.parameters);
    } else {
        res.redirect(web.env.factorymenuIndex);
    }
    return;
});

router.get('/testCalAPI', function(req, res, next) {
    if (web.env.supportLedSignage) {
	    res.render('factorymenu/testCalAPI', req.parameters);
    } else {
        res.redirect(web.env.factorymenuIndex);
    }
    return;
});

router.get('/testMbiAPI', function(req, res, next) {
    if (web.env.supportLedSignage) {
	    res.render('factorymenu/testMbiAPI', req.parameters);
    } else {
	    res.redirect(web.env.factorymenuIndex);
    }
    return;
});

router.get('/testSumAPI', function(req, res, next) {
    if (web.env.supportLedSignage) {
	    res.render('factorymenu/testSumAPI', req.parameters);
    } else {
	    res.redirect(web.env.factorymenuIndex);
    }
    return;
});


router.get('/cinemaStatus', function(req, res, next) {
    if (web.env.supportLedSignage) {
	    res.render('factorymenu/cinemaStatus', req.parameters);
    } else {
	    res.redirect(web.env.factorymenuIndex);
    }
    return;
});

router.get('/testLayoutAPI', function(req, res, next) {
    if (web.env.supportLedSignage) {
	    res.render('factorymenu/testLayoutAPI', req.parameters);
    } else {
        res.redirect(web.env.factorymenuIndex);
    }
    return;
});

router.get('/testDbcAPI', function(req, res, next) {
    if (web.env.supportLedSignage) {
	    res.render('factorymenu/testDbcAPI', req.parameters);
    } else {
	    res.redirect(web.env.factorymenuIndex);
    }
    return;
});

router.get('/debugInfo', function(req, res, next) {
    if (web.env.supportLedSignage) {
        res.render('factorymenu/debugInfo', req.parameters);
    } else {
        res.redirect('factorymenu/index');
    }
    return;
});

router.get('/fan', function(req, res, next) {
	res.render('factorymenu/fan', req.parameters);
	return;
});

router.get('/checkScreen', function(req, res, next) {
	res.render('factorymenu/checkScreen', req.parameters);
	return;
});

router.get('/humid', function (req, res, next) {
	var page = req.query.page;
	if (page == undefined || defaultRouter.osCommandFilter.test(page) || /\D/.test(page)) {
		page = 1;
	}
	req.parameters.page = page;
	res.render('factorymenu/humid', req.parameters);
	return;
});

router.get('/emergency', function(req, res, next) {
	res.render('factorymenu/emergency', req.parameters);
	return;
});


router.get('/testsuite', function(req, res, next) {
	res.render('factorymenu/testsuite', req.parameters);
	return;
});

if (process.env['GROUP']) {
	router.get('/group', function(req, res, next) {
		res.render('factorymenu/group', req.parameters);
		return;
	});
}

router.get('/chartRaw', function(req, res) {
	var finished = 0;

	res.set({
		"Content-Disposition": 'attachment; filename="chartRaw.csv"'
	});

	var allHistory = [];

	sequence.sequence(function(next) {
		web.env.curTempSensor.forEach(function(sensor) {
			var param = {
				id: sensor.id
			};

			lunaAPI.getTempHistory(param, function(m) {
				var history = m.payload.history;
				history.push(sensor.name);
				history = history.reverse();

				allHistory.push(history);

				++finished;
				if (finished == web.env.curTempSensor.length) {
					next();
				}
			});
		});
	}, function(next) {
		if (!web.env.fanControl.support) {
			next();
			return;
		}

		var fans = web.env.fanControl.fans
		var fanNums = 0;
		fans.forEach(function (fan) {
			fanNums = fanNums + fan.numberOfFans;
		});

		fans.forEach(function (fan) {
			for (var i = 0; i < fan.numberOfFans; i++) {
				(function (index) {
					lunaAPI.getFanRpmHistory({
						id: fan.id,
						index: index
					}, function(msg) {
						var history = msg.payload.history;
						history.push(fan.name + (index + 1));
						history = history.reverse();

						allHistory.push(history);
						fanNums = fanNums - 1;

						if (fanNums === 0) {
							next();
						}
					});
				})(i);
			}
		});
	}, function(next) {
		lunaAPI.getBacklightHistory({}, function(msg) {
            if (msg.payload.returnValue) {
			    var history = msg.payload.history;
			    history.push('Backlight');
			    allHistory.push(history.reverse());
            }
			next();
		});
	}, function(next) {
		lunaAPI.getEyeQSensorHistory({}, function(msg) {
            if (msg.payload.returnValue) {
			    var history = msg.payload.history;
			    history.push('EyeQ');
			    allHistory.push(history.reverse());
            }
			next();
		});
	}, function(next) {
		if (!web.env.supportHumid) {
			next();
			return;
		}

		lunaAPI.getHumidityHistory({}, function(msg) {
            if (msg.payload.returnValue) {
			    var history = msg.payload.history;
			    history.push('Humidity');
			    allHistory.push(history.reverse());
            }
			next();
		});
	}, function(next) {
		lunaAPI.getSolarHistory({}, function(msg) {
			if (msg.payload.returnValue) {
			    var history = msg.payload.history;
			    history.push('Solar');
			    allHistory.push(history.reverse());
            }
			next();
		});
	}, function() {
		var kinds = allHistory.length;
		var size = allHistory[0].length;

		for (var i = 0; i != size; ++i) {
			for (var j = 0; j != kinds; ++j) {
				res.write(allHistory[j][i] + ',');
			}
			res.write('\n');
		}
		res.end();
	});
});

router.get('/deviceRawData', function(req, res) {
	res.set({
		"Content-Disposition": 'attachment; filename="deviceRawData.json"'
	});

	var json = {};
	sequence.join(function(next) {
		lunaAPI.getCurrentTime(function(ret) {
			json.time = ret;
			next();
		});
	}, function(next) {
		lunaAPI.getAllTemperature(function(m) {
			json.temperature = m;
			next();
		});
	}, function(next) {
		lunaAPI.getNetworkStatus(function(m) {
			m.payload.wired.returnValue = m.payload.returnValue;
			json.wired = m.payload.wired;
			next();
		});
	}, function(next) {
		lunaAPI.getDoorStatus(function(m) {
			json.door = m.payload;
			next();
		});
	}, function(next) {
		lunaAPI.getFanStatus(function(m) {
			json.fan = {};
			if (m.payload.returnValue && m.payload.support) {
				var types = m.payload.group;
				types.forEach(function (type) {
					json.fan[type] = m.payload[type].status;
				});
			}
			next();
		});
	}, function(next) {
		lunaAPI.getCheckScreenOn(function(m) {
			var ret = {
				checkScreen: m.payload.settings.checkScreen
			};

			if (ret.checkScreen == 'off') {
				json.checkScreen = ret;
				next();
				return;
			}

			lunaAPI.getCheckScreenInfo(function(info) {
				ret.info = info.payload;
				ret.returnValue = info.payload.returnValue;
				delete ret.info.returnValue;
				json.checkScreen = ret;
				next();
			});
		});
	}, function(next) {
		lunaAPI.isNoSignal(function(signal) {
			var ret = {};

			ret.signal = !signal.payload.noSignal;
			ret.returnValue = signal.payload.returnValue;

			lunaAPI.getVideoSize(function(size) {
				ret.videoSize = size.payload;
				ret.returnValue = ret.returnValue && size.payload.returnValue;

				lunaAPI.getVideoStillStatus(function(stall) {
					ret.stillStatus = stall.payload;
					ret.returnValue = ret.returnValue && stall.payload.returnValue;
					delete ret.stillStatus.returnValue;
					
					json.signal = ret;
					next();
				});
			});
		});
	}, function(next) {
		lunaAPI.getBasicInfo(function(info) {
			json.info = info.payload;
			next();
		});
	}, function() {
		json.returnValue = true;
		res.json(json);
	});
});

module.exports = router;
