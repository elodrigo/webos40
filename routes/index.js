var fs = require('fs');
var express = require('express');
var multer = require('multer');
var path = require('path');
var captchapng = require('captchapng');
var child_process = require('child_process');
var password = require('../passwd');
var web = require('../web');
var api = require('../api');
var lunaAPI = require('../luna');
var languageManager = require('../LanguageManager');
var loginChecker = require('../loginChecker');
var router = express.Router();
var failoverMediaDir = "/mnt/lg/appstore/signage/.failover/";
var WEBGATEWAY_PORT = 3736;
var storage = multer.diskStorage(getDiskStorageInfo());
var upload = multer({
    storage: storage,
    fileFilter : function(req, file, cb) {
        if (web.env.supportLedSignage) {
            var ret = true;
            if (file.fieldname == 'update') {
                ret = /\.epk$/.test(file.originalname);
            } else if (file.fieldname == 'upload_mask') {
                ret = /\.txt$/.test(file.originalname);
            } else {
                ret = false;
            }
            cb(null, ret);
        }
        cb(null, true);
    }
});
var osCommandFilter = /`|\$|\'|"|;|\||&|\\|<|>|\n|%27|%22|%3B|%7C|%26|%5c|%60|%24|%0a|%0d/;




function redirectToLoginPage(req, res) {
    if (web.env.supportLedSignage) {
        res.redirect('/loginLed');
    } else {
        web.getLocalIPAddress(req, function (ipAddress) {
            console.log("Go to main page: " + 'https://' + ipAddress + ":" + WEBGATEWAY_PORT);
            res.redirect('https://' + ipAddress + ":" + WEBGATEWAY_PORT);
        });
    }
}

function processLogin(passwd, req, res) {
    if (web.env.supportLedSignage) {
        redirectToLoginPage(req, res);
        return;
    }
    if (password.checkPasswd(passwd)) {
        // process session
        req.session.login = true;

        if (web.env.supportLedSignage) {
            res.redirect('/ledSignage/language');
            return;
        }

        if (req.body.device == "pc")
            res.redirect('/dashBoard');
        else
            res.redirect('/dashBoard');
            // res.redirect('/mobile');
    } else {
        redirectToLoginPage(req, res);
    }
}

router.get('/login', function (req, res, next) {
    var passwd = req.query.password;
    processLogin(passwd, req, res);
});

router.post('/login', function (req, res, next) {
    var passwd = req.body.password;
    processLogin(passwd, req, res);
});

router.get('/logout', function (req, res, next) {
    req.session.destroy();
    redirectToLoginPage(req, res);
});

router.get('/loginLed', function (req, res, next) {
    if (web.env.fixedModel  && !web.env.powerOnlyMode) {
        res.json({err: 'not supported'});
        res.end();
        return;
    }
    req.parameters = {
        languageTable: require("../LanguageManager").languageTable,
        localeInfo: api.getLocaleInfo(),
        
    };
   
    if (req.session && req.session.login) {
        res.redirect("ledSignage/language");
    } else {
        req.parameters.layout = false;
        req.parameters.env = web.env;
        res.render("ledSignage/signage_login", req.parameters);
    }
});

router.get('/checkLoginLed', function(req, res, next) {
    if (!web.env.supportLedSignage) {
        redirectToLoginPage(req, res);
        return;
    }

    var logined = req.session && req.session.login;
    res.json({
        login: logined
    });
    res.end();
});

router.post('/loginLed', function (req, res, next) {
    loginChecker.checkPassword(req, res);
});

router.get('/ledSignage/signage_change_pwd', function(req, res, next) {
    if (web.env.fixedModel  && !web.env.powerOnlyMode) {
        res.json({err: 'not supported'});
        res.end();
        return;
    }
    req.parameters = {
        languageTable: require("../LanguageManager").languageTable,
        localeInfo: api.getLocaleInfo(),
        layout: '../ledSignage/layout2.hbs'
    };
    // req.parameters.layout = '../ledSignage/layout.hbs';
    res.render('ledSignage/signage_change_pwd', req.parameters);
});

router.get('/getLoginStatus', function(req, res) {
    res.end(loginChecker.getCurrentStatus());
});

router.post("/getServerTime", function (req, res) {
    var serverTime = new Date().getTime();
    res.send(String(serverTime));
});

router.get("/request/captchapng", function (req, res) {
    captchaStr = parseInt(Math.random() * 9000 + 1000, 10);
    loginChecker.setCaptchaStr(captchaStr);
    
    if (captchaStr === "") {
        res.end();
        return;
    }
    
    var p = new captchapng(80, 30, captchaStr);
    p.color(0, 0, 0, 0);
    p.color(255, 255, 255, 255);

    var img = p.getBase64();
    var imgbase64 = new Buffer(img, 'base64');

    res.writeHead(200, {
        'Content-Type': 'image/png'
    });
    res.end(imgbase64);
});

router.get("/getCaptchaString", function (req, res) {
         res.status(200).send((loginChecker.getCaptchaStr()).toString());
})

router.post('/setLanguage', function (req, res, next) {
    // if (!web.isSupportedBrowserLocale()) {
    //     res.send('');
    //     return;
    // }

    var locale = req.body.locale
    if (!locale || locale.indexOf('..') >= 0) {
        locale = 'en-US';
    }

    if (languageManager.clientLocale !== locale) {
        languageManager.setLanguageTableClient(locale);
        res.send('changed');
    } else {
        res.send('');
    }
});

router.get("/getLanguageTable", function (req, res, next) {
    // if (web.isSupportedBrowserLocale()) {
    if (false == true) {
        res.json(require("../LanguageManager").languageTableClient);
    } else {
        res.json(require("../LanguageManager").languageTable);
    }
});

router.all('*', function (req, res, next) {
    checkLogin(req, res, next, false);
});

/* GET home page. */
router.get('/', function (req, res, next) {
    if (web.env.supportLedSignage) {
        res.redirect('/ledSignage/language');
        return;
    }
    res.render('dashBoard', req.parameters);
});

router.get('/dashBoard', function (req, res, next) {
    res.render('dashBoard', req.parameters);
});

router.get('/device', function (req, res, next) {
    res.render('device', req.parameters);
});

router.get('/system', function (req, res, next) {
    res.render('system', req.parameters);
});

router.get('/sound', function (req, res, next) {
    res.render('sound', req.parameters);
});

router.get('/input', function (req, res, next) {
    res.render('input', req.parameters);
});

router.get('/network', function (req, res, next) {
    res.render('network', req.parameters);
});

router.get('/time', function (req, res, next) {
    res.render('time', req.parameters);
});

router.get('/door', function (req, res, next) {
    if (web.env.outdoor) {
        res.render('door', req.parameters);
    } else {
        res.sendStatus(404);
    }
});

router.get('/power', function (req, res, next) {
    res.render('power', req.parameters);
});

router.get('/bright', function (req, res, next) {
    res.render('bright', req.parameters);
});

router.get('/checkScreen', function (req, res, next) {
    if (web.env.outdoor) {
        res.render('checkScreen', req.parameters);
    } else {
        res.sendStatus(404);
    }
});

router.get('/fan', function (req, res, next) {
    if (web.env.outdoor) {
        res.render('fan', req.parameters);
    } else {
        res.sendStatus(404);
    }
});

router.get('/emergency', function (req, res, next) {
    res.render('emergency', req.parameters);
});

router.get('/remocon', function (req, res, next) {
    if (web.env.outdoor) {
        res.render('remocon', req.parameters);
    } else {
        res.sendStatus(404);
    }
});

router.get('/charts', function (req, res, next) {
    var page = req.query.page;
    if (page == undefined || osCommandFilter.test(page) || /\D/.test(page)) {
        page = 1;
    }
    req.parameters.page = page;
    res.render('charts', req.parameters);
});

router.get('/logFile', function (req, res, next) {
    getLog(req, res, false);
    res.render('log', req.parameters);
});

router.get('/media', function (req, res, next) {
    if (web.env.outdoor) {
        getMedia(req, res);
        res.render('media', req.parameters);
    } else {
        res.sendStatus(404);
    }
});

router.get('/playViaUrl', function (req, res, next) {
    res.render('playViaUrl', req.parameters);
});

router.get('/tileMode', function (req, res, next) {
    res.render('tileMode', req.parameters);
});

router.get('/failOver', function (req, res, next) {
    if (!fs.existsSync(failoverMediaDir)) {
        fs.mkdirSync(failoverMediaDir);
    }

    res.render('failOver', req.parameters);
});

router.get('/failoverimage', function (req, res, next) {
    var interval = setInterval(function () {
        fs.readFile('/tmp/outdoorweb/tnm/failoverTmn.jpg', function (error, data) {
            if (!error) {
                res.writeHead(200, {'content-Type': 'text/html'});
                res.end(data);
                clearInterval(interval);
            }
        });
    }, 1000);
});

router.get('/update', function (req, res, next) {
    getUpdate(req, res, ['epk', 'txt']);
    res.render('update', req.parameters);
});

router.get('/changePasswd', function (req, res, next) {
    web.getLocalIPAddress(req, function (ipAddress) {
        console.log("Go to main page: " + 'https://' + ipAddress + ":" + WEBGATEWAY_PORT + '/changePasswd');
        res.redirect('https://' + ipAddress + ":" + WEBGATEWAY_PORT + '/changePasswd');
    });
});

router.get('/changePassword', function (req, res, next) {
    web.getLocalIPAddress(req, function (ipAddress) {
        console.log("Go to password page: " + 'https://' + ipAddress + ":" + WEBGATEWAY_PORT + '/changePassword');
        res.redirect('https://' + ipAddress + ":" + WEBGATEWAY_PORT + '/changePassword');
    });
});

router.get('/contentManager', function (req, res, next) {
    web.getLocalIPAddress(req, function (ipAddress) {
        console.log("Go to Content Manager: " + 'https://' + ipAddress + ":" + WEBGATEWAY_PORT + '/contentManager');
        res.redirect('https://' + ipAddress + ":" + WEBGATEWAY_PORT + '/contentManager');
    });
});

router.get('/groupManager', function (req, res, next) {
    web.getLocalIPAddress(req, function (ipAddress) {
        console.log("Go to Group Manager: " + 'https://' + ipAddress + ":" + WEBGATEWAY_PORT + '/groupManager');
        res.redirect('https://' + ipAddress + ":" + WEBGATEWAY_PORT + '/groupManager');
    });
});

router.get('/download', function (req, res, next) {
    var root = '/media/signage/';
    downloadFile(root, req, res, next);
});

router.get('/logDown', function (req, res, next) {
    var root = '/var/controlmanager/download/';
    downloadLogFile(root, req, res, next);
});

router.get('/downloadIncidents', function (req, res, next) {
    downloadFile('/tmp/outdoorweb/', req, res, next);
});

router.get('/ledSignage/language', function(req, res, next) {
    req.parameters.layout = '../ledSignage/layout.hbs';
    req.parameters.localeInfo = api.getLocaleInfo();
    res.render('ledSignage/language', req.parameters);
});

router.get('/ledSignage/network', function(req, res, next) {
    req.parameters.layout = '../ledSignage/layout.hbs';
    req.parameters.localeInfo = api.getLocaleInfo();
    res.render('ledSignage/network', req.parameters);
});

router.get('/ledSignage/ledAssistant', function(req, res, next) {
    req.parameters.layout = '../ledSignage/layout.hbs';
    req.parameters.localeInfo = api.getLocaleInfo();
    res.render('ledSignage/ledAssistant', req.parameters);
});

router.get('/ledSignage/system', function(req, res, next) {
    getUpdate(req, res, ['epk']);
    req.parameters.layout = '../ledSignage/layout.hbs';
    req.parameters.localeInfo = api.getLocaleInfo();
    res.render('ledSignage/system', req.parameters);
});

router.get('/ledSignage/signage365Care', function(req, res, next) {
    req.parameters.layout = '../ledSignage/layout.hbs';
    req.parameters.localeInfo = api.getLocaleInfo();
    res.render('ledSignage/signage365Care', req.parameters);
});

router.get('/ledSignage/sitemap', function(req, res, next) {
    req.parameters.layout = '../ledSignage/layout.hbs';
    req.parameters.localeInfo = api.getLocaleInfo();
    res.render('ledSignage/sitemap', req.parameters);
});

router.get('/ledSignage/imb', function(req, res, next) {
    req.parameters.layout = '../ledSignage/layout.hbs';
    req.parameters.localeInfo = api.getLocaleInfo();
    res.render('ledSignage/imb', req.parameters);
});

router.get('/ledSignage/time', function(req, res, next) {
    req.parameters.layout = '../ledSignage/layout.hbs';
    req.parameters.localeInfo = api.getLocaleInfo();
    res.render('ledSignage/time', req.parameters);
});

router.post('/upload', upload.any(), function (req, res, next) {
    if (web.env.supportLedSignage) {
        var dir = '/media/update/';
        fs.readdir(dir, function(err, files) {
            lunaAPI.listUpdates(function(ret) {
                ret.payload.fileinfo.forEach(function(file) {
                    var index = files.indexOf(file.name);
                    if (index !== -1) {
                        files.splice(index, 1);
                    }
                });

                if (files.length == 0) {
                    res.end();
                    return;
                }

                files.forEach(function(file) {
                    fs.unlinkSync(dir + file);
                });
                res.status(406).send({ error: "unsupported file"});
            });
        });
        return;
    }

    res.end();
});

router.post('/uploadGain', upload.any(), function (req, res, next) {
    console.log("Gain upload");
    res.end();
});

router.get('/language', function (req, res, next) {
    if (web.env.outdoor) {
        res.render('language', req.parameters);
    } else {
        res.sendStatus(404);
    }
});

function downloadFile(root, req, res, next) {
    var fileName = req.query.name;
    var fullPath = path.normalize(root + fileName);
    if (fullPath.indexOf(root) < 0) {
        res.status(404);
        res.send('Unauthorized access');
        res.end();
        return;
    }
    res.download(fullPath);
    return;
}

function sendError(res, err) {
    console.log(err);
    res.send(err);
    res.end();
}

function downloadLogFile(root, req, res, next) {
    var fileName = req.query.name;
    var filePath = path.normalize(root + fileName);
    var outputPath = filePath.substring(0, filePath.lastIndexOf(".")) + ".csv";
    fs.readFile(filePath, "utf8", function (err, data) {
        if (err) {
            sendError(res, err);
            return;
        } else {
            data = "Category, Sensor, Event, State, Occurrence Time\n" + data;
            data = data.replace(/^ */g, "").replace(/\n[ ]+/g, "\n").replace(/\t/g, " ").replace(/ {2,}/g, " ");
            data = data.replace(/Please, Set a time/g, "Please Set a time");
            fs.writeFile(outputPath, data, function (err) {
                if (err) {
                    sendError(res, err);
                    return;
                } else {
                    res.download(outputPath, function (err) {
                        fs.unlinkSync(outputPath);
                    });
                    return;
                }
            });
        }
    });
}

function getLog(req, res, isMobile) {
    var dir = "/var/controlmanager/";
    var downDir = "/var/controlmanager/download/";
    var availableFilters = [' ' /* All*/];
    var baseFileName = path.basename('' + req.query.file);

    web.env.logList.forEach(function (log) {
        availableFilters.push(log.toUpperCase());
    });

    if (req.query.delete) {
        var delPath = path.normalize(dir + baseFileName);
        if(fs.existsSync(delPath)){
            fs.unlinkSync(delPath);
        }
        req.query.file = undefined;
        baseFileName = undefined;
    }

    var filter = req.query.filter;
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }

    if (!fs.existsSync(downDir)) {
        fs.mkdirSync(downDir);
    }

    var files = fs.readdirSync(dir);
    var re = /^log\d{5}\.txt$/;
    files = files.filter(function(name) {
        return re.test(name);
    });
    files.sort();

    if (osCommandFilter.test(req.query.file) || !re.test(req.query.file)) {
        req.query.file = undefined;
    }

    var fileName = req.query.file ? req.query.file : files[files.length - 1];

    var cmd = 'cat ' + dir + fileName;
    if (availableFilters.indexOf(filter) > -1) {
        cmd = 'grep "' + filter + '" ' + dir + fileName;
    } else {
        filter = ' '; // ALL
    }

    var log = "";

    var getLanguageText = require("../LanguageManager").getLanguageText;
    var columnTitle =
            getLanguageText("Category") + ", " + getLanguageText("Sensor") + ", " +
            getLanguageText("Event") + ", " + getLanguageText("State") + ", " +
            getLanguageText("Occurrence Time") + "\n";
    try {
        log = child_process.execSync(cmd, {
            timeout: 2000
        });
        log = String(log);
        log = log.replace(/Please, Set a time/g, "Please Set a time");

        fs.writeFileSync(downDir+fileName, log);

        log = columnTitle + log;
    } catch (e) {
        log = columnTitle;
    }

    req.parameters.log = {log: log};
    req.parameters.filter = filter;
    req.parameters.files = files;
    req.parameters.curFile = fileName;
}

function getFreeSpace() {
    var ret = 0;
    try {
        ret = Number(child_process.execSync("df -B 1 | grep media$ | awk '{print $4}'"));
    } catch (e) {
        console.log(e);
        ret = 0;
    }
    return ret;
}

function getFileSize(dir, files) {
    var size = [];
    files.forEach(function (file) {
        var stats = fs.statSync(dir + file);
        size.push(stats.size);
    });

    return size;
}

function getMedia(req, res) {
    var root = "/media/signage/";
    var dir = root;
    var curDir = '/';
    var filter = '';
    var dirs = [];
    var files = [];
    var newFiles = [];

    var filter = {
        video: ["avi", "mp4", "wmv", 'divx', 'asf', 'm4v', 'mov',
            '3gp', '3g2', 'mkv', 'ts', 'trp', 'tp', 'mts',
            'mpg', 'mpeg', 'dat', 'vob'],
        image: ["png", "bmp", "jpg", 'jpeg', 'jpe']
        //	music: ["mp3", "wav"]
    };

    if (req.query.dir) {
        dir += req.query.dir + '/';
        dir = path.normalize(dir);
        curDir = dir.substring(root.length - 1);
        if (curDir.length > 1) {
            dirs.push('..');
        }

        if (dir.search(new RegExp('^' + root)) < 0) {
            dir = root;
            curDir = '/';
        }
    }

    if (req.query.filter && filter[req.query.filter]) {
        req.parameters.filter = req.query.filter;
    }

    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
    var fileNames = fs.readdirSync(dir);

    var cur = new Date();

    fileNames.forEach(function (fileName) {
        var stat = fs.statSync(dir + fileName);
        if (stat.isDirectory()) {
            if (fileName !== '.failover') {
                dirs.push(fileName);
            }
            return;
        }

        if (req.query.filter && filter[req.query.filter]) {
            var ext = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();
            var index = filter[req.query.filter].indexOf(ext)
            if (index < 0) {
                return;
            }
        } else {
            var ext = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();
            var indexVideo = filter["video"].indexOf(ext);
            var indexImage = filter["image"].indexOf(ext);
            if ((indexVideo < 0) && (indexImage < 0)) {
                return;
            }
        }

        var diffTime = cur.getTime() - stat.mtime.getTime();
        if (diffTime >= 0 && diffTime <= 60 * 1000) { // one min.
            newFiles.push(fileName);
        }

        files.push(fileName);
    });

    function compare(s1, s2) {
        function splitNumber(str) {
            return str.toLowerCase().split(/(\d+)|([a-z]+)/).filter(function (val) {
                return val !== '' && val !== undefined;
            });
        }

        function compareStr(s1, s2) {
            function getClass(str) {
                if (str.search(/[0-9]+/) >= 0) {
                    return 1;
                }
                if (str.search(/[a-z]+/) >= 0) {
                    return 2;
                }
                return 0;
            }

            var diff = getClass(s1) - getClass(s2);
            if (diff != 0) {
                return diff;
            }

            return s1.localeCompare(s2);
        }

        var token1 = splitNumber(s1);
        var token2 = splitNumber(s2);

        while (token1.length > 0 && token2.length > 0) {
            var val1 = token1.shift();
            var val2 = token2.shift();

            if (Number.isNaN(Number(val1)) || Number.isNaN(Number(val2))) {
                if (val1 === val2) {
                    continue;
                }
                return compareStr(val1, val2);
            }

            var num1 = Number(val1);
            var num2 = Number(val2);

            var diff = num1 - num2;
            if (diff != 0) {
                return diff;
            }
        }

        return token1.length - token2.length;
    }

    files.sort(compare);
    dirs.sort(compare);

    req.parameters.curDir = curDir;
    req.parameters.files = files;
    req.parameters.dirs = dirs;
    req.parameters.newFiles = newFiles;
    req.parameters.maxSize = getFreeSpace();
}

function getUpdate(req, res, filter) {
    var dir = "/media/update/";
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }

    var files = fs.readdirSync(dir).filter(function (name) {
        var ext = name.substring(name.lastIndexOf('.') + 1).toLowerCase();
        return filter.indexOf(ext) >= 0;
    });

    req.parameters.files = files;
    req.parameters.size = getFileSize(dir, files);
    req.parameters.maxSize = getFreeSpace();
}

function checkLogin(req, res, next, isMobile) {
    if (!req.session || !req.session.login) {
        redirectToLoginPage(req, res);
    } else {
        if (loginChecker.isFirstLogin()) {
            res.redirect("/logout");
        }
        // var supportBrowserLocale = web.isSupportedBrowserLocale();
        var supportBrowserLocale = false;
        var languageManager = require("../LanguageManager");
        var languageTable = supportBrowserLocale ? languageManager.languageTableClient : languageManager.languageTable;
        req.parameters = {
            env: web.env,
            languageTable: languageTable,
            currentTime: (new Date()).toString(),
            wordsList: require("../wordsSelector").wordsList
        };
        // if (isMobile) {
        //     req.parameters.layout = 'mobile/mobile_layout';
        // }
        next();
    }
}

function getDiskStorageInfo() {
    return {
        destination: function (req, file, cb) {
            var to = '/media/update/';

            if (file.fieldname == 'media') {
                var root = '/media/signage/';
                to = root;
                if (req.body.dir) {
                    to += req.body.dir;
                    to = path.normalize(to);
                    if (to.search(new RegExp('^' + root)) < 0) {
                        to = root;
                    }
                }
            } else if (file.fieldname == 'failOver') {
                to = '';
                if (req.body.dir) {
                    to += req.body.dir;
                    to = path.normalize(to);
                }
            } else if (file.fieldname == "upload_mask") {
                to = '/var/';
            }
            cb(null, to);
        },
        filename: function (req, file, cb) {
            var name = file.originalname;

            if (req.body.newname) {
                name = req.body.newname;
            }

            cb(null, name);
        }
    }
}

module.exports = router;

module.exports.WEBGATEWAY_PORT = WEBGATEWAY_PORT;
module.exports.getMedia = getMedia;
module.exports.getLog = getLog;
module.exports.getUpdate = getUpdate;
module.exports.checkLogin = checkLogin;
module.exports.osCommandFilter = osCommandFilter;
