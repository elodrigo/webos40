var express = require('express');
var router = express.Router();
var lunaAPI = require('../luna');
var fs = require('fs');
var passwd = require('../passwd');
var web = require('../web');
var group = require('../group');
var crypto = require('crypto');
var sequence = require('../sequence');
var ledAPI = require('../ledAPI');
var loginChecker = require('../loginChecker');

var salt = 'restfulOutdoorwebcontrol';

var tokenList = [];
var tokenChecker = setInterval(function() {
	var cur = new Date().valueOf();
	var delEnd = -1;

	for (var i = 0; i != tokenList.length; ++i) {
		if (cur <= tokenList[i].expire) {
			break;
		}
		delEnd = i;
	}

	if (delEnd >= 0) {
		tokenList.splice(0, delEnd + 1);
	}
}, 60 * 1000);

function createToken(req) {
	var date = new Date();
	var token = salt + date.valueOf() + req.connection.remoteAddress;
	var genHash = crypto.createHash('sha256').update(token).digest('hex');
	return genHash;
}

function isValidToken(token) {
	if (token === group.secureKey()) {
		return true;
	}

	for (var i = 0; i != tokenList.length; ++i) {
		if (tokenList[i].token === token) {
			return true;
		}
	}
	return false;
}

function errorReturn(message, res) {
	res.json({
		returnValue: false,
		message: message
	});
}

function isDbcFunction(url) {
	return url.indexOf('getPmVal') > 0 || url.indexOf('setSharedPmVal') > 0;
}

function checkToken(req, res, next) {
	if (web.env.supportLedSignage) {
		var ip = req.headers['x-forwarded-for'] ||
			req.connection.remoteAddress ||
			req.socket.remoteAddress ||
			req.connection.socket.remoteAddress;
			
		ip = ip.substring(ip.lastIndexOf(':') + 1);
		ledAPI.readGroupFile(function(ret) {
			if (isDbcFunction(req.originalUrl) && ret.group && ret.group.length > 0 && ret.group.indexOf(ip) >= 0) {
				next();
				return;
			}
			
			if (!req.query.token || !isValidToken(req.query.token)) {
				errorReturn("invalid or timed out token.", res);
				return;
			}			
			next();
		});
    } else {
	    if (!req.query.token || !isValidToken(req.query.token)) {
		    errorReturn("invalid or timed out token.", res);
		    return;
    	}
  		next();
    }
}

var loginFailCounter = 0;

router.get('/login', function(req, res) {
	var password = '' + req.query.passwd;

    if (loginFailCounter >= 5) {
		errorReturn('Incorrect password for 5 times. Wait for 5 minutes to try again.', res);
        return;
    }

    if (loginChecker.isFirstLogin()) {
		errorReturn('Change Password before using restful API', res);
        return;
    }

	if (!passwd.checkPasswd(password)) {
        ++loginFailCounter;
		errorReturn('Password is not correct (' + loginFailCounter + '/5)', res);
        if (loginFailCounter >= 5) {
            setTimeout(function() {
                loginFailCounter = 0;
            }, 5 * 60 * 1000);
        }
		return;
	}
    
	var ret = {
		token: createToken(req),
		expire: new Date().valueOf() + (60 * 60 * 1000), // for 1 hour.
		returnValue: true
	};
	tokenList.push(ret);
	res.json(ret);
});

router.all('*', function(req, res, next) {
	checkToken(req, res, next);
});

router.get('/incidents', function(req, res) {
	lunaAPI.getDowntimeIncident(function(m) {
		res.json(m.payload);
	});
});

router.get('/status', function(req, res) {
	var json = {};
	sequence.join(function(next) {
		lunaAPI.getCurrentTime(function(ret) {
			json.time = ret;
			next();
		});
	}, function(next) {
		lunaAPI.getAllTemperature(function(m) {
			json.temperature = m;
			next();
		});
	}, function(next) {
		lunaAPI.getNetworkStatus(function(m) {
			m.payload.wired.returnValue = m.payload.returnValue;
			json.wired = m.payload.wired;
			next();
		});
	}, function(next) {
		lunaAPI.getDoorStatus(function(m) {
			json.door = m.payload;
			next();
		});
	}, function(next) {
		lunaAPI.getFanStatus(function(m) {
			json.fan = {
				returnValue: m.payload.returnValue
			};
			if (m.payload.returnValue && m.payload.support) {
				var types = m.payload.group;
				types.forEach(function (type) {
					json.fan[type] = m.payload[type].status;
				})
			}
			next();
		});
	}, function(next) {
		lunaAPI.getCheckScreenOn(function(m) {
			var ret = {
				checkScreen: m.payload.settings.checkScreen
			};

			if (ret.checkScreen == 'off') {
				json.checkScreen = ret;
				next();
				return;
			}

			lunaAPI.getCheckScreenInfo(function(info) {
				ret.info = info.payload;
				ret.returnValue = info.payload.returnValue;
				delete ret.info.returnValue;
				json.checkScreen = ret;
				next();
			});
		});
	}, function(next) {
		lunaAPI.isNoSignal(function(signal) {
			var ret = {};

			ret.signal = !signal.payload.noSignal;
			ret.returnValue = signal.payload.returnValue;

			lunaAPI.getVideoSize(function(size) {
				ret.videoSize = size.payload.videoSize;
				ret.returnValue = ret.returnValue && size.payload.returnValue;

				lunaAPI.getVideoStillStatus(function(stall) {
					ret.stillStatus = stall.payload;
					ret.returnValue = ret.returnValue && stall.payload.returnValue;
					delete ret.stillStatus.returnValue;
					
					json.signal = ret;
					next();
				});
			});
		});
	}, function(next) {
		lunaAPI.getBasicInfo(function(info) {
			json.info = info.payload;
			next();
		});
	}, function() {
		json.returnValue = true;
		res.json(json);
	});
});

router.get('/currentTime', function(req, res) {
	lunaAPI.getCurrentTime(function(ret) {
		res.json(ret);
	});
});

var lastShotDate = 0;
router.get('/screenShot', function(req, res) {
	var curDate = new Date().valueOf();
	var height = 480;

	if (curDate - lastShotDate < 500) {
		var fileName = '/tmp/outdoorweb/rest' + lastShotDate + '.jpg';
		setTimeout(function() {
			res.sendFile(fileName);
		}, 300);
		return;
	}

	lastShotDate = curDate;

	if (req.query.height) {
		var num = Number(req.query.height);
		if (num) {
			height = num;
		}
	}

	var fileName = '/tmp/outdoorweb/rest' + curDate + '.jpg';
	lunaAPI.screenShot(fileName, height, function(m) {
		res.sendFile(fileName);
	}, 'jpg');

	setTimeout(function() {
		fs.unlink(fileName, function(err) {
			if (err) {
				console.log(err);
			}
		});
	}, 5 * 1000);
});

router.get('/temperature', function(req, res) {
	var id = req.query.id;
	if (id) {
		lunaAPI.getTemperature(function(m) {
			res.json({
				returnValue: m.payload.returnValue,
				temperature: m.payload[id].celsius
			});
		});
	} else {
		lunaAPI.getAllTemperature(function(m) {
			res.json(m);
		});
	}
});

router.get('/network', function(req, res) {
	lunaAPI.getNetworkStatus(function(m) {
		m.payload.wired.returnValue = m.payload.returnValue;
		res.json(m.payload.wired);
	});
});

router.get('/door', function(req, res) {
	lunaAPI.getDoorStatus(function(m) {
		var ret = m.payload;
		ret.opened = ret.status;
		delete ret.status;
		res.json(ret);
	});
});

router.get('/fan', function(req, res) {
	lunaAPI.getFanStatus(function(m) {
		var fan = {
			returnValue: m.payload.returnValue
		};
		if (m.payload.returnValue && m.payload.support) {
			var types = m.payload.group;
			types.forEach(function (type) {
				fan[type] = m.payload[type].status;
			})
		}
		res.json(fan);
	});
});

router.get('/checkScreen', function(req, res) {
	lunaAPI.getCheckScreenOn(function(m) {
		if (!m.payload.returnValue) {
			errorReturn('fail to call luna api', res);
			return;
		}

		var ret = {
			checkScreen: m.payload.settings.checkScreen
		};

		if (ret.checkScreen == 'off') {
			res.json(ret);
			return;
		}

		lunaAPI.getCheckScreenInfo(function(info) {
			ret.info = info.payload;
			ret.returnValue = info.payload.returnValue;
			delete ret.info.returnValue;
			res.json(ret);
		});
	});
});

router.get('/signal', function(req, res) {
	lunaAPI.isNoSignal(function(signal) {
		var ret = {};

		ret.signal = !signal.payload.noSignal;
		ret.returnValue = signal.payload.returnValue;

		lunaAPI.getVideoSize(function(size) {
			ret.videoSize = size.payload.videoSize;
			ret.returnValue = ret.returnValue && size.payload.returnValue;

			lunaAPI.getVideoStillStatus(function(stall) {
				ret.stillStatus = stall.payload;
				ret.returnValue = ret.returnValue && stall.payload.returnValue;
				delete ret.stillStatus.returnValue;

				res.json(ret);
			});
		});
	});
});

router.get('/version', function(req, res) {
	lunaAPI.getBasicInfo(function(info) {
		res.json(info.payload);
	});
});

router.get('/input', function(req, res) {
	lunaAPI.getInputList(function(list) {
		var devices = list.payload.devices;
		var ret = {
			list: [],
			returnValue: list.payload.returnValue
		};
		devices.forEach(function(dev) {
			ret.list.push({
				id: dev.id,
				label: dev.label,
				deviceName: dev.deviceName
			});
		});

		lunaAPI.getCurrentInput(function(cur) {
			ret.currentInputID = cur.payload.deviceID;
			ret.returnValue = ret.returnValue && cur.payload.returnValue;
			res.json(ret);
		});
	});
});

router.put('/input', function(req, res) {
	if (!req.body || !req.body.id) {
		errorReturn('invalid input id', res);
		return;
	}
	lunaAPI.setCurrentInput(req.body.id, function(m) {
		res.json(m.payload);
	});
});

router.get('/volume', function(req, res) {
	lunaAPI.getVolume(function(m) {
		res.json(m.payload);
	});
});

router.put('/volume', function(req, res) {
	if (!req.body) {
		errorReturn('invalid volume value', res);
		return;
	}

	var ret = {
		returnValue: true
	};

	function setVolume() {
		if (!isNaN(req.body.volume)) {
			lunaAPI.setVolume(Number(req.body.volume), function(m) {
				ret.returnValue = ret.returnValue && m.payload.returnValue;
				res.json(ret);
			});
		} else {
			res.json(ret);
		}
	}

	if (typeof(req.body.muted) === 'boolean') {
		lunaAPI.setMuted(req.body.muted, function(mute) {
			ret.returnValue = ret.returnValue && mute.payload.returnValue;
			setVolume();
		});
		return;
	}

	setVolume();
});

router.get('/getPmVal', function(req, res) {
	ledAPI.getPmVal(function(ret) {
		res.json(ret.payload);
	});
});

router.put('/setSharedPmVal', function(req, res) {
	if (!req.body) {
		errorReturn('invalid shared pm value', res);
		return;
	}
	ledAPI.setSharedPmVal(req.body.sharedPmVal, function(ret) {
        ledAPI.setLastDbcTime(Date.now());
		res.json(ret.payload);
	});
});

module.exports = router;
