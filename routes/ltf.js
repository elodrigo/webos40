var express = require('express');
var router = express.Router();
var api = require('../api');
var fs = require('fs');
var passwd = require('../passwd');
var web = require('../web');
var lunaAPI = require('../luna');
var sequence = require('../sequence');
var path = require('path');

/* GET users listing. */
router.all('*', function(req, res, next) {
	if (!req.session || ! req.session.login) {
        if (web.env.supportLedSignage) {
		    res.redirect('/loginLed');
        } else {
		    res.redirect('/login.html');
        }
	} else {
		console.log(req.session.login);
		res.sendFile(path.join(__dirname + '/public/ltf/index.html'));
		next();
	}
});

router.get('/', function(req, res, next) {
	if (!req.session || ! req.session.login) {
        if (web.env.supportLedSignage) {
		    res.redirect('/loginLed');
        } else {
		    res.redirect('/login.html');
        }
	} else {
		res.sendFile(path.join(__dirname + '/public/ltf/index.html'));
	}
	return;
});

module.exports = router;
