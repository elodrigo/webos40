var fs = require('fs');
var express = require('express');
var router = express.Router();
var password = require('../passwd');
var child_process = require('child_process');
var web = require('../web');
var defaultRouter = require('./index');

var WEBGATEWAY_PORT = 443;

router.all('*', function(req, res, next) {
	defaultRouter.checkLogin(req, res, next, true);
});

/* GET home page. */
router.get('/', function(req, res, next) {
	res.render('mobile/dashBoard', req.parameters);
});

router.get('/dashBoard', function(req, res, next) {
	res.render('mobile/dashBoard', req.parameters);
});

router.get('/device', function(req, res, next) {
	res.render('mobile/device', req.parameters);
});

router.get('/system', function(req, res, next) {
	res.render('mobile/system', req.parameters);
});

router.get('/sound', function(req, res, next) {
	res.render('mobile/sound', req.parameters);
});

router.get('/input', function(req, res, next) {
	res.render('mobile/input', req.parameters);
});

router.get('/network', function(req, res, next) {
	res.render('mobile/network', req.parameters);
});

router.get('/time', function(req, res, next) {
	res.render('mobile/time', req.parameters);
});

router.get('/door', function(req, res, next) {
	if (web.env.outdoor) {
		res.render('mobile/door', req.parameters);
	} else {
		res.sendStatus(404);
	}
});

router.get('/power', function(req, res, next) {
	res.render('mobile/power', req.parameters);
});

router.get('/bright', function(req, res, next) {
	res.render('mobile/bright', req.parameters);
});

router.get('/checkScreen', function (req, res, next) {
	if (web.env.outdoor) {
		res.render('mobile/checkScreen', req.parameters);
	} else {
		res.sendStatus(404);
	}
});

router.get('/fan', function(req, res, next) {
	if (web.env.outdoor) {
		res.render('mobile/fan', req.parameters);
	} else {
		res.sendStatus(404);
	}
});

router.get('/emergency', function(req, res, next) {
	res.render('mobile/emergency', req.parameters);
});

router.get('/remocon', function (req, res, next) {
	if (web.env.outdoor) {
		res.render('mobile/remocon', req.parameters);
	} else {
		res.sendStatus(404);
	}
});

router.get('/charts', function(req, res, next) {
	var page = req.query.page;
	if (page == undefined || defaultRouter.osCommandFilter.test(page) || /\D/.test(page)) {
		page = 1;
	}
	req.parameters.page = page;
	res.render('mobile/charts', req.parameters);
});

router.get('/logFile', function(req, res, next) {
	defaultRouter.getLog(req, res, true);
	res.render('mobile/log', req.parameters);
});

router.get('/media', function (req, res, next) {
	if (web.env.outdoor) {
		defaultRouter.getMedia(req, res);
		res.render('mobile/media', req.parameters);
	} else {
		res.sendStatus(404);
	}
});

router.get('/playViaUrl', function(req, res, next) {
	res.render('mobile/playViaUrl', req.parameters);
});

router.get('/tileMode', function(req, res, next) {
	res.render('mobile/tileMode', req.parameters);
});

router.get('/failOver', function(req, res, next) {
	res.render('mobile/failOver', req.parameters);
});

router.get('/update', function(req, res, next) {
	defaultRouter.getUpdate(req, res, ['epk', 'txt']);
	res.render('mobile/update', req.parameters);
});

router.get('/changePasswd', function(req, res, next) {
    web.getLocalIPAddress(req, function (ipAddress) {
        console.log("Go to changePasswd page: " + 'https://' + ipAddress + ":" + WEBGATEWAY_PORT + "/changePasswd");
        res.redirect('https://' + ipAddress + ":" + WEBGATEWAY_PORT + "/changePasswd");
    });
});

router.get('/language', function (req, res, next) {
	if (web.env.outdoor) {
		res.render('mobile/language', req.parameters);
	} else {
		res.sendStatus(404);
	}
});

module.exports = router;

