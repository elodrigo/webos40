set arg1=%1
set var=244
IF "%arg1%"=="243" (
    set var=243
) else (
    set var=244
)

scp irunn.sh root@192.168.1.%var%:~/
scp surekill.sh root@192.168.1.%var%:~/
scp kill.sh root@192.168.1.%var%:~/
scp orirunn.sh root@192.168.1.%var%:~/
scp wcopy.sh root@192.168.1.%var%:~/
scp ycopy.sh root@192.168.1.%var%:~/
scp initiate.sh root@192.168.1.%var%:~/
scp webgateway.json root@192.168.1.%var%:/var/luna/preferences/webgateway.json
