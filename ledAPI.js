var fs = require('fs');
var child_process = require('child_process');
var api = require('./api');
var lunaAPI = require('./luna');
var web = require('./web');
var https = require('https');
var psu = require('./searchPsu');
var signage365Care = require('./365careDownload');

var systemserviceURL = 'palm://com.palm.systemservice/'
var applicationManagerURL = 'luna://com.webos.applicationManager/';
var care365URL = 'luna://com.webos.app.commercial.care365.service/';
var appInstallServiceURL = 'luna://com.webos.appInstallService/';
var ledcontrlURL = 'luna://com.webos.service.commercial.ledcontrol/control';
var storageManagerURL = 'luna://com.webos.service.attachedstoragemanager/listDevices2';
var updateURL = 'luna://com.webos.service.commercial.ledcontrol/updateSW';
var updateProgressURL = 'luna://com.webos.service.commercial.ledcontrol/getUpdateProgress';
var gainProgressURL = 'luna://com.webos.service.commercial.ledcontrol/getGainProgress';
var systemTimeURL = 'luna://com.palm.systemservice/time/getSystemTime';

var fpgaUpdateProgress = 0;
var fpgaFileName = ''; 

var dbcGroupFile = '/var/ledGroupIPList';
var manualTimeSetFile = '/var/manualTimeSet';
var dbcOffFile = '/var/dbcOff';
var dbcAccountNumber = '0413';
var marriageTimer = undefined;

var command = {};

var dbcInterval = 60 * 1000; // 60 seconds
var lastDbcSetTime = 0;
var isDbcMaster = false;
var isFirstBooting = true;
var isMarriageTimerOn = false;

var maskingArray = ['led_15.txt', 'led_15_DSBJ.txt', 'led_15_DSBJ_Cu.txt', 'led_15_DSBJ_Cu_18.txt',
                    'led_20.txt', 'led_20_DSBJ.txt', 'led_25_DSBJ_Cu.txt', 'led_20_DSBJ_Cu_18.txt',
                    'led_25.txt', 'led_25_DSBJ.txt', 'led_25_DSBJ_Cu.txt', 'led_25_DSBJ_Cu_18.txt',
                    'led_15_DSBJ_18.txt', 'led_15_DSBJ_Cu_18_PIXEL.txt', 'led_15_GEN25_DSBJ_Cu.txt', 'led_15_GEN25_DSBJ_Cu_18.txt',
                    'led_20_DSBJ_18.txt', 'led_20_DSBJ_Cu_18_PIXEL.txt', 'led_20_GEN25_DSBJ_Cu.txt', 'led_20_GEN25_DSBJ_Cu_18.txt',
                    'led_25_DSBJ_18.txt', 'led_25_DSBJ_Cu_18_PIXEL.txt', 'led_25_GEN25_DSBJ_Cu.txt', 'led_25_GEN25_DSBJ_Cu_18.txt'
                    ];

function init() {
    initCommand();
    if (!web.env.supportCinemaModel) {
        setInterval(checkDBC, dbcInterval);
    }
}

function printLedLog(str) {
       var lines = str.split('\n');
       lines.forEach(function(line) {
               if (/.*ledc.*/.exec(line)) {
            api.io.emit("ledLog", line);
               }
       });
}


var logWatcher = undefined;

function switchLoggingMode(socket, msg) {
       api.luna().call('luna://com.webos.service.config/setConfigs', {
        configs: {
            'system.collectDevLogs': (msg.on === true)
        }
    }, function(ret) {
        var cmd = 'PmLogCtl set commercial-ledcontrolservice ' + (msg.on === true ? 'debug' : 'info');
        child_process.execSync(cmd);
           socket.emit('testLedAPI' + msg.eventID, 'ok');
    });
}

function registerLogHandler(socket, msg) {
    if (logWatcher) {
        return;
    }

    var logFile = '/var/log/messages'
    var len = 0;

    fs.stat(logFile, function (err, stats) {
        if (err) {
                   socket.emit('testLedAPI' + msg.eventID, 'log file not found');
            return;
        }
               socket.emit('testLedAPI' + msg.eventID, 'ok');

        len = stats.size;

        logWatcher = fs.watch(logFile, function (event, filename) {
            if (event == 'change') {
                var newLen = 0;
                try {
                    newLen = fs.statSync(logFile).size;
                } catch (e) {
                    console.log('file not found ' + logFile);
                    return;
                }
                if (newLen == len) {
                    return;
                }
                fs.open(logFile, 'r', function (err, fd) {
                    if (err) {
                        return;
                    }
                    var buffer = new Buffer(newLen - len);
                    fs.read(fd, buffer, 0, buffer.length, len, function (err, bytesRead, buffer) {
                        var data = buffer.toString('utf8');
                        printLedLog(data);
                    });
                    len = newLen;
                });
               }

               if (event == 'rename') {
                       fs.readFile(logFile, 'utf8', function (err, data) {
                           if (err) {
                        return;
                    }
                           printLedLog(data);
                });
               }
        });
    });
}

function unregisterLogHandler() {
    if (!logWatcher) {
        return;
    }
    logWatcher.close();
    logWatcher = undefined;
}

function lunaCallControl(command, param, callback) {
	api.luna().call(ledcontrlURL, {
		command: command,
		parameter: param
	}, callback);
}

function getPmVal(callback) {
    lunaCallControl("getPmVal", {}, callback);
}

function setSharedPmVal(pmVal, callback) {
    lunaCallControl("setSharedPmVal", {
        sharedPmVal: pmVal
    }, callback);
}

function getDuplexInfoForDBC(callback) {
    lunaCallControl("getSysCtrlId", {}, function(ret) {
        if (!ret.payload.returnValue) {
            callback(false);
            return;
        }
        lunaCallControl("getDuplexInfo", {
            id: ret.payload.result.id
        }, callback);
    });
}

function getSysCtrlId(socket, msg) {
    lunaCallControl("getSysCtrlId", {}, function(ret) {
		socket.emit('testLedAPI' + msg.eventID, ret.payload);
    });
}

function loadLayoutFromHW(socket, msg) {
    lunaCallControl("loadLayoutFromHW", {}, function(ret) {
		socket.emit('testLedAPI' + msg.eventID, ret.payload);
    });
}

function getOverallLayout(socket, msg) {
    lunaCallControl("getOverallLayout", {}, function(ret) {
		socket.emit('testLedAPI' + msg.eventID, ret.payload);
    });
}

function setTestPattern(socket, msg) {
    lunaCallControl("setTestPattern", {
        id: msg.id,
        onOff: msg.on,
        r: msg.r,
        g: msg.g,
        b: msg.b
    }, function(ret) {
		socket.emit('testLedAPI' + msg.eventID, ret.payload);
    });
}

function getLedCtrlError(socket, msg) {
    lunaCallControl("getLedCtrlError", {
        id: msg.id,
    }, function(ret) {
		socket.emit('testLedAPI' + msg.eventID, ret.payload);
    });
}


function getLdmError(socket, msg) {
	lunaCallControl("getLdmError", {
		id: msg.id,
	}, function(ret) {
		socket.emit('testLedAPI' + msg.eventID, ret.payload);
	});
};

function getSysCtrlError(socket, msg) {
	lunaCallControl("getSysCtrlError", {
		id: msg.id,
	}, function(ret) {
		socket.emit('testLedAPI' + msg.eventID, ret.payload);
	});
};

function setGainLoad(socket, msg) {
	lunaCallControl("setGainLoad", {
		id: msg.id,
	}, function(ret) {
		socket.emit('testLedAPI' + msg.eventID, ret.payload);
	});
};

function getLdmRgbInfo(socket, msg) {
	lunaCallControl("getLdmRgbInfo", {
		id: msg.id,
	}, function(ret) {
		socket.emit('testLedAPI' + msg.eventID, ret.payload);
	});
};

function getLdmEdgeCalInfo(socket, msg) {
	lunaCallControl("getLdmEdgeCalInfo", {
		id: msg.id,
	}, function(ret) {
		socket.emit('testLayoutAPI' + msg.eventID, ret.payload);
	});
};

function setLdmEdgeCalInfo(socket, msg) {
    lunaCallControl("setLdmEdgeCalInfo", {
        id: msg.id,
        color: msg.color,
        edgeVal: msg.edgeVal
    }, function(ret) {
        socket.emit('testLayoutAPI' + msg.eventID, ret.payload);
    });
}

function setLdmRgbInfoAll(socket, msg) {
	lunaCallControl("setLdmRgbInfoAll", {
		r: msg.r,
        g: msg.g,
        b: msg.b
	}, function(ret) {
		socket.emit('testCalAPI' + msg.eventID, ret.payload);
	});
};


function setSystemControlPattern(socket, msg) {
     lunaCallControl("setSystemControlPattern", {
        onOff: msg.on,
        patternNumber: msg.patternNumber
    }, function(ret) {
        socket.emit('testLedAPI' + msg.eventID, ret.payload);
    });
};

function setLedCtrlPortPattern(socket, msg) {
     lunaCallControl("setLedCtrlPortPattern", {
        onOff: msg.on
    }, function(ret) {
        socket.emit('testLedAPI' + msg.eventID, ret.payload);
    });
};

function getDuplexInfo(socket, msg) {
     lunaCallControl("getDuplexInfo", {
        id: msg.id
    }, function(ret) {
        socket.emit('testLedAPI' + msg.eventID, ret.payload);
    });
};

function getPitchInfo(socket, msg) {
    lunaCallControl("getPitchInfo", {
        id: msg.id
    }, function(ret) {
        socket.emit('testLedAPI' + msg.eventID, ret.payload);
    });
};

function setPitch(socket, msg) {
     lunaCallControl("setPitch", {
        id: msg.id,
        pitch: msg.pitch
    }, function(ret) {
        socket.emit('testLedAPI' + msg.eventID, ret.payload);
    });
}
     
function getFpgaVersion(socket, msg) {
     lunaCallControl("getFpgaVersion", {}, function(ret) {
        socket.emit('testLedAPI' + msg.eventID, ret.payload);
    });
}

function getPsuVersion(socket, msg) {
     lunaCallControl("getPsuVersion", {
        id: msg.id
    }, function(ret) {
        socket.emit('testLedAPI' + msg.eventID, ret.payload);
    });
}

function getPsuList(socket, msg) {
     lunaCallControl("getPsuList", {}, function(ret) {
        socket.emit('testLedAPI' + msg.eventID, ret.payload);
    });
}

function getPsuError(socket, msg) {
     lunaCallControl("getPsuError", {
            id: msg.id     
        }, function(ret) {
        socket.emit('testLedAPI' + msg.eventID, ret.payload);
    });
}

function setPsuOutput(socket, msg) {
     lunaCallControl("setPsuOutput", {
            on: msg.on     
        }, function(ret) {
        socket.emit('testLedAPI' + msg.eventID, ret.payload);
    });
}

function setSysCtrlPortPosition(socket, msg) {
    lunaCallControl('setSysCtrlPortPosition', {
        idPosArr: [{
            id: msg.id,
            position: msg.pos
        }]
    }, function(ret) {
        socket.emit('testLedAPI' + msg.eventID, ret.payload);
    });
}

function setLedCtrlDaisyPosition(socket, msg) {
    lunaCallControl('setLedCtrlDaisyPosition', {
        idPosArr: [{
            id: msg.id,
            position: msg.pos
        }]
    }, function(ret) {
        socket.emit('testLedAPI' + msg.eventID, ret.payload);
    });
}

function setLedCtrlPort(socket, msg) {
    lunaCallControl('setLedCtrlPort', {
        idSortArr: [{
            id: msg.id,
            ledPortSortNum: msg.pos
        }]
    }, function(ret) {
        socket.emit('testLedAPI' + msg.eventID, ret.payload);
    });
}

function setLdmConnection(socket, msg) {
    lunaCallControl('setLdmConnection', {
        startPosInfo: [{
            id: msg.id,
            ldmStartPos: msg.pos
        }]
    }, function(ret) {
        socket.emit('testLedAPI' + msg.eventID, ret.payload);
    });
}

function fpgaUpdateChk(socket, msg) {
    
    api.luna().call(updateProgressURL, {
        type: "fpga"
    }, function(ret) {
        socket.emit('testLedAPI' + msg.eventID, {
            fpgaUpdateChk :ret.payload.status,
            fpgaUpdateProgress :fpgaUpdateProgress,
            fpgaFileName :fpgaFileName
        });
    });
}

function fpgaUpdate(socket, msg) {
    lunaCallControl("fpgaUpdate", {
		id: msg.id,
		filePath: msg.filePath
	}, function(ret) {
		socket.emit('testLedAPI' + msg.eventID, ret.payload);
    });
}

function spiFpgaUpdate(socket, msg) {
    lunaCallControl("spiFpgaUpdate", {
		filePath: msg.filePath
	}, function(ret) {
		socket.emit('testLedAPI' + msg.eventID, ret.payload);
    });
}

function getGainLoadProgressSubscribe(socket, msg) {
    gainLoadProgress = api.luna().subscribe(gainProgressURL, {
        subscribe: true
    });

    gainLoadProgress.on("response", function(m) {
        if ((m.payload.status == 'NG') || (m.payload.description == 'done')) {
            console.log("progress End");
            gainLoadProgress.cancel();
        }
        api.io.emit("gainLoadProgressSub", m.payload);
	});
}

function getFpgaUpdateProgress(socket, msg) {
    updateProgress = api.luna().subscribe(updateProgressURL, {
        type: "fpga",
        subscribe: true
    });

    updateProgress.on("response", function(m) {
        if (m.payload.status == 'done' || m.payload.status == 'error') {
            console.log("progress End");
            fpgaFileName = '';
            fpgaUpdateProgress = 0;
            updateProgress.cancel();
        }
        api.io.emit("fpgaUpdateProgress", m.payload);
        fpgaUpdateProgress = m.payload.progress;
	});
}

function spiFpgaUpdateFactoryMenu(socket, msg) {
    fpgaFileName = msg.filePath;
    api.luna().call(updateURL, {
        type: 'fpga',
        path: msg.filePath
    });
}

function saveLayout(socket, msg) {
    lunaCallControl('setI2cConfig', {
        id: msg.id,
        category: 'layout',
        action: 'save'
    }, function(ret) {
        socket.emit('testLedAPI' + msg.eventID, ret.payload);
    });
}

function getGainMaskDone(socket, msg) {
     lunaCallControl("getGainMaskDone", {}, function(ret) {
        socket.emit('testCalAPI' + msg.eventID, ret.payload);
    });
}

function setPixelGain(socket, msg) {
    lunaCallControl("setPixelGain", {
        id: msg.id,
        filePath: msg.filePath
    }, function(ret) {
        socket.emit('testCalAPI' + msg.eventID, ret.payload);
    });
}

function setMaskingJIG(socket, msg) {
     lunaCallControl("setGainMask", {
            id: msg.id
        }, function(ret) {
        socket.emit('testCalAPI' + msg.eventID, ret.payload);
    });
}

function setMaskingJIGforUSB(socket, msg) {
    console.log(msg.filePath);
     lunaCallControl("setGainMask", {
            id: msg.id,
            filePath: msg.filePath
        }, function(ret) {
        socket.emit('testCalAPI' + msg.eventID, ret.payload);
    });
}

function getFileList(socket, msg) {
    var files = '';
    var targetArray = [];
    var filePath = msg.filePath;

    files = fs.readdirSync(filePath);
    for (file in files) {
        var checkFile = false;
        for (mask in maskingArray) {
            if (maskingArray[mask] == files[file]) {
                checkFile = true;
                break;
            }
        }
        if (checkFile) {
            targetArray.push(files[file]);
        }
    }
    console.log(targetArray);
    socket.emit('testCalAPI' + msg.eventID, {targetArray: targetArray});
}

function getListDevice(socket, msg) {
    api.luna().call(storageManagerURL, {
        }, function(ret) {
            var files = '';
            var usbPath = '';
            var targetArray = [];
            for (index in ret.payload.devices) {
                if (ret.payload.devices[index].deviceType == 'usb') {
                    usbPath = ret.payload.devices[index].subDevices[0].deviceUri;
                    files = fs.readdirSync(ret.payload.devices[index].subDevices[0].deviceUri);
                    for (file in files) {
                        var suffix = files[file].substr(files[file].length -3, files[file].length);
                        if (suffix == msg.type) {
                            targetArray.push(files[file]);
                        }
                    }
                }
            }
        socket.emit('testLedAPI' + msg.eventID, 
            {usbPath: usbPath, targetArray: targetArray});
    });
}

function gainFormatChk(socket, msg) {
    var gainPath = '/var/' + msg.gainFile;
    var width = 240;
    var height = 180;

    lunaCallControl("getSysCtrlId", {}, function(sysRet) {
        lunaCallControl("getPitchInfo", {
            id: sysRet.payload.result.id 
        }, function(pitchRet) {
            var pitch = pitchRet.payload.result.pitch;
            pitch = Number(pitch)/10;

            fs.readFile(gainPath, function(err, data) {
                if(err) {
                    return;
                }
                var gainArray = data.toString().split('\n');
                var pitchInfo = gainArray[0].toString().split(' ');
                var lastPos = (width/pitch) * (height/pitch) + 1;

                console.log(gainArray.length);
                if (gainArray.length >= lastPos) {
                    if (gainArray.length != lastPos) {
                        var lastVal = gainArray[lastPos].trim();
                        if (lastVal == '' && gainArray.length == lastPos+1) {
                            console.log("Blank");
                        } else if (lastVal == 'EOF' && (gainArray.length == lastPos+1 
                            || gainArray.length == lastPos+2)) {
                            console.log("EOF");
                        } else {
                            socket.emit('testCalAPI' + msg.eventID, false);
                        }
                    }
                } else {
                    socket.emit('testCalAPI' + msg.eventID, false);
                }

                if (Number(pitchInfo[0]) == width/pitch && 
                    Number(pitchInfo[1]) == height/pitch) {
                    console.log("Valid Format");
                    socket.emit('testCalAPI' + msg.eventID, true);
                } else {
                    socket.emit('testCalAPI' + msg.eventID, false);
                }
            });
        });
    });
}

function removeFile(socket, msg) {
    for (file in msg.fileList) {
        var filePath = "/var/";
        filePath += msg.fileList[file];

        var checkFile = false;
        for (mask in maskingArray) {
            if (maskingArray[mask] == msg.fileList[file]) {
                checkFile = true;
                break;
            }
        }

        fs.unlink(filePath, function(err) {
            if (err || !checkFile) {
                socket.emit('testCalAPI' + msg.eventID, false);
                return;
            }
            socket.emit('testCalAPI' + msg.eventID, true);
        });
    }
}

function copyFile(socket, msg) {
    var inputPath = msg.inputPath;
    var outputPath = '/var/' + msg.outputPath;

    var input = fs.createReadStream(inputPath);
    var output = fs.createWriteStream(outputPath);
    input.pipe(output);

    socket.emit('testledAPI' + msg.eventID, true);
}

function getGainLoadProgress(socket, msg) {
    lunaCallControl('getGainLoadProgress', {
        id :msg.id
    }, function(ret) {
        socket.emit('testledAPI' + msg.eventID, ret.payload);
    });
}

function searchPsu(socket, msg) {
    psu.searchPsu(msg.ipRange, function(ret) {
        socket.emit('testPsuAPI' + msg.eventID, ret);
    });
}

function savePsuIp(socket, msg) {
    psu.savePsuIp(msg.psuIpArray, function(ret) {
        socket.emit('testPsuAPI' + msg.eventID, ret);
    });
}

function setPsuList(socket, msg) {
    lunaCallControl('setPsuList', {
        psuList :msg.setPsuIpArray
    }, function(ret) {
        socket.emit('testPsuAPI' + msg.eventID, ret.payload);
    });
}

function deletePsuList(socket, msg) {
    lunaCallControl('deletePsuList', {}, function(ret) {
        socket.emit('testPsuAPI' + msg.eventID, ret.payload);
    });
}

// MBI
function getMBI5153UserMode(socket, msg) {
    lunaCallControl('getMBI5153UserMode', {}, function(ret) {
        socket.emit('testMbiAPI' + msg.eventID, ret.payload);
    });
}

function setMBI5153UserMode(socket, msg) {
    lunaCallControl('setMBI5153UserMode', {
        on :msg.on
    }, function(ret) {
        socket.emit('testMbiAPI' + msg.eventID, ret.payload);
    });
}

function getMBI5153RGB(socket, msg) {
    lunaCallControl('getMBI5153RGB',  {
        color :msg.color
    }, function(ret) {
        socket.emit('testMbiAPI' + msg.eventID, ret.payload);
    });
}

function setMBI5153RGB(socket, msg) {
    lunaCallControl('setMBI5153RGB',  {
        color :msg.color,
        data :msg.data
    }, function(ret) {
        socket.emit('testMbiAPI' + msg.eventID, ret.payload);
    });
}

// SUM
function getSUM2035UserMode(socket, msg) {
    lunaCallControl('getSUM2035UserMode', {}, function(ret) {
        socket.emit('testSumAPI' + msg.eventID, ret.payload);
    });
}

function setSUM2035UserMode(socket, msg) {
    lunaCallControl('setSUM2035UserMode', {
        on :msg.on
    }, function(ret) {
        socket.emit('testSumAPI' + msg.eventID, ret.payload);
    });
}

function getSUM2035RGB(socket, msg) {
    lunaCallControl('getSUM2035RGB',  {
        color :msg.color
    }, function(ret) {
        socket.emit('testSumAPI' + msg.eventID, ret.payload);
    });
}

function setSUM2035RGB(socket, msg) {
    lunaCallControl('setSUM2035RGB',  {
        color :msg.color,
        data :msg.data
    }, function(ret) {
        socket.emit('testSumAPI' + msg.eventID, ret.payload);
    });
}

function setLdmPatternBlinking(socket, msg) {
    lunaCallControl('setLdmPatternBlinking',  {
        id :msg.id,
        onOff :true,
        second: 5,
    }, function(ret) { 
        socket.emit('testLayoutAPI' + msg.eventID, ret.payload);
    });
}

function get365CareServiceMode(socket, msg) {
    api.luna().call("luna://com.webos.service.tv.systemproperty/getProperties", { keys:["commer365CareServiceMode"] }, function(ret) {
        socket.emit('signage365Care' + msg.eventID, ret.payload);
    });
}

function searchAccountName(socket, msg) {
    signage365Care.searchAccountName(msg.accountNumber, function(ret) {
        socket.emit('signage365Care' + msg.eventID, ret);
    });
}

function care365Enable(socket, msg) {
    api.luna().call(care365URL + 'start', {
	}, function(ret) {
        socket.emit('signage365Care' + msg.eventID, ret.payload);
    });
}

function care365Disable(socket, msg) {
    api.luna().call(care365URL + 'stop', {
	}, function(ret) {
        socket.emit('signage365Care' + msg.eventID, ret.payload);
    });
}

function appDownloadURL(socket, msg) {
    signage365Care.appDownloadURL(msg.serviceMode, function(ret) {
        socket.emit('signage365Care' + msg.eventID, ret);
    });
}


function getServerStatusCare365(socket, msg) {
    api.luna().call(care365URL + 'getServerStatus', {
    }, function(ret) {
        socket.emit('signage365Care' + msg.eventID, ret.payload);
    });
}

function getServerStatusCare365Subscribe(socket, msg) {
    
    serverStatus = api.luna().subscribe(care365URL + 'getServerStatus', {
        subscribe: true
	});
    

    serverStatus.on("response", function(m) {
        if (!m.payload.returnValue) {
            console.log("365 Care Server Subscribe End");
            serverStatus.cancel();
        } else {
            api.io.emit("serverStatus", m.payload);
        }
    });

}

function remove365CareApp(socket, msg) {
     api.luna().call(appInstallServiceURL + 'remove', {
        id: "com.webos.app.commercial.care365"
    }, function(ret) {
        socket.emit('signage365Care' + msg.eventID, ret.payload);
    });
}

function getListApps(socket, msg) {
    api.luna().call(applicationManagerURL + 'listApps', {
	}, function(ret) {
        socket.emit("getListAppsInfo" + msg.eventID, ret.payload);
    });
}

function getInitialTimeSetChk(socket, msg) {
    api.luna().call(systemTimeURL, {
    }, function(ret) {
        socket.emit("imb" + msg.eventID, ret.payload.systemTimeSource);
    });
}

function getUseNetworkTime(socket, msg) {
    api.luna().call(systemserviceURL + 'getPreferences', {
        keys: ["useNetworkTime"]
    }, function(ret) {
        console.log(ret);
        socket.emit("signage365Care" + msg.eventID, ret.payload);
    });
}

function setAutomaticallyTime(socket, msg) {
    api.luna().subscribe(systemserviceURL + 'setPreferences', {
        useNetworkTime: true,
        subscribe: true
    });

    lunaAPI.setSystemSettings('time', {
        autoClock: 'on'
    }, function(ret) {
        socket.emit("signage365Care" + msg.eventID, ret.payload);
    });
}

function isFirstBoot(socket, msg) {
    socket.emit('signage365Care' + msg.eventID, isFirstBooting);
    isFirstBooting = false;
}

function installIpkDownload(socket, msg) {

    var ipkDownload = api.luna().subscribe("palm://com.palm.downloadmanager/download", {
        target: msg.downloadUrl,
        targetDir: "/media/internal/downloads/",
        targetFileName: "com.webos.lapp.commercial.care365.ipk",
		subscribe: true
	});

	ipkDownload.on("response", function(m) {
        if (m.payload.hasOwnProperty("completionStatusCode")) {
            console.log(m.payload.completionStatusCode);
            api.io.emit("installIpk", m.payload);
            ipkDownload.cancel();
        }
	})
}

function install365CareApp(socket, msg) {
    var careInstall = api.luna().subscribe("luna://com.webos.appInstallService/commercial/installInternal", {
        id: "com.webos.app.commercial.care365",
        ipkUrl: "/media/internal/downloads/" + msg.destFile,
        signature: msg.signature,
        subscribe: true
    });

    careInstall.on("response", function(m) {
        if (m.payload.hasOwnProperty("details") && (m.payload.details.state == "installed" || m.payload.details.state == "install failed")) {
            api.io.emit("careInstall", m.payload);
            careInstall.cancel();
        }
    });
}

function getMaxThermalVal(socket, msg) {
    lunaCallControl("getMaxThermalVal", {
    }, function(ret) {
        socket.emit('testDbcAPI' + msg.eventID, ret.payload);
    });
}

function getFinalPmVal(socket, msg) {
    lunaCallControl("getFinalPmVal", {
    }, function(ret) {
        socket.emit('testDbcAPI' + msg.eventID, ret.payload);
    });
}

function getDbcTable(socket, msg) {
    lunaCallControl("getDbcTable", {
    }, function(ret) {
        socket.emit('testDbcAPI' + msg.eventID, ret.payload);
    });
}

function setDbcTable(socket, msg) {
    lunaCallControl("setDbcTable", {
        "5C": msg.dbcVal["5C"],
        "10C": msg.dbcVal["10C"],
        "15C": msg.dbcVal["15C"],
        "20C": msg.dbcVal["20C"],
        "25C": msg.dbcVal["25C"],
        "30C": msg.dbcVal["30C"],
        "35C": msg.dbcVal["35C"],
        "40C": msg.dbcVal["40C"],
        "45C": msg.dbcVal["45C"],
        "50C": msg.dbcVal["50C"],
        "55C": msg.dbcVal["55C"],
        "60C": msg.dbcVal["60C"],
        "65C": msg.dbcVal["65C"],
        "70C": msg.dbcVal["70C"],
        "75C": msg.dbcVal["75C"],
        "80C": msg.dbcVal["80C"],
        "85C": msg.dbcVal["85C"],
        "90C": msg.dbcVal["90C"],
        "95C": msg.dbcVal["95C"],
        "100C": msg.dbcVal["100C"]
    }, function(ret) {
        socket.emit('testDbcAPI' + msg.eventID, ret.payload);
    });
}

function setLdmRgbInfo(socket, msg) {
    lunaCallControl("setLdmRgbInfo", {
        id: msg.id,
		r: msg.r,
        g: msg.g,
        b: msg.b
	}, function(ret) {
		socket.emit('testLayoutAPI' + msg.eventID, ret.payload);
	});
}

function getLdmRgbInfo(socket, msg) {
    lunaCallControl("getLdmRgbInfo", {
        id: msg.id
    }, function(ret) {
        socket.emit('testLayoutAPI' + msg.eventID, ret.payload);
    });
}

function setLowLatencyMode(socket, msg) {
    lunaCallControl("setLowLatencyMode", {
        lowLatencyMode: msg.lowLatencyMode
    }, function(ret) {
        socket.emit('testLedAPI' + msg.eventID, ret.payload);
    });
}

function getLowLatencyMode(socket, msg) {
    lunaCallControl("getLowLatencyMode", {
    }, function(ret) {
        socket.emit('testLedAPI' + msg.eventID, ret.payload);
    });
}

//Cinema
function getImbStatus(socket, msg) {
    //Get_IMB_status(int imb_security_index, char *Ret);
    lunaCallControl('getImbStatus', {
        index: msg.index,
    }, function(ret) {
        socket.emit('imb' + msg.eventID, ret.payload);
    });
    //socket.emit('imb' + msg.eventID, 'green');
}

function setImbLogin(socket, msg) {
    //Set_IMB_Login(char *login_id, char *password, char * Ret);
    lunaCallControl('setImbLogin', {
        id: msg.id,
        password: msg.password
    }, function(ret) {
        socket.emit('imb' + msg.eventID, ret.payload);
    });
    //socket.emit('imb' + msg.eventID, 'success');
}

function setImbMarriage(socket, msg) {
    //Set_IMB_Marriage(char *auth_id, char * Ret);
    lunaCallControl('setImbMarriage', {
    }, function(ret) {
        socket.emit('imb' + msg.eventID, ret.payload);
    });
    //socket.emit('imb' + msg.eventID, 'timeout');
}

function setImbLogout(socket, msg) {
    //Set_IMB_Logout(char * Ret);
    lunaCallControl('setImbLogout', {
    }, function(ret) {
        socket.emit('imb' + msg.eventID, ret.payload);
    });
    //socket.emit('imb' + msg.eventID, 'success');
}

function setSecureSiliconTimeSerial(socket, msg) {
    //Internel Interface
    lunaCallControl('setSecureSiliconTimeSerial', {
    }, function(ret) {
        socket.emit('imb' + msg.eventID, ret.payload);
    });
    //socket.emit('imb' + msg.eventID, true);
}

function getSecureSiliconStatus(socket, msg) {
    //Internel Interface
    lunaCallControl('getSecureSiliconStatus', {
    }, function(ret) {
        socket.emit('imb' + msg.eventID, ret.payload);
    });
    //socket.emit('imb' + msg.eventID, '1');
}

function getSecureSiliconCert(socket, msg) {
    lunaCallControl('getSecureSiliconCert', {
    }, function(ret) {
        socket.emit('imb' + msg.eventID, ret.payload);
    });
}

function getFpgaStatus(socket, msg) {
    //Internel Interface
    lunaCallControl('getFpgaStatus', {
    }, function(ret) {
        socket.emit('imb' + msg.eventID, ret.payload);
    });
    //socket.emit('imb' + msg.eventID, 'success');
}

function getCpuStatus(socket, msg) {
    //Internel Interface
    lunaCallControl('getCpuStatus', {
    }, function(ret) {
        socket.emit('imb' + msg.eventID, ret.payload);
    });
    //socket.emit('imb' + msg.eventID, 'success');
}

function setGpio(socket, msg) {
    //Set_IMB_Login(char *login_id, char *password, char * Ret);
    lunaCallControl('setGpio', {
        pin: msg.pin,
        level: msg.level
    }, function(ret) {
        socket.emit('imb' + msg.eventID, ret.payload);
    });
    //socket.emit('imb' + msg.eventID, 'success');
}

function getButtonStatus(socket, msg) {
    //Internal Interface
    lunaCallControl('getButtonStatus', {
    }, function(ret) {
        socket.emit('imb' + msg.eventID, ret.payload);
    });
    //socket.emit('imb' + msg.eventID, true);
}


function startMarriageTimer(socket, msg) {
    isMarriageTimerOn = msg.on === true;
    if (marriageTimer) {
        clearTimeout(marriageTimer);
    }
    if (msg.on) {
        marriageTimer = setTimeout( function() {
            isMarriageTimerOn = false;
            marriageTimer = undefined;
        }, 60 * 1000);
    }
	console.log("isMarriageTimerOn", isMarriageTimerOn);
	socket.emit('imb' + msg.eventID, true);
}

function isManualTimeSet(socket, msg) {
    fs.access(manualTimeSetFile, fs.F_OK, function(err) {
        var exist = true;
        if (err) {
            exist = false;
        }
        socket.emit('imb' + msg.eventID, exist);
    })
}

function isDbcOff(socket, msg) {
    fs.access(dbcOffFile, fs.F_OK, function(err) {
        var exist = true;
        if (err) {
            exist = false;
        }
        socket.emit('testLedAPI' + msg.eventID, exist);
    })
}

function setDbcOff() {
    fs.closeSync(fs.openSync(dbcOffFile, 'w'));
    return true;
}

function rmDbcOff(socket, msg) {
    fs.access(dbcOffFile, fs.F_OK, function(err) {
        if (!err) {
            fs.unlink(dbcOffFile, function(err) {
                if (!err) {
                    socket.emit('testLedAPI' + msg.eventID, true);
                }
            });
        } else {
            socket.emit('testLedAPI' + msg.eventID, false);
        }
    });
}

function checkAccountFunc(socket, msg) {
    lunaAPI.getSystemProperties(['serialNumber'], function(ret) {
        if (ret.payload.serialNumber + dbcAccountNumber == msg.currentPin) {
            if (msg.category == 'dbc') {
                if (msg.on) {
                    socket.emit('testLedAPI' + msg.eventID, setDbcOff());
                } else {
                    rmDbcOff(socket, msg);
                }
            } else if (msg.category == 'cal') {
                console.log("11");
                lunaCallControl("turnOnCalServer", {
                    on: msg.on
                }, function(ret) {
                    socket.emit('testLedAPI' + msg.eventID, ret.payload);
                });
            }
        } else {
            socket.emit('testLedAPI' + msg.eventID, false);
        }
    });
}

function setManualTimeSet(socket, msg) {
    fs.closeSync(fs.openSync(manualTimeSetFile, 'w'));
    socket.emit('imb' + msg.eventID, true);
}

function initCommand() {
    command["getSysCtrlId"] = getSysCtrlId;
    command["loadLayoutFromHW"] = loadLayoutFromHW;
    command["getOverallLayout"] = getOverallLayout;
    command["setTestPattern"] = setTestPattern;
    command["getLedCtrlError"] = getLedCtrlError;
	command["getLdmError"] = getLdmError;
    command["getLedCtrlError"] = getLedCtrlError;
    command["getSysCtrlError"] = getSysCtrlError;
    command["setGainLoad"] = setGainLoad;
    command["getLdmRgbInfo"] = getLdmRgbInfo;
    command["setSystemControlPattern"] = setSystemControlPattern;
    command["setLedCtrlPortPattern"] = setLedCtrlPortPattern;
    command["getDuplexInfo"] = getDuplexInfo;
    command["getPitchInfo"] = getPitchInfo;
    command["setPitch"] = setPitch;
    command["getFpgaVersion"] = getFpgaVersion;
    command["getPsuVersion"] = getPsuVersion;
    command["getPsuList"] = getPsuList;
    command["getPsuError"] = getPsuError;
    command["setPsuOutput"] = setPsuOutput;
    command["searchPsu"] = searchPsu;
    command["savePsuIp"] = savePsuIp;
    command["deletePsuList"] = deletePsuList;
    command["setPsuList"] = setPsuList;
    command["setMaskingJIG"] = setMaskingJIG;
    command["getGainMaskDone"] = getGainMaskDone;
    command["setMaskingJIGforUSB"] = setMaskingJIGforUSB;
    command["searchAccountName"] = searchAccountName;
    command["care365Enable"] = care365Enable;
    command["care365Disable"] = care365Disable;
    command["appDownloadURL"] = appDownloadURL;
    command["getServerStatusCare365"] = getServerStatusCare365;
    command["getServerStatusCare365Subscribe"] = getServerStatusCare365Subscribe;
    command["remove365CareApp"] = remove365CareApp;
    command["installIpkDownload"] = installIpkDownload;
    command["install365CareApp"] = install365CareApp;
    command['get365CareServiceMode'] = get365CareServiceMode;
    command['getListApps'] = getListApps;
    command['getUseNetworkTime'] = getUseNetworkTime;
    command['setAutomaticallyTime'] = setAutomaticallyTime;
    command['isFirstBoot'] = isFirstBoot;
    command['setLdmRgbInfoAll'] = setLdmRgbInfoAll;
    command['setLdmRgbInfo'] = setLdmRgbInfo;
    command['setLdmEdgeCalInfo'] = setLdmEdgeCalInfo;
    command['getLdmRgbInfo'] = getLdmRgbInfo;
    command['getLdmEdgeCalInfo'] = getLdmEdgeCalInfo;
    command['getDbcTable'] = getDbcTable;
    command['setDbcTable'] = setDbcTable;
    command['getMaxThermalVal'] = getMaxThermalVal;
    command['getFinalPmVal'] = getFinalPmVal;
	command['startMarriageTimer'] = startMarriageTimer;
    command['isDbcOff'] = isDbcOff;
    command['checkAccountFunc'] = checkAccountFunc;
    command['gainFormatChk'] = gainFormatChk;
    command['setLowLatencyMode'] = setLowLatencyMode;
    command['getLowLatencyMode'] = getLowLatencyMode;
    command['getGainLoadProgressSubscribe'] = getGainLoadProgressSubscribe;
    command['setPixelGain'] = setPixelGain;

    command['getImbStatus'] = getImbStatus;
    command['setImbLogin'] = setImbLogin;
    command['setImbMarriage'] = setImbMarriage;
    command['setImbLogout'] = setImbLogout;
    command['setSecureSiliconTimeSerial'] = setSecureSiliconTimeSerial;
    command['getSecureSiliconStatus'] = getSecureSiliconStatus;
    command['getSecureSiliconCert'] = getSecureSiliconCert;
    command['getFpgaStatus'] = getFpgaStatus;
    command['getCpuStatus'] = getCpuStatus;
    command['getButtonStatus'] = getButtonStatus;
    command['setGpio'] = setGpio;

    command['setSysCtrlPortPosition'] = setSysCtrlPortPosition;
    command['setLedCtrlDaisyPosition'] = setLedCtrlDaisyPosition;
    command['setLedCtrlPort'] = setLedCtrlPort;
    command['setLdmConnection'] = setLdmConnection;
    command['saveLayout'] = saveLayout;
    command['fpgaUpdate'] = fpgaUpdate;
    command['fpgaUpdateChk'] = fpgaUpdateChk;
    command['spiFpgaUpdate'] = spiFpgaUpdate;
    command['spiFpgaUpdateFactoryMenu'] = spiFpgaUpdateFactoryMenu;
    command['getFpgaUpdateProgress'] = getFpgaUpdateProgress;
    command['getListDevice'] = getListDevice;
    command['getMBI5153UserMode'] = getMBI5153UserMode;
    command['setMBI5153UserMode'] = setMBI5153UserMode;
    command['getMBI5153RGB'] = getMBI5153RGB;
    command['setMBI5153RGB'] = setMBI5153RGB;
    command['getSUM2035UserMode'] = getSUM2035UserMode;
    command['setSUM2035UserMode'] = setSUM2035UserMode;
    command['getSUM2035RGB'] = getSUM2035RGB;
    command['setSUM2035RGB'] = setSUM2035RGB;
    command['getInitialTimeSetChk'] = getInitialTimeSetChk;
    command['isManualTimeSet'] = isManualTimeSet;
    command['setManualTimeSet'] = setManualTimeSet;
    command['getFileList'] = getFileList;
    command['removeFile'] = removeFile;
    command['getGainLoadProgress'] = getGainLoadProgress;
    command['copyFile'] = copyFile;
    command['setLdmPatternBlinking'] = setLdmPatternBlinking;
    command['registerLogHandler'] = registerLogHandler;
    command['unregisterLogHandler'] = unregisterLogHandler;
    command['switchLoggingMode'] = switchLoggingMode;
}

function commandHandler(socket, msg) {
	var handler = command[msg.command];
	if (!handler) {
		console.log('No matching handler for ' + msg.command);
		return;
	}

	handler(socket, msg);
}

function readGroupFile(callback) {
	fs.readFile(dbcGroupFile, function(err, data) {
		if (err) {
			callback('error');
			return;
		}
		data = '' + data;
		var line = data.split('\n');
		
		if (line.length < 1) {
			callback('error');
			return;
		}
		
		var filteredLine = line.filter(function(ip) {
		    return ip.length > 0;
		});
		
		var ret = {
			group: filteredLine
		};		
		
		callback(ret);
	}); 
}


function createRequestOptions(ip, port, api, method) {
	return {
		host: ip,
		port: port,
		path: '/rest/' + api,
		rejectUnauthorized: false,
		requestCert: true,
		agent: false,
		method: method,
		headers: {
			"Content-Type": "application/json"
		}
	};
}

function setSharedPmForAll(min) {
	getPmVal(function(ret) {
		min = Math.min(ret.payload.result.pmVal, min);
		
		restCallFunction('setSharedPmVal', {
			sharedPmVal: min
		}, true);
		
		setSharedPmVal(min, function() {});
	});
}

function runLocalDBC() {
	getPmVal(function(ret) {
		val = ret.payload.result.pmVal;
		setSharedPmVal(val, function(ret) {
		    //console.log(ret.payload);
		});
	});
}

function checkDBCforSlave(group) {
	var cur = Date.now();

	// exception handling for connection error
	if (cur - lastDbcSetTime < dbcInterval * 5) {
		return;
	}
	
	// run DBC locally
	runLocalDBC();
}

function checkDBCforMaster(group) {
	var completed = 0;
	var min = 0xff;
	
	function setSharedPmHandler() {
		++completed;
		if (completed === group.length - 1) {
            console.log("MIN ", min);
			setSharedPmForAll(min);
		}
	}
	
	getPmVal(function(ret) {
		min = ret.payload.result.pmVal;
		
		group.forEach(function(ip, i) {
            if (ipAddress == ip) {
                return;
            }
			var options = createRequestOptions(ip, web.env.port, 'getPmVal', 'GET');
			
			var req = https.request(options, function(res) {
				res.setEncoding('utf8');
                var retStr = '';
				res.on('data', function (chunk) {
                    retStr += chunk;
                });
                res.on('end', function() {
					try {
						var ret = JSON.parse(retStr);
						if (ret.returnValue) {
                            console.log(ret.result);
							min = Math.min(ret.result.pmVal, min);
						}
					} catch (e) {
                        console.log(retStr);
						console.log(e);
					}
					setSharedPmHandler();
				});
			});
			
			req.on('error', function(e) {
				setSharedPmHandler();
				console.log('checkDBCforMaster - problem with request: ' + e.message);
			});
			
			req.end();
		});
	});
}

var ipAddress = '';

function checkDBC() {
	if (!web.env.supportLedSignage) {
		return;
	}

    getDuplexInfoForDBC(function(dupRet) {
        if (!dupRet) {
            return;
        }
        var dupInfo = dupRet.payload.result
        var isDupSlaveDbcMaster = !dupInfo.isMaster && !dupInfo.isDuplexAlive;

        readGroupFile(function(ret) {
            if (ret == 'error' || (ret.group && ret.group.length < 2)) {
                return;
            }

		    if (ret.group.length == 2 && !dupInfo.isMaster) {
                // standalone mode
                console.log("Standalone mode");
			    runLocalDBC();
			    return;
		    }

		    lunaAPI.getNetworkStatus(function(network) {
                ipAddress = network.payload.wired.ipAddress
                if (ret.group[0] === ipAddress
                        || (ret.group[1] === ipAddress && isDupSlaveDbcMaster)) {
                    console.log('DBC FOR MASTER');
			        checkDBCforMaster(ret.group);
                    return;
                }
                console.log('DBC FOR SLAVE');
				checkDBCforSlave(ret.group);
		    });
	    });
    });
}

function restCallFunction(api, param) {
	if (!web.env.supportLedSignage) {
		return;
	}
	
	readGroupFile(function(ret) {
		if (ret == 'error') {
			return;
		}
		lunaAPI.getNetworkStatus(function(network) {
			if (!ret.group) {
				return;
			}

            // only master call this function
            /*
			if (ret.group[0] !== network.payload.wired.ipAddress) {
				return;
			}
            */

			ret.group.forEach(function(ip, i) {
			    if (ip == network.payload.wired.ipAddress) {
			        return;
			    }
				var options = createRequestOptions(ip, web.env.port, api, 'PUT');

				var req = https.request(options, function(res) {
					res.setEncoding('utf8');
					res.on('data', function (chunk) {
					});
				});
				
				req.on('error', function(e) {
					console.log('restCallFunction - problem with request: ' + e.message);
				});
				
				// write data to request body
				req.end(JSON.stringify(param));
			});
		});
	});
}

exports.commandHandler = commandHandler;
exports.init = init;

exports.readGroupFile = readGroupFile;
exports.getPmVal = getPmVal;
exports.setSharedPmVal = setSharedPmVal;
exports.setLastDbcTime = function(t) {
    lastDbcSetTime = t;
};

exports.isMarriageTimerOn = function() {
	return isMarriageTimerOn;
}
