var fs = require('fs');
var crypto = require('crypto');

var password = process.argv[2];

var salt = Math.round((new Date().valueOf() * Math.random())) + '';

var hashpass = crypto.createHash('sha512').update(salt + password).digest('hex');

var save = {};
save.salt = salt;
save.hash = hashpass;

fs.writeFileSync('env/pass.json', JSON.stringify(save));
