#!/bin/bash

if [ -z ${1+x} ]; 
then ip=192.168.1.244; 
else ip=$1;
fi


scp -r public/assets root@$ip:~/sources/public/
scp public/js/client-common.js root@$ip:~/sources/public/js/client-common.js
scp public/js/page/common.js root@$ip:~/sources/public/js/page/common.js
scp app.js root@$ip:~/sources/app.js
scp run-web-service root@$ip:~/sources/run-web-service
scp -r resources root@$ip:~/sources/