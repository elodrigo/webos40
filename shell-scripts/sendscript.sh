#!/bin/bash

if [ -z ${1+x} ]; 
then ip=192.168.1.244; 
else ip=$1;
fi

scp irunn.sh root@$ip:~/
scp surekill.sh root@$ip:~/
scp kill.sh root@$ip:~/
scp orirunn.sh root@$ip:~/
scp wcopy.sh root@$ip:~/
scp ycopy.sh root@$ip:~/
scp initiate.sh root@$ip:~/
scp webgateway.json root@$ip:/var/luna/preferences/webgateway.json