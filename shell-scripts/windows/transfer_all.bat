echo off
set arg1=%1
set default=244
set pre=192.168.1.
set var=%pre%%default%

IF "%arg1%" == "" (
    set var=%pre%%default%
) else (
    set var=%pre%%arg1%
)

scp -r views/*.* root@%var%:~/downloads/media/developer/com.webos.service.outdoorwebcontrol/views/
scp -r public/js/design/*.* root@%var%:~/downloads/media/developer/com.webos.service.outdoorwebcontrol/public/js/design/
scp views/partials/*.* root@%var%:~/downloads/media/developer/com.webos.service.outdoorwebcontrol/views/partials/
scp views/ledSignage/*.* root@%var%:~/downloads/media/developer/com.webos.service.outdoorwebcontrol/views/ledSignage/
scp routes/index.js root@%var%:~/downloads/media/developer/com.webos.service.outdoorwebcontrol/routes/index.js
scp app.js root@%var%:~/downloads/media/developer/com.webos.service.outdoorwebcontrol/app.js
scp -r public/js/design/ledSignage root@%var%:~/downloads/media/developer/com.webos.service.outdoorwebcontrol/public/js/design/
scp -r public/js/design/modal root@%var%:~/downloads/media/developer/com.webos.service.outdoorwebcontrol/public/js/design/
scp LanguageManager.js root@%var%:~/downloads/media/developer/com.webos.service.outdoorwebcontrol/LanguageManager.js
scp web.js root@%var%:~/downloads/media/developer/com.webos.service.outdoorwebcontrol/web.js
scp public/js/page/ledSignage/signage_common.js root@%var%:~/downloads/media/developer/com.webos.service.outdoorwebcontrol/public/js/page/ledSignage/signage_common.js
scp public/js/page/ledSignage/signage_login.js root@%var%:~/downloads/media/developer/com.webos.service.outdoorwebcontrol/public/js/page/ledSignage/signage_login.js