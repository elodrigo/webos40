$(document).ready(function() {
    // 브라우져의 크기가 변경될 때마다 테이블 head의 크기를 변경합니다. //
    $(window).on('load', function() {


    }).on('resize', function() {
        // 화면에 출력된 테이블 개수만큼 실행합니다. //
        var $tableWrapper = $('.table-wrapper');
        $tableWrapper.each(function() {
            $thisTable = $(this);
            resizeTableHead($thisTable);
        });
    }).resize();
});

function resizeTableHead($tableWrapper) {
    var $thisTable = $tableWrapper;

    // td //
    var $tableCell = $thisTable.find('table').find('tbody').children('tr').eq(0).children('th, td');
    // thead //
    var $tableHead = $thisTable.find('thead');

    // 첫 번째 row 안에 있는 각 td의 width를 구합니다. //
    $tableCell.each(function() {
        var $this = $(this);
        var thisWidth = $this.outerWidth(); // cell Width
        var index = $tableCell.index(this); // cell index

        // 구한 td의 width를 th 내에 있는 요소(.field, button, text-block)에 적용합니다. //
        $tableHead.find('th').eq(index).children('.field, button, .text-block').css('width', thisWidth);
    });
}