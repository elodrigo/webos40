
$(document).ready(function() {
  $('.header-box').load('./header.html', function() {
    $('.gnb-box').load('./gnb_content.html', function() {
      $('.lnb-box').load('./lnb.html', function() {

        console.log('onload');
        
        //================== Child Menu ==================//

        var $childMenu = $('.child-menu');

        $childMenu.find('a').on('click', function(e) {
          var $this = $(this);
          var $parent = $this.closest('li');
          var $parents = $this.closest('ul').children('li');

          if(!$parent.hasClass('active')) {
            $parents.removeClass('active');
            $parent.addClass('active');
          }else {
            $parent.removeClass('active');
          }
        }).on('keydown', function(e) {
          var $this = $(this);
          var $parent = $this.closest('li');
          var $parents = $this.closest('ul').children('li');

          var key = e.keyCode;
          var length = $parents.length;
          var index =  $parent.index();

          // Left //
          if(key == 37) {
            console.log('Left');

            // 맨 좌측 메뉴일 경우 parent menu로 focus 이동 //
            $parentMenu.eq(parentIndex).find('.btn-main-menu').focus();
          }
          // Right //
          else if(key == 39) {
            console.log('Right');
          }
          // Top //
          else if(key == 38) {
            console.log('Top');
            if($parent.hasClass('.child-menu') && index == 0) {
              var $tabGroup = $parent.closest('.child-menu-container').children('.tab-group');
            // 상위에 버튼 그룹이 있다면 위로 이동 //
              if($tabGroup.length > 0) {
                $tabGroup.children('.active').focus();
              }

              return false;
            }
            moveUpChildMenu(index, length, $parents);
          }
          // Bottom //
          else if(key == 40) {
            console.log('Bottom');
            moveDownChildMenu(index, length, $parents);
          }
        });

        // Focus Parent Menu //
        function focusParentMenu(index) {
          $parentMenu.eq(index).find('.btn-main-menu').focus();
        }
        
        // Active Parent Menu //
        function activeParentMenu(index) {
          parentIndex = index;    // 현재 parent index를 저장
          $parentMenu.removeClass('active');
          $parentMenu.eq(index).addClass('active');

          // button group 이 있을경우 button group의 첫 번째 버튼에 포커스 //
          if($parentMenu.eq(index).find('.tab-group').length > 0) {
            var $tabGroup = $parentMenu.eq(index).find('.tab-group');
            $tabGroup.children('button').eq(0).focus();
          }
          // button group이 없을 경우 첫 번째 child-menu에 포커스 //
          else {
            $parentMenu.eq(index).find('.child-menu').eq(0).children('a').focus();
          }
        }

        // 하위 메뉴 위로 이동 함수 //
        function moveUpChildMenu(index, length, $menu) {
          $parent = $menu.closest('ul').closest('li');
          if($menu.eq(index - 1).hasClass('has-child') && $menu.eq(index - 1).hasClass('active')) {

            $menu.eq(index - 1).children('ul').children('li').last().children('a').focus();
          }
          else {
            if(index == 0) {
              if($parent.length > 0 && $parent.hasClass('active')) {
                $parent.children('a').focus();
              }
              else {
                $menu.last().children('a').focus();
              }
            }
            else {
              $menu.eq(index - 1).children('a').focus();
            }
          }
        }

        // 하위 메뉴 아래로 이동 함수 //
        function moveDownChildMenu(index, length, $menu) {
          $parent = $menu.closest('ul').closest('li');
          
          if(index < length - 1) {
            if($menu.eq(index).hasClass('active') && $menu.eq(index).hasClass('has-child')) {
              $menu.eq(index).children('ul').children('li').eq(0).children('a').focus();
            }
            else {
              $menu.eq(index + 1).children('a').focus();
            }
          }
          // 더 이상 아래로 이동할 메뉴가 없을 경우 //
          else if(index == length - 1) {
            console.log('last');
            if($parent.hasClass('active')) {
              console.log($parent.next());
              $parent.next().children('a').focus();
            }
            else {
              $menu.first().children('a').focus();
            }
          }
        }

        var $childrenContainer = $('.child-container');

        // Open Child Menu //
        $('.btn-open-child').on('click', function(e) {
          if(!$childrenContainer.hasClass('active')) {
            $childrenContainer.addClass('active');
          }
        });

        $childrenContainer.find('.btn-back').on('click', function(e) {
          var $this = $(this);
          
          if($childrenContainer.hasClass('active')) {
            $childrenContainer.removeClass('active');
          }
          else {
            $childrenContainer.addClass('active');
          }
        });
      });
    });
  });
});