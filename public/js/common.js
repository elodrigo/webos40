$(document).ready(function() {
  var $tableWrapper = $('.table-wrapper');
  
  $('.slide-color-bar input[type=range]').on('input', function(){
    var val = $(this).val();
    $(this).css('background', 'linear-gradient(to right, #a50034 0%, #a50034 '+ val +'%, #f1f1f1 ' + val + '%, #f1f1f1 100%)');
  });

  $(window).on('load', function() {

    //========== Header ==========//

    var $wrapper = $(('.wrapper'));
    var $header = $('.header-container');

    $header.find('.btn-expand').on('click', function(e) {
      // 세션 유지시간 확장 //
    }).on('focus', function(e) {
      $header.find('.notice-list-container').removeClass('expand');
    });

    // Account //
    $header.find('.btn-account').on('click', function(e) {
      if(!$header.find('.user-menu').hasClass('expand')) {
        $header.find('.user-menu').addClass('expand');
      }
      else {
        $header.find('.user-menu').removeClass('expand');
      }
    }).on('focus', function(e) {
      $header.find('.notice-list-container').removeClass('expand');
    });

    // Language //
    $header.find('.btn-language').on('click', function(e) {
      if(!$header.find('.language-menu').hasClass('expand')) {
        $header.find('.language-menu').addClass('expand');
      }
      else {
        $header.find('.language-menu').removeClass('expand');
      }
    });

    $header.find('.user-menu a').on('blur', function(e) {
      console.log('blur breadcrumb');
    });

    $header.find()

    
    //========== Side Bar ==========//

    var $mainMenu = $('.main-menu');
    var $parentMenu = $('.parent-menu');
    var $contentContainer = $('.content-container');
    var $sideBar = $('.side-bar');
    var menuLength = $mainMenu.find('.btn-main-menu').length;
    

    var parentIndex = 0;

    // Hamburger Menu //

    $hamburgerButton = $('.btn-hamburger');

    $hamburgerButton.on('click', function(e) {
      console.log('handle hamburder');
      if(!$contentContainer.hasClass('expand')) {
        $header.addClass('expand');
        $sideBar.addClass('expand');
        $contentContainer.addClass('expand');
      }
      else {
        $header.removeClass('expand');
        $sideBar.removeClass('expand');
        $contentContainer.removeClass('expand');
      }
    }).on('focus', function(e) {
      $header.find('.user-menu').removeClass('expand');
    });

    //================== PARENT MENU ==================//

    // 메인메뉴 클릭 //
    $mainMenu.find('.btn-main-menu').click(function(e) {
      var $this = $(this);
      var index = $mainMenu.find('.btn-main-menu').index(this);
      var $menu = $parentMenu.eq(index);

      console.log('index = ' + index);
      
      if($menu.hasClass('active')) {
        $menu.removeClass('active');
        if($this.hasClass('has-child')) {
          $sideBar.removeClass('show-child');  
        }
      }
      else {
        $parentMenu.removeClass('active');
        $sideBar.removeClass('show-child');  
        $menu.addClass('active');
        if($this.hasClass('has-child')) {
          $sideBar.addClass('show-child');
        }
      }
    })
    // 메인메뉴 포커스 //
    .on('focus', function(e) {
      var $this = $(this);
      var index = $mainMenu.find('.btn-main-menu').index(this);
      var $menu = $parentMenu.eq(index);
      
      $parentMenu.removeClass('focus');
      $menu.addClass('focus');
    })
    .on('keydown', function(e) {
      var key = e.keyCode;
      var index = $mainMenu.find('.btn-main-menu').index(this);

      // Left //
      if(key == 37) {
        console.log('Left');
      }
      // Right //
      else if(key == 39) {
        console.log('Right');

        // Child menu가 있는지 확인 //
        if($parentMenu.eq(index).hasClass('has-child')) {
          // Child menu가 있으면
          activeParentMenu(index);
          $parentMenu.removeClass('focus');
          $mainMenu.find('.btn-main-menu').blur();
        }
      }
      // Top //
      else if(key == 38) {
        console.log('Top');

        // 상단에 이동할 메뉴가 있으면 이동한다. //
        if(index > 0 && index < menuLength) {
          focusParentMenu(index - 1);
        }
      }
      // Bottom //
      // 하단에 이동할 메뉴가 있으면 이동한다. //
      else if(key == 40) {
        console.log('Bottom');
        if(index >= 0 && index < menuLength) {
          focusParentMenu(index + 1);
        }
      }
    });

    //================== bUTTON GROUP ==================//

    var $tabGroup = $('.tab-group');

    $tabGroup.children('.btn').on('focus', function(e) {
      console.log('focus button group');
    }).on('keydown', function(e) {
      var $this = $(this);
      var $parent = $this.parent('.tab-group');
      var $grandParent = $this.closest('.child-menu-container');

      var key = e.keyCode;
      var length = $parent.children('button').length;
      var index =  $parent.children('button').index(this);

      // Left //
      if(key == 37) {
        console.log('Left');

        // 맨 좌측 메뉴일 경우 parent menu로 focus 이동 //
        if(index == 0) {
          $parentMenu.eq(parentIndex).find('.btn-main-menu').focus();
        }
        else {
          $parent.children('button').eq(index - 1).focus();
        }
      }
      // Right //
      else if(key == 39) {
        console.log('Right');

        if(index < length) {
          $parent.children('button').eq(index + 1).focus();
        }
      }
      // Top //
      else if(key == 38) {
        console.log('Top');
      }
      // Bottom //
      else if(key == 40) {
        console.log('bottom');
        moveDownChildMenu(-1, $grandParent.find('.child-menu').length, $grandParent.find('.child-menu'));
        $this.blur();
      }

      // return false;
    }).on('click', function(e) {
      var $this = $(this);
      var $parent = $this.parent('.tab-group');
      var $grandParent = $this.closest('.child-menu-container');

      var index = $parent.children('.btn').index(this);

      console.log(index);

      if(!$this.hasClass('active')) {
        $parent.children('.btn').removeClass('active');
        $grandParent.find('.tab-panel').removeClass('active');

        $this.addClass('active');
        $grandParent.find('.tab-panel').eq(index).addClass('active');
      }
    });

    //===================== Tab ========================//

    $('.tab-group').children('.tab').find('a').on('click', function(e) {
      var $this = $(this);
      var $tab = $this.closest('.tab');
      var $tabGroup = $this.closest('.tab-group');
      var $currentTabMenu = $tabGroup.prev('.current-tab-menu');
      var tabId = $this.attr('id');
      var tabTitle = $tab.text();

      $currentTabMenu.find('a').text(tabTitle);

      $tabGroup.children('.tab').removeClass('active');
      $tab.addClass('active');
      
      $('.tab-title').addClass('hide');
      $('.title-' + tabId).removeClass('hide');

      $('.tab-panel').addClass('hide');
      $('.panel-' + tabId).removeClass('hide');

      $tabGroup.removeClass('expand');

      var $tableWrapper = $('.table-wrapper');
      $tableWrapper.each(function() {
        $thisTable = $(this);
        resizeTableHead($thisTable);
      });  
      
      return false;
    });

    /*
      Device management - 상세페이지 좌측에 있는 탭을 제어합니다.
      ex)device_manage_overview_media.html 카테고리 tab 일경우
    */
    
    // 해당 셀렉터는 Reflow 400%에 대응하는 셀렉터 입니다. //
    var $categoryMenu = $('.category-menu');
    var $currentTabMenu = $('.current-tab-menu');

    $currentTabMenu.each(function() {
      var $thisTabMenu = $(this);
    
      $thisTabMenu.next('.tabs').find('.tab').each(function() {
        var $this = $(this);
        var $parent = $this.closest('.tabs, .tab-group');
        var thisTitle = $this.find('a, label').text();
  
        if($this.hasClass('active')) {
          $parent.prev('.current-tab-menu').find('a').text(thisTitle);
          return false;
        }
      });

      $thisTabMenu.find('a').on('click', function(e) {
        var $this = $(this);
        var $parent = $this.closest('.current-tab-menu');

        if($parent.next('.tabs, .tab-group').hasClass('expand')) {
          $parent.next('.tabs, .tab-group').removeClass('expand');
        }
        else {
          $parent.next('.tabs, .tab-group').addClass('expand');
        }
      });
    });

    $('.tabs').children('.tab').find('a').on('click', function(e) {
      var $this = $(this);
      var $tab = $this.closest('.tab');
      var $tabs = $this.closest('.tabs');
      var $tabContent = $('.tablist-content');
      var $currentTabMenu = $tabs.prev('.current-tab-menu');
      var tabId = $tab.attr('id');
      var tabTitle = $tab.text();

      $currentTabMenu.find('a').text(tabTitle);

      $tabs.children('.tab').removeClass('active');
      $tab.addClass('active');
      $tabContent.addClass('hide');

      $('.panel-' + tabId).removeClass('hide');

      if($('.panel-' + tabId).find('.table-wrapper').length > 0) {
        $('.panel-' + tabId).find('.table-wrapper').each(function() {
          // resizeTableHead($(this));
        });
      }

      $tabs.removeClass('expand');

      return false;
    });

    $('.btn-dropdown').on('click', function(){
      var $this = $(this);
      var $parent = $this.closest('.dropdown');
      var $parentTable = $this.closest('table').not('.no-scroll, has-not-header');

      $('.btn-dropdown').not($this).attr('aria-selected', 'false');
      console.log($this.attr('aria-selected'));
      if($this.attr('aria-selected') === 'true') {
        $this.attr('aria-selected', 'false');
      }
      else {
        // preventScroll();
        if($parentTable.length > 0) {
          var scrollTop = $(document).scrollTop();
          var offsetTop = $parent.offset().top;
          var offsetLeft = $parent.offset().left;
          var parentWidth = $parent.outerWidth();
          var parentHeight = $parent.outerHeight();

          $this.next('.dropdown-menu').css({
            top:offsetTop + parentHeight - scrollTop,
            left:offsetLeft,
            width:parentWidth,
          });
        }
        $this.attr('aria-selected', true);
      }
    });
  
    $(window).on('scroll', function() {
      $('.btn-dropdown').attr('aria-selected', 'false');
    });

    $('tbody').on('scroll', function() {
      $('.btn-dropdown').attr('aria-selected', 'false');
    });

    $(document).on('mouseup', function(e) {
      $('.btn-dropdown').attr('aria-selected', 'false');
      // enableScroll();
    });

    $('.btn-account').on('click', function(){
      var $this = $(this);
      $(this).toggleClass('active');
    });
  });
});

// ================== Radio Tab ======================//

var $radioTab = $('.radio-tab');

$radioTab.find('input[type="radio"]').on('input', function(e) {
  var $this = $(this);
  var $parent = $radioTab.closest('.tab-container');
  var index = $radioTab.find('input[type="radio"]').index(this);

  if($this.val() == 'on') {
    $parent.find('.tab-panel').addClass('hide');
    $parent.find('.tab-panel').eq(index).removeClass('hide');

    var $tableWrapper = $('.table-wrapper');
    $tableWrapper.each(function() {
      $thisTable = $(this);
      resizeTableHead($thisTable);
    });  
  } 
});

//===================== Table ========================//

$(document).ready(function() {
  // 브라우져의 크기가 변경될 때마다 테이블 head의 크기를 변경합니다. //
  $(window).on('load', function() {
    

  }).on('resize', function() {
    // 화면에 출력된 테이블 개수만큼 실행합니다. //
    var $tableWrapper = $('.table-wrapper');
    $tableWrapper.each(function() {
      $thisTable = $(this);
      resizeTableHead($thisTable);
    });  
  }).resize();
});

// Device Management - Detail page //

var $topInfoBox = $('.top-info-box');

if($topInfoBox.find('.info-box').find('.connected-displays-container').length > 0) {
  var $detailButton = $topInfoBox.find('.info-box').find('.btn-detail');
  var $connectedDisplay = $topInfoBox.find('.info-box').find('.connected-displays-container');

  $detailButton.on('click', function(e) {
    var $this = $(this);

    if($this.hasClass('active')) {
      $this.removeClass('active');
      $connectedDisplay.addClass('hide');
    }
    else {
      $this.addClass('active');
      $connectedDisplay.removeClass('hide');
    }
  });
}

//===================== Convert table to lsit ========================//

var $convertList = $('.convert-table-to-list');

$convertList.each(function() {
  var $list = $(this);
  var $tableRow = $list.find('.table-row');
  var $btnExpand = $tableRow.find('.btn-expand');

  $btnExpand.on('click', function(e) {
    console.log('expand');

    var $this = $(this);
    var $parent = $this.closest('.table-row');

    if($parent.hasClass('expand')) {
      $parent.removeClass('expand');
    }
    else {
      $parent.addClass('expand');
    }
  });
});


//===================== Field Slide bar ========================//

var $fieldSlideBar = $('.field-slide-bar');

$fieldSlideBar.each(function() {
  
  var $slideBar = $(this);
  var $inputRange = $slideBar.find('input[type="range"]');
  var $inputNumber = $slideBar.find('input[type="number"], input[type="text"]');
  var value = $slideBar.find('input[type="range"]').val();

  // Tint 값 보정 //
  if($slideBar.hasClass('field-set-tint')) {
    
    $inputNumber.val(converTintValue(value));
  }

  // 슬라이드 바의 값 변경 이벤트 //
  $inputRange.on('input', function(e) {
    var $this = $(this);
    // 색조 변경 필드일 경우 값을 해당 필드에 맞게 보정해준다. //
    if($slideBar.hasClass('field-set-tint')) {
      $inputNumber.val(converTintValue($this.val()));
    }
    else {
      $inputNumber.val($this.val());
    }
  });

  // 슬라이드 바 왼쪽에 있는 수 입력 핕드 값 변경 이벤트 //
  $inputNumber.on('change', function(e) {
    var $this = $(this);

    // 색조 변경 필드일 경우 값을 해당 필드에 맞게 보정해준다. //
    if($slideBar.hasClass('field-set-tint')) {
      $inputRange.val(convertNormalValue($this.val()));
    }
    else {
      $inputRange.val($this.val());
    }
  });

  // - 버튼 클릭 이벤트 //
  $slideBar.find('.btn-minus').on('click', function(e) {
    var value = Number($inputRange.val());
    
    if(value > 0) {

      // 색조 변경 필드일 경우 값을 해당 필드에 맞게 보정해준다. //
      if($slideBar.hasClass('field-set-tint')) {
        $inputNumber.val(converTintValue(value - 5));
      }
      else {
        $inputNumber.val(value - 5);
      }
      $inputRange.val(value - 5);
    }
  });

  // + 버튼 클릭 이벤트 //
  $slideBar.find('.btn-plus').on('click', function(e) {
    var value = Number($inputRange.val());
    
    if(value < 100) {
      if($slideBar.hasClass('field-set-tint')) {
        $inputNumber.val(converTintValue(value + 5));
      }
      else {
        $inputNumber.val(value + 5);
      }
      $inputRange.val(value + 5);
    }
  });
});

//===================== Set Slide show ========================//

var $setSlideShow = $('.set-slide-show-box');
var $btnExpand = $setSlideShow.find('.btn-expand');

$btnExpand.on('click', function(e) {
  var $this = $(this);
  var $parent = $this.closest('.set-slide-show-box');

  if($parent.hasClass('expand')) {
    $parent.removeClass('expand');
  }
  else {
    $parent.addClass('expand');
  }
});

function converTintValue(value) {
  if(value >= 50) {
    var textValue = 'G' + String(value - 50);
  }
  else {
    var textValue = 'R' + String(50 - value);
  }

  return textValue;
}

//===================== Set font style ========================//

function getEventSlide() {
  console.log('getEvent');
  var $fieldSildeBar = $('.field-slide-bar');

  $fieldSildeBar.each(function() {
    var $thisField = $(this);
    var $thisSilde = $thisField.find('.slide-bar-container').find('input[type="range"]');
    var $thisRuler = $thisField.find('.ruler');
    var $thisRadio = $thisField.find('.field-radio');
    var radioButtonCount = $thisRadio.length;
    var radioButtonRange = Math.floor(100 / radioButtonCount);
    var rulerCount = $thisRuler.length;
    var rulerRange = Math.floor(100 / rulerCount);

    // 슬라이드를 제어할 경우 라디오 버튼도 함께 제어되도록 적용한다. //
    $thisSilde.on('input', function(e) {

      var $this = $(this);
      var thisValue = $this.val();
      var currentRange = Math.floor(thisValue / radioButtonRange);

      if(thisValue >= 100) {
        currentRange--;
      }

      $thisRadio.find('input[type="radio"]').prop('checked', false);
      $thisRadio.eq(currentRange).find('input[type="radio"]').prop('checked', true);
    });

    // 라디오 버튼을 제어할 경우 슬라이드 버튼도 함께 제어되도록 적용한다. //
    $thisRadio.find('input[type="radio"]').on('click', function(e) {

      var $this = $(this);
      var thisIndex = $thisRadio.find('input[type="radio"]').index(this);

      $thisSilde.val(rulerRange * (thisIndex + 0));
    });
  });
}
getEventSlide();

function convertNormalValue(tintValue) {
  var tintColor = tintValue.substring(0, 1);
  var tintNumber = tintValue.substring(1, tintValue.length);

  console.log('tintColor = ' + tintColor);
  console.log('tintNumber = ' + tintNumber);

  if(tintColor == 'R') {
    var normalValue = 50 - tintNumber;
  }
  else {
    var normalValue = tintNumber + 50;
  }

  return normalValue;
}

// 테이블 th width 제어 함수 //
function resizeTableHead($tableWrapper) {
  var $thisTable = $tableWrapper;

  // td //
  var $tableCell = $thisTable.find('table').find('tbody').children('tr').eq(0).children('th, td');
  // thead //
  var $tableHead = $thisTable.find('thead');
  
  // 첫 번째 row 안에 있는 각 td의 width를 구합니다. //
  $tableCell.each(function() {
    var $this = $(this);
    var thisWidth = $this.outerWidth(); // cell Width
    var index = $tableCell.index(this); // cell index

    // 구한 td의 width를 th 내에 있는 요소(.field, button, text-block)에 적용합니다. //
    $tableHead.find('th').eq(index).children('.field, button, .text-block').css('width', thisWidth);
  });
}

function togglePopup(toggler, id) {
  var $this = $('#' + toggler);
  var $popup = $('#' + id);
  var $tabbable = $popup.find("button, input:not([type='hidden']), select, iframe, textarea, [href], [tabindex]:not([tabindex='-1'])");
  var firstTab = $tabbable.first();
  var lastTab = $tabbable.last();
  
  if($popup.hasClass('show')) {
    $this.focus();
    $popup.removeClass('expand');
    setTimeout(function() { $popup.removeClass('show') }, 0);
  }
  else {
    $popup.addClass('show');
    // 팝업 안에 테이블이 있을 경우 resizeTableHead 함수를 실행합니다. //
    if($popup.find('.table-wrapper').length > 0) {
      resizeTableHead($popup.find('.table-wrapper'));
    }
    setTimeout(function() { $popup.addClass('expand') }, 10);

    if($tabbable.length > 0) {
      if(firstTab.prop('nodeName') != 'BUTTON') {
        firstTab.focus();  
      }
      
      firstTab.on('keydown', function(e) {
        if(e.shiftKey && (e.keyCode || e.which) === 9) {
          e.preventDefault();
          lastTab.focus();
        }
      });

      lastTab.on('keydown', function(e) {
        var $this = $(this);
        if(!e.shiftKey && (e.keyCode || e.which) === 9) {
          e.preventDefault();
          firstTab.focus();
        }
      });
    }
    getEventSlide();
  }
  return false;
}



// ==================== input text remove button toggle ====================//
var $input = $('input');

$input.on('keyup', function(e) {2
  var $this = $(this);
  var val = $this.val();

  if(val.length > 0) {
    $this.next('.btn-input-remove').removeClass('hide');
  } else {
    $this.next('.btn-input-remove').addClass('hide');
  }
});

$('.btn-input-remove').on('click', function(e){
  $(this).siblings('input').val('');
  $(this).siblings('input').focus();
  $(this).addClass('hide');
})


// ==================== more button active ====================//
var $moreBtnWrap = $('body').find('.reflow-btns'),
    $moreBtn = $moreBtnWrap.find('.btn-remore');

$moreBtn.on('click', function (e){
  var $moreBtnWrap = $(this).parent($moreBtnWrap) 
  if( $moreBtnWrap.hasClass('active') ) {
    $moreBtnWrap.removeClass('active');
  } else {
    $moreBtnWrap.addClass('active');
  }
})

