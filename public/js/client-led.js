function callLedAPI(command, data, event, callback) {
	if (event && callback) {
		var id = Math.round(Math.random() * 1000000);
		socket.on(event + id, function(msg) {
			callback(msg);
			socket.removeAllListeners(event + id);
		});
		data.eventID = id;
	}
	data.command = command;
	socket.emit("led", data);
}

function getSysCtrlId(callback) {
	callLedAPI("getSysCtrlId", {}, "testLedAPI", callback);
}

function loadLayoutFromHW(callback) {
	callLedAPI("loadLayoutFromHW", {}, "testLedAPI", callback);
}

function getOverallLayout(callback) {
	callLedAPI("getOverallLayout", {}, "testLedAPI", callback);
}

function setTestPattern(id, on, r, g, b, callback) {
	callLedAPI("setTestPattern", {
        id: id,
        on: on,
        r: r,
        g: g,
        b: b
    }, "testLedAPI", callback);
}

function getSysCtrlError(id, callback) {
	callLedAPI("getSysCtrlError", {
		id: id
	}, "testLedAPI", callback);
}

function getLedCtrlError(id, callback) {
	callLedAPI("getLedCtrlError", {
		id: id
	}, "testLedAPI", callback);
}

function getLdmError(id, callback) {
	callLedAPI("getLdmError", {
		id: id
	}, "testLedAPI", callback);
}

function setGainLoad(id, callback) {
    callLedAPI("setGainLoad", {
        id: id
    }, "testLedAPI", callback);
}

function getLdmRgbInfo(id, callback) {
    callLedAPI("getLdmRgbInfo", {
        id: id
    }, "testLedAPI", callback);
}

function setSystemControllerPattern(on, patternNumber, callback) {
    callLedAPI("setSystemControlPattern", {
        on: on,
        patternNumber: patternNumber
    }, "testLedAPI", callback);
}

function setLedCtrlPortPattern(on, callback) {
    callLedAPI("setLedCtrlPortPattern", {
        on: on
    }, "testLedAPI", callback);
}

function getDuplexInfo(id, callback) {
    callLedAPI("getDuplexInfo", {
        id: id
    }, "testLedAPI", callback);
}

function getPitchInfo(id, callback) {
    callLedAPI("getPitchInfo", {
        id: id
    }, "testLedAPI", callback);
}

function setPitch(id, pitch, callback) {
    callLedAPI("setPitch", {
        id: id,
        pitch: pitch
    }, "testLedAPI", callback);
}

function getFpgaVersion(callback) {
    callLedAPI("getFpgaVersion", {
    }, "testLedAPI", callback);
}

function getPsuVersion(id, callback) {
    callLedAPI("getPsuVersion", {
        id: id
    }, "testLedAPI", callback);
}

function getPsuList(callback) {
    callLedAPI("getPsuList", {
    }, "testLedAPI", callback);
}

function getPsuError(id, callback) {
    callLedAPI("getPsuError", {
        id: id
    }, "testLedAPI", callback);
}

function setPsuOutput(on, callback) {
    callLedAPI("setPsuOutput", {
        on: on
    }, "testLedAPI", callback);
}

function setSysCtrlPortPosition(id, pos, callback) {
    callLedAPI('setSysCtrlPortPosition', {
        id: id,
        pos: pos
    }, "testLedAPI", callback);
}

function setLedCtrlDaisyPosition(id, pos, callback) {
    callLedAPI('setLedCtrlDaisyPosition', {
        id: id,
        pos: pos
    }, "testLedAPI", callback);
}

function setLedCtrlPort(id, pos, callback) {
    callLedAPI('setLedCtrlPort', {
        id: id,
        pos: pos
    }, "testLedAPI", callback);
}

function setLdmPatternBlinking(id, callback) {
    callLedAPI('setLdmPatternBlinking', {
        id: id,
    }, "testLayoutAPI", callback);
}

function setLdmConnection(id, pos, callback) {
    callLedAPI('setLdmConnection', {
        id: id,
        pos: pos
    }, "testLedAPI", callback);
}

function saveLayout(id, callback) {
    callLedAPI('saveLayout', {
        id: id,
    }, "testLedAPI", callback);
}

function searchPsu(ipRange, callback) {
	callLedAPI("searchPsu", {
        ipRange: ipRange,
    }, "testPsuAPI", callback);
}

function savePsuIp(psuIpArray, callback) {
    callLedAPI("savePsuIp", {
        psuIpArray: psuIpArray,
    }, "testPsuAPI", callback);
}

function deletePsuList(callback) {
    callLedAPI("deletePsuList", {}, "testPsuAPI", callback);
}

function setPsuList(setPsuIpArray, callback) {
    callLedAPI("setPsuList", {
        setPsuIpArray: setPsuIpArray,
    }, "testPsuAPI", callback);
}

function getFileList(filePath, type, callback) {
    callLedAPI("getFileList", {
        filePath: filePath,
        type: type        
    }, 'testCalAPI', callback);
}

function removeFile(fileList, callback) {
    callLedAPI("removeFile", {
        fileList: fileList,
    }, 'testCalAPI', callback);
}

function getListDevice(type, callback) {
    callLedAPI("getListDevice", {
        type: type,        
    }, 'testLedAPI', callback);
}

function getFpgaUpdateProgress(callback) {
    callLedAPI('getFpgaUpdateProgress', {}, 'testLedAPI', callback)
}

function getGainLoadProgressSubscribe(callback) {
    callLedAPI('getGainLoadProgressSubscribe', {}, 'testLedAPI', callback)
}

function spiFpgaUpdate(filePath, callback) {
    callLedAPI('spiFpgaUpdateFactoryMenu', {
        filePath: filePath
    }, 'testLedAPI', callback)
}

function fpgaUpdateChk(callback) {
    callLedAPI('fpgaUpdateChk', {}, 'testLedAPI', callback);
}

//365 Care
function searchAccountName(accountNumber, callback) {
     callLedAPI('searchAccountName', {
        accountNumber: accountNumber,
    }, "signage365Care", callback);
}

function care365Enable(callback) {
    callLedAPI('care365Enable', {}, "signage365Care", callback)
}

function care365Disable(callback) {
    callLedAPI('care365Disable', {}, "signage365Care", callback);
}

function  appDownloadURL(serviceMode, callback) {
    callLedAPI('appDownloadURL', {
        serviceMode: serviceMode
    }, "signage365Care", callback);
}

function getServerStatusCare365(callback) {
    callLedAPI('getServerStatusCare365', {}, "signage365Care", callback);
}

function getServerStatusCare365Subscribe(callback) {
    callLedAPI('getServerStatusCare365Subscribe', {}, "signage365Care", callback);
}

function remove365CareApp(callback) {
    callLedAPI('remove365CareApp', {}, "signage365Care", callback);
}

function getListApps(callback) {
    callLedAPI("getListApps", {}, 'getListAppsInfo', callback);
}

function installIpkDownload(downloadUrl, callback) {
    callLedAPI('installIpkDownload', {
        downloadUrl: downloadUrl
    }, 'signage365Care', callback);
}

function install365CareApp(signature, destFile, callback) {
    callLedAPI('install365CareApp', {
        signature: signature,
        destFile: destFile
    }, 'signage365Care', callback);
}

function getUseNetworkTime(callback) {
    callLedAPI("getUseNetworkTime", {}, "signage365Care", callback);
}

function setAutomaticallyTime(callback) {
    callLedAPI("setAutomaticallyTime", {}, "signage365Care", callback);
}

function get365CareServiceMode(callback) {
    callLedAPI("get365CareServiceMode", {}, "signage365Care", callback);
}

//Masking
function isFirstBoot(callback) {
    callLedAPI("isFirstBoot", {}, "signage365Care", callback);
}

function setMaskingJIG(id, callback) {
    callLedAPI('setMaskingJIG', {
        id: id
    }, 'testCalAPI', callback);
}

function setMaskingJIGforUSB(filePath, id, callback) {
    callLedAPI('setMaskingJIGforUSB', {
        id: id,
        filePath: filePath
    }, 'testCalAPI', callback);
}

function setLdmRgbInfoAll(r, g, b, callback) {
     callLedAPI('setLdmRgbInfoAll', {
        r: r,
        g: g,
        b: b
    }, 'testCalAPI', callback);
}

function getGainLoadProgress(id, callback) {
    callLedAPI('getGainLoadProgress', {
        id: id
    }, 'testledAPI', callback);
}

function getGainMaskDone(callback) {
    callLedAPI('getGainMaskDone', {}, 'testCalAPI', callback);
}

function getMBI5153UserMode(callback) {
    callLedAPI('getMBI5153UserMode', {}, 'testMbiAPI', callback);
}

function setMBI5153UserMode(on, callback) {
    callLedAPI('setMBI5153UserMode', {
        on: on
    }, 'testMbiAPI', callback);
}

function getMBI5153RGB(color, callback) {
    callLedAPI('getMBI5153RGB', {
        color: color
    }, 'testMbiAPI', callback);
}

function setMBI5153RGB(color, data, callback) {
    callLedAPI('setMBI5153RGB', {
        color: color,
        data: data
    }, 'testMbiAPI', callback);
}

function getSUM2035UserMode(callback) {
    callLedAPI('getSUM2035UserMode', {}, 'testSumAPI', callback);
}

function setSUM2035UserMode(on, callback) {
    callLedAPI('setSUM2035UserMode', {
        on: on
    }, 'testSumAPI', callback);
}

function getSUM2035RGB(color, callback) {
    callLedAPI('getSUM2035RGB', {
        color: color
    }, 'testSumAPI', callback);
}

function setSUM2035RGB(color, data, callback) {
    callLedAPI('setSUM2035RGB', {
        color: color,
        data: data
    }, 'testSumAPI', callback);
}

//Cinema
function getImbStatus(index, callback) {
    callLedAPI('getImbStatus', {
        index: index,
    }, 'imb', callback)
}

function setImbLogin(id, password, callback) {
    callLedAPI('setImbLogin', {
        id: id,
        password: password
    }, 'imb', callback)
}

function setImbMarriage(callback) {
     callLedAPI('setImbMarriage', {
    }, 'imb', callback)
}

function setImbLogout(callback) {
    callLedAPI('setImbLogout', {
    }, 'imb', callback)
}

function setSecureSiliconTimeSerial(callback) {
    callLedAPI('setSecureSiliconTimeSerial', {
    }, 'imb', callback)
}

function getSecureSiliconStatus(callback) {
    callLedAPI('getSecureSiliconStatus', {
    }, 'imb', callback)
}

function getSecureSiliconCert(callback) {
    callLedAPI('getSecureSiliconCert', {
    }, 'imb', callback)
}

function getFpgaStatus(callback) {
    callLedAPI('getFpgaStatus', {
    }, 'imb', callback)
}

function getCpuStatus(callback) {
    callLedAPI('getCpuStatus', {
    }, 'imb', callback)
}

function setGpio(pin, level, callback) {
	console.log(">>>>> setGpio > client-led");
    callLedAPI('setGpio', {
        pin: pin,
        level: level
    }, 'imb', callback)
}

function getButtonStatus(callback) {
    callLedAPI('getButtonStatus', {
    }, 'imb', callback)
}

function getInitialTimeSetChk(callback) {
    callLedAPI('getInitialTimeSetChk', {
    }, 'imb', callback)
}

function isManualTimeSet(callback) {
    callLedAPI('isManualTimeSet', {
    }, 'imb', callback)
}

function setManualTimeSet(callback) {
    callLedAPI('setManualTimeSet', {
    }, 'imb', callback)
}

function copyFile(inputPath, outputPath, callback) {
    callLedAPI('copyFile', {
        inputPath: inputPath,
        outputPath: outputPath
    }, 'testledapi', callback)
}

function setLdmRgbInfo(id, r, g, b, callback) {
    callLedAPI('setLdmRgbInfo', {
        id: id,
        r: r,
        g: g,
        b: b
    }, "testLayoutAPI", callback);
}

function setLdmEdgeCalInfo(id, color, edgeVal, callback) {
    callLedAPI('setLdmEdgeCalInfo', {
        id: id,
        color: color,
        edgeVal: edgeVal
    }, "testLayoutAPI", callback);
}

function getLdmRgbInfo(id, callback) {
    callLedAPI('getLdmRgbInfo', {
        id: id
    }, "testLayoutAPI", callback);
}

function getLdmEdgeCalInfo(id, callback) {
    callLedAPI('getLdmEdgeCalInfo', {
        id: id
    }, "testLayoutAPI", callback);
}

function getDbcTable(callback) {
    callLedAPI('getDbcTable', {
    }, 'testDbcAPI', callback);
}

function setDbcTable(dbcVal, callback) {
    callLedAPI('setDbcTable', {
        dbcVal: dbcVal
    }, 'testDbcAPI', callback);
}

function getMaxThermalVal(callback) {
     callLedAPI('getMaxThermalVal', {
    }, 'testDbcAPI', callback);
}

function getFinalPmVal(callback) {
     callLedAPI('getFinalPmVal', {
    }, 'testDbcAPI', callback);
}

function startMarriageTimer(on, callback) {
	callLedAPI('startMarriageTimer', {
		on: on
	}, 'imb', callback);
}
function registerLogHandler(callback) {
    callLedAPI('registerLogHandler', {}, 'log', callback);
}

function unregisterLogHandler(callback) {
    callLedAPI('unregisterLogHandler', {}, 'log', callback);
}

function switchLoggingMode(on, callback) {
    callLedAPI('switchLoggingMode', {
        on: on
    }, callback);
}

function isDbcOff(callback) {
    callLedAPI('isDbcOff', {
    }, 'testLedAPI', callback);
}

function checkAccountFunc(currentPin, category, on, callback) {
    callLedAPI('checkAccountFunc', {
        currentPin: currentPin,
        category: category,
        on: on
    }, 'testLedAPI', callback);
}

function gainFormatChk(gainFile, callback) {
    callLedAPI('gainFormatChk', {
        gainFile: gainFile
    }, 'testCalAPI', callback);
}

function getLowLatencyMode(callback) {
    callLedAPI('getLowLatencyMode', {
    }, 'testLedAPI', callback);
}

function setLowLatencyMode(lowLatencyMode, callback) {
	callLedAPI('setLowLatencyMode', {
		lowLatencyMode: lowLatencyMode
	}, 'testLedAPI', callback);
}

function setPixelGain(id, filePath, callback) {
    callLedAPI('setPixelGain', {
        id: id,
        filePath: filePath
    }, 'testCalAPI', callback);
}
