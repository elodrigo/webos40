var testSuiteParams = {}

function showError(msg) {
	var li = "<li class='list-group-item list-group-item-danger'><font color=red>" + msg + "</font></li>";
	$('#direction').append(li);
}

function showMessage(msg) {
	msg = msg.replace(/\n/gm, '<br>').replace(/NG/gm, '<font color=red>NG</font>');
	var li = "<li class='list-group-item'>" + msg + "</li>";
	$('#direction').append(li);
}

function showDirection(id, msg, callback) {
	console.log(id);
	addConfirmModal(id, '수행사항', msg, callback);
	$('#' + id).modal();
}

function unhighlightDirection() {
	$('.direction.list-group-item-info').hide();
}

describe('I2C', function () {
	this.timeout(60 * 1000);
	it('DIL function test', function (done) {
		testsuiteForDIL(function (msg) {
			showMessage(msg.reason);
			console.log("msg.reason::" + msg.reason);
			try {
				expect(msg.returnValue).be(true);
			} catch (e) {
				console.log("I2C::catch");
				showError("DIL test 중 하나 이상이 실패하였습니다.");
				throw e;
			}
			done();
		});
	});
});

describe('eyeQ Sensor', function () {
	console.log("eyeQ Sensor Start::1");
	this.timeout(60 * 1000);
	it('eyeQ Sensor test', function (done) {
		console.log("eyeQ Sensor Start::2");
		testsuiteForEyeQ(function (msg) {

			console.log(msg);
			console.log("eyeQ Sensor::returnValue = " + msg.returnValue);
			console.log("eyeQ Sensor::eyecheckstatus = " + msg.eyecheckstatus);

			showMessage("eyeQ Sensor test = " + msg.eyecheckstatus);

			try {

				console.log("eyeQ Sensor::try");
				expect(msg.eyecheckstatus).be("true");

			} catch (e) {

				console.log("eyeQ Sensor::catch");
				showError("eyeQ Sensor test가 실패하였습니다.");
				throw e;

			}
			done();
		});
	});
});


describe('Input', function () {
	this.timeout(60 * 1000);

	before(function (done) {
		showDirection('input', 'HDMI 1 에 신호를 연결하세요', function () {
			setInputSouce('com.webos.app.hdmi1', function (msg) {
				console.log('change input to hdmi1:', msg);
			})
			setTimeout(done, 5 * 1000);
		});
	});

	describe('Signal Test', function () {
		it('HDMI_1 Signal Test', function (done) {
			isNoSignal(function (msg) {
				try {
					expect(msg.noSignal).not.be.ok();
				} catch (e) {
					showError("HDMI 1에 신호가 연결되어 있지 않습니다.");
					throw e;
				}
				done();
			});
		});
	});

	after(function () {
		unhighlightDirection();
	});
});

describe('Temperature', function () {
	describe('Temp Sensor Test', function () {
		it('sensor Test : 현재 온도값을 읽어온다.', function (done) {
			getTemp(function (res) {
				var types = res.group ? res.group : [];
				types.forEach(function (type) {
					var temp = res[type];
					var temperature = temp.celsius;
					showMessage(type + ' sensor ' + temperature + '°C');
					try {
						expect(res.returnValue).be(true);
					} catch (e) {
						showError(type + ' 온도 센서를 읽는 데 실패하였습니다.');
						throw e;
					}
					done();
				});
			});
		});
	});
});

describe('Checkscreen', function () {
	this.timeout(2 * 60 * 1000);

	describe('get Sensor Info', function () {
		it('Sensor 상태확인', function (done) {
			getCheckScreenInfo(function (msg) {
				showMessage('Checkscreen Sensor 상태 :' + msg.isSensorOk);
				try {
					expect(msg.isSensorOk).be(true);
				} catch (e) {
					showError("Checkscreen sensor의 상태가 비정상입니다.");
					throw e;
				}
				done();
			});
		});
	});

	describe('do Calibration', function () {
		it('Calibration 실행', function (done) {

			calibrateCheckScreenEYEKey(function (msg) {
				showMessage('Calibration 결과 :' + msg.returnValue);
				try {
					expect(msg.returnValue).be(true);
				} catch (e) {
					showError("Calibration에 실패했습니다.");
					throw e;
				}
				done();
			});
		});
	});
});

describe('Fan', function () {
	this.timeout(60 * 1000);

	var FAN_TIMEOUT = 15 * 1000;
	var FAN_IDLE_DUTY = 50;

	var fanID = [];

	function requestFanDuty(id, duty) {
		if (env.fanControl.numOfFanIndex === 2 && (id === 'openLoop' || id === 'closedLoop')) {
			setFanDuty(id, duty, 0);
			setFanDuty(id, duty, 1);
		} else {
			setFanDuty(id, duty, 0);
		}
	}

	function testRpmFan(target, rpm) {
		var min = 0.8;
		var max = 1.2;
		try {
			expect(rpm).within(min * target, max * target);
		} catch (e) {
			throw e;
		}
	}

	function allFanTest(duty, index, done) {
		var setDutyInterval = setInterval(function () {
			fanID.forEach(function (id) {
				requestFanDuty(id, duty);
			});
		}, 1000);

		var timeOut = FAN_TIMEOUT;
		if (duty === 0) {
			timeOut = FAN_TIMEOUT + 5 * 1000;
		}

		setTimeout(function () {
			clearInterval(setDutyInterval);

			getFanStatus(function (res) {
				var errors = 0;
				var types = res.group;
				types.forEach(function (type) {
					var target = testSuiteParams.rpm[type][index];
					var fan = res[type];
					fan.rpm.forEach(function (rpm, index) {
						try {
							testRpmFan(target, rpm);
						} catch (e) {
							showError(type + "[" + (index + 1) + "] fan의 RPM(" + rpm + ")이 " + target +
								" 의 +-20%에 속하지 않습니다. (duty=" + duty + ")");
							++errors;
						}
					});
				});
				expect(errors).to.be(0);
				done();
			});
		}, timeOut);
	}

	before('Stopping auto fan control', function (done) {
		getFanStatus(function (res) {
			fanID = res.group;
			stopFanControl();
			setTimeout(done, FAN_TIMEOUT);
		});
	});

	beforeEach('Set the duty of all fans ' + FAN_IDLE_DUTY, function (done) {
		stopFanControl();

		var setDutyInterval = setInterval(function () {
			fanID.forEach(function (id) {
				requestFanDuty(id, FAN_IDLE_DUTY);
			});
		}, 1000);

		setTimeout(function () {
			clearInterval(setDutyInterval);
			done();
		}, FAN_TIMEOUT);
	});

	describe('Duty - RPM test', function () {
		var duty = [0, 127, 255];

		duty.forEach(function (curDuty, index) {
			it('All Fans : 모든 fan의 duty를 ' + curDuty + '로 설정하면 해당 RPM으로 동작한다.',
				function (done) {
					allFanTest(curDuty, index, done);
				});
		});
	});

	after('Restart auto fan control', function (done) {
		restartFanControl();
		setTimeout(done, FAN_TIMEOUT);
	});
});

describe('Downtime incidents', function () {
	it('Downtime Incident의 "NG" 항목이 없어야 한다.', function (done) {
		getDowntimeIncident(function (msg) {
			var ngCount = 0;

			msg.incident.forEach(function (event) {
				if (event.state == 'NG') {
					++ngCount;
				}
			});

			if (ngCount) {
				showMessage('Check "Downtime incidents" in Basic Status page');
			}

			expect(ngCount).be(0);
			done();
		});
	});
});

if (env.supportDoor) {
	describe('Door', function () {
		this.timeout(60 * 60 * 1000);

		afterEach(function () {
			unhighlightDirection();
		});

		describe('Door Sensor Test', function () {
			it('문을 열면 door status open으로 변경되어야 한다.', function (done) {
				showDirection('door1', '문을 여세요 (Open the back cover)', function () {
					getDoorStatus(function (msg) {
						msg.status.forEach(function (status) {
							try {
								expect(status).be(true);
							} catch (e) {
								showError("문이 열려있지 않습니다.");
								throw e;
							}
						});
						socket.removeAllListeners('door');
						done();
					});
				});
			});

			it('문을 닫으면 door status close로 변경되어야 한다.', function (done) {
				showDirection('door2', '문을 닫으세요 (Close the back cover)', function () {
					getDoorStatus(function (msg) {
						msg.status.forEach(function (status) {
							try {
								expect(status).be(false);
							} catch (e) {
								showError("문이 닫혀있지 않습니다.");
								throw e;
							}
						});
						socket.removeAllListeners('door');
						done();
					});
				});
			});
		});
	});
}

after(function () {
	onTestEnd();
});

afterEach(function () {
	window.scrollTo(0, document.body.scrollHeight);
});
