var path = location.protocol + '//' + location.host + location.pathname;

function drawFilterSelect(curFile) {
    var dropDown = createDropDown('filter', '-', function (selected) {
        var newFilter = selected;
        var newPath = path + "?filter=" + newFilter + "&file=" + curFile;
        window.location.replace(newPath);
    });

    addDropDownOption(dropDown, " ", getLanguageText(languageTable, "All"));

    for (var i = 0; i < env.logList.length; i++) {
        addDropDownOption(dropDown, env.logList[i].toUpperCase(), getLanguageText(languageTable, toCapitalize(env.logList[i])))
    }

    $('#filter').append(dropDown);
}

function initLog(curFile, filter) {
    $("#fileName").text($("#fileName").text().replace(".txt", ".csv"));
    drawFilterSelect(curFile);

    if (filter == '') {
        filter = ' ';
    }
    setDropButtonText('filter', filter);

    $('#delete').click(function () {
        if (!confirm(getLanguageText(languageTable, "Are you sure to delete ALL log file?"))) {
            return;
        }
        var newPath = path + "?delete=true&file=" + curFile;
        window.location.replace(newPath);
    });

    $('#pageTitle').html(getLanguageText(languageTable, "Log"));
    $('#log').append(convertCsvToTable(logJson.log));

    if (locale == "ar-SA") {
        changeRtl();
    }

    if (!curFile) {
        $('#downloadLog').removeAttr('href');
    }
}

function changeRtl() {
    // $("#log table").attr("dir", "ltr");
    $("table").attr("dir", "rtl");
    $(".table>thead>tr>th").css("text-align", "right");
    $(".col-md-9").css("float", "right");
    $(".col-md-3").css("float", "right");
}
