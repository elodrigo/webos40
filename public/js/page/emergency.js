var emergencyTemp = [];

function checkRange() {
	console.log("checking");
	checkRangeUI('powerOff', 72, 150);
	checkRangeUI('backOff', 71, 100);
	checkRangeUI('backOn', 61, 90);
}

function init() {

	$('#powerOff').change(function () {
		var backVal = Number($('#backOff').val());
		var powerVal = Number($('#powerOff').val());

		$('#powerOffVal').html(powerVal);

		if (backVal >= powerVal) {
			$('#backOff').val(powerVal - 1);
			$('#backOff').change();
		}
	}).on('input', function () {
		var powerVal = Number($('#powerOff').val());
		$('#powerOffVal').html(powerVal);
	});

	$('#backOff').change(function () {
		var backVal = Number($('#backOff').val());
		var powerVal = Number($('#powerOff').val());

		$('#backOffVal').html(backVal);

		if (backVal >= powerVal) {
			$('#powerOff').val(backVal + 1);
			$('#powerOffVal').html(backVal + 1);
			$('#powerOff').change();
		}

		var onVal = Number($('#backOn').val());
		if (onVal > backVal - 10) {
			$('#backOn').val(backVal - 10);
			$('#backOn').change();
		}
	}).on('input', function () {
		var backVal = Number($('#backOff').val());
		$('#backOffVal').html(backVal);
	})

	$('#backOn').change(function () {
		var offVal = Number($('#backOff').val());
		var onVal = Number($('#backOn').val());

		$('#backOnVal').html($('#backOn').val());

		if (onVal + 10 > offVal) {
			$('#backOff').val(onVal + 10);
			$('#backOff').change();
		}
	}).on('input', function () {
		$('#backOnVal').html($('#backOn').val());
	});

	$('#lcmOffset').change(function () {

	}).on('input', function () {
		$('#lcmOffsetVal').html($('#lcmOffset').val());
	});

	$('#panelOffset').change(function () {

	}).on('input', function () {
		$('#panelOffsetVal').html($('#panelOffset').val());
	});

	$('.custom').change(function (e) {
		checkRange();
	}).on('input', function (e) {
		checkRange();
	});

	function getValue() {
		getEmergency(function (msg) {
			console.log(msg);
			$('#powerOff').val(msg.temperaturePowerOff);
			$('#backOff').val(msg.temperatureBacklightOff);
			$('#backOn').val(msg.temperatureBacklightOn);
			$('#lcmOffset').val(msg.lcmOffset);
			$('#panelOffset').val(msg.panelOffset);

			$('#powerOffVal').html($('#powerOff').val());
			$('#backOffVal').html($('#backOff').val());
			$('#backOnVal').html($('#backOn').val());
			$('#lcmOffsetVal').html($('#lcmOffset').val());
			$('#panelOffsetVal').html($('#panelOffset').val());

			checkRange();
		});
	}

	function resetConfig() {
		var turnOn, turnOff, powerOff;
		emergencyTemp.forEach(function (emergencyTemp) {

			if (emergencyTemp.event == "turnOn")
				turnOn = emergencyTemp.temperature;
			else if (emergencyTemp.event == "turnOff")
				turnOff = emergencyTemp.temperature;
			else
				powerOff = emergencyTemp.temperature;
		});

		setEmergency($('#lcmOffset').val(), $('#panelOffset').val(), turnOn, turnOff, powerOff);
		getValue();
	}

	getValue();

	$("#apply").click(function () {
		setEmergency($('#lcmOffset').val(), $('#panelOffset').val(),
			$('#backOn').val(), $('#backOff').val(), $('#powerOff').val());
	});

	$("#revert").click(function () {
		getValue();
	});

	$("#reset").click(function () {
		resetConfig();
	});

	$('#pageTitle').html('Emergency Control');
}
