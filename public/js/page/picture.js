var currentValue = {};
var backlightValues = [5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100];
var backlightStrings = ['5%', '10%', '15%', '20%', '25%', '30%', '35%', '40%', '45%',
	'50%', '55%', '60%', '65%', '70%', '75%', '80%', '85%', '90%', '95%', '100%'
];

var pictureModeNameMap = {
	sports: getLanguageText(languageTable, 'Transportation'),
	game: getLanguageText(languageTable, 'Education'),
	vivid: getLanguageText(languageTable, 'Mall/QSR'),
	cinema: getLanguageText(languageTable, 'Gov./Corp.'),
	govCorp: getLanguageText(languageTable, 'Gov./Corp.'),
	normal: getLanguageText(languageTable, 'General'),
	eco: getLanguageText(languageTable, 'APS'),
	photo: getLanguageText(languageTable, 'Photo'),
	expert1: getLanguageText(languageTable, 'Expert'),
	expert2: getLanguageText(languageTable, 'Calibration'),
	dicom: getLanguageText(languageTable, 'DICOM'),
	hdrVivid: getLanguageText(languageTable, 'HDR Vivid'),
	hdrBright: getLanguageText(languageTable, 'HDR Bright'),
	hdrStandard: getLanguageText(languageTable, 'HDR Standard'), // i18n It will display picture mode value list
	hdrEffect: getLanguageText(languageTable, 'HDR Effect'),
	hdrExternal: getLanguageText(languageTable, 'HDR Standard'), //it is amazon only spec
	dolbyHdrDark: getLanguageText(languageTable, 'Movie Dark'),
	dolbyHdrBright: getLanguageText(languageTable, 'Movie Bright'),
	dolbyHdrVivid: getLanguageText(languageTable, 'Vivid'),
	dolbyHdrDarkAmazon: getLanguageText(languageTable, 'Movie Dark') //it is amazon only spec
}

function setPortVal(msg) {
	if (!msg) {
		return;
	}

	currentValue = msg;

	setDropButtonText('portrait', msg.osdPortraitMode);
	setDropButtonText('rotation', msg.contentRotation);
	setDropButtonText('aspectRatio', msg.rotationAspectRatio);

	setPortraitStatus(msg.osdPortraitMode);
	setRotationStatus(msg.contentRotation);

	disableDropButton('aspectRatio', msg.deactiveMenus.indexOf('rotationAspectRatio') >= 0);
}

function getPictureVal() {
	getPictureMode(function (msg) {
		var modified = msg.pictureSettingModified;
		var mode = msg.pictureMode;

		setDropButtonText('pictureModeSelect', mode);

		$('#modified').html(mode !== 'expert1' && modified[mode] ? '(' + getLanguageText(languageTable, 'User') + ')' : '');
	});
}

function pictureCallback() {
	getPictureDBVal(['energySaving'], function (msg) {
		var value = msg.energySaving;
		setVisibleMinMaxTable(value);
		getPowerState(function (msg) {
			if (msg.state == 'Screen Off') {
				value = 'screen_off';
			}
			setDropButtonText('energySavingSelect', value);
		});
	});

	getPictureVal();
}

function offRotationImg() {
	$('#rotation_off').attr('src', '../images/rotation_off_n.png');
	$('#rotation_90').attr('src', '../images/rotation_90_n.png');
	$('#rotation_180').attr('src', '../images/rotation_180_n.png');
	$('#rotation_270').attr('src', '../images/rotation_270_n.png');
}

function setPortraitMode() {
	setPortrait(currentValue);
}

function setPortraitStatus(val) {
	var imgID = 'rotation_' + val;
	offRotationImg();
	$('#' + imgID).attr('src', '../images/' + imgID + '_p.png');
}

function setRotationStatus(degree) {
	$('#rotate_img').removeClass(function () {
		return $('#rotate_img').attr("class");
	});
	$('#rotate_img').addClass('img_rotate_' + degree);
}

function drawPictureModeSelect() {
	var dropDown = createDropDown('pictureModeSelect', '-', function (selected) {
		setPictureMode(selected);
		pictureCallback();
	});

	for (var i = 0; i < env.pictureModeList.length; i++) {
		addDropDownOption(dropDown, env.pictureModeList[i], pictureModeNameMap[env.pictureModeList[i]]);
	}

	$('#pictureModeSelect').append(dropDown);
}

function drawEnergySavingSelect() {
	var dropDown = createDropDown('energySavingSelect', '-', function (selected) {
		setPictureDBVal({
			energySavingModified: true,
			energySaving: selected
		});
		if (selected != "screen_off") {
			turnOnScreen();
		}
		setVisibleMinMaxTable(selected);
		// device.js api
		getPowerState(getPowerStateResponse);
	});
	addDropDownOption(dropDown, 'auto', getLanguageText(languageTable, 'Auto'));
	addDropDownOption(dropDown, 'off', getLanguageText(languageTable, 'Off'));
	addDropDownOption(dropDown, 'min', getLanguageText(languageTable, 'Minimum'));
	addDropDownOption(dropDown, 'med', getLanguageText(languageTable, 'Medium'));
	addDropDownOption(dropDown, 'max', getLanguageText(languageTable, 'Maximum'));
	addDropDownOption(dropDown, 'screen_off', getLanguageText(languageTable, 'Screen Off'));

	$('#energySavingSelect').append(dropDown);
}

function drawPortraitSelect() {
	var dropDown = createDropDown('portrait', '-', function (selected) {
		setPortraitStatus(selected);
		currentValue.osdPortraitMode = selected;
		setPortraitMode();
	});
	addDropDownOption(dropDown, 'off', getLanguageText(languageTable, 'Off'));
	addDropDownOption(dropDown, '90', '90°');
	addDropDownOption(dropDown, '180', '180°');
	addDropDownOption(dropDown, '270', '270°');

	$('#portrait').append(dropDown);
}

function drawRotationSelect() {
	var dropDown = createDropDown('rotation', '-', function (selected) {
		setRotationStatus(selected);
		currentValue.contentRotation = selected;
		setPortraitMode();
		disableDropButton('aspectRatio', (selected == 'off') || (selected == '180'));
	});

	addDropDownOption(dropDown, 'off', getLanguageText(languageTable, 'Off'));
	addDropDownOption(dropDown, '90', '90°');
	addDropDownOption(dropDown, '180', '180°');
	addDropDownOption(dropDown, '270', '270°');

	$('#rotation').append(dropDown);
}

function drawAspectRatioSelect() {
	var dropDown = createDropDown('aspectRatio', '-', function (selected) {
		currentValue.rotationAspectRatio = selected;
		setPortraitMode();
	});

	addDropDownOption(dropDown, 'original', getLanguageText(languageTable, 'Original'));
	addDropDownOption(dropDown, 'full', getLanguageText(languageTable, 'Full'));

	$('#aspectRatio').append(dropDown);
}

function drawMinBacklightSelect(maxBacklight, minBacklight) {
	$('#MinBacklightSelect').empty();
	var dropDown = createDropDown('MinBacklightSelect', '-', function (selected) {
		setMinMaxBacklight('minBacklight', selected);
	});

	backlightValues.forEach(function (currentValue, index, arry) {
		if (currentValue < maxBacklight) {
			addDropDownOption(dropDown, currentValue, backlightStrings[index]);
		}
	});

	$('#MinBacklightSelect').append(dropDown);
}

function drawMaxBacklightSelect(maxBacklight, minBacklight) {
	$('#MaxBacklightSelect').empty();
	var dropDown = createDropDown('MaxBacklightSelect', '-', function (selected) {
		setMinMaxBacklight('maxBacklight', selected);
	});

	backlightValues.forEach(function (currentValue, index, arry) {
		if (currentValue > minBacklight) {
			addDropDownOption(dropDown, currentValue, backlightStrings[index]);
		}
	});

	$('#MaxBacklightSelect').append(dropDown);
}

function setVisibleMinMaxTable(energySaving) {
	$('#min-max-table').css('display', (energySaving == 'auto') ? 'table' : 'none');
}

function minMaxCallback() {
	getBacklight(function (msg) {
		var backlight = msg.settings;
		drawMaxBacklightSelect(backlight.maxBacklight, backlight.minBacklight);
		drawMinBacklightSelect(backlight.maxBacklight, backlight.minBacklight);

		setDropButtonText('MaxBacklightSelect', backlight.maxBacklight);
		setDropButtonText('MinBacklightSelect', backlight.minBacklight);
	});
}

function setMinMaxBacklight(id, val) {
	setBacklight(id, val, function () {
		minMaxCallback();
	});
}

function drawMecSelect() {
	var dropDown = createDropDown('mecSelect', '-', function (selected) {
		setPictureDBVal({
			motionEyeCare: selected
		});
	});

	addDropDownOption(dropDown, 'off', 'Off');
	addDropDownOption(dropDown, 'low', 'Low');
	addDropDownOption(dropDown, 'high', 'High');

	$('#mecSelect').append(dropDown);
}


function initPicture() {
	drawPictureModeSelect();
	drawEnergySavingSelect();
	drawPortraitSelect();
	drawRotationSelect();
	drawAspectRatioSelect();

	minMaxCallback();

	getPictureVal();
	pictureCallback();
	getPortrait(setPortVal);

	getForegroundAppInfo(function (msg) {
		if ((msg.appId.indexOf("dsmp") > 0) || (msg.appId.indexOf("browser") > 0)) {
			disableDropButton('portrait', true);
			disableDropButton('rotation', true);
			disableDropButton('aspectRatio', true);
		}
	});

	getRecentsAppInfo(function (msg) {
		var recentApp = msg.recentsAppList;
		recentApp.forEach(function (appinfo) {
			if (appinfo.indexOf("browser") > 0) {
				disableDropButton('portrait', true);
			}
		});
	});
}
