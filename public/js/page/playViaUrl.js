function init() {
    getPlayViaUrl(function (msg) {
        $('#loader').prop('checked', msg.playViaUrlMode == 'on');
        $('#url').val(msg.playViaUrl);
        onChangedLoader();
    });

    $("#apply").click(function () {
        var playViaUrlMode = $('#loader').prop('checked') ? "on" : "off";
        var url = $('#url').val();
        if (checkInvalidUrl(url)) {
            if (playViaUrlMode === 'on') {
                alert(getLanguageText(languageTable, "The URL format is invalid."));
                return;
            } else {
                getPlayViaUrl(function (msg) {
                    $('#url').val(msg.playViaUrl);
                    setPlayViaUrl(playViaUrlMode, msg.playViaUrl);
                });
                return;
            }
        }

        setPlayViaUrl(playViaUrlMode, url);
    });

    $("#preview").click(function () {
        var url = $('#url').val();
        if (checkInvalidUrl(url)) {
            alert(getLanguageText(languageTable, "The URL format is invalid."));
            return;
        }

        var protocols = ['http:', 'https:', 'ftp:'];
        for (var i = 0; i < protocols.length; i++) {
            var machingResult = false;
            if (url.toLowerCase().indexOf(protocols[i]) > -1) {
                machingResult = true;
                break;
            }

            if (i === protocols.length -1 && machingResult === false) {
                url = "http://" + url;
            }
        }
        console.log("url : " + url);
        window.open(url, '_blank');
    });

    $('#loader').change(function (e) {
        onChangedLoader();
        e.preventDefault();
    });
}

function onChangedLoader() {
    var isLoaderOn = $('#loader').prop('checked');
    if (isLoaderOn) {
        $('#url').prop('disabled', false);
        $("#preview").prop('disabled', false);
    } else {
        $('#url').prop('disabled', true);
        $("#preview").prop('disabled', true);
    }
}

function checkInvalidUrl(url) {
    var isInvalidUrl = false;
    if (url === undefined || url.length === 0 || url.trim().length === 0) {
        isInvalidUrl = true;
    }

    return isInvalidUrl;
}

$(document).ready(function () {
    init();
    $('#pageTitle').html(getLanguageText(languageTable, "Play via URL"));
});
