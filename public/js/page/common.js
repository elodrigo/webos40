var isPortrait = false;
var isContentRotated = false;
var isMobile = false;

var topNavbarWidth = 0;
var locale = "en-US";

// getIsMobile();
changeLanguage();

// $(document).ready(function () {
//     changeRtlOverall();
// });
//
// $(window).load(function () {
//     if (isMobile) {
//         topNavbarWidth = $(document).width();
//     } else {
//         topNavbarWidth = window.innerWidth;
//     }
//
//     adjustTitleTextArea();
//     setRollingText();
//     $("#navMenuButton").on("click", function () {
//         adjustTitleTextArea();
//         setRollingText();
//     });
//
//     $("#userMenu").on("click", function () {
//         setRollingText();
//     });
//
//     handleWindowSizeChanged();
// });

function overlayControlSC(on, msg, parentDiv) {
    if (parentDiv == undefined) {
        parentDiv = 'nav.navbar-overlay';
    }
    if (on === true) {
        var over = '<div id="overlay" class="update_overlay_sc"> '
            + '<div class="update_in_progress_sc">'
            + '<center>'
            + '<img src="../images/loading.gif">'
            + '<span id=overlay_progress></span><br>'
            + '<span>' + msg + '</span>'
            + '</center>'
            + '</div>'
            + '</div>';
        $(over).prependTo(parentDiv);
    } else {
        $('#overlay').remove();
    }
}

function overlayControl(on, msg, parentDiv) {
    if (parentDiv == undefined) {
        parentDiv = 'nav.navbar-overlay';
    }
    if (on === true) {
        var over = '<div id="overlay" class="update_overlay"> '
            + '<div class="update_in_progress">'
            + '<center>'
            + '<img src="../images/loading.gif"><br>'
            + '<span id=overlay_progress></span><br>'
            + '<span style="word-break: break-all;" wrap="hard;">' + msg + '</span>'
            + '</center>'
            + '</div>'
            + '</div>';
        $(over).prependTo(parentDiv);
    } else {
        $('#overlay').remove();
    }
}

function overlayControlLed(on, msg) {
    overlayControl(on, msg, '.main-body');
    changeRtlDir();
}

function showRotationWarning() {
    $("#screenshotStatus").html('');
    getPortrait(function (msg) {
        getOSDPortrait(function (msgOSD) {
            getAspectRatio(function (rotationAspectRatio) {
                isPortrait = msgOSD.screenRotation != "off";
                isContentRotated = msg.contentRotation != 'off';

                var text = "";
                if (isPortrait) {
                    console.log("osd portraited!");
                    text += getLanguageText(languageTable, "Screen Rotation is ON") + "(" + msgOSD.screenRotation + "°)<br>";
                }

                if (isContentRotated) {
                    text += getLanguageText(languageTable, "External Input Rotation is ON") +
                        "(" + getLanguageText(languageTable, msg.contentRotation) + "°, " +
                        getLanguageText(languageTable, toCapitalize(rotationAspectRatio)) + ")";
                }

                if (isPortrait || isContentRotated) {
                    $("#screenshotStatus").html(text);
                }
            });
        });
    });
}

function getCaretClass() {
    var caretClass = 'caret' + (isMobile ? ' mobile' : '');
    if (locale == "ar-SA") {
        caretClass = caretClass + " arabic";
    }
    return caretClass
}

function setDropButtonText(id, val) {
    var caretClass = getCaretClass();
    var li = $('#' + id).find('div ul li [data-value="' + val + '"]');
    var text = li.text();
    var button = $('#' + id).find('div button');
    button.html(text + '&nbsp;&nbsp;<span class="' + caretClass + '"></span>');
}

function setDropButtonTextForce(id, text) {
    var caretClass = getCaretClass();
    var button = $('#' + id).find('div button');
    button.html(text + '&nbsp;&nbsp;<span class="' + caretClass + '"></span>');
}

function disableDropButton(id, disabled) {
    var li = $('#' + id).find('div button').attr('disabled', disabled);
}

function createDropDown(id, tag, callback) {
    var caretClass = getCaretClass();
    var selectClass = 'dropdown-select';
    if (isMobile) {
        selectClass += ' mobile';
    }

    if (locale == "ar-SA") {
        selectClass = selectClass + " arabic";
    }

    var div = $('<div class="dropdown"/>');
    var button = $('<button class="btn btn-default dropdown-toggle ' + selectClass + '" type="button" id="' + id
        + 'Drop" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">'
        + tag + '<span class="' + caretClass + '"></span></button>');
    div.append(button);

    var menu = $('<ul class="dropdown-menu scrollable-menu ' + selectClass + '" aria-labelledby="' + id + '"/>');
    menu.on('click', 'li a', function () {
        if (!$(this).attr('data-value') || $(this).parent().hasClass('disabled')) {
            return;
        }
        setDropButtonText(id, $(this).attr('data-value'));
        callback($(this).attr("data-value"));
    });

    div.append(menu);

    if (locale == "ar-SA") {
        div.find(".dropdown-select").attr('dir', 'rtl').css('text-align', 'right');
    }

    return div;
}

function addDropDownOption(menu, value, text, disabled) {
    var m = menu.children('ul');
    var item = '';
    if (!disabled) {
        item = $('<li><a href="javascript:;" data-value="' + value + '">' + text + '</a></li>');
    } else {
        item = $('<li class=disabled><a href="javascript:;" data-value="' + value + '">' + text + '</a></li>');
    }

    m.append(item);
}

function getIsMobile() {
    isMobile = document.location.pathname.indexOf('mobile') > 0;
}

function addConfirmModal(id, title, content, callback, callbackCancel) {
    var html =
        '<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" id="' + id + '">' +
            '<div class="modal-dialog modal-sm">' +
                '<div class="modal-content">' +
                    '<div class="modal-header">' +
                        '<button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
                            '<span aria-hidden="true">&times;</span>' +
                        '</button>' +
                        '<h4 class="modal-title" id="gridSystemModalLabel">' + title + '</h4>' +
                    '</div>' +
                    '<div class="modal-body">' +
                        '<span id="' + id + 'Content">' + content + '</span>' +
                        '<br>' +
                        '<div align="right">' +
                            '<button class="btn btn-primary" data-dismiss="modal" id="' + id + 'ModalOK">' + getLanguageText(languageTable, "OK") + '</button>&nbsp;&nbsp;' +
                            '<button class="btn" data-dismiss="modal" id="' + id + 'ModalCancel">' + getLanguageText(languageTable, "Cancel") + '</button>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>';

    var modal = $(html);
    // $('body').append(modal);

    $('#' + id + 'ModalOK').click(callback);
    $('#' + id + 'ModalCancel').click(callbackCancel);
}

function addLedConfirmModal(id, title, content, callback, callbackCancel) {
    var html =
        '<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="' + id + '">' +
            '<div class="modal-dialog modal-lg">' +
                '<div class="modal-content">' +
                    '<div class="modal-header">' +
                        '<button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
                            '<span aria-hidden="true">&times;</span>' +
                        '</button>' +
                        '<h4 class="modal-title"><span id="gridSystemModalLabel">' + title + '</span></h4>' +
                    '</div>' +
                    '<div class="modal-body">' +
                        '<span id="' + id + 'Content">' + content + '</span>' +
                        '<br>' +
                        '<div align="right">' +
                            '<button class="btn btn-primary" data-dismiss="modal" id="' + id + 'ModalOK">' + getLanguageText(languageTable, "OK") + '</button>&nbsp;&nbsp;' +
                            '<button class="btn" data-dismiss="modal" id="' + id + 'ModalCancel">' + getLanguageText(languageTable, "Cancel") + '</button>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>';

    var modal = $(html);
    $('body').append(modal);

    $('#' + id + 'ModalOK').click(callback);
    $('#' + id + 'ModalCancel').click(callbackCancel);
}

function toCapitalize(text) {
    return text.replace(/\b[a-z]/g, function (f) {
        return f.toUpperCase();
    });
}

function checkByteLength(string) {
    var byteLength = 0;
    for (var i = 0; i < string.length; i++) {
        if (escape(string.charAt(i)).length >= 4) {
            byteLength += 3;
        } else if (escape(string.charAt(i)) == "%A7") {
            byteLength += 3;
        } else if (escape(string.charAt(i)) != "%0D") {
            byteLength++;
        }
    }

    return byteLength;
}

function isImageFile(fileName) {
    if (!fileName) {
        return false;
    }

    var imageFileExtList = ["png", "bmp", "jpg", 'jpeg', 'jpe'];
    var ext = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();
    if (imageFileExtList.indexOf(ext) === -1) {
        return false;
    } else {
        return true;
    }
}

function isVideoFile(fileName) {
    if (!fileName) {
        return false;
    }

    var videoFileExtList = [
        "mp4", "mpeg", "avi", "tp", "mkv", "mov", "mpg", "asf", "wmv", "m4v", "3gp", "3g2", "trp", "ts", "mts", "dat", "vob"];
    var ext = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();
    if (videoFileExtList.indexOf(ext) === -1) {
        return false;
    } else {
        return true;
    }
}

function getLanguageText(languageTable, text) {
    var text = text + "";

    for (var i = languageTable.length - 1; i >= 0; i--) {
        if (languageTable[i][text]) {
            text = languageTable[i][text];
        }
    }
    return text;
}

function capitalizeFirstLetter(str) {
    var str = str.substring(0, 1).toUpperCase() + str.substring(1, str.length).toLowerCase();
    return str;
}

function setRollingText() {
    setTimeout(function () {
        $(".txt_rolling span").each(function () {
            if ($(this).children("marquee").length > 0) {
                $(this).css("overflow", "hidden");
                var html = $(this).children("marquee").html();
                $(this).html(html);
            }
        });

        $(".txt_rolling").each(function () {
            $(this).children("span").unbind("mouseenter mouseleave");
            var width = $(this).width();
            var childWidth = $(this).children("span").width();
            if (childWidth > width) {
                $(this).children("span").hover(function () {
                    console.log("mouseover");
                    $(this).css("overflow", "visible");
                    var html = "<marquee>" + $(this).html() + "</marquee>";
                    $(this).html(html);
                }, function () {
                    console.log("mouseout");
                    $(this).css("overflow", "hidden");
                    var html = $(this).children("marquee").html();
                    $(this).html(html);
                })
            }
        });
    }, 100);
}

function adjustTitleTextArea() {
    var sideNavWidth = $(".side-menu-container").width();
    var logWidth = 250;
    var menuWidth = 60;

    if (locale === "ar-SA") {
        var width = topNavbarWidth - sideNavWidth - menuWidth - 50;
        $("#boxPageTitle").width(width);
        if (!isMobile) {
            if (topNavbarWidth > 1024) {
                var left = sideNavWidth + menuWidth;
                $(".navbar-right").css("position", "absolute").css("left", left + "px");
            } else {
                $(".navbar-right").css("position", "absolute").css("left", "initial");
            }

        }
        return;
    }

    if (topNavbarWidth > 1024) {
        var width = topNavbarWidth - logWidth - sideNavWidth - menuWidth - 50;
        $("#boxPageTitle").width(width);
    } else {
        var width = topNavbarWidth - sideNavWidth - menuWidth - 50;
        $("#boxPageTitle").width(width);
    }
}

function handleWindowSizeChanged() {
    if (isMobile) {
        var reArrange = function () {
            if ($(document).width() !== topNavbarWidth) {
                topNavbarWidth = $(document).width();
                adjustTitleTextArea();
                setRollingText();
            }
        };

        $(window).on("orientationchange", function () {
            var isIphone = navigator.userAgent.toLowerCase().indexOf('iphone') >= 0;
            if (isIphone) {
                setTimeout(function () {
                    reArrange();
                }, 80);

            } else {
                setTimeout(function () {
                    reArrange();
                }, 300);
            }
        });

        $(window).on("resize", function () {
            setTimeout(function () {
                reArrange();
            }, 100);
        });

        $(document).on("touchend", function () {
            setTimeout(function () {
                topNavbarWidth = $(document).width();
            }, 300);
        });
    } else {
        $(window).on("resize", function () {
            topNavbarWidth = window.innerWidth;
            adjustTitleTextArea();
            setRollingText();
        });
    }
}

function changeLanguage() {
    var localeFormatter = {
        'es': 'es-ES',
        'ko': 'ko-KR',
        'ar': 'ar-SA',
        'zh-HK' : 'zh-Hant-HK',
        'zh-TW' : 'zh-Hant-TW',
        'zh-CN' : 'zh-Hans-CN'
    };
    locale = navigator.language || navigator.browerLanguage;
    if (localeFormatter[locale]) {
        locale = localeFormatter[locale];
    }

    console.log("locale: " + locale);
    $.ajax({
        type: "post",
        data: {locale: locale},
        url: "/setLanguage",
        success: function (data) {
            console.log(data);
            if (data === "changed") {
                location.reload();
                return;
            }
        },
        error: function (req, status, err) {
            console.log("error");
        }
    });
}

function changeRtlOverall() {
    if (locale === "ar-SA") {
        // to arrange top menu size and location
        adjustTitleTextArea();

        // text common
        $(".rtl_language").each(function () {
            $(this).attr("dir", "rtl");
        });

        // common
        $(".panel-title").attr("dir", "rtl");
        $("table").attr("dir", "rtl");
        $(".table>thead>tr>th").css("text-align", "right");
        $("h4").attr("dir", "rtl");
        $(".list-group-item label").attr("dir", "rtl");
        $(".list-group-item").css("text-align", "right");
        $("input").attr("dir", "rtl");
        $(".dropdown-menu").attr("dir", "rtl").css("text-align", "right");

        // right menu
        $(".navbar-nav").attr("dir", "rtl").css("padding-right", "0px");
        $(".navbar-nav .dropdown").addClass("arabic");

        // top menu
        $(".breadcrumb").css({"text-align": "right"});
        $(".navbar-right-expand-toggle.pull-right").remove();
    }

    // to prevent flickering of the top menu
    $(".breadcrumb").show();
    $(".navbar-right").show();
}

function convertCsvToTable(data) {
    var allRows = data.split(/\r?\n|\r/);
    var table = "<table class='table'>";
    for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
        if (allRows[singleRow]) {
            if (singleRow === 0) {
                table += "<thead>";
                table += "<tr>";
            } else {
                table += "<tr>";
            }

            var rowCells = allRows[singleRow].split(',');
            for (var rowCell = 0; rowCell < rowCells.length; rowCell++) {
                if (singleRow === 0) {
                    table += "<th>";
                    table += rowCells[rowCell].replace(/^ */g, "");
                    table += "</th>";
                } else {
                    table += "<td>";
                    table += rowCells[rowCell].replace(/^ */g, "");
                    table += "</td>";
                }
            }
            if (singleRow === 0) {
                table += "</tr>";
                table += "</thead>";
                table += "<tbody>";
            } else {
                table += "</tr>";
            }
        }
    }
    table += "</tbody>";
    table += "</table>";
    return table;
}

function queryString () {
    var query_string = {};
    var query = window.location.search.substring(1);
    var vars = query.split("&");

    for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
        // If first entry with this name
        if (typeof query_string[pair[0]] === "undefined") {
            query_string[pair[0]] = decodeURIComponent(pair[1]);
            // If second entry with this name
        } else if (typeof query_string[pair[0]] === "string") {
            var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
            query_string[pair[0]] = arr;
            // If third or later entry with this name
        } else {
            query_string[pair[0]].push(decodeURIComponent(pair[1]));

        }
    }
    return query_string;
}

function getInternetExplorerVersion()
{
    var rv = -1;
    if (navigator.appName == 'Microsoft Internet Explorer')
    {
        var ua = navigator.userAgent;
        var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
        if (re.exec(ua) != null)
            rv = parseFloat( RegExp.$1 );
    }
    else if (navigator.appName == 'Netscape')
    {
        var ua = navigator.userAgent;
        var re  = new RegExp("Trident/.*rv:([0-9]{1,}[\.0-9]{0,})");
        if (re.exec(ua) != null)
            rv = parseFloat( RegExp.$1 );
    }
    return rv;
}

function filteringXSS(name) {
    // [LGPSVE-499]
    return name.replace(/\<|\>|\"|\'|\%|\;|\&|\+/g, "");
}