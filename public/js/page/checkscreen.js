var calibMsg = ["Default size", "Calibrating...", "Calibrated"];
var bgTable = ['#F00', '#0F0', '#00F'];
var calibrated = false;
var inited = false;
var disabled = true;
var prevDisabled = true;
var getColorInterval = undefined;

function showColor() {
    getCheckScreenColor(function (msg) {
        var readValue = msg.hexValue == '' ? getLanguageText(languageTable, 'Sensor Error') : msg.hexValue;
        var readColor = msg.readRGB < 3 ? bgTable[msg.readRGB] : '#000';
        $('#drawColor').css('background-color', bgTable[msg.drawRGB]);
        $('#sensorValue').html(readValue);
        $('#sensorColor').css('background-color', readColor);
        $('#valid').html(msg.colorValid ? getLanguageText(languageTable, 'OK') : getLanguageText(languageTable, 'NG'));
    });
}

function showCheckScreenInfo(msg) {
    if (!msg.returnValue) {
        console.log('Failed getCheckScreenInfo()');
        return;
    }

    var calibStatus = "";
    if (msg.calibStatus != undefined) {
        calibStatus = getLanguageText(languageTable, calibMsg[msg.calibStatus]);
        calibrated = msg.calibStatus == 2;
    }

    $("#calibrated").html(calibStatus);
    $("#width").html(msg.rgbRect.width);
    $("#height").html(msg.rgbRect.height);

    var position = ["Top Left", "Top Right", "Bottom Left", "Bottom Right"];
    $("#position").html(getLanguageText(languageTable, position[msg.position - 1]));

    $('#showColor').prop('disabled', disabled);
}

function disableControl(disabled) {
    $('#calibrate').prop('disabled', disabled);
    $('#position').prop('disabled', disabled);
    $('#positionChange').prop('disabled', disabled);
    if (disabled) {
        $('#showColor').attr('checked', false);
    }
    $('#showColor').prop('disabled', disabled || !calibrated);
}

function getCheckscreenData() {
    getCheckScreenOn(function (msg) {
        $('#isCheckScreenOn').attr('checked', msg != 'off');
        disabled = !msg || msg == 'off';
        disableControl(disabled);
        inited = true;
    });
    getCheckScreenInfo(showCheckScreenInfo);

    getCheckScreenColor(function (msg) {
        $('#valid').html(msg.colorValid ? getLanguageText(languageTable, "OK") : getLanguageText(languageTable, "NG"));
    });
}

function initCheckScreen() {
    getCheckscreenData();

    if (env.supportPsuStatus) {
        getPsuStatus();
    }

    if (env.supportPanelErrorOut) {
        getBluStatus();
    }

    $("#positionChange").click(function () {
        setCheckScreenPos($("#position").val());
        getCheckScreenInfo(showCheckScreenInfo);
        calibrated = false;
        $('#showColor').attr('checked', false);
        $('#showColor').prop('disabled', true);
    });

    $('#isCheckScreenOn').change(function () {
        if (!inited) {
            return;
        }
        var checked = $('#isCheckScreenOn').prop('checked');
        setCheckScreenOn(checked ? 'on' : 'off');

        disableControl(!checked);
        disabled = !checked;

        if (!checked) {
            if (getColorInterval) {
                clearInterval(getColorInterval);
            }
            $('#drawColor').css('background-color', '')
            $('#sensorValue').html('');
            $('#sensorColor').css('background-color', '')
            $('.showColorStatus').hide();
        }

        if (!disabled) {
            var i = 0;
            var calibTimer = setInterval(function () {
                getCheckscreenData();
                if (calibrated || i == 9) {
                    clearInterval(calibTimer);
                }
                ++i;
            }, 5000);
        }
    });

    $('#showColor').change(function () {
        var checked = $(this).prop('checked');
        if (checked) {
            getColorInterval = setInterval(showColor, 500);
            $('.showColorStatus').show();
        } else {
            if (getColorInterval) {
                clearInterval(getColorInterval);
            }
            $('#drawColor').css('background-color', '')
            $('#sensorValue').html('');
            $('#sensorColor').css('background-color', '')
            $('.showColorStatus').hide();
        }
    });

    $('.showColorStatus').hide();
}

function getPsuStatus() {
    getSDMInfo(function (msg) {
        var PSUStatus = msg.PSUStatus;
        for (var i = 0; i < PSUStatus.length; i++) {
            var checkResult = true;
            for (var variable in PSUStatus[i]) {
                if (!PSUStatus[i][variable]) {
                    $('#PSU' + (i + 1) + 'Status').html('NG');
                    $('#PSU' + (i + 1) + "Status").css('color', 'red');
                    checkResult = false;
                    break;
                }
            }
            if (!checkResult) {
                continue;
            }
            $('#PSU' + (i + 1) + 'Status').html('OK');
        }
    });
}

function getBluStatus() {
    getPanelErrorOut(function (msg) {
        var isNG = !msg.isPanelBLUOk;
        $('#BLUReturn').html(isNG ? getLanguageText(languageTable, "NG") : getLanguageText(languageTable, "OK"));
        if (isNG)
            $('#BLUReturn').css('color', 'red');
    });
}
