var env = {};

function init(_env) {
    env = _env;
    $('#pageTitle').html(getLanguageText(languageTable, 'Menu Language'));
    drawLanguageSelect();
}

function drawLanguageSelect() {
    $.getJSON('/resources/languageTable.json', function (table) {
        table.languages.forEach(function (lang) {
            var code = lang.languageCode;
            if (code == 'ar' && env.commerAddLanguageType != 'ar') {
                return;
            }

            var langItem =
                '<div class="col-lg-4 langItem">' +
                    '<input class=custom type=radio id=' + code + ' name=lang value="' + lang.languageCode + '">' +
                    '<label for=' + code + ' style="margin-top:10px; font-size: 15px; font-weight: normal;">' + lang.name + '</label>' +
                '</div>';

            $('#langTable').append(langItem);
        });

        $('[name=lang]').change(function () {
            setMenuLanguage($(this).val());
        });

        getMenuLanguage(function (ret) {
            var langParse = ret.split('-');
            var langCode = langParse[0];
            if (langCode == 'pt') {
                langCode = ret;
            }
            langCode += (langParse.length == 2) ? '' : '-' + langParse[1];

            $('#' + langCode).attr('checked', true);
        });
    });
}