var interval = undefined;

var height = 0;

function reloadCapture() {
	var h = Math.ceil($('#scr-wrap').height());

	if (height == 0) {
		height = isMobile ? 214 : 254;
	} else if (Math.abs(h - height) > 10) {
		height = h;
	}

	getCapture(height, function (m) {
		$("#scr").attr('src', m);
	});
}

function refresh() {
	var count = 0;
	if (interval) {
		clearInterval(interval)
	}
	$('#play').prop('disabled', true);
	interval = setInterval(function () {
		if (count % 10 == 0) {
			showRotationWarning();
		}
		reloadCapture();
		++count;
		if (count >= 20) {
			$('#play').prop('disabled', false);
			clearInterval(interval);
			count = 0;
		}
	}, 1000);
}


function showRemote(index) {
	var buttonID = "p_" + index;
	var direction = ["up", "right", "down", "left", "ok"];

	$("#" + buttonID).css("visibility", "visible");

	setTimeout(function () {
		$("#" + buttonID).css("visibility", "hidden");
	}, 100);

	sendRemocon(direction[index - 1]);
	refresh();
}


$(document).ready(function () {
	$("#screenshotStatus").css("background-color", "transparent");
	$("button.play").click(function () {
		if (interval) {
			clearInterval(interval);
		}
		refresh();
	});

	$("button.remo").click(function () {
		var command = $(this).val();
		getOSDLockMode(function (msg) {
			if (msg.settings.enableOsdVisibility == "off") {
				if ((command == "menu") || (command == "home") || (command == "inputKey")) {
					return;
				} else {
					sendRemocon(command);
					refresh();
				}
			} else {
				sendRemocon(command);
				refresh();
			}
		});
	});

	var captureSize = {
		"1": {
			class: "col-mo-4",
			height: 150
		},
		"2": {
			class: "col-mo-6",
			height: 254
		},
		"3": {
			class: "col-mo-12",
			height: 450
		}
	};
	$("button.size").click(function () {
		var size = $(this).val();
		$("#scr-wrap").height(captureSize[size].height);

		var pardiv = $('#scr-wrap').parent();
		pardiv.removeClass('col-mo-4 col-mo-6 col-mo-12');
		pardiv.addClass(captureSize[size].class);

		$('#scr').click();
	});

	$("#scr").click(function (e) {
		refresh();
	});

	var width = document.body.clientWidth;
	if (width <= 378) {
		$("#small").click();
	} else {
		$("#middle").click();
	}
	$("button.play").click();

	var keyCodeMap = {}
	keyCodeMap["8"] = "backspace";
	keyCodeMap["13"] = "enter";
	keyCodeMap["32"] = "space";
	keyCodeMap["190"] = ".";
	keyCodeMap["37"] = "left"
	keyCodeMap["38"] = "up"
	keyCodeMap["39"] = "right"
	keyCodeMap["40"] = "down"

	showRotationWarning();

	$("img, area, map").focus(function (e) {
		e.preventDefault();
	});

	if ((navigator.appName == 'Netscape' && navigator.userAgent.search('Trident') != -1) || (navigator.userAgent.toLowerCase().indexOf("msie") != -1)) {
		$('.4way-imagemap').attr('onfocus', "blur()");
	}

	getOSDLockMode(function (msg) {
		if (msg.settings.enableOsdVisibility == "off") {
			$("button[value*=menu]").css("visibility", "hidden");
			$("button[value*=home]").css("visibility", "hidden");
			$("button[value*=inputKey]").css("visibility", "hidden");
		}
	});

	$('#play').click(function () {
		refresh();
	});

	reloadCapture();
	refresh();
});
