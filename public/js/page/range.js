var isFF = true;
var agent = navigator.userAgent.toLowerCase();
var browserType = "";

if ( agent.indexOf("webkit") != -1 ) {
	browserType = "webkit";
} else if ( agent.indexOf("firefox") != -1 ) {
	browserType = "firefox";
} else if ( agent.indexOf("msie") != -1 ) {
	browserType = "msie";
}

var addRule = (function (style) {
	var sheet = document.head.appendChild(style).sheet;
	return function (selector, css) {
		if ( isFF ) {
			if ( sheet.cssRules.length > 0 ) {
				for (var i = 0; i < sheet.cssRules.length; i++) {
					if(sheet.cssRules[i].selectorText == selector)
						sheet.deleteRule( i );
		        }
			}

			try {
				sheet.insertRule(selector + "{" + css + "}", 0);
			} catch ( ex ) {
				isFF = false;
			}
		}
	};
})(document.createElement("style"));

function checkRangeUI(id, min, max) {
    var val = $('#' + id).val();
    var percent = (val - min) / (max - min) * 100;

    var colors = {
        "custom": '#a50034',
        "custom-grey": '#6e6e6e',
        "custom-red": '#ff4a4a',
        "custom-green": '#2dc4ac',
        "custom-blue": '#00b0f0'
    };

    var classes = $('#' + id).attr('class');
    var color = colors['custom'];

    if (classes) {
        classes = classes.split(' ');
        classes.forEach(function (custom) {
            if (custom.indexOf('custom-') == 0) {
                color = colors[custom];
            }
        });
    }

    var direction = "right";
    if (locale == "ar-SA") {
        direction = "left";
    }

    if (browserType == "webkit") {
        var css = 'linear-gradient(to ' + direction + ', ' + color + ',' + color + ' ' + percent + '%, #f1f1f1 ' + percent + '%, #f1f1f1 100%)';
        $('#' + id).css('background', css);
    }
}

function setupRangeUI(id, min, max) {
	checkRangeUI(id, min, max);
	
	$('#' + id).change(function() {
		$('#' + id + 'Val').html($(this).val());
		$('#' + id + 'Num').val($(this).val());
		checkRangeUI(id, min, max);
	})
	.on('input', function() {
		$('#' + id + 'Val').html($(this).val());
		$('#' + id + 'Num').val($(this).val());
		checkRangeUI(id, min, max);
	});

	$('#' + id + 'Num').change(function() {
		$('#' + id).val($(this).val()).change();
	});
}
