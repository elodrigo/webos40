var env = {};
var chartPage = 1;
var defaultHistorySize = isMobile ? 30 : 50;
var pageSet = 10;
var start = 0;
var end = 50;

var tempSensors = [];
var fans = [];
var color = [
	"#4572a7", "#aa4643", "#89a54e", "#71588f", "#4198af", "#db843d",
	"#93a9cf", "#d19392", "#b9cd96", "#a99bbd", "#88c8da", "#f3b07a",
	"#0d62f5", "#ec1f1c", "#a1ec20", "#6e1ce2", "#1eb6e0", "#f3740c"
];

var totalHistorySize = 0;
var totalPage = 1;

var MAX_QUEUE_SIZE = 10000;

var chartOption = {
    pointDot: true,
    pointDotRadius: 1,
    pointHitDetectionRadius: 2,
    datasetStrokeWidth: 2,

    scaleLabel: " <%=value%>",

    tooltipTemplate: "<%if (label){%><%=label%> <%}%><%=value%>",

    responsive: true,
    bezierCurve: false,
    maintainAspectRatio: true,
    animation: false
};

chartOption.legendTemplate =
    "<ul class=list-group>" +
        "<% for (var i=0; i<datasets.length; i++){%>" +
            "<li class=list-group-item>" +
                "<span class=badge style='background-color:<%=datasets[i].strokeColor%>'>&nbsp;</span>" +
                "<%if(datasets[i].label){%>" +
                "<%=getLanguageText(languageTable, datasets[i].label)%><%}%>" +
            "</li>" +
        "<%}%>" +
    "</ul>";


var isCelsius = true;

function toTimeStr(date) {
    var timeStr = date.toTimeString();
    timeStr = timeStr.substr(0, timeStr.indexOf(" "));
    timeStr = timeStr.substr(0, timeStr.lastIndexOf(":"));
    return timeStr;
}

function createLabels(length, interval) {
    var beforeLogNums = 0;
    if ((totalPage - chartPage -1) >= 0) {
        beforeLogNums = totalHistorySize % defaultHistorySize + (totalPage - chartPage - 1) * defaultHistorySize;
    }

    var labels = [];
    var bootTime = env.bootTime.split(' ').splice(0, 5).join(' ');
    var bootDate = new Date(bootTime);
    var tempDate = new Date();
    interval = interval * 1000;
    for (var i = 0; i != length; ++i) {
        tempDate.setTime(bootDate.getTime() + interval * (i - 1 + beforeLogNums));
        var timeStr = toTimeStr(tempDate);
        labels.push(timeStr);
    }

    return labels;
}

function init(_env, _chartPage) {
    env = _env;
    chartPage = _chartPage;
    $('#pageTitle').html(getLanguageText(languageTable, 'Charts'));

    if (chartPage < 1) {
        chartPage = 1;
    }

    start = (chartPage - 1) * defaultHistorySize;
    end = start + defaultHistorySize;
    tempSensors = env.curTempSensor;

    $('#toggle-temperature').click(function () {
        if (!isCelsius) {
            $("#celsius").css("color", "#b0b0b0");
            $("#fahrenheit").css("color", "#6f6f6f");
        } else {
            $("#fahrenheit").css("color", "#b0b0b0");
            $("#celsius").css("color", "#6f6f6f");
        }
        writeTempType(!isCelsius);
        window.location.reload();
    });

    initChart();
}

function initChart() {
    if (env.isFactoryMenu !== undefined) {
        return;
    }

    readTempType(function (tempType) {
        console.log("isCelsius = " + tempType.isCelsius);
        if (tempType.isCelsius) {
            $("#fahrenheit").css("color", "#b0b0b0");
            $("#celsius").css("color", "#6f6f6f");
        } else {
            $("#celsius").css("color", "#b0b0b0");
            $("#fahrenheit").css("color", "#6f6f6f");
        }
        isCelsius = tempType.isCelsius;
        startTemperatureChart(tempSensors);
    });

    if (env.supportFan) {
        fans = env.fanControl.fans;
        startFanChart();
    }

    if (env.supportBacklight) {
        startBacklightChart();
    }

    if (env.supportBrighness) {
        startBrightnessChart();
    }

    if (env.supportAcCurrent) {
        startAcCurrentChart();
    }

    if (env.supportSolarVoltage) {
        startSolarChart();
    }

    if (locale == "ar-SA") {
        changeRtl();
    }
}

function drawTempChart(history, interval) {
    if (!history || history.length === 0) {
        showWarning(getLanguageText(languageTable, "Temperature history is not yet collected"));
        return;
    }

    var refHistory = undefined;
    for (var i = 0; i != history.length; ++i) {
        if (!history[i]) {
            continue;
        }
        if (history[i].length == 0) {
            alert(getLanguageText(languageTable, "Data is not collected now. wait for maximum 60 seconds").replace("60", interval));
            return;
        }
        refHistory = history[i];
        break;
    }

    for (var i = 0; i != history.length; ++i) {
        if (history[i]) {
            history[i].reverse();
        }
    }

    var min = 987;
    var max = -987;

    var errorSensors = '';
    for (var i = 0; i != tempSensors.length; ++i) {
        var error = false;
        for (var j = 0; history[i] && j != history[i].length; ++j) {

            if (!isCelsius) {
                var calcFahrenheit = (history[i][j] * 1.8) + 32;
                history[i][j] = calcFahrenheit;
            }

            if (history[i][j] < -40) {
                history[i][j] = -40;
                if (!(totalHistorySize < MAX_QUEUE_SIZE && chartPage === totalPage && j === 0)) {
                    error = true;
                }
            }
            history[i][j] = Number(history[i][j].toFixed(1));

            min = history[i][j] < min ? history[i][j] : min;
            max = history[i][j] > max ? history[i][j] : max;
        }
        if (error) {
            errorSensors += getLanguageText(languageTable, tempSensors[i].name) + ' ';
        }
    }

    if (errorSensors) {
        var warningMsg = getLanguageText(languageTable, "Check Temperature sensors. -40 means errors when reading Temperature sensors") + '\u202A (' + errorSensors + ')\u202C';
        showWarning(warningMsg);
    }

    var labels = createLabels(refHistory.length, interval);
    var data = {
        labels: labels,
        datasets: []
    };

    var colorIndex = 0;
    for (var i = 0; i != tempSensors.length; ++i) {
        if (!history[i]) {
            continue;
        }

        data.datasets.push({
            label: tempSensors[i].name,
            fillColor: "rgba(0,0,0,0)",
            strokeColor: color[colorIndex],
            pointColor: color[colorIndex],
            data: history[i]
        });
        ++colorIndex;
    }

    if (min >= 5) {
        min = 0;
    } else if (min > -40) {
        min = Math.floor(min / 5) * 5 - 5;
    }

    var stepWidth = 10;
    steps = (max - min) / stepWidth + 1;

    chartOption.scaleOverride = true;
    chartOption.scaleStepWidth = stepWidth;
    chartOption.scaleStartValue = min;
    chartOption.scaleSteps = steps;
    var ctx = document.getElementById("tempChart").getContext("2d");
    var tempChart = new Chart(ctx).Line(data, chartOption);
    tempChart.update();
    var legend = tempChart.generateLegend();
    $('#tempLegend').append(legend);

    if (locale == "ar-SA") {
        $(".list-group-item").css("text-align", "right");
        $('.badge').css('float', 'left');
    }
}

function drawPager() {
    var maxPage = Math.ceil(totalHistorySize / defaultHistorySize);

    function addNav(page, label) {
        if (page > maxPage) {
            page = maxPage;
        }
        if (page < 1) {
            page = 1;
        }
        var html = "<li><a href='charts?page=" + page + "'><span aria-hidden=true>" + label + "</span></a></li>";
        $('#pager').append(html);
    }

    var prevSet = chartPage + pageSet;
    var prevOne = chartPage + 1;

    addNav(prevSet, getLanguageText(languageTable, "Prev.") + pageSet);
    if (pageSet > 5) {
        addNav(prevOne, "&laquo;");
    }

    var startSet = (Math.ceil(chartPage / pageSet) - 1) * pageSet + 1;
    var endSet = startSet + pageSet - 1;
    if (endSet > maxPage) {
        endSet = maxPage;
    }

    for (var i = endSet; i >= startSet; --i) {
        html = '<li ';
        if (i == chartPage) {
            html += 'class=active';
        }
        html += '><a href="charts?page=' + i + '">' + i;

        html += '</a></li>';

        $('#pager').append(html);
    }

    var nextSet = chartPage - pageSet;
    var nextOne = chartPage - 1;
    if (pageSet > 5) {
        addNav(nextOne, "&raquo;");
    }
    addNav(nextSet, getLanguageText(languageTable, "Next") + " " + pageSet);
}

function drawBacklightChart(backlightData, eyeqData) {
    if (!backlightData || !backlightData.returnValue) {
        showWarning(getLanguageText(languageTable, "History is not yet collected"));
        return;
    }
    var labels = createLabels(backlightData.history.length, backlightData.interval);
    var chartData = {};
    chartData.labels = labels;
    chartData.datasets = [];

    var backlightSet = {
        label: getLanguageText(languageTable, "Backlight") + "(%)",
        fillColor: "rgba(0, 0, 0, 0)",
        strokeColor: color[0],
        pointColor: color[0],
        data: backlightData.history.reverse()
    };
    chartData.datasets.push(backlightSet);

    var eyeqSet = {
        label: getLanguageText(languageTable, "EyeQ sensor"),
        fillColor: "rgba(0, 0, 0, 0)",
        strokeColor: color[1],
        pointColor: color[1],
        data: eyeqData.history.reverse()
    };
    chartData.datasets.push(eyeqSet);

    var max = -999;
    backlightData.history.forEach(function (val) {
        if (val > max) {
            max = val;
        }
    });
    eyeqData.history.forEach(function (val) {
        if (val > max) {
            max = val;
        }
    });

    chartOption.scaleOverride = false;
    chartOption.scaleBeginAtZero = true;

    var ctx = document.getElementById("backlightChart").getContext("2d");
    var backlightChart = new Chart(ctx).Line(chartData, chartOption);
    backlightChart.update();

    var legend = backlightChart.generateLegend();
    $('#backlightLegend').append(legend);
}

function startTemperatureChart(tempSensors) {
    var index = 0;
    var tempHistory = [];

    function tempHistoryCallback(msg) {
        if (msg.returnValue) {
            tempHistory.push(msg.history);
        }
        if (index === tempSensors.length - 1) {
            totalHistorySize = msg.queueSize;
            totalPage = parseInt(totalHistorySize / defaultHistorySize) + 1;
            drawPager();
            drawTempChart(tempHistory, msg.interval);
            index = 0;
            return;
        }
        ++index;
        getTempHistory(tempSensors[index].id, start, end, tempHistoryCallback);
    }
    getTempHistory(tempSensors[index].id, start, end, tempHistoryCallback);
}

function startBacklightChart() {
    getBacklightHistory(start, end, function (backMsg) {
        getEyeQSensorHistory(start, end, function (eyeqMsg) {
            drawBacklightChart(backMsg, eyeqMsg);
        });
    });
}

function startFanChart() {
    var fanHistory = {};
    var fanNums = 0;

    fans.forEach(function (fan) {
        fanNums = fanNums + fan.numberOfFans;
        fanHistory[fan.id] = [];
    });

    fans.forEach(function (fan) {
        for (var i = 0; i < fan.numberOfFans; i++) {
            getFanRpmHistory(fan.id, i, start, end, function (res) {
                fanHistory[fan.id].push(res);
                fanNums = fanNums - 1;

                if (fanNums === 0) {
                    drawFanRpmChart(fanHistory);
                }
            });
        }
    });
}

function drawFanRpmChart(data, numFans) {
    var firstFan = data[fans[0].id][0];

    if (!firstFan.returnValue) {
        showWarning(getLanguageText(languageTable, "Fan RPM history is not yet collected"));
        return;
    }
    var labels = createLabels(firstFan.history.length, firstFan.interval);
    var chartData = {};
    chartData.labels = labels;
    chartData.datasets = [];

    var colorIndex = 0;
    fans.forEach(function (fan) {
        for (var i = 0; i < fan.numberOfFans; ++i) {
            var dataset = {
                label: getLanguageText(languageTable, fan.name.replace(/ /gi, "")) + (data[fan.id][i].num + 1),
                fillColor: "rgba(0, 0, 0, 0)",
                strokeColor: color[colorIndex],
                pointColor: color[colorIndex],
                data: data[fan.id][i].history.reverse()
            };
            ++colorIndex;
            chartData.datasets.push(dataset);
        }
    });

    var stepWidth = 1000 * 3;
    var maxRPM = 16000;

    chartOption.scaleOverride = true;
    chartOption.scaleStepWidth = stepWidth;
    chartOption.scaleStartValue = 0;
    chartOption.scaleSteps = maxRPM / stepWidth;
    var ctx = document.getElementById("fanChart").getContext("2d");
    var fanChart = new Chart(ctx).Line(chartData, chartOption);
    fanChart.update();

    var legend = fanChart.generateLegend();
    $('#fanLegend').append(legend);
}

function checkHistoryData(data) {
    var result = true;
    if (!data || !data.returnValue) {
        showWarning(getLanguageText(languageTable, 'History is not yet collected'));
        result = false;
    }

    return result;
}

function drawBrightnessChart(res) {
    var labels = createLabels(res.history.length, res.interval);
    var data = {
        labels: labels,
        datasets: []
    };

    for (var i = 0; i < res.history.length; i++) {
        if (res.history[i] < 0) {
            res.history[i] = 0;
        }
    }

    data.datasets.push({
        label: getLanguageText(languageTable, 'Brightness'),
        fillColor: "rgba(0,0,0,0)",
        strokeColor: color[0],
        pointColor: color[0],
        data: res.history.reverse()
    });

    chartOption.scaleOverride = true;
    chartOption.scaleStepWidth = isMobile ? 1000 : 500;
    chartOption.scaleStartValue = 0;
    chartOption.scaleSteps = isMobile ? 5 : 10;

    var ctx = document.getElementById('brightnessChart').getContext("2d");
    var chart = new Chart(ctx).Line(data, chartOption);
    chart.update();
    var legend = chart.generateLegend();
    $('#brightnessLegend').append(legend);
}

function startBrightnessChart() {
    getBluMaintainHistory(start, end, function (res) {
        if (!checkHistoryData(res)) {
            return;
        }
        drawBrightnessChart(res);
    })
}

function drawAcCurrentChart(res) {
    var labels = createLabels(res.history.length, res.interval);
    var data = {
        labels: labels,
        datasets: []
    };

    for (var i = 0; i < res.history.length; i++) {
        if (res.history[i] < 0) {
            res.history[i] = 0;
        }
    }

    data.datasets.push({
        label: getLanguageText(languageTable, 'AC Current'),
        fillColor: "rgba(0,0,0,0)",
        strokeColor: color[0],
        pointColor: color[0],
        data: res.history.reverse()
    });

    chartOption.scaleOverride = true;
    chartOption.scaleStepWidth = isMobile ? 6 : 3;
    chartOption.scaleStartValue = 0;
    chartOption.scaleSteps = isMobile ? 5 : 10;

    var ctx = document.getElementById('AcCurrentChart').getContext("2d");
    var chart = new Chart(ctx).Line(data, chartOption);
    chart.update();
    var legend = chart.generateLegend();
    $('#AcCurrentLegend').append(legend);
}

function startAcCurrentChart() {
    getPowerCurrentHistory(start, end, function (res) {
        if (!checkHistoryData(res)) {
            return;
        }
        drawAcCurrentChart(res);
    })
}

function drawSolarChart(res) {
    var labels = createLabels(res.history.length, res.interval);
    var data = {
        labels: labels,
        datasets: []
    };

    for (var i = 0; i < res.history.length; i++) {
        if (res.history[i] < 0) {
            res.history[i] = 0;
        }
    }

    data.datasets.push({
        label: getLanguageText(languageTable, 'Solar') + ' (V)',
        fillColor: "rgba(0,0,0,0)",
        strokeColor: color[0],
        pointColor: color[0],
        data: res.history.reverse()
    });

    chartOption.scaleOverride = true;
    chartOption.scaleStepWidth = 1;
    chartOption.scaleStartValue = 0;
    chartOption.scaleSteps = 5.5;

    var ctx = document.getElementById('solarChart').getContext("2d");
    var chart = new Chart(ctx).Line(data, chartOption);
    chart.update();
    var legend = chart.generateLegend();
    $('#solarLegend').append(legend);
}

function startSolarChart() {
    getSolarHistory(start, end, function (res) {
        if (!checkHistoryData(res)) {
            return;
        }
        drawSolarChart(res);
    })
}

function changeRtl() {
    $(".col-md-2").css("float", "right");
    $(".col-md-10").css("float", "right");
    $(".top-right-refresh").css("float", "left");
    $(".top-refresh").css("float", "left");

    // $(".btn-refresh").css({"background-position-x": "10px", "text-align": "right"});
}
