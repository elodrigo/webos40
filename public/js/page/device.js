var envirment = {};
var wordsList = {};

function toggleMute(val) {
    var muted = val ? "on" : "off";
    $("#muted").bootstrapToggle(muted);
}

var init = false;
var pictureMode = undefined;

function addListener() {
    init = true;
    $("#muted").change(function () {
        setMuted($("#muted").prop('checked'));
    });

    $("#volumn-level").change(function () {
        $('#muted').prop('checked', false);
        $("#volumn-val").html($(this).val());
        setMuted(false);
        setVolume($(this).val());
    }).on('input', function () {
        $("#volumn-val").html($(this).val());
        checkRangeUI('volumn-level', 0, 100);
    });
}

function backlightCallback(powerState) {
    var enableBacklightVal = false;
    getPictureDBVal(['backlight', 'energySaving', 'pictureMode'], function (msg) {
        getEasyBrightnessMode(function (result) {
            var isScreenOn = powerState;
            if (powerState === undefined) {
                isScreenOn = true;
            }
            var isOnEnergySaving = !((msg.energySaving == "off") || (msg.energySaving == "min") || (msg.energySaving == "med"));
            var isOnEasyBrightnessMode = result.easyBrightnessMode === "on" ? true : false;

            if (!isOnEnergySaving && !isOnEasyBrightnessMode && isScreenOn) {
                enableBacklightVal = true;
            }

            $('#backlight').val(msg.backlight);

            checkRangeUI('backlight', 0, 100);

            if (!enableBacklightVal) {
                disableBacklight();
            } else {
                enableBacklight();
                $('#backlightVal').html((msg.backlight + "%"));
            }

            pictureMode = msg.pictureMode;

            if (powerState && msg.energySaving === "screen_off") {
                setTimeout(function () {
                    backlightCallback(powerState);
                }, 150);
            }
        });
    });
}

function disableBacklight() {
    $('#backlight').prop('disabled', true);
    $('#backlightVal').html(getLanguageText(languageTable, "Energy Saving"));
    $('#contrast').prop('disabled', true);
    $('#contrastVal').html(getLanguageText(languageTable, "Energy Saving"));
}

function enableBacklight() {
    $('#backlight').prop('disabled', false);
    $('#contrast').prop('disabled', false);
    $('#contrastVal').html($('#contrast').val());
}

function setHdmi(msg) {
    if (!msg) {
        return;
    }

    $("input:radio[name=hdmi]:input[value='" + msg + "']").val(msg).attr("checked", true);
}

function drawDPMSelect() {
    var dropDown = createDropDown('DPMSelect', '-', function (selected) {
        setDPM(selected);
    });

    if (locale == "ar-SA") {
        dropDown.find(".dropdown-menu").css("right", 0);
    }

    var displayName = {
        'off': getLanguageText(languageTable, "Off"),
        '10sec': getLanguageText(languageTable, "10 sec"),
        '1min': "1 " + getLanguageText(languageTable, "min"),
        '3min': "3 " + getLanguageText(languageTable, "min"),
        '5min': "5 " + getLanguageText(languageTable, "min"),
        '10min': "10 " + getLanguageText(languageTable, "min")
    };

   var itemList = envirment.dpmMode.itemList;
   for (var i = 0; i < itemList.length; i++) {
       var item = itemList[i];
       if (item.visible) {
           addDropDownOption(dropDown, item.value, displayName[item.value]);
       }
   }

    $('#DPMSelect').append(dropDown);

    getDPM(function (msg) {
        setDropButtonText('DPMSelect', msg.dpmMode);
    });
}

function drawPmModeSelect() {
    if (!envirment.pmMode.visible) {
        return;
    }

    var dropDown = createDropDown('pmModeSelect', '-', function (selected) {
        setPmMode(selected);
    });

    if (locale == "ar-SA") {
        dropDown.find(".dropdown-menu").css("right", 0);
    }

    var displayName = {
        "powerOff": getLanguageText(languageTable, "Power Off(Default)"),
        "sustainAspectRatio": getLanguageText(languageTable, "Sustain Aspect Ratio"),
        "screenOff": getLanguageText(languageTable, "Screen Off"),
        "screenOffAlways": getLanguageText(languageTable, "Screen Off Always"),
        "screenOffBacklight": getLanguageText(languageTable, "Screen Off & Backlight On"),
        "networkReady": getLanguageText(languageTable, "Network Ready")
    }

    var itemList = envirment.pmMode.itemList;
    for (var i = 0; i < itemList.length; i++) {
        var item = itemList[i];
        if (item.visible) {
            if (item.value === "sustainAspectRatio" || item.value === "screenOff" || item.value === "networkReady") {
                addDropDownOption(dropDown, item.value, displayName[item.value], true);
            } else {
                addDropDownOption(dropDown, item.value, displayName[item.value], false);
            }
        }
    }

    $("#pmModeSelect").append(dropDown);

    getPmMode(function (msg) {
        setDropButtonText("pmModeSelect", msg.pmMode);
    });
}

function getContrastVal() {
    getPictureDBVal(['contrast'], function (msg) {
        $('#contrast').val(msg.contrast).change();
        $('#contrastVal').html(msg.contrast);
        checkRangeUI('contrast', 0, 100);
        $('#contrast').change(function () {
            var val = $(this).val();
            $('#contrastVal').html(val);
            setPictureDBVal({contrast: val}, 'backlight');
        }).on('input', function () {
            checkRangeUI('contrast', 0, 100);
        });

    });
}

function initDevice(env, wList) {
    envirment = env;
    wordsList = wList;

    if (env.supportScreenOnOff === true) {
        $("#btnScreen").click(function () {
            if ($("#btnScreen").is(":checked")) {
                turnOnScreen();
                backlightCallback(true);
            } else {
                turnOffScreen();
                backlightCallback(false);
            }
        });
    }

    $('#backlight').change(function () {
        var setting = {
            backlight: Number($(this).val())
        }
        setting.pictureSettingModified = {};
        setting.pictureSettingModified[pictureMode] = true;
        setPictureDBVal(setting, "backlight");
        $('#backlightVal').html($(this).val() + "%");
    }).on('input', function () {
        $('#backlightVal').html($(this).val() + "%");

        checkRangeUI('backlight', 0, 100);
    });

    $("#btnReboot").click(function () {
        $('#rebootModal').modal();
    });

    getVolume(function (ret) {
        $("#volumn-level").val(ret.volume);

        checkRangeUI('volumn-level', 0, 100);

        $("#volumn-val").html(ret.volume);
        $('#muted').prop('checked', ret.muted).change();

        if (!init) {
            addListener();
        }
    });

    getInputList(function (msg) {
        var analogInputList = ["RGB", "COMPONENT", "AV"];
        var analogInput = [];
        var digitalInput = [];
        for (var i = 0; i != msg.length; ++i) {
            var isAnalogInput = false;
            for (var j = 0; j < analogInputList.length; j++) {
                if (msg[i].name === analogInputList[j]) {
                    isAnalogInput = true;
                    break;
                }
            }

            if (isAnalogInput) {
                analogInput.unshift(msg[i]);
            } else {
                digitalInput.push(msg[i]);
            }
        }

        var rtl = locale == "ar-SA" ? 'dir=rtl' : '';
        for (var i = 0; i != digitalInput.length; ++i) {
            var radio = $(
                '<li class=list-group-item>' +
                    '<label class="radio-inline"' + rtl + '>' +
                        '<input type="radio" class="custom" value="' + digitalInput[i].id + '" name="hdmi" id="'+
                            digitalInput[i].id + '" ' + rtl + '>' +
                        '<label for="' + digitalInput[i].id + '">' + digitalInput[i].name + '</label>' +
                    '</label>' +
                '</li>'
            );
            radio.appendTo('#inputList');
        }

        for (var i = 0; i != analogInput.length; ++i) {
            var radio = $(
                '<li class=list-group-item>' +
                    '<label class="radio-inline"' + rtl + '>' +
                        '<input type="radio" class="custom" value="' + analogInput[i].id + '" name="hdmi" id="' +
                            analogInput[i].id + '" ' + rtl + '>' +
                        '<label for="' + analogInput[i].id + '">' + analogInput[i].name + '</label>' +
                    '</label>' +
                '</li>'
            );
            radio.appendTo('#inputList');
        }

        $('[name=hdmi]').click(function () {
            for (var i = 0; i != msg.length; ++i) {
                if ($(this).val() === msg[i].id) {
                    setInputSouce(msg[i].appId, function (msg) {
                    });
                }
            }
        });
        getHdmiInput(setHdmi);
    });

    getPowerState(getPowerStateResponse);

    $('#pageTitle').html(getLanguageText(languageTable, wordsList.deviceTitle));

    drawPmModeSelect();
    drawDPMSelect();

    addConfirmModal('rebootModal', getLanguageText(languageTable, "Reboot?"), getLanguageText(languageTable, "Are you sure to reboot?"), function () {
        overlayControl(true, getLanguageText(languageTable, "Rebooting") + "......");
        rebootScreen('commerReset');

        setTimeout(function () {
            window.location.replace("/login");
        }, 60 * 1000);
    });

    if (locale == "ar-SA") {
        changeRtl();
    }
}

function changeRtl() {
    $("#inputList").attr("dir", "rtl").css("padding", "0 0 0 0");
}

function getPowerStateResponse(msg) {
    var powerState = msg.state;
    if (powerState === "Screen Off") {
        if (env.supportScreenOnOff) {
            $("#btnScreen").attr("checked", false);
        }
        backlightCallback(false);
    } else {
        if (env.supportScreenOnOff) {
            $("#btnScreen").attr("checked", true);
        }
        backlightCallback(true);
    }
}