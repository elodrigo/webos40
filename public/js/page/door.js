var inited = false;

function getDoorStatusString(opened) {
	var statusString = getLanguageText(languageTable, opened ? 'Opened' : 'Closed');
	if (opened) {
		return '<font color="red">' + statusString + '</font>';
	}
	return statusString;
}

function doorStatusCallback(msg) {
	$('.alert-warning').hide();

	$('#lockout').html(msg.muteUntilReset ? getLanguageText(languageTable, "Yes" ) : getLanguageText(languageTable, "No" ) );
	setDropButtonText('muteOption', msg.muteOpt);

	if (!inited) {
		inited = true;
		var size = msg.status.length;
		for (var i = 0; i != size; ++i) {
			$('#doorTable > tbody:last').append('<tr><td class=first>' + getLanguageText(languageTable, 'Door Sensor') + (i + 1) + '</td>'
				+ '<td class=second id="door' + (i + 1) + '">'
				+ getDoorStatusString(msg.status[i]) + '</td></tr>');
		}
	} else {
		var size = msg.status.length;
		for (var i = 0; i != size; ++i) {
			$('#door' + (i + 1)).html( getLanguageText(languageTable, getDoorStatusString(msg.status[i])));
			if (msg.status[i]) {
				showWarning(getLanguageText(languageTable, "Door xxx is opened").replace("xxx", (i + 1)));
			}
		}
	}
}

function drawDoorSelect() {
	var dropDown = createDropDown('muteOption', '-', function(selected) {
		changeDoorMuteOpt(selected);
	});
	addDropDownOption(dropDown, "never", getLanguageText(languageTable, 'On When Open'));
	addDropDownOption(dropDown, "whenOpen", getLanguageText(languageTable, 'Off When Open'));
	addDropDownOption(dropDown, "untilReset", getLanguageText(languageTable, 'Off Until Reset'));

	$('#muteOption').append(dropDown);

	getDoorStatus(doorStatusCallback);
}

function initDoor(env) {
	socket.on('door', doorStatusCallback);

	drawDoorSelect();

	$('#reset').click(function () {
		resetDoorState();
	});

	$('#pageTitle').html(getLanguageText(languageTable, 'Door Monitor'));
}

