var currentTime;
var fullRefreshTimer;
var captureRefreshTimer;
var criticalTemp = 85;
var warningTemp = criticalTemp - 3;

function setCurTime(currentTime) {
    var dateArr = currentTime.split(' ');

    var today = "";
    var monthYear = "";

    var weekDay = {
        Sun: getLanguageText(languageTable, "Sunday"),
        Mon: getLanguageText(languageTable, "Monday"),
        Tue: getLanguageText(languageTable, "Tuesday"),
        Wed: getLanguageText(languageTable, "Wednesday"),
        Thu: getLanguageText(languageTable, "Thursday"),
        Fri: getLanguageText(languageTable, "Friday"),
        Sat: getLanguageText(languageTable, "Saturday")
    };
    var month = {
        Jan: getLanguageText(languageTable, "January"),
        Feb: getLanguageText(languageTable, "February"),
        Mar: getLanguageText(languageTable, "March"),
        Apr: getLanguageText(languageTable, "April"),
        May: getLanguageText(languageTable, "May"),
        Jun: getLanguageText(languageTable, "June"),
        Jul: getLanguageText(languageTable, "July"),
        Aug: getLanguageText(languageTable, "August"),
        Sep: getLanguageText(languageTable, "September"),
        Oct: getLanguageText(languageTable, "October"),
        Nov: getLanguageText(languageTable, "November"),
        Dec: getLanguageText(languageTable, "December")
    };

    today = weekDay[dateArr[0]] + ', ' + dateArr[2];
    monthYear = month[dateArr[1]] + ' ' + dateArr[3];

    $("#day").html(today);
    $("#monthYear").html(monthYear);
    $("#curTime").html(dateArr[4]);
}

function secToTime(sec) {
    if (sec == 0) {
        return "-"
    }
    var hour = Math.floor(sec / 60 / 60);
    var min = Math.floor(sec / 60) % 60;
    sec = sec % 60;

    return "" + hour + "H " + min + "M " + sec + "S";
}

function getMobileTimeFormat(time) {
    return time.replace(/-/g, '.').substring(2, time.length - 3);
}

function addIncident(inc) {
    currentTime = currentTime.replace(" ", "T");
    var startTime = inc.startTime.replace(" ", "T");

    var isNG = inc.state == 'NG';
    var duration = inc.duration;

    if (duration == 0 && inc.endTime.length > 0 && inc.startTime.length > 0) {
        duration = (new Date(currentTime)).getTime() - (new Date(startTime)).getTime();
        duration = Math.round(duration / 1000);
    }

    if (duration < 0) {
        duration = 0;
    }

    if (!isMobile) {
        var arabicAttr = '';
        if (locale === "ar-SA") {
            arabicAttr = 'dir="ltr" style="text-align: right"';
        }

        $('#incidentTable > tbody:last').prepend(
            '<tr>'
            //+ '<td>' + inc.category + '</td>'
            + '<td ' + arabicAttr + '>' + inc.category + "(" + inc.sensor + ')</td>'
            + '<td>' + inc.event + '</td>'
            + '<td>' + (isNG ? "<font color=red>" : "") + inc.state + '</td>'
            + '<td>' + inc.startTime + '</td>'
            + '<td>' + inc.endTime + '</td>'
            + '<td>' + secToTime(duration) + '</td></tr>');
    } else {
        $('#incidentTable > tbody:last').prepend(
            '<tr>'
            + '<td style="font-size:10px">' + inc.category + '</td>'
            + '<td style="font-size:10px">' + inc.sensor
            + '<br>' + inc.event + '</td>'
            + '<td style="font-size:10px">' + getMobileTimeFormat(inc.startTime)
            + '<br>' + getMobileTimeFormat(inc.endTime) + '</td>'
            + '<td style="font-size:10px">' + (isNG ? "<font color=red>" : "") + inc.state
            + '<br>' + secToTime(duration) + '</td></tr>');
    }
}

function setBackground(card, isOK) {
    if (isOK) {
        card.removeClass('red');
        card.addClass('green');
        return;
    }

    card.removeClass('green');
    card.addClass('red');
}

function getSensorData() {
    getSignalInfo();
    updateTemperature();

    if (env.checkscreen.support) {
        getCheckScreenOn(function (m1) {
            if (m1 == 'off') {
                $('#checkScreen').html(getLanguageText(languageTable, "Off"));
                setBackground($('#screenCard'), true);
                return;
            }
            getCheckScreenColor(function (msg) {
                $('#checkScreen').html(msg.colorValid ? getLanguageText(languageTable, "OK") : getLanguageText(languageTable, "NG"));
                setBackground($('#screenCard'), msg.colorValid);
            });
        });
    }

    if (env.supportPanelErrorOut) {
        getPanelErrorOut(function (msg) {
            var isNG = !msg.isPanelBLUOk;
            $('#BLU').html(isNG ? getLanguageText(languageTable, "NG") : getLanguageText(languageTable, "OK"));
            setBackground($('#BLUCard'), !isNG);
        });
    }

    getSignageName(function (ret) {
        $("#signageName").html(ret);
        setBackground($('#signageNameCard'), true);
    });

    getSetID(function (msg) {
        $('#setID').html(msg.setId);
        setBackground($('#setIDCard'), true);
    });

    getDPM(function (msg) {
        $("#DPM").html((msg.dpmMode == "off") ? getLanguageText(languageTable, "Off") : getLanguageText(languageTable, "On"));
        var dpmInfo = (msg.dpmMode == "off") ? "" : msg.dpmMode + ". off";
        if (dpmInfo !== "") {
            if (msg.dpmMode === '10sec') {
                dpmInfo = getLanguageText(languageTable, "10 sec") + ". " + getLanguageText(languageTable, "Off");
            } else {
                dpmInfo = msg.dpmMode.substring(0, msg.dpmMode.length - 3) + getLanguageText(languageTable, "min") + ". " + getLanguageText(languageTable, "Off");
            }
        }
        $("#DPMmode").html(dpmInfo);
        setBackground($('#DPMCard'), true);
    });

    getPlayViaUrl(function (msg) {
        var playViaUrlMode = msg.playViaUrlMode;
        $("#URLStatus").html((playViaUrlMode == "off") ? getLanguageText(languageTable, "Off") : getLanguageText(languageTable, "On"));

        if (playViaUrlMode === "on") {
            var playViaUrl = msg.playViaUrl;
            if (!playViaUrl) {
                $("#URL").html(getLanguageText(languageTable, "Check your URL input"));
            } else if (playViaUrl.length > 20) {
                $("#URL").html(playViaUrl.substr(0, 20) + "...")
            } else {
                $("#URL").html(playViaUrl);
            }
        } else {
            $("#URL").html("");
        }
        setBackground($('#URLCard'), true);
    });

    updateFanStatus();
}

function getData() {
    getBasicInfo(function (msg) {
        $('#firmware').html(msg.firmwareVersion);
    });

    getNetworkStatus(function (msg) {
        var connection = msg.wired.state === 'connected' ? msg.wired : msg.wifi;
        $('#ip').html(connection.state === 'connected' ? connection.ipAddress : getLanguageText(languageTable, "Not Connected"));
        $('#wan').html(msg.isInternetConnectionAvailable ? getLanguageText(languageTable, "Internet available") : getLanguageText(languageTable, "Internet Not available"));

        setBackground($('#networkCard'), connection.state === 'connected');
    });

    getSensorData();

    if (env.supportPsuStatus) {
        getPsuStatus();
    }

    getDowntimeIncident(function (msg) {
        if (!msg.returnValue) {
            $('#downloadLog').removeAttr('href');
            return;
        }
        $('#incidentTable > tbody').empty();
        var incs = msg.incident;

        incs.forEach(function (inc) {
            if (inc.state != 'NG') {
                addIncident(inc);
            }
        });
        incs.forEach(function (inc) {
            if (inc.state == 'NG') {
                addIncident(inc);
            }
        });
        $('#downloadLog').attr('href', '/downloadIncidents?name=downloadIncidents.csv');
    });

    updateTileModeInfo();
    updateFailOverInfo();

    if (env.supportBrighness) {
        getBrightness();
    }
    if (env.supportAcCurrent) {
        getAcCurrent();
    }
    if (env.supportSolarVoltage) {
        getSolarVoltage();
    }

}

function showDoorStatus(doors) {
    if (!doors) {
        return;
    }

    var opened = false;

    for (var i = 0; i != doors.length; ++i) {
        if (doors[i]) {
            opened = true;
            break;
        }
    }

    $('#door').html(opened ? getLanguageText(languageTable, "Opened") : getLanguageText(languageTable, "Closed"));
    setBackground($('#doorCard'), !opened);
}

function getAllData() {
    getCurrentTime(function (msg) {
        currentTime = msg.current;
        setCurTime(msg.current);
        $('#currentTime').html(msg.current);
        $('#upTime').html(msg.uptime);
        getData();
        $("#scr").click();
    });
}

function getCookie(key) {
    var replace = new RegExp("(?:(?:^|.*;\\s*)" + key + "\\s*\\=\\s*([^;]*).*$)|^.*$");
    return document.cookie.replace(replace, "$1");
}

function getExpireDate() {
    var curDay = new Date();
    curDay = new Date(curDay.getTime() + 24 * 60 * 60 * 1000);
    var ret = ';expires=' + curDay.toGMTString() + ';';

    return ret;
}

function setInputName(currInput) {
    $('#stall').html('');
    getInputList(function (allInput) {
        allInput.forEach(function (input) {
            if (currInput == input.id) {
                $("#signalCard .tail").show();
                $("#stall").html("(" + input.name + ")");
            }
        });
    });
}

function init() {
    criticalTemp = env.criticalTemp? env.criticalTemp : criticalTemp;

    alignCardWithCenter();
    getIsMobile();
    var sensorCount = $('.temperature').length;
    $('.temperature').css('width', 100 / sensorCount + '%');
    $('.sensorname').css('width', 100 / sensorCount + '%');

    if (sensorCount >= 5) {
        $(".temperature").css("font-size", 100 / sensorCount);
        $(".sensorname").css("font-size", 100 / sensorCount);
    }

    if (env.supportDoor) {
        socket.on('door', function (msg) {
            showDoorStatus(msg.status);
        });

        getDoorStatus(function (msg) {
            showDoorStatus(msg.status);
        });
    }

    getAllData();

    $('#refresh').click(function () {
        getSensorData();
    });

    $("#scr").click(function (e) {
        showRotationWarning();
        getCapture($("#scr").height(), function (m) {
            $("#scr").attr('src', m);
        });

        getPowerState(function (msg) {
            if (msg.state === 'Screen Off') {
                $('#scr').hide();
            } else {
                $('#scr').show();
            }
        });
    });

    $('#blackScr').click(function () {
        $('#scr').click();
    });

    $('#dataRefresh').click(function () {
        var checked = $(this).prop('checked');
        if (checked) {
            fullRefreshTimer = setInterval(function () {
                getAllData();
                $('#scr').click();
            }, 5 * 60 * 1000);
            document.cookie = 'dataRefresh=1' + getExpireDate() + ';secure';
        } else {
            if (fullRefreshTimer) {
                clearTimeout(fullRefreshTimer);
            }
            document.cookie = 'dataRefresh=;secure';
        }
    });

    $('#captureRefresh').click(function () {
        var checked = $(this).prop('checked');

        if (checked) {
            captureRefreshTimer = setInterval(function () {
                $('#scr').click();
            }, 10 * 1000);
            document.cookie = 'captureRefresh=1' + getExpireDate() + ';secure';
        } else {
            if (captureRefreshTimer) {
                clearTimeout(captureRefreshTimer);
            }
            document.cookie = 'captureRefresh=;secure';
        }
    });

    if (getCookie('dataRefresh')) {
        $('#dataRefresh').click();
        $('#dataRefresh').prop('checked', true);
    }

    if (getCookie('captureRefresh')) {
        $('#captureRefresh').click();
        $('#captureRefresh').prop('checked', true);
    }

    $("#pageTitle").html(getLanguageText(languageTable, "Dashboard"));

    $('#scr').click();

    if (locale == "ar-SA") {
        changeRtl();
    }
}

// LED Signage
var unitStatus = {
    ok: 0,
    ng: 0,
    off: 0
};

function showCardMessage() {
    var isOk = unitStatus.ng == 0 && unitStatus.off == 0;
    $('#ledUnit').html(isOk ? getLanguageText(languageTable, 'OK') : getLanguageText(languageTable, 'NG'));
    setBackground($('#ledCard'), isOk);
}

function checkUnitOk(unitID, gridID) {
    getLedPowerStatus(unitID * 2, function (msg) {
        console.log(msg);
        var grid = $('#gridCol' + gridID);
        if (!msg.isPowerOk) {
            grid.addClass('off');
            unitStatus.off++;
            showCardMessage();
            return;
        }
        getLedOverallStatus(unitID * 2, function (msg) {
            console.log(msg);
            grid.addClass(msg.isOverallOk ? 'ok' : 'ng');
            unitStatus[msg.isOverallOk ? 'ok' : 'ng']++;
            showCardMessage();
        });
    });
}

function alignCardWithCenter() {
    // align card body with center
    $(".card.summary-inline .card-body .content").css({
        "text-align": "center",
        "display": "table-cell",
        "vertical-align": "middle"
    });

    // Standard of card is signalCard.
    var width = $("#signalCard").width();
    var height = $("#signalCard").height() - $("#signalCard .card-header").height();

    // To align location of table-cell should has width and height.
    // Set to card body
    $(".card.summary-inline .card-body .content").css({
        "width": width,
        "height": height,
    });

    // Standard of big card is tempCard
    width = $("#tempCard").width();
    height = $("#tempCard").height() - $("#tempCard .card-header").height();

    // Set to big card body
    $(".card.big.summary-inline .card-body .content").css({
        "width": width,
        "height": height,
    });

    // Set to mobile big card body with
    $(".card.big-mobile.summary-inline .card-body .content").css({
        "width": width,
        "height": height,
    });

    // Hidden overflow of card header.
    $(".card.summary-inline .card-header").css({"overflow-y": "hidden"});
}

function updateFailOverInfo() {
    getFailOver(function (msg) {
        // Cahnge first letter  to upper case
        var displayInfo = msg.failover.substring(0, 1).toUpperCase() + msg.failover.substring(1, msg.failover.length);
        $("#failOver").html(getLanguageText(languageTable, displayInfo));

        if (msg.failover === "off") {
            $("#failOverPriority").html("");
        } else {
            getFailOverPriorityInfo(function (failOverPriorityInfo) {
                var html = "";

                if (failOverPriorityInfo.length === 1) {
                    html = "(1 " + failOverPriorityInfo[0].name + ")";
                } else {
                    if (msg.failover === "auto") {
                        var priority = [];
                        for (var i = 0; i < failOverPriorityInfo.length; i++) {
                            if (failOverPriorityInfo[i].id === "HDMI_1") {
                                priority[0] = failOverPriorityInfo[i].name;
                            } else if (failOverPriorityInfo[i].id === "HDMI_2") {
                                priority[1] = failOverPriorityInfo[i].name;
                            }
                        }
                        html = "(1 " + priority[0];
                        html += " > 2 " + priority[1];
                    } else if (msg.failover === "manual") {
                        html = "(1 " + failOverPriorityInfo[0].name;
                        html += " > 2 " + failOverPriorityInfo[1].name;
                    }

                    if (failOverPriorityInfo.length === 2) {
                        html += ")";
                    } else {
                        html += "..)";
                    }
                }
                $("#failOverPriority").html(html);
                if ($('#failOverPriority').width() > $('#failOverCard').width()) {
                    setTimeout(function () {
                        var html = "<marquee>" + $("#failOverPriority").html() + "</marquee>";
                        $("#failOverPriority").html(html);

                    }, 1000);
                }
            })
        }
        setBackground($('#failOverCard'), true);
    });
}

function updateTileModeInfo() {
    getTileModeValue(function (msg) {
        var tileMode = (msg.tileMode == "off") ? "Off" : "On";
        $("#tileMode").html(getLanguageText(languageTable, tileMode));
        setBackground($('#tileModeCard'), true);
        if (tileMode === "On") {
            $("#tileModeSettings").html("(" + msg.tileRow + "x" + msg.tileCol + ", " + getLanguageText(languageTable, "Tile ID") + ": " + msg.tileId + ")");
        } else {
            $("#tileModeSettings").html("");
        }
    });
}

function getSignalInfo() {
    function setSignalStatusAttr() {
        if (isMobile) {
            $('#signal-status').attr('class', 'status mobile');
        } else {
            $('#signal-status').attr('class', 'status');
        }
    }

    function checkExternalInput(foregroundAppInfo, inputList) {
        var isExternalInput = false;
        for (var i = 0; i < inputList.length; i++) {
            if (foregroundAppInfo.appId === inputList[i].appId) {
                isExternalInput = true;
                break;
            }
        }
        console.log("checkExternalInput() isExternalInput: " + isExternalInput);
        return isExternalInput;
    }

    function getIsPcMode(videoInfo, hdmiPcMode, currInput) {
        var isPcMode = false;
        if (videoInfo.timingMode === "PC") {
            isPcMode = true;
        } else if (videoInfo.timingMode === "TV") {
            isPcMode = false;
        } else {
            if ((hdmiPcMode.hdmi1 !== undefined && hdmiPcMode.hdmi1 && currInput === "HDMI_1")
                || (hdmiPcMode.hdmi2 !== undefined && hdmiPcMode.hdmi2 && currInput === "HDMI_2")
                || (hdmiPcMode.hdmi3 !== undefined && hdmiPcMode.hdmi3 && currInput === "HDMI_3")
                || (hdmiPcMode.hdmi4 !== undefined && hdmiPcMode.hdmi4 && currInput === "HDMI_4")) {
                isPcMode = true;
            }
            else {
                isPcMode = false;
            }
        }
        return isPcMode;
    }

    function getScanType(videoInfo) {
        var scanType = "";
        if (videoInfo.scanType === "VIDEO_PROGRESSIVE") {
            scanType = "p";
        } else if (videoInfo.scanType === "VIDEO_INTERLACED") {
            scanType = "i";
        }
        return scanType;
    }

    getForegroundAppInfo(function (foregroundAppInfo) {
        getInputList(function (inputList) {
            if (!checkExternalInput(foregroundAppInfo, inputList)) {
                $('#signal').html(capitalizeFirstLetter(getLanguageText(languageTable, "NOT AVAILABLE")));
                setBackground($('#signalCard'), true);
                $("#signalCard .tail").hide();
                return;
            }

            isNoSignal(function (msg) {
                setSignalStatusAttr();

                getHdmiInput(function (currInput) {
                    var isNoSignal = !msg || msg.noSignal;
                    if (isNoSignal) {
                        $('#signal').html(getLanguageText(languageTable, "No Signal"));
                        setBackground($('#signalCard'), false);
                        setInputName(currInput);
                        return;
                    }

                    getVideoSize(function (videoInfo) {
                        if (!videoInfo || !videoInfo.returnValue || (videoInfo.width === 0 && videoInfo.height === 0)) {
                            $('#signal').html(capitalizeFirstLetter(getLanguageText(languageTable, "NOT AVAILABLE")));
                            setBackground($('#signalCard'), true);
                            $("#signalCard .tail").hide();
                            return;
                        }

                        getHdmiPcMode(function (hdmiPcMode) {
                            var isPcMode = getIsPcMode(videoInfo, hdmiPcMode, currInput);
                            var scanType = getScanType(videoInfo);

                            var generalResolution = videoInfo.height + scanType;
                            var pcModeResolution = videoInfo.width + " X " + videoInfo.height;
                            var text = isPcMode ? pcModeResolution : generalResolution;

                            $('#signal').html(text);
                            setBackground($('#signalCard'), true);
                            setInputName(currInput);

                            getVideoStillStatus(function (msg) {
                                if (msg.state == "stalled") {
                                    $('#signal').html(getLanguageText(languageTable, "Stalled Image"));
                                    $('#stall').html(getLanguageText(languageTable, "over {period} min.").replace('{period}', msg["period(minutes)"]));
                                    setBackground($('#signalCard'), false);
                                }
                            });

                        });
                    });
                });
            });
        });
    });
}

function getBrightness() {
    getBluMaintain(function (res) {
        if (!res.returnValue || !res.status) {
            $('#brightness').text(getLanguageText(languageTable, 'NG'));
            setBackground($('#brighnessCard'), false);
        } else {
            $('#brightness').text(res.bluMaintain + ' cd/m2');
            setBackground($('#brighnessCard'), true);
        }
    })
}

function getAcCurrent() {
    getPowerCurrent(function (res) {
        if (!res.returnValue || !res.status) {
            $('#acCurrent').text(getLanguageText(languageTable, 'NG'));
            setBackground($('#acCurrentCard'), false);
        } else {
            $('#acCurrent').text(res.powerCurrent + ' A');
            setBackground($('#acCurrentCard'), true);
        }
    })
}

function getSolarVoltage() {
    getSolar(function (res) {
        if (!res.returnValue || !res.status) {
            $('#solarVoltage').text(getLanguageText(languageTable, 'NG'));
            setBackground($('#solarVoltageCard'), false);
        } else {
            $('#solarVoltage').text(res.solar + ' mV');
            setBackground($('#solarVoltageCard'), true);
        }
    });
}

function getPsuStatus() {
    getSDMInfo(function (msg) {
        var PSUStatus = msg.PSUStatus;
        var checkResult = true;

        if (PSUStatus) {
            for (var i = 0; i < PSUStatus.length; i++) {
                for (var variable in PSUStatus[i]) {
                    if (PSUStatus[i].hasOwnProperty(variable)) {
                        if (!PSUStatus[i][variable]) {
                            checkResult = false;
                            break;
                        }
                    }
                }
                if (!checkResult) {
                    break;
                }
            }

            $('#PSU').html(checkResult ? 'OK' : 'NG');
            setBackground($('#PSUCard'), checkResult);
        } else {
            setBackground($('#PSUCard'), false);
        }
    });
}

function updateTemperature() {
    getTemp(function (res) {
        var tempStatus = true;
        var types = res.group ? res.group : [];
        types.forEach(function (type) {
            var sensor = res[type];
            var temperature = sensor.celsius;

            readTempType(function (tempType) {
                if (!tempType.isCelsius) {
                    $('#' + type + 'Temp').html(Math.floor((temperature) * 1.8 + 32) + '°F');
                } else {
                    $('#' + type + 'Temp').html(temperature + '°C');
                }
            });

            if (!sensor.status || temperature >= warningTemp) {
                tempStatus = false;
            }
        });

        setBackground($('#tempCard'), tempStatus);
    });
}

function updateFanStatus() {
    getFanStatus(function (res) {
        if (!res.returnValue || !res.support) {
            return;
        }

        var isOK = true;
        var types = res.group;
        types.some(function (type) {
            var fan = res[type];
            fan.status.some(function (status) {
                isOK = status
                return !isOK;
            });
            return !isOK;
        });

        $('#fan').html(isOK ? getLanguageText(languageTable, 'OK') : getLanguageText(languageTable, 'NG'));
        setBackground($('#fanCard'), isOK);
    });
}

function changeRtl() {
    $(".top-refresh").css("float", "left");
    $(".screenshot-message").css({"float": "right", "left": "initial", "right": 10, "text-align": "right"});
    $(".date").css({"float": "right", "padding-right": "25px"});
    $(".cur-time").css({"float": "right", "padding-right": "25px"});
    $(".run-time").css({"float": "left", "padding-left": "25px"});

    $(".card-header").attr("dir", "rtl");
    $(".card .card-header .card-title").css({"float": "right", "padding-left": 0, "padding-right": 15});
    $(".card .card-header .linkarrow").css({"float": "left", "padding-left": 3, "padding-right": 0});
    $(".card .card-header .linkarrow img").css("float", "left");

// $("#incidentTable").attr("dir", "ltr");
// $("#incidentTable .table>tbody>tr>th").css("text-align", "right");+
}