var statusTxt = {
    connected: "Connected",
    notConnected: "Not Connected",
    notApproval: "Not Approval"
};

var prevData = {
    enable: false,
    ip: "",
    ipv6: "",
    port: 0,
    ipType: "ipv4",
    status: ""
};

function loadLedAssistantInfo(all) {
    getLedAssisantInfo(function(ret) {
        if (all) {
            if (ret.linkServerEnable == 'on') {
                $('#serverEnable').attr('checked', true);
            }
            $('input:radio[name=advOpt][value=' + ret.linkServerIpType + ']').prop('checked', true);
            $('#serverIP').val(ret.linkServerIP);
            $('#serverIPv6').val(ret.linkServerIPv6);
            $('#serverPort').val(ret.linkServerPort);

            if (ret.linkServerIpType == 'ipv4') {
                $('#ipv4Row').show();
                $('#ipv6Row').hide();
            } else {
                $('#ipv6Row').show();
                $('#ipv4Row').hide();
            }

            prevData.enable = ret.linkServerEnable;
            prevData.ipType = ret.linkServerIpType;
            prevData.ip = ret.linkServerIP;
            prevData.ipv6 = ret.linkServerIPv6;
            prevData.port = ret.linkServerPort;

            if (prevData.ipv6.length > 20) {
                $('#serverIPv6').prop('size',prevData.ipv6.length);
            }
        }

        $('#serverStatus').val(getLanguageText(languageTable, statusTxt[ret.linkServerStatus]));
        prevData.status = ret.linkServerStatus;
    });

}

function initLedAssistant() {
    loadLedAssistantInfo(true);

    setInterval(function() {
        loadLedAssistantInfo(false);
    }, 5 * 1000);

    $('#apply').click(function() {
        var enable = $('#serverEnable').prop('checked') ? 'on' : 'off';
        console.log(enable);
        var ip = $('#serverIP').val();
        var ipv6 = $('#serverIPv6').val();
        var port = $('#serverPort').val();
        var ipType = $('input:radio[name=advOpt]:checked').val();

        if (!checkIP(ip, false)) {
            alert("Error IP");
            $('#serverIP').val(prevData.ip);
            return;
        }

        if (!checkIP(ipv6, true)) {
            alert("Error IPv6");
            $('#serverIPv6').val(prevData.ipv6);
            return;
        }

        var portNum = Number(port);
        console.log(portNum);

        if (isNaN(portNum) || portNum < 0 || portNum > 65535) {
            alert("Error Port");
            $('#serverPort').val(prevData.port);
            return;
        }

        setLedAssistantInfo(enable, ip, portNum, ipType,  ipv6, function(ret) {
            loadLedAssistantInfo();
            console.log(ret);
        });
    });

    $('input:radio[name=advOpt]').change(function() {
        if ($(this).val() == 'ipv4') {
            $('#ipv4Row').show();
            $('#ipv6Row').hide();
        } else {
            $('#ipv6Row').show();
            $('#ipv4Row').hide();
        }
    });

    $('#serverIPv6').on('input', function() {
        if ($('#serverIPv6').val().length > 20) {
            $('#serverIPv6').prop('size', $('#serverIPv6').val().length);
        }
    });
}
