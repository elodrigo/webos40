var prevData = {
    ipv4: {},
    ipv6: {}
};

function isIPv6() {
    return $('input[name=ipType]:checked').val() == 'ipv6';
}

function showIPv4() {
    var isDHCP = prevData.ipv4.method;
    $('#dhcp').prop('checked', isDHCP).prop('disabled', false);
    $('#ip').val(prevData.ipv4.ip).prop('disabled', isDHCP).prop('size', 20);
    $('#netMask').val(prevData.ipv4.netmask).prop('disabled', isDHCP)
    $('#gateway').val(prevData.ipv4.gateway).prop('disabled', isDHCP)
    $('#dns').val(prevData.ipv4.dns).prop('disabled', isDHCP)
    $('#netPrefixRow').hide()
    $('#netMaskRow').show()
}

function showIPv6() {
    var isAuto = prevData.ipv6.method;
    $('#dhcp').prop('checked', isAuto).prop('disabled', true);
    $('#ip').val(prevData.ipv6.ip).prop('disabled', isAuto).prop('disabled', true).prop('size', 20);
    if (prevData.ipv6.ip && prevData.ipv6.ip.length > 20) {
        $('#ip').prop('size', prevData.ipv6.ip.length);
    }
    $('#netPrefix').val(prevData.ipv6.prefixLength).prop('disabled', isAuto).prop('disabled', true);
    $('#gateway').val(prevData.ipv6.gateway).prop('disabled', isAuto).prop('disabled', true);
    $('#dns').val(prevData.ipv6.dns).prop('disabled', isAuto).prop('disabled', true);
    $('#netPrefixRow').show().prop('disabled', true);
    $('#netMaskRow').hide()
}

function getNetworkData() {
    getNetworkStatus(function (ret) {
        var wired = ret.wired;

        prevData.ipv4 = {
            method: wired.method != 'fixed' && wired.method != 'manual',
            ip: wired.ipAddress,
            netmask: wired.netmask,
            gateway: wired.gateway,
        }

        prevData.ipv6 = {};

        if (jQuery.type(wired.ipv6) !== 'undefined') {
            prevData.ipv6.method = wired.ipv6.method == 'auto',
            prevData.ipv6.ip = wired.ipv6.ipAddress,
            prevData.ipv6.gateway = wired.ipv6.gateway,
            prevData.ipv6.prefixLength = wired.ipv6.prefixLength
        }

        var idx = 0;
        while (true) {
            ++idx;
            if (wired["dns" + idx]) {
                var dns = wired["dns" + idx];
                if (dns.indexOf(":") >= 0) {
                    prevData.ipv6.dns = dns;
                } else if (dns.indexOf(".") >= 0) {
                    prevData.ipv4.dns = dns;
                }
            } else {
                break;
            }
        }

        showIPv4();
    });
    
    $('input[name=ipType]').change(function() {
        var type = $(this).val();
        if (type == 'ipv4') {
            showIPv4();
            $('#apply').prop('disabled', false);
        } else {
            showIPv6();
            $('#apply').prop('disabled', true);
        }
    });

    $('#dhcp').change(function() {
        var checked = $(this).prop('checked');
        if (checked) {
            var msg = getLanguageText(languageTable, "Are you sure to use Wired DHCP?") + "\n";;
            msg += getLanguageText(languageTable, "You may not be able to reconnect to web pages if you cannot find out new IP address.");
            if (!confirm(msg)) {
                $(this).prop('checked', false);
                return;
            }
        }
        $('#ip').prop('disabled', checked);
        $('#netPrefix').prop('disabled', checked);
        $('#netMask').prop('disabled', checked);
        $('#gateway').prop('disabled', checked);
        $('#dns').prop('disabled', checked);
    });
}

function addPingModal() {
    var title = getLanguageText(languageTable, "Enter the network address to ping");
    var html
        = '<div class="modal fade" tabindex="-1" role="dialog" id="pingModal">'
            + '<div class="modal-dialog" style="height: 60%; width: 70%;">'
                + '<div class="modal-content" id=pingInput>'
                    + '<div class="modal-header">'
                        + '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
                        + '<h4 class="modal-title" id="gridSystemModalLabel">' + title + '</h4>'
                    + '</div>'
                    + '<div class="modal-body">'
                        + '<table class="table">'
                            + '<tr><th style="border-top: none;">' + getLanguageText(languageTable, 'Type') + '</th></tr>'
                            + '<tr><td style="border-top: none;">'
                                + '<label class="rad">'
                                    + '<input type="radio" name="pingType" value="ip" checked/>'
                                    + '<i></i> IP'
                                + '</label>&nbsp;&nbsp;'
                                + '<label class="rad">'
                                    + '<input type="radio" name="pingType" value="url" />'
                                    + '<i></i> URL'
                                + '</label>'
                            + '</td></tr>'
                            + '<tr><th style="border-top: none;"><span id=pingTypeSpan>' + getLanguageText(languageTable, 'IP Address') + '</span></th></tr>'
                            + '<tr><td style="border-top: none;"><input type=text id=pingAddr class=custom style="color: black; width: 400px;"></td></tr>'
                            + '<tr><td style="border-top: none;"><span id=txtErrorMsg class="txt_error_msg"></span></td></tr>'
                        + '</table>'
                    + '</div>'
                    + '<div class="modal-body"> &nbsp;'
                        + '<button class="cardBtn" data-dismiss="modal" style="margin-left:10px;">' + getLanguageText(languageTable, 'Cancel') + '</button>'
                        + '<button class="cardBtn" id=startPing>' + getLanguageText(languageTable, 'Begin test') + '</button>'
                    + '</div>'
                + '</div>'
                + '<div class="modal-content" id=pingProgress style="display:none; height:60%; overflow:auto;">'
                    + '<div class="modal-header">'
                        + '<h4 class="modal-title" id="gridSystemModalLabel">' + getLanguageText(languageTable, 'Ping test is in progress') + '</h4>'
                    + '</div>'
                    + '<div class="modal-body">'
                        + '<span id=pingTitle></span>'
                        + '<ul id=pingResult>'
                        + '</ul>'
                    + '</div>'
                + '</div>'
                 + '<div class="modal-content" id=pingEnd style="display:none; height:60%">'
                    + '<div class="modal-header">'
                        + '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
                        + '<h4 class="modal-title" id="gridSystemModalLabel">' + getLanguageText(languageTable, 'Ping test result') + '</h4>'
                    + '</div>'
                    + '<div class="modal-body">'
                        + '<table>'
                            + '<tr><th id=pingStatTitle></th></tr>'
                            + '<tr><td id=pingStatBody></td></tr>'
                            + '<tr><th id=pingTimeTitle>' + getLanguageText(languageTable, 'Approximate round trip in milli-seconds') + '</th></tr>'
                            + '<tr><td id=pingTimeBody></td></tr>'
                        + '</table>'
                        + '<button class="btn" data-dismiss="modal">OK</button>'
                    + '</div>'
                + '</div>'
            + '</div>'
        + '</div>';

    var modal = $(html);
    $('body').append(modal);
}

var invalidTxt = '';

function checkIPv4() {
    var checkResult = true;
    invalidTxt = '';

    if (!checkIP($('#ip').val(), false)) {
        invalidTxt += "<br>" + getLanguageText(languageTable, "IP Address");
        $('#ip').val(prevData.ipv4.ip);
        checkResult = false;
    }

    if (!checkIP($('#gateway').val(), false)) {
        invalidTxt += "<br>" + getLanguageText(languageTable, "Gateway");
        $('#gateway').val(prevData.ipv4.gateway);
        checkResult = false;
    }

    if (!checkNetMask($('#netMask').val())) {
        invalidTxt += "<br>" + getLanguageText(languageTable, "Subnet Mask");
        $('#netMask').val(prevData.ipv4.netmask);
        checkResult = false;
    }

    if (!checkIP($('#dns').val(), false)) {
        invalidTxt += "<br>" + getLanguageText(languageTable, "DNS Server");
        $('#dns').val(prevData.ipv4.dns);
        checkResult = false;
    }

    return checkResult;
}

function checkIPv6() {
    var checkResult = true;
    invalidTxt = '';

    if (!checkIP($('#ip').val(), true)) {
        invalidTxt += "<br>" + getLanguageText(languageTable, "IP Address");
        $('#ip').val(prevData.ipv4.ip);
        checkResult = false;
    }

    if (!checkIP($('#gateway').val(), true)) {
        invalidTxt += "<br>" + getLanguageText(languageTable, "Gateway");
        $('#gateway').val(prevData.ipv6.gateway);
        checkResult = false;
    }

    if (!checkNetPrefix($('#netPrefix').val())) {
        invalidTxt += "<br>" + getLanguageText(languageTable, "Subnet Prefix Length");
        $('#netPrefix').val(prevData.ipv6.prefixLength);
        checkResult = false;
    }

    if (!checkIP($('#dns').val(), false) && !checkIP($('#dns').val(), true)) {
        invalidTxt += "<br>" + getLanguageText(languageTable, "DNS Server");
        $('#dns').val(prevData.ipv4.dns);
        checkResult = false;
    }

    return checkResult;
}

function setIPv4() {
    var dhcp = $('#dhcp').prop('checked');
    var ip = $('#ip').val();
    var needReconnection = (dhcp != prevData.ipv4.method) || !(!dhcp && (ip == prevData.ipv4.ip));

    console.log(dhcp, prevData.ipv4.method);
    console.log(ip, prevData.ipv4.ip);
    console.log(needReconnection);

    prevData.ipv4.method = $('#dhcp').prop('checked');
    prevData.ipv4.ip = $('#ip').val()
    prevData.ipv4.gateway = $('#gateway').val()
    prevData.ipv4.netmask = $('#netMask').val()
    prevData.ipv4.dns = $('#dns').val()

    if (prevData.ipv4.method) {
        prevData.ipv4.ip = '192.168.0.101';
        prevData.ipv4.netmask = '255.255.255.0';
        prevData.ipv4.gateway = '192.168.0.1';
        prevData.ipv4.dns = '';
    }

    setNetworkDNS(prevData.ipv4.dns);
    setNetworkStatus(prevData.ipv4.method, prevData.ipv4.ip, prevData.ipv4.netmask, prevData.ipv4.gateway);

    if (needReconnection) {
        showWarningModal(getLanguageText(languageTable, "You changed IP address or DHCP status. Please reconnect to changed IP address"));
    }
}

/*
function setIPv6() {
    prevData.ipv6.method = $('#dhcp').prop('checked');
    prevData.ipv6.ip = $('#ip').val()
    prevData.ipv6.gateway = $('#gateway').val()
    prevData.ipv6.prefixLength = parseInt($('#netPrefix').val());
    prevData.ipv4.dns = $('#dns').val()

    console.log(prevData);
    setNetworkDNS(prevData.ipv4.dns);
    setNetworkStatus6(prevData.ipv6.dhcp, prevData.ipv6.ip, prevData.ipv6.prefixLength, prevData.ipv6.gateway);
}
*/

function initPingModal() {
    $('#pingTest').click(function() {
        $('#pingInput').show();
        $('#pingProgress').hide();
        $('#pingEnd').hide();
        $('#pingResult').html('');
        $('#pingTitle').html('');
        $('#pingModal').modal({
            backdrop: 'static',
            keyboard: false
        });
        $('#txtErrorMsg').html("");
        $('#pingAddr').focus();
    });

    $('input:radio[name=pingType]').change(function() {
        var val = $(this).val();
        $('#pingTypeSpan').text(val.toUpperCase() == 'IP' ? getLanguageText(languageTable, 'IP Address') : getLanguageText(languageTable, 'URL'));
        $('#pingAddr').val('');
    });

    $('#startPing').click(function() {
        var hostname = $('#pingAddr').val()
        var cnt = 0;
        var success = 0;
        var totalTime = 0;
        var min = Number.MAX_VALUE;
        var max = 0;
        var isIPv6 = checkIP(hostname, true);
        var isIPv4 = checkIP(hostname, false);

        var type = $('input:radio[name=pingType]:checked').val();

        console.log(type);

        if (env.supportCinemaModel) {
            if (((type == 'ip') && (!isIPv6 && !isIPv4)) ||
                ((type == 'url') && (isIPv4 || isIPv6)) || (hostname.length == 0)) {
                    $('#txtErrorMsg').html(getLanguageText(languageTable, 'Ping test failed. Invalid format of network address field.'));
                    $('#pingAddr').val("");
                    return;
            }
        } else {
            if (type == 'ip') {
                if (!isIPv6 && !isIPv4) {
                    alert(getLanguageText(languageTable, 'Error') + '\n' + getLanguageText(languageTable, 'IP Address'));
                    return;
                }
            }
            if (type == 'url') {
                if (isIPv4 || isIPv6) {
                    alert(getLanguageText(languageTable, 'Error') + '\n' + getLanguageText(languageTable, 'URL'));
                    return;
                }
            }
            if (hostname.length == 0) {
                alert(getLanguageText(languageTable, 'Error') + '\n' + getLanguageText(languageTable, 'URL'));
                return;
            }
        }

        $('#pingInput').hide();
        $('#pingProgress').show();
        $('#pingEnd').hide();

        function processPing(result) {
            ++cnt;
            if (result.bytes == undefined) {
                result.bytes = "000";
            }
            $('#pingTitle').text(getLanguageText(languageTable, 'Pinging [X.X.X.X] with YYY bytes of data').replace('X.X.X.X', hostname).replace('YYY', result.bytes));
            if (result.returnValue) {
                ++success;
                totalTime += result.time;
                min = Math.min(min, result.time);
                max = Math.max(max, result.time);
                $('#pingResult').append('<li' + (isRtlDir() ? ' dir=rtl' : '') + '>' + getLanguageText(languageTable, 'Reply from [X.X.X.X] : bytes = YY, time = ZZms, TTL = WW').replace('X.X.X.X', result.requestAddress).replace('YY', result.bytes).replace('ZZ', result.time).replace('WW', result.ttl) + '</li>');
            } else {
                var orig = result.errorText;
                var trans = getLanguageText(languageTable, orig);
                if (orig == trans) {
                    trans = getLanguageText(languageTable, orig + '.');
                }
                $('#pingResult').append('<li' + (isRtlDir() ? ' dir=rtl' : '') + '>' + trans + '</li>');
            }
            if (cnt != 10) {
                setTimeout(function() {
                    detailPing(hostname, isIPv6, processPing);
                }, 1000);
            } else {
                setTimeout(function() {
                    $('#pingInput').hide();
                    $('#pingProgress').hide();
                    $('#pingEnd').show();
                    $('#pingTimeTitle').show();

                    if (success == 0) {
                        $('#pingTimeBody').text(getLanguageText(languageTable, 'Minimum = XXms, Maximum = YYms, Average = ZZms').replace('XX', '0').replace('YY', '0').replace('ZZ', '0.000'));

                    } else {
                        $('#pingTimeBody').text(getLanguageText(languageTable, 'Minimum = XXms, Maximum = YYms, Average = ZZms').replace('XX', min).replace('YY', max).replace('ZZ', Math.round(totalTime / success * 1000) / 1000));
                    }
                    $('#pingStatTitle').text(getLanguageText(languageTable, 'Ping statistics for [X.X.X.X]').replace('X.X.X.X', hostname));
                    $('#pingStatBody').text(getLanguageText(languageTable, 'Packets : Sent = XX. Received = YY. Lost = ZZ (WW% loss)').replace('XX', 10).replace('YY', success).replace('ZZ', 10- success).replace('WW', (10 - success) * 10));
                }, 1000);
            }
        }

        detailPing(hostname, isIPv6, processPing);
    });

}
