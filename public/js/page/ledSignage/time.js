var ampm = '';
var var_dstStartMonth = '';
var var_dstStartWeek = '';
var var_dstStartWeekday = '';
var var_dstStartHour = '';
var var_dstEndMonth = '';
var var_dstEndWeek = '';
var var_dstEndWeekday = '';
var var_dstEndHour = '';
var agent = navigator.userAgent.toLowerCase();
var ampmArray = ['am', 'pm'];
var monthArray = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
var weekArray = ['1st', '2nd', '3rd', '4th', 'Last'];
var weekDayArray = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

function drawAmpm() {
    var dropDown = createDropDown('ampm', '-', function (selected) {
        if (ampm != selected) {
            clearInterval(currentTimeRefresh);
        }
        ampm = selected;
    });

    for (var i in ampmArray) {
        addDropDownOption(dropDown, ampmArray[i], getLanguageText(languageTable, ampmArray[i].toUpperCase()));
    }

    $('#ampm').html(dropDown);
    setDropButtonText('ampm', ampm);

    if ((agent.indexOf("firefox") != -1) && (window.location.pathname.indexOf("mobile") == -1)) {
        $('#ampm').css('margin-right', 90);
    }
}

function drawDaylightSaving() {
    //start, End Month--------------------------------------------
    var dropDown = createDropDown('dstStartMonth', '-', function (selected) {
        var_dstStartMonth = selected;
    });
    
    for (var i in monthArray) {
        addDropDownOption(dropDown, (Number(i)+1).toString(), getLanguageText(languageTable, monthArray[i]));
    }
    
    $('#dstStartMonth').html(dropDown);

    var dropDown = createDropDown('dstEndMonth', '-', function (selected) {
        var_dstEndMonth = selected;
    });

     for (var i in monthArray) {
        addDropDownOption(dropDown, (Number(i)+1).toString(), getLanguageText(languageTable, monthArray[i]));
    }

     $('#dstEndMonth').html(dropDown);

    //start, End Week---------------------------------------------------
    var dropDown = createDropDown('dstStartWeek', '-', function (selected) {
        var_dstStartWeek = selected;
    });

     for (var i in weekArray) {
        addDropDownOption(dropDown, (Number(i)+1).toString(), getLanguageText(languageTable, weekArray[i]));
    }

    $('#dstStartWeek').html(dropDown);

    dropDown = createDropDown('dstEndWeek', '-', function (selected) {
        var_dstEndWeek = selected;
    });

     for (var i in weekArray) {
        addDropDownOption(dropDown, (Number(i)+1).toString(), getLanguageText(languageTable, weekArray[i]));
    }

    $('#dstEndWeek').html(dropDown);

    //start, End Weekday--------------------------------------------------
    dropDown = createDropDown('dstStartWeekday', '-', function (selected) {
        var_dstStartWeekday = selected;
    });

    for (var i in weekDayArray) {
        addDropDownOption(dropDown, Number(i).toString(), getLanguageText(languageTable, weekDayArray[i]));
    }

    $('#dstStartWeekday').html(dropDown);

    dropDown = createDropDown('dstEndWeekday', '-', function (selected) {
        var_dstEndWeekday = selected;
    });

    for (var i in weekDayArray) {
        addDropDownOption(dropDown, Number(i).toString(), getLanguageText(languageTable, weekDayArray[i]));
    }

    $('#dstEndWeekday').html(dropDown);


    //start, End Hour-----------------------------------------------------
    dropDown = createDropDown('dstStartHour', '-', function (selected) {
        var_dstStartHour = selected;
    });

    for (var i = 0; i < 24; i++) {
        addDropDownOption(dropDown, i, i);
    }

    $('#dstStartHour').html(dropDown);

    dropDown = createDropDown('dstEndHour', '-', function (selected) {
        var_dstEndHour = selected;
    });

    for (var i = 0; i < 24; i++) {
        addDropDownOption(dropDown, i, i);
    }

    $('#dstEndHour').html(dropDown);
}

function initDST() {
    drawDaylightSaving();
    getDSTInfo(function (dstInfo) {
        var_dstStartMonth = dstInfo.dstStartMonth;
        var_dstStartWeek = dstInfo.dstStartWeek;
        var_dstStartWeekday = dstInfo.dstStartDayOfWeek;
        var_dstStartHour = dstInfo.dstStartHour;

        var_dstEndMonth = dstInfo.dstEndMonth;
        var_dstEndWeek = dstInfo.dstEndWeek;
        var_dstEndWeekday = dstInfo.dstEndDayOfWeek;
        var_dstEndHour = dstInfo.dstEndHour;

        $('#useDST').attr('checked', dstInfo.dstMode == "on" ? true : false);
        $('#useDST').change();

        setDropButtonText('dstStartMonth', dstInfo.dstStartMonth);
        setDropButtonText('dstStartWeek', dstInfo.dstStartWeek);
        setDropButtonText('dstStartWeekday', dstInfo.dstStartDayOfWeek);
        setDropButtonText('dstStartHour', dstInfo.dstStartHour);

        setDropButtonText('dstEndMonth', dstInfo.dstEndMonth);
        setDropButtonText('dstEndWeek', dstInfo.dstEndWeek);
        setDropButtonText('dstEndWeekday', dstInfo.dstEndDayOfWeek);
        setDropButtonText('dstEndHour', dstInfo.dstEndHour);
    });
}

function checkDateValid() {

    var number1to12Regex = /(^[1-9]{1}$|^[1]{1}[0-2]{1}$)/;
    var number0to23Regex = /(^[0-9]{1}$|^[1]{1}[0-9]{1}$|^[2]{1}[0-3]{1}$)/;
    var number1to31Regex = /(^[1-9]{1}$|^[1-2]{1}[0-9]{1}$|^[3]{1}[0-1]{1}$)/;
    var number1to59Regex = /(^[1-9]{1}$|^[0-5]{1}[0-9]{1}$)/;
    // To change year can be from current year to 2037.
    // This is same with setting of set.
    var number2018to2037Regex = /(^[2]{1}[0]{1}[1]{1}[8-9]{1}$|^[2]{1}[0]{1}[2]{1}[0-9]{1}$|^[2]{1}[0]{1}[3]{1}[0-7]{1}$)/;
    var maxDaysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    if (!number2018to2037Regex.test($('#year').val())) {
        alert(getLanguageText(languageTable, "Invalid year value"));
        return false;
    }

    if ($('#year').val() % 4 == 0 && $('#year').val() % 100 != 0 || $('#year').val() % 400 == 0)
        maxDaysInMonth[1] = 29;

    if (!number1to12Regex.test($('#month').val())) {
        alert(getLanguageText(languageTable, "Invalid month value"));
        return false;
    }

    if (( !number1to31Regex.test($('#day').val()) ) || ( $('#day').val() <= 0 || $('#day').val() > maxDaysInMonth[$('#month').val() - 1] )) {
        alert(getLanguageText(languageTable, "Invalid day value"));
        return false;
    }

    if (!number1to12Regex.test($('#hour').val())) {
        alert(getLanguageText(languageTable, "Invalid hour value"));
        return false;
    }

    if (!number1to59Regex.test($('#minute').val())) {
        alert(getLanguageText(languageTable, "Invalid minute value"));
        return false;
    }

    return true;

}

function addZero(n, digits) {
    var zero = '';
    n = n.toString();

    if (n.length < digits) {
        for (var i = 0; i < digits - n.length; i++)
            zero += '0';
    }
    return zero + n;
}

function initCurrentTime(callback) {
    getCurrentTime(function (msg) {
        $('#currentTime').html(msg.current);
        $('#year').val(msg.year);
        $('#month').val(msg.month);
        $('#day').val(msg.day);

        ampm = msg.hour < 12 ? 'am' : 'pm';
        setDropButtonText('ampm', ampm);

        $('#hour').val((msg.hour % 12) == 0 ? 12 : (msg.hour % 12));
        msg.minute.toString().length == 1 ? $('#minute').val('0' + msg.minute) : $('#minute').val(msg.minute);

        if (callback instanceof Function) {
            callback();
        }
    });
}

function timeRefreshStop() {
    $("input[type=text]").keydown( function(ret) {
        clearInterval(currentTimeRefresh);
    });
}

$(document).ready(function () {

    $("#useDST").change(function () {
        $('.dstTable').css('display', $("#useDST").prop('checked') ? 'table' : 'none');
        setDisableControlElement(true);
        setDstOnOff($("#useDST").prop('checked') ? 'on' : 'off', function () {
            initCurrentTime(function () {
                setDisableControlElement(false);
            });
        });
    });

    addConfirmModal('applyTimeModal', getLanguageText(languageTable, "Apply?"), '', function () {
        setDisableControlElement(true);
        setManualTimeSet(function(ret) {
        });
        var currentHour = ampm == "am" ? $('#hour').val() : parseInt($('#hour').val()) + 12;
        if ($('#hour').val() == 12)
            currentHour -= 12;

        var timeInfo = {
            hour: addZero(currentHour, 2),
            minute: addZero($('#minute').val(), 2),
            year: $('#year').val(),
            month: addZero($('#month').val(), 2),
            day: addZero($('#day').val(), 2)
        };
        setCurrentTime(timeInfo);

        if ($("#useDST").prop('checked')) {
            var Month = var_dstStartMonth;
            var Week = var_dstStartWeek;
            var Weekday = var_dstStartWeekday;
            var Hour = var_dstStartHour;
            setDstStartTime(Month, Week, Weekday, Hour);

            Month = var_dstEndMonth;
            Week = var_dstEndWeek;
            Weekday = var_dstEndWeekday;
            Hour = var_dstEndHour;
            setDstEndTime(Month, Week, Weekday, Hour);
        }
        setTimeout(function () {
            initCurrentTime(function () {
                setDisableControlElement(false);
            });
        }, 500);

        clearInterval(currentTimeRefresh);
        currentTimeRefresh = setInterval( function() {
            initCurrentTime();
        }, 10000);
    }, function () {
        initCurrentTime();
        if ($("#useDST").prop('checked')) {
            initDST();
        }
    });
    $('#applyTimeModalContent').html(getLanguageText(languageTable, "Changing Time may cause you to be logged out.") + "<br>");

    $('#time-apply').click(function () {
        if (!checkDateValid()) {
            initCurrentTime();
            return;
        }
        $('#applyTimeModal').modal();
    });

    currentTimeRefresh = setInterval( function() {
        initCurrentTime();
    }, 10000);
    drawAmpm();
    initDST();

    if (locale == "ar-SA") {
        changeRtl();
    }

    timeRefreshStop();
});


function setDisableControlElement(disable) {
    $("input[type=checkbox]").prop("disabled", disable);
    $('#time-apply').prop("disabled", disable);
}

function changeRtl() {
    $(".dropdown-ampm").css({"float": "left", "margin-right": "0", "margin-left": "120px"});
    $(".dropdown-ampm.mobile").css({"float": "left", "margin-right": "0", "margin-left": "0px"});
}
