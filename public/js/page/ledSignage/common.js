var regExps = {
    ipv4: new RegExp('^([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$'),
    subnetMask: new RegExp('^(((128|192|224|240|248|252|254)\\.0\\.0\\.0)|(255\\.(0|128|192|224|240|248|252|254)\\.0\\.0)|(255\\.255\\.(0|128|192|224|240|248|252|254)\\.0)|(255\\.255\\.255\\.(0|128|192|224|240|248|252|254|255)))$'),
    ipv6: new RegExp('^(([0-9a-fA-F]{1,4}:){7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|::(ffff(:0{1,4}){0,1}:){0,1}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))$'),
    subnetPrefixLength: new RegExp('^([1-9]|[1-9][0-9]|1[0-1][0-9]|12[0-7])$')
}

function checkIP(ip, isV6) {
    if (isV6) {
        return regExps.ipv6.test(ip);
    }
    return regExps.ipv4.test(ip);
}

function checkNetMask(netMask) {
    return regExps.subnetMask.test(netMask);
}

function checkNetPrefix(netPrefix) {
    return regExps.subnetPrefixLength.test(netPrefix);
}

function isRtlDir() {
    return localeInfo.locales.UI == 'ar-SA';
}

function changeRtlDir() {
    console.log(localeInfo);
    if (isRtlDir()) {
        $('.txt_error_msg').attr('dir', 'rtl');
        $('#pingStatTitle').attr('dir', 'rtl');
        $('#pingStatBody').attr('dir', 'rtl');
        $('#pingTimeBody').attr('dir', 'rtl');
        $('#gridSystemModalLabel').attr('dir', 'rtl');
        $('span').attr('dir', 'rtl');
        // $('li').attr('dir', 'rtl');
        $("h3").css("text-align", "left");
        $("h4").css("text-align", "left");
        $("table tr").css("text-align", "left");
        $('#currentPin').attr("dir", "ltr");
    }
}
