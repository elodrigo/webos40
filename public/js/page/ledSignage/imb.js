var imbIndex = 1;
var marriageIndex = 2;

var remain = 60000;
var marrigeInterval = undefined;
var imbColor = "";
var marriageColor = "";
var imbString = "";
var marriageString = "";
var secureSiliconStatus = "";
var imbTimeout = undefined;
var marriageTimeout = undefined;
var initialMarriageTime = 0;

function addTimeoutModal() {
     var html
        = '<div class="modal-content" id=marriageTimeOut>'
            + '<div class="modal-header">'
                + '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
            + '</div>'
            + '<div class="modal-body">'
                + '<table style="border:0px;">'
                    + '<tr><th style="border-top: none;"><font color="#F2F2F2">' + getLanguageText(languageTable, 'You did not finish the process in time.') + '</font></th></tr>'
                    + '<tr><th style="border-top: none;"><font color="#F2F2F2">' + getLanguageText(languageTable, 'Please restart the marriage authentication process.') + '</font></th></tr>'
                +'</table>'
            + '</div>'
            + '<div class="modal-footer">'
                + '<button class="cardBtn" data-dismiss="modal">' + getLanguageText(languageTable, 'OK') + '</button>'
            + '</div>'
        +'</div>'
    return html;
}

function addPhysicalMarriageModal() {
    var html
        = '<div class="modal-content" id=physicalMarriage>'
            + '<div class="modal-header">'
                + '<button type="button" class="close" name="btnTimerResetOn" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
                    + '<h4 class="modal-title" id="gridSystemModalLabel">' + getLanguageText(languageTable, 'Step 1. Physical Marriage') + '</h4>'
                    + '<br/>'
                    + '<div name="txtMarriageTimer" class="timer-cinema">&nbsp;</div>'
            + '</div>'
            + '<div class="modal-body">'
                + '<table style="border:0px;">'
                    + '<tr><th style="border-top: none;"><font color="#F2F2F2">' + getLanguageText(languageTable, 'Please press the physical marriage button.') + '</font></th></tr>'
                +'</table>'
            + '</div>'
            + '<div class="modal-footer">'
                + '<button class="cardBtn" name="btnTimerResetOn">' + getLanguageText(languageTable, 'Cancel') + '</button>'
            + '</div>'
        +'</div>'
    return html;
}

function addMarriageGuideModal() {
    var html
        = '<div class="modal-content" id=marriageGuide>'
            + '<div class="modal-header">'
                + '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
                    + '<h4 class="modal-title" id="gridSystemModalLabel">' + getLanguageText(languageTable, 'Marriage Guide (2 Steps)') + '</h4>'
            + '</div>'
            + '<div class="modal-body">'
                + '<table style="border:0px;">'
                    + '<tr><th style="border-top: none;"><font color="#F2F2F2">' + getLanguageText(languageTable, 'Step 1. Physical Marriage') + '</font></th></tr>'
                + '<tr id="addColonPhysical"><th style="border-top: none; text-indent: 50px;"><font color="#F2F2F2">' + getLanguageText(languageTable, 'Press the physical marriage button on your master cabinet.') + '</font></th></tr>'
                + '<tr><th style="border-top: none;"><font color="#F2F2F2">' + getLanguageText(languageTable, 'Step 2. Logical Marriage') + '</font></th></tr>'
                + '<tr id="addColonLogical"><th style="border-top: none; text-indent: 50px;"><font color="#F2F2F2">' + getLanguageText(languageTable, 'Log in with the authorized ID and password.') + '</font></th></tr>'
                + '<tr style="height: 30px;"></tr>'
                + '<tr><th style="border-top: none;"><font color="#F2F2F2">' + getLanguageText(languageTable, 'Pressing Start will give you 1 minute to perform the process.') + '</font></th></tr>'
                +'</table>'
            + '</div>'
            + '<div class="modal-footer">'
                + '<button class="cardBtn" data-dismiss="modal" style="margin-left:10px;">' + getLanguageText(languageTable, 'Cancel') + '</button>'
                + '<button class="cardBtn" id=marriageStart >' + getLanguageText(languageTable, 'Start') + '</button>'
            + '</div>'
        +'</div>'
    return html;
}

function addTimeZoneSetErrorModal() {
    var html
        = '<div class="modal-content" id=timeZoneSetError>'
            + '<div class="modal-header">'
                + '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
                    + '<h4 class="modal-title" id="gridSystemModalLabel">' + getLanguageText(languageTable, 'Time & Date Settings Required') + '</h4>'
            + '</div>'
            + '<div class="modal-body">'
                + '<table style="border:0px;">'
                    + '<tr><th style="border-top: none;"><font color="#F2F2F2">' + getLanguageText(languageTable, 'Your device needs the correct time & date to authenticate marriage.')
                    + '<br/>' + getLanguageText(languageTable, 'Do you want to set the time & date?') + '</font></th></tr>'
                +'</table>'
            + '</div>'
            + '<div class="modal-footer">'
                + '<button class="cardBtn" id=timeSet style="margin-left:10px;">' + getLanguageText(languageTable, 'Time & Date Settings') + '</button>'
                + '<button class="cardBtn" data-dismiss="modal">' + getLanguageText(languageTable, 'NO') + '</button>'
            + '</div>'
        +'</div>'
    return html;
}

function addAuthenticateSuccessModal() {
    var html
        = '<div class="modal-content" id=authenticateSuccess>'
            + '<div class="modal-header">'
                + '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
            + '</div>'
            + '<div class="modal-body">'
                + '<table style="border:0px;">'
                    + '<tr><th style="border-top: none;"><font color="#F2F2F2">' + getLanguageText(languageTable, 'Authentication completed.') + '</font></th></tr>'
                + '</table>'
            + '</div>'
            + '<div class="modal-footer">'
                + '<button class="cardBtn" id=authenticateSuccess>' + getLanguageText(languageTable, 'OK') + '</button>'
            + '</div>'
        +'</div>'
    return html;
}

function addAuthenticateFailModal() {
    var html
        = '<div class="modal-content" id=authenticateFail>'
            + '<div class="modal-header">'
                + '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
                + '<h4 class="modal-title" id="gridSystemModalLabel">' + getLanguageText(languageTable, 'Authentication Failed') + '</h4>'
            + '</div>'
            + '<div class="modal-body">'
                + '<table style="border:0px;">'
                    + '<tr><th style="border-top: none;"><font color="#F2F2F2">' + getLanguageText(languageTable, 'Please check the following items and try again.') + '</font></th></tr>'
                    + '<tr style="height: 20px;"></tr>'
                    + '<tr id="addColonTime"><th style="border-top: none;"><font color="#F2F2F2">' + getLanguageText(languageTable, 'Time & Date Settings') + '</font></th></tr>'
                    + '<tr id="addColonIMB"><th style="border-top: none;"><font color="#F2F2F2">' + getLanguageText(languageTable, 'IMB Status') + '</font></th></tr>'
                    + '<tr id="addColonID"><th style="border-top: none;"><font color="#F2F2F2">' + getLanguageText(languageTable, 'ID/password') + '</font></th></tr>'
                + '</table>'
            + '</div>'
            + '<div class="modal-footer">'
                + '<button class="cardBtn" data-dismiss="modal" id=authenticateFailed>' + getLanguageText(languageTable, 'OK') + '</button>'
            + '</div>'
        +'</div>'
    return html;
}

function addTimerResetModal() {
     var html
        = '<div class="modal fade" tabindex="-1" role="dialog" id="timeResetModal">'
            + '<div class="modal-dialog" style="height: 60%; width: 70%;">'
                +'<div class="modal-content" id=timerReset>'
                    + '<div class="modal-header">'
                    + '</div>'
                    + '<div class="modal-body">'
                        + '<table style="border:0px;">'
                            + '<tr><th style="border-top: none;"><font color="#F2F2F2">' + getLanguageText(languageTable, 'The timer is reset and the marriage process must be restarted from the beginning.') + '</font></th></tr>'
                        +'</table>'
                    + '</div>'
                + '<div class="modal-footer">'
                    + '<button class="cardBtn" id=timeResetChk>' + getLanguageText(languageTable, 'OK') + '</button>'
                + '</div>'
                +'</div>'
            +'</div>'
        +'</div>'

    var modal = $(html);
    $('body').append(modal);
}

function addAuthenticateModal() {
    var title = getLanguageText(languageTable, "Step 2. Logical Marriage");
    var html
        = '<div class="modal fade" tabindex="-1" role="dialog" id="authenticateModal">'
            + '<div class="modal-dialog" style="height: 60%; width: 70%;">'
                + '<div class="modal-content" id=authenticateCheck>'
                    + '<div class="modal-header">'
                        + '<button type="button" class="close" name="btnTimerResetOn" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
                        + '<h4 class="modal-title" id="gridSystemModalLabel">' + title + '</h4>'
                        + '<br/>'
                        + '<div name="txtMarriageTimer" class="timer-cinema">&nbsp;</div>'
                    + '</div>'
                    + '<div class="modal-body">'
                        + '<table  style="color: white; width:60%;">'
                            + '<tr><th colspan="3" style="border-top: none;"><font color="#F2F2F2">' + getLanguageText(languageTable, 'Please enter the authorized ID and password.') + '</font></th></tr>'
                            + '<tr style="height: 20px;"></tr>'
                            + '<tr><th style="border-top: none;">' + getLanguageText(languageTable, 'ID') + '</th><td></td>'
                            + '<td><div class="box_input_pin"><input class="input_pin" type="text" id="imbId" style="text-align: left;"></div></td>'
                            + '</tr>'
                            + '<tr><th style="border-top: none;">' + getLanguageText(languageTable, 'Password') + '</th><td></td>'
                            + '<td><div class="box_input_pin"><input class="input_pin" type="password" id="imbPassword" style="text-align: left;"></div></td>'
                            + '</tr>'
                        +'</table>'
                    + '</div>'
                    + '<div class="modal-footer">'
                        + '<button class="cardBtn" name="btnTimerResetOn" style="margin-left:10px;">' + getLanguageText(languageTable, 'Cancel') + '</button>'
                        + '<button class="cardBtn" id=accountChk>' + getLanguageText(languageTable, 'OK') + '</button>'
                    + '</div>'
            + '</div>'
                + addTimeoutModal()
                + addPhysicalMarriageModal()
                + addMarriageGuideModal() 
                + addTimeZoneSetErrorModal()
                + addAuthenticateSuccessModal()
                + addAuthenticateFailModal()
        + '</div>'
    + '</div>';

    var modal = $(html);
    $('body').append(modal);
}

function refreshPinPassword() {
    $(".input_pin").val("");
}

function imbAccountChk() {
    setImbLogin($('#imbId').val(), $('#imbPassword').val(), function(msg1) {
        $('#authenticateCheck').hide();
        if (msg1.returnValue && msg1.result.status == "success") {
            setImbMarriage( function(msg2) {
                if (msg2.returnValue && msg2.result.status == "success") {
                    $('#authenticateSuccess').show();
                    // initial Timer & Stirng
                    $('#imbStatus').html("");
                    $('#marriageStatus').html("");
                    clearTimeout(imbTimeout);
                    imbTimeout = undefined;
                    clearTimeout(marriageTimeout);
                    marriageTimeout = undefined;

                    getImbStatusOnLoad(0);
                    setImbLogout( function(msg3) {
                    });
                } else {
                    $('#authenticateFail').show();
                }
            });
        } else {
            $('#authenticateFail').show();
        }
    });
}

function statusEffect(index, text) {
    if (index == imbIndex) {
        text = "<I>" + imbString + "</I>" + text;
        text = "<font color='" + imbColor + "'>●&nbsp;&nbsp;</font>" + text;
    } else if (index == marriageIndex) {
        text = "<I>" + marriageString + "</I>" + text;
        text = "<font color='" + marriageColor + "'>●&nbsp;&nbsp;</font>" + text;
    }
    return text;
}

function imbStatusProcess(index, imbStatus) {
    var imbStringVal = "";
    var imbColorVal = "";
    
    if (imbStatus == 'green') {
        imbStringVal = "OK";
        imbColorVal = "#00FF00";
    } else {
        imbStringVal = "NG"
        imbColorVal = "#FF0000";
    }

    if (index == imbIndex) {
       imbString = imbStringVal;
       imbColor = imbColorVal;
    } else if (index == marriageIndex) {
       marriageString = imbStringVal;
       marriageColor = imbColorVal;
    }
}

function getSecureSiliconStatusResponse() {
    getSecureSiliconStatus( function(msg) {
        if (msg.returnValue) {
            secureSiliconStatus = msg.result.status;
        } else {
            secureSiliconStatus = false;
        }
    });
}

function getImbStatusOnLoad(delayTime) {
    getImbStatus(imbIndex, function(msg1) {
        imbTimeout = setTimeout ( function() {
            getImbStatus(marriageIndex, function(msg2) {
                if ((msg1.returnValue && msg1.result.status == 'green') && (msg2.returnValue && msg2.result.status == 'green')) {
                    $('#imbAuthenticate').prop('disabled', true);
                } else if (msg1.returnValue && msg1.result.status == 'green') {
                    $('#imbAuthenticate').prop('disabled', false);
                } else {
                    $('#imbAuthenticate').prop('disabled', true);
                }

                // imb Status
                if (msg1.returnValue) {
                    imbStatusProcess(imbIndex, msg1.result.status);
                } else {
                    imbStatusProcess(imbIndex, 'red');
                }
                // marriage Status
                if (msg2.returnValue) {
                    imbStatusProcess(marriageIndex, msg2.result.status);
                } else {
                    imbStatusProcess(marriageIndex, 'red');
                }
                $('#imbStatus').html(statusEffect(imbIndex, ""));
                $('#marriageStatus').html(statusEffect(marriageIndex, ""));
                marriageTimeout = setTimeout ( function() {
                    getImbStatusOnLoad(5);
                }, 5 * 1000);
            });
        }, delayTime * 1000);
    });
}

function timeResetModalOn(){
    if (0 < remain && remain < 60000) {
        if((event.ctrlKey == true && (event.keyCode == 78 || event.keyCode == 82)) || (event.keyCode == 116)) {
            event.keyCode = 0;
            event.cancelBubble = true;
            event.returnValue = false;

            $('#timeResetModal').modal({
                backdrop: 'static',
                keyboard: false
            });
		}
    }
}

function updateRestrictedMsg(node, remainTime) {
    var msg = "";
    var sec = Math.floor(remainTime / 1000);
    var centisec = Math.floor(remainTime - (sec * 1000));
    centisec = Math.floor(centisec/10);

    if (sec < 10) {
        sec = "0" + sec;
    }
    if (centisec < 10) {
        centisec = "0" + centisec;
    }

    msg = "00:00:00";
    msg = msg.replace("00:00:00", "00:" + sec + ":" + centisec);
    node.html(msg);
}

function addPhysicalMarriageSocket() {
    //physical Button Test
    socket.on('buttonStatus', function(ret) {
        if (0 < remain && remain < 60000) {
            $('#physicalMarriage').hide();
            $('#authenticateCheck').show();
        }
    });
}

function marriageTimer() {
    updateRestrictedMsg($('[name="txtMarriageTimer"]'), remain);
    addPhysicalMarriageSocket();

    if (marrigeInterval == undefined) {
		startMarriageTimer(true, function() {
        });

        initialMarriageTime = Date.now();
        marrigeInterval = setInterval(function() {
            var currentTime = Date.now();
            remain = remain - (currentTime - initialMarriageTime);
            initialMarriageTime = currentTime;
            updateRestrictedMsg($('[name="txtMarriageTimer"]'), remain);

            if (remain <= 0) {
                clearInterval(marrigeInterval);
                marrigeInterval = undefined;
                $('#physicalMarriage').hide();
                $('#authenticateCheck').hide();
                $('#marriageTimeOut').show();
                socket.removeListener('buttonStatus');
				startMarriageTimer(false, function() {
                    remain = 60000;
                });
			}
        }, 10);
    }
}

function exitToImbPage() {
    console.log("Exit To IMB");
    clearInterval(marrigeInterval);
    marrigeInterval = undefined;
    startMarriageTimer(false, function() {
        remain = 60000;
    });
}

function initTimerResetModal() {
    $('#timeResetChk').click(function() {
        $('#timeResetModal').modal('hide');
        $('#authenticateModal').modal('hide');
        remain = 60000;
        clearInterval(marrigeInterval);
        marrigeInterval = undefined;
		startMarriageTimer(false, function() {
		});
    });
}

function initAuthenticateModal() {
    $('#imbAuthenticate').click(function() {
        $('#marriageGuide').hide();
        $('#authenticateCheck').hide();
        $('#authenticateSuccess').hide();
        $('#authenticateFail').hide();
        $('#timeZoneSetError').hide();
        $('#marriageTimeOut').hide();
        $('#physicalMarriage').hide();
        $('#authenticateModal').modal({
            backdrop: 'static',
            keyboard: false
        });

        if (imbString !== 'OK') {
            $('#authenticateFail').show();
            return;
        }

        isManualTimeSet( function(ret) {
            if (ret) {
                $('#marriageGuide').show();
            } else {
                $('#timeZoneSetError').show();
            }
        });
        refreshPinPassword();
    });

    $('#accountChk').click(function() {
        clearInterval(marrigeInterval);
        marrigeInterval = undefined;
        imbAccountChk();
		startMarriageTimer(false, function() {
		    remain = 60000;
        });
     });

    $('#authenticateSuccess').click(function() {
        $('#authenticateModal').modal('hide');
    });

    $('#marriageStart').click(function() {
        $('#marriageGuide').hide();
        $('#physicalMarriage').show();
        marriageTimer();
    });
    
    $("button[name='btnTimerResetOn']").click(function() {
        socket.removeListener('buttonStatus');
        $('#timeResetModal').modal({
            backdrop: 'static',
            keyboard: false
        });
    });

    $('#timeSet').click(function() {
        location.href = "/ledSignage/time";
    });

    window.onbeforeunload = exitToImbPage;
}

function changeRtlCinema() {
    if (localeInfo.locales.UI == "ar-SA") {
        $("h3").css("text-align", "left");
        $("h3").attr("dir", "rtl");
        $("h4").css("text-align", "left");
        $("h4").attr("dir", "rtl");
	    $("table tr").attr("dir", "rtl");
        $("table tr").css("text-align", "left");
	    $("#addColonPhysical font").append(": ");
	    $("#addColonLogical font").append(": ");
	    $("#addColonTime font").append("- ");
	    $("#addColonIMB font").append("- ");
	    $("#addColonID font").append("- ");
    } else {
	    $("#addColonPhysical font").prepend(": ");
        $("#addColonLogical font").prepend(": ");
	    $("#addColonTime font").prepend("- ");
	    $("#addColonIMB font").prepend("- ");
	    $("#addColonID font").prepend("- ");
    }
}
