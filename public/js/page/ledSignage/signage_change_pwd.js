/**
 * Created by byungjin.park on 17. 5. 8.
 */
var currentPin = "";
var newPin = "";
var confirmPin = "";

var NEW_PIN_GUIDE_MSG = getLanguageText(languageTable, "To make your password more secure, use a mixture of mixed-case letters, numbers and special characters");

var socket = io();
initSocket();

$(document).ready(function () {
    applyLanguageText();
    changeRtlDir();
    initNewPin();
});

$(window).load(function () {
    setActiveForTouch();
});

function setActiveForTouch() {
    $(".btn_default").on("touchstart", function (event) {
        $(this).addClass("active");
    });
    $(".btn_default").on("touchend", function (event) {
        $(this).removeClass("active");
    });
}

function passwdCheck(newPasswd) {
    return /[\@\#\$\%\^\&\*\(\)\_\+\!]/.test(newPasswd) && /[a-z]/.test(newPasswd) && /[0-9]/.test(newPasswd) && /[A-Z]/.test(newPasswd);
}

function changePassword() {
    console.log("changePassword clicked");
    currentPin = $("#currentPin .input_pin").val();
    newPin = $("#newPin .input_pin").val();
    confirmPin = $("#confirmPin .input_pin").val();

    if (newPin.length < 8) {
        $(".txt_error_msg").text(getLanguageText(languageTable, "Invalid New password. Password must be at least 8 characters."));

    } else if (!passwdCheck(newPin)) {
        $(".txt_error_msg").text(getLanguageText(languageTable, "Invalid New password. You must use a mixture of mixed-case letters, numbers and special characters."));
    } else if (newPin != confirmPin) {
        $(".txt_error_msg").html(getLanguageText(languageTable, "Validation Error.") + "<br>" + getLanguageText(languageTable, "New Password and Confirm Password do not match."));
    } else {
        socket.emit("changePassword", {
            currentPasswd: currentPin,
            newPasswd: newPin
        });
    }
}

function goBack() {
    history.back();
}

function applyLanguageText() {
    $(".text_header_title .txt_change_password").text($(".text_header_title .txt_change_password").text().toUpperCase());
    $(".btn_bottom_menu .txt_cancel").text($(".btn_bottom_menu .txt_cancel").text().toUpperCase());
}

function initSocket() {
    socket.on('connect', function (socket) {
        console.log('client Connected!');
    });

    socket.on('disconnect', function (socket) {
        console.log('client Disconnected!');
    });

    socket.on("changePassword", function (msg) {
        if (msg == "success") {
            alert(getLanguageText(languageTable, "Your Password has been changed."));
            window.location = '/ledSignage/language'
        } else if (msg == "fail_same") {
            $(".txt_error_msg").text(getLanguageText(languageTable, "You've entered the same password as the current one. Please enter a different password."));
        } else if (msg == "fail_incorrect") {
            $(".txt_error_msg").text(getLanguageText(languageTable, "Incorrect Password entered. Please try again."));
            return;
        }
    });
}

// Device is connected firstly to set ip, focus/blue event is not occurred.
// This api include exception handling about it.
function initNewPin() {
    var setMsg = function () {
        var size = $("#newPin .input_pin:focus").size();
        if (size > 0) {
            $(".txt_error_msg").text(NEW_PIN_GUIDE_MSG);
        } else {
            $(".txt_error_msg").text("");
        }
    };

    $(".input_pin").click(function () {
        var parentId = $(this).closest("div").attr("id");
        setTimeout(function () {
            if (parentId === "newPin") {
                $(".txt_error_msg").text(NEW_PIN_GUIDE_MSG);
            } else {
                $(".txt_error_msg").text("");
            }
        }, 0);
    });

    $("#newPin .input_pin").on("focus", function () {
        setMsg();
    });

    $("#newPin .input_pin").on("blur", function () {
        setMsg();
    });
}
