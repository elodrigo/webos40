var updateStarted = false;
var updatePercent = 0;

function initFactoryReset() {
    $('#factoryResetBtn').click(function() {
        $('#factoryResetModal').modal();
    });

    addLedConfirmModal('factoryResetModal', getLanguageText(languageTable, "Factory Reset"),
                    getLanguageText(languageTable, "All settings will be reset. Do you want to continue?"), function () {
        overlayControlLed(true, getLanguageText(languageTable, "Factory Reset") + "......");

        setTimeout(doFactoryDefault, 1000);

        setTimeout(function () {
            window.location.replace("/login");
        }, 60 * 1000);
    });
}

function initReboot() {
    $('#rebootBtn').click(function() {
        $('#rebootModal').modal();
    });

    addLedConfirmModal('rebootModal', getLanguageText(languageTable, "System Reboot"),
                    getLanguageText(languageTable, "System controller will restart. Do you want to continue?"), function () {
        overlayControlLed(true, getLanguageText(languageTable, "Rebooting") + "......");

        rebootScreen('commerReset');

        setTimeout(function () {
            window.location.replace("/login");
        }, 60 * 1000);
    });
}

function enableUserInput(enable) {
    document.onkeydown = function (e) {
        return enable;
    }

    $('input').prop('disabled', !enable);
    $('button').prop('disabled', !enable);

}

function uploadAfterCheckLogin(file) {
    checkLogin(function(ret) {
        if (!ret.login) {
            console.log('session expired');
            window.location.replace("/login");
            return;
        }
        uploadFile(file);
    });
}

function checkLogin(callback) {
     jQuery.ajax({
         type:"GET",
         url:"/checkLoginLed",
         dataType:"JSON",
         success : function(data) {
             callback(data);
         }, complete : function(data) {
             console.log(data);
         }, error : function(xhr, status, error) {
             console.log(status);
         }
     });
}

function uploadFile(file) {
    var formData = new FormData();
    var maxSize = env.maxSize;
    formData.append('update', file);
    formData.append('target', 'update');

    console.log(file.size);
    if (file.size > maxSize) {
        showWarningModal(getLanguageText(languageTable, 'Flash memory is not enough.'), 5 * 1000);
        return;
    }

    $('#upload-prog').show();
    overlayControlLed(true, getLanguageText(languageTable, "Uploading") + "\n" + file.name);
    enableUserInput(false);

    var xhr = new XMLHttpRequest();

    xhr.open('post', '/upload', true);
    //xhr.setRequestHeader("Content-type","multipart/form-data");

    xhr.upload.onprogress = function (e) {
        if (e.lengthComputable) {
            var per = Math.round((e.loaded / e.total) * 100)
            $('#uploadProgress').html(per + "%");
            $('#uploadProgress').css('width', per + '%');
            $('#overlay_progress').html(per + "%");
        }
    };

    var reloadTimer = undefined;
    xhr.upload.onload = function (e) {
        reloadTimer = setTimeout(function () {
            overlayControlLed(false);
            if ($('#micomUpdate').prop('checked')) {
                location.href = "/ledSignage/system?micom=on";
            } else {
                location.href = "/ledSignage/system";
            }
        }, 2000);
    }

    xhr.onerror = function (e) {
        overlayControlLed(false);
        showWarningModal(getLanguageText(languageTable, "An error occurred while submitting the form."), 5 * 1000);
    };

    xhr.onload = function () {
        if (this.status == 406) {
            if (reloadTimer) {
                clearTimeout(reloadTimer);
            }
            overlayControlLed(false);
            enableUserInput(true);
            document.getElementById('upload').value = '';
            showWarningModal(getLanguageText(languageTable, "Unsupported File type."));
        }
    };

    xhr.send(formData);
}

function addUpdateHandler() {
    socket.on('swupdate', function (msg) {
        var micomProgress = -1;
        if (!updateStarted) {
            getMicomUpdateProgress(function (msg) {
                if (micomProgress === -1) {
                    $('.progress-bar').removeClass("active progress-bar-striped");
                    $('#update-prog').hide();
                    $('#micom-prog').show();
                }

                micomProgress = msg.progress;

                $('#micomProgressTitle').html(getLanguageText(languageTable, 'Micom Progress').replace('Micom', msg.type));
                $('#micomProgress').html(msg.progress + "%");
                $('#micomProgress').css('width', msg.progress + '%');
                $('#overlay_progress').html(msg.type + " " + getLanguageText(languageTable, "Update") + " : " + msg.progress + "%");
            });
        }

        updateStarted = true;

        if (updatePercent > 0 && msg == 0) {
            overlayControlLed(false);
            showWarningModal(getLanguageText(languageTable, "Fail to update. Check xxx file please.").replace(/xxx/gi, fileName));
            return;
        }

        if (updatePercent > 0 && updatePercent == msg) {
            return;
        }

        updatePercent = msg;

        $('#updateProgress').html(msg + "%");
        $('#overlay_progress').html(msg + "%");
        $('#updateProgress').css('width', msg + '%');

        if (msg == 100) {
            var updateCompletedMsg = getLanguageText(languageTable, "S/W Update succeeded");
            overlayControlLed(false);
            overlayControlLed(true, updateCompletedMsg);

            setTimeout(function () {
                setSWUpdateStatus(false);
                window.location.replace("/login");
            }, 2 * 60 * 1000);
        }
    });
}

function addUpdateModal() {
    addLedConfirmModal('updateModal', getLanguageText(languageTable, "S/W will update. Do you want to continue?"), '', function () {
        $('#update-prog').show();
        $('.progress-bar').addClass("active progress-bar-striped");

        overlayControlLed(true, getLanguageText(languageTable, "S/W Updating") + "......<br>" + updateMsg);

        var micomUpdate = $('#micomUpdate').prop('checked');

        updatePercent = 0;
        updateStarted = false;

        swupdate(fileName, micomUpdate, function (m) {
            if (m.returnValue) {
                setSWUpdateStatus(true);
            }
        });

        setTimeout(function () {
            getUpdateStatus(function (msg) {
                if (msg.status != 'in progress' && !updateStarted) {
                    overlayControlLed(false);
                    showWarningModal(getLanguageText(languageTable, "Cannot start S/W update. Check xxx file please.").replace(/xxx/gi, fileName));
                }
            });
        }, 10 * 1000);
    });

    $(".update").click(function () {
        fileName = $(this).attr('name');
        var newVer = $(this).attr('id');
        if (newVer === undefined) {
            newVer = '-';
        }
        var modalMsg = getLanguageText(languageTable, "Do you want to install this S/W Update file?") + "<br>";
        modalMsg += "(" + getLanguageText(languageTable, "Newer Ver.") + ") " + newVer + "<br>";
        if (env.supportCinemaModel && newVer.substr(0,2) == "00") {
            modalMsg += "(" + getLanguageText(languageTable, "Current Ver.") + ") " + fpgaVer + "<br>";
        } else {
            modalMsg += "(" + getLanguageText(languageTable, "Current Ver.") + ") " + currentVer + "<br>";
        }
        $('#updateModalContent').html(modalMsg);
        $('#updateModal').modal();
    });
}

function addDeleteModal() {
    addLedConfirmModal('deleteModal', getLanguageText(languageTable, "Delete the file"), '', function () {
        deleteFile("update", fileName, function () {
            if ($('#micomUpdate').prop('checked')) {
                location.href = "/ledSignage/system?micom=on";
            } else {
                location.href = "/ledSignage/system";
            }
        });
    });


    $(".delete").click(function () {
        fileName = $(this).attr('name');
        $('#deleteModalContent').attr("style", "word-break:break-all");
        $('#deleteModalContent').attr("wrap", "hard");
        $('#deleteModalContent').html(getLanguageText(languageTable, "Do you want to delete this S/W Update file?") + "<br>" + fileName);
        $('#deleteModal').modal();
    });
}

function addInfoToUpdateList() {
    listUpdates(function (msg) {
        if (!msg || !msg.returnValue) {
            return;
        }

        msg.fileinfo.forEach(function (file) {
            var button = $(".update[name='" + file.name + "']");
            var newVersion = file.version.toString(16);
            var versionLength = newVersion.length;
            var fpgaChk = env.supportCinemaModel && versionLength == 5;
            if (fpgaChk) {
                newVersion = '00.0' + newVersion.substring(0, 1) + '.' + newVersion.substring(1, 3);
            } else {
                newVersion = '0' + newVersion.substring(0, 1) + '.' + newVersion.substring(1, 3) + '.' + newVersion.substring(3, 5);
            }
            button.prop('id', newVersion);
            button.prop('disabled', false);

            $('.file-info[name="' + file.name + '"]').append('(' + newVersion + ')');
 
            if (!fpgaChk) {
                var curVerInteger = parseInt(currentVer.replace(/[.]/g, ""));
                var newVerInteger = parseInt(newVersion.replace(/[.]/g, ""));

                if (isNaN(curVerInteger)) {
                    curVerInteger = 0;
                }
 
                if (curVerInteger == newVerInteger) {
                    $('.file-list[name="' + file.name + '"]').css("background-color", "#c6342f");
                    $('.file-list[name="' + file.name + '"]').css("color", "white");
                    $('.img-button-update[name="' + file.name + '"]').css("visibility", "hidden");
                } else if (curVerInteger > newVerInteger) {
                    $('.file-list[name="' + file.name + '"]').css("background-color", "#bfb7b7");
                    $('.file-list[name="' + file.name + '"]').css("color", "white");
                }
            }
        });
    });
}
