function adjustInputPin() {
    var height = 40;
    $(".input_circle_pin").css("width", height);
    $(".input_circle_pin").css("height", height);
}

function updateInputPin() {
    $(".input_circle_pin").each(function () {
            var isFocused = $(this).attr("data-checked") === "true" ? true : false;
            var nonVal = $(this).val() === "" || $(this) === undefined;
            if (isFocused && nonVal) {
            $(this).css("background-image", "url('/images/2017UX/pin_code_pressed.png')");
            } else if (isFocused && !nonVal) {
            $(this).css("background-image", "url('/images/2017UX/pin_code_input_pressed.png')");
            } else if (!isFocused && nonVal) {
            $(this).css("background-image", "url('/images/2017UX/pin_code_normal.png')");
            } else {
            $(this).css("background-image", "url('/images/2017UX/pin_code_input_normal.png')");
            }
            });
}

// Device is connected firstly to set ip, focus/blue event is not occurred.
// This api include exception handling about it.
function initInputPin() {
    var pinMaxLength = 1;
    $(".input_circle_pin").each(function () {
            $(this).attr("maxlength", pinMaxLength);

            });

    $(".input_circle_pin").click(function () {
           $(".input_circle_pin").each(function () {
                $(this).blur();
                });

            onFocusPin($(this));
            $(this).focus();
            });

    $(this).css({"color": "#4c4c4c", "border": "2px solid #cf0652"});

    $(".input_circle_pin").on("keyup", function (event) {
            var pin = $(this);
            var nextPin = $(this).next(".input_circle_pin");
            if (pin.val() && nextPin.length >= 1) {
            onBlurInputPin(pin);
            pin.next(".input_circle_pin").val("");
            pin.next(".input_circle_pin").css("background-image", "url('/images/2017UX/pin_code_pressed.png')");
            pin.next(".input_circle_pin").focus();
            } else if (pin.val() && !(nextPin.length >= 1)) {
            onBlurInputPin(pin);
            } else if (!(nextPin.length >= 1)){
            pin.css("background-image", "url('/images/2017UX/pin_code_pressed.png')");
            }
            });

    $(".input_circle_pin").on("focus", function () {
            onFocusPin($(this));
            });

    $(".input_circle_pin").on("blur", function () {
            onBlurInputPin($(this))
            });
}

function onFocusPin(elem) {
    elem.val("");
    elem.css("background-image", "url('/images/2017UX/pin_code_pressed.png')");
}

function onBlurInputPin(elem) {
    var val = elem.val();
    if (!val) {
        elem.css("background-image", "url('/images/2017UX/pin_code_normal.png')");
    }
}


