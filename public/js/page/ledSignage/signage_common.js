/**
 * Created by byungjin.park on 17. 5. 8.
 */

$(document).ready(function () {
    window.onload = function () {
        setRollingText();
    }
});

/*$(window).load(function () {
    setRollingText();
});*/

function goSignageService(url) {
    var port = 443;

    var isLogin = $.cookie("isLogin") === "true" ? true : false;
    console.log("isLogin : " + isLogin);
    if (!isLogin && url !== "main") {
        location.href = "https://" + location.hostname + ":" + port + "/login";
        return;
    }

    console.log("go >>> http://" + location.hostname + ":" + port);
    if (url === "ControlManager") {
        location.href = "https://" + location.hostname + ":3737";
    } else {
        location.href = "https://" + location.hostname + ":" + port + "/" + url;
    }
}

function getLanguageText(languageTable, text) {
    var text = text + "";

    for (var i = languageTable.length - 1; i >= 0; i--) {
        if (languageTable[i][text]) {
            text = languageTable[i][text];
        }
    }
    return text;
}

function changeLanguage() {
    var localeFormatter = {
        es: "es-ES",
        ko: "ko-KR"
    };

    var locale = navigator.language || navigator.browerLanguage;
    if (localeFormatter[locale]) {
        locale = localeFormatter[locale];
    }
    console.log("locale: " + locale);
    $.ajax({
        type: "post",
        data: {locale: locale},
        url: "/setLanguage",
        success: function (data) {
            console.log(data);
            if (data === "changed") {
                location.reload();
            }
        },
        error: function (req, status, err) {
            console.log("error");
        }
    });
}

function setRollingText() {
    setTimeout(function () {
        $(".txt_rolling span").each(function () {
            if ($(this).children("marquee").length > 0) {
                $(this).css("overflow", "hidden");
                var html = $(this).children("marquee").html();
                $(this).html(html);
            }
        });

        $(".txt_rolling").each(function () {
            $(this).children("span").unbind("mouseenter mouseleave");
            var width = $(this).width();
            var childWidth = $(this).children("span").width();
            if (childWidth > width) {
                $(this).children("span").hover(function () {
                    console.log("mouseover");
                    $(this).css("overflow", "visible");
                    var html = "<marquee>" + $(this).html() + "</marquee>";
                    $(this).html(html);
                }, function () {
                    console.log("mouseout");
                    $(this).css("overflow", "hidden");
                    var html = $(this).children("marquee").html();
                    $(this).html(html);
                })
            }
        });
    }, 100);
}

function isAlNum(str) {
    var retVal = true;
    for (var i = 0; i < str.length; i++) {
        var char = str.charAt(i)
        if (!char.match(/[0-9]|[a-z]|[A-Z]/)) {
            retVal = false;
        }
    }
    console.log("" + retVal);
    return retVal;
}
