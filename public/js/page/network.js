var prevData = {
    wired: {},
    wifi: {}
};
var invalidTxt = '';
var isSupportIpv6 = false;
var isSupportIpv6Wifi = false;
var isSelectIpv6 = false;
var isSelectIpv6Wifi = false;
var path = location.protocol + '//' + location.host + location.pathname;

function checkIPAddress(ip) {
    var numbers = ip.split(".");

    if (numbers.length != 4) {
        return false;
    }

    for (var i = 0; i != 4; ++i) {
        if (!$.isNumeric(numbers[i])) {
            return false;
        }
        var n = parseInt(numbers[i]);

        if (n < 0 || n > 255) {
            return false;
        }
    }

    return true;
}

function checkNetMask(netMask) {
    var regExp = new RegExp('^(((128|192|224|240|248|252|254)\\.0\\.0\\.0)|(255\\.(0|128|192|224|240|248|252|254)\\.0\\.0)|(255\\.255\\.(0|128|192|224|240|248|252|254)\\.0)|(255\\.255\\.255\\.(0|128|192|224|240|248|252|254|255)))$');
    var result = regExp.test(netMask);
    return result;
}

function controlDhcp(wifi) {
    var suffix = wifi ? 'wifi' : '';
    var prev = wifi ? prevData.wifi : prevData.wired;

    var checked = $('#dhcp' + suffix).prop('checked');
    var isIpv6 = (wifi && isSelectIpv6Wifi) || (!wifi && isSelectIpv6);
    $("#ip" + suffix).prop('disabled', checked || isIpv6);
    $("#gateway" + suffix).prop('disabled', checked || isIpv6);
    $("#netMask" + suffix).prop('disabled', checked || isIpv6);
    $("#dns" + suffix).prop('disabled', checked || isIpv6);

    if (checked) {
        $("#ip" + suffix).val(prev.ip);
        $("#gateway" + suffix).val(prev.gateway);
        $("#netMask" + suffix).val(prev.netMask);
        $("#dns" + suffix).val(prev.dns);
    }
}

function checkDHCP() {
    var suffix = $(this).attr('id').indexOf('wifi') > 0 ? 'wifi' : '';

    if ($(this).prop('checked')) {
        var msg = getLanguageText(languageTable, "Are you sure to use Wired DHCP?") + "\n";;
        if (suffix) {
            msg = getLanguageText(languageTable, "Are you sure to use Wi-Fi DHCP?") + "\n";
        }
        msg += getLanguageText(languageTable, "You may not be able to reconnect to web pages if you cannot find out new IP address.");
        var ret = confirm(msg);
        $('#dhcp' + suffix).attr('checked', ret);
    }
}

function init(env) {
    $('#signageName').on('input', function () {
        var value = $(this).val();
        var maxLength = $(this).attr("maxLength");
        if (value.length > maxLength) {
            console.log("Name is over max langth.");
            $(this).val(value.slice(0, maxLength));
        }
    });

    $('#dhcp').change(function () {
        controlDhcp(false);
    });

    $('#dhcpwifi').change(function () {
        controlDhcp(true);
    });

    $('.grey').click(checkDHCP);
    $('.media-checkBox').click(checkDHCP);

    initIPv6();

    function savePrevData(wifi, data) {
        var prev = wifi ? prevData.wifi : prevData.wired;
        data = wifi ? data.wifi : data.wired;

        prev.ip = data.ipAddress;
        prev.gateway = data.gateway;
        prev.netMask = data.netmask;
        prev.dhcp = data.method == 'dhcp';
        prev.dns = getDnsInfo(wifi, data);
        prev.connected = data.state === 'connected';

        if (wifi && isSelectIpv6Wifi) {
            var ipv6 = data.ipv6;
            prev.gateway = ipv6.gateway;
            prev.ip = ipv6.ipAddress;
        }

        if (!wifi && isSelectIpv6) {
            var ipv6 = data.ipv6;
            prev.gateway = ipv6.gateway;
            prev.ip = ipv6.ipAddress;
        }

        if (wifi) {
            prev.ssid = data.ssid;
        }
    }

    function setPanelValue(wifi, data) {
        var data = wifi ? data.wifi : data.wired;
        var which = wifi ? 'wifi' : '';

        if ((wifi && isSelectIpv6Wifi) || (!wifi && isSelectIpv6)) {
            var ipv6 = data.ipv6;
            data.gateway = ipv6.gateway;
            data.ipAddress = ipv6.ipAddress;
            data.prefixLength = ipv6.prefixLength;
            $("#netPrefix" + which).val(data.prefixLength);
        }

        if ((wifi && !isSelectIpv6Wifi) || (!wifi && !isSelectIpv6)) {
            $("#netMask" + which).val(data.netmask);
        }

        $('#interfaceName' + which).html(data.interfaceName);
        $("#ip" + which).val(data.ipAddress);
        $("#gateway" + which).val(data.gateway);
        $("#dhcp" + which).attr('checked', data.method == 'dhcp');
        $('#dhcp' + which).change();
        $('#dns' + which).val(getDnsInfo(wifi, data));
        $('#mac' + which).val(data.macAddress);
        $('#interfaceName' + which).html(data.interfaceName);
    }

    function getNetworkData() {
        getNetworkStatus(function (ret) {
            isSupportIpv6 = getIsSupportIpv6(false, ret);
            isSupportIpv6Wifi = getIsSupportIpv6(true, ret);
            isSupportIpv6 ? isSelectIpv6 = isSelectIpv6 : isSelectIpv6 = false;
            isSupportIpv6Wifi ? isSelectIpv6Wifi = isSelectIpv6Wifi : isSelectIpv6Wifi = false;

            $('#wiredConnection').html(
                ret.wired.state === 'connected' ? getLanguageText(languageTable, "Connected") : getLanguageText(languageTable, "Disconnected"));
            $('#wifiConnection').html(
                ret.wifi.state === 'connected' ? ret.wifi.ssid : getLanguageText(languageTable, "Disconnected"));

            if (!isSupportIpv6) {
                $('#filter').hide();
            }
            if (!isSupportIpv6Wifi) {
                $('#wifiFilter').hide();
            }
            changeIPv6View(false);
            changeIPv6View(true);

            if (ret.wired.state !== 'connected') {
                $('#wiredPanel').hide();
            } else {
                savePrevData(false, ret);
                setPanelValue(false, ret);
            }

            if (ret.wifi.state !== 'connected') {
                $('#wifiPanel').hide();
            } else {
                savePrevData(true, ret);
                setPanelValue(true, ret);
            }
        });

        getSignageName(function (ret) {
            prevData.signageName = ret;
            $("#signageName").val(ret);
        });
    }

    function getUserValue(wifi) {
        var suffix = wifi ? 'wifi' : '';

        var dhcp = $("#dhcp" + suffix).prop('checked');
        var ip = $("#ip" + suffix).val();
        var netMask = $("#netMask" + suffix).val();
        var gateway = $("#gateway" + suffix).val();
        var dns = $("#dns" + suffix).val();

        return {
            dhcp: dhcp,
            ip: ip,
            netMask: netMask,
            gateway: gateway,
            dns: dns
        };
    }

    function changePrevData(wifi, data) {
        var prev = wifi ? prevData.wifi : prevData.wired;
        prev.dhcp = data.dhcp;
        prev.ip = data.ip;
        prev.netMask = data.netMask;
        prev.gateway = data.gateway;
        prev.dns = data.dns;
    }

    function checkValid(wifi) {
        var suffix = wifi ? 'wifi' : '';
        var user = getUserValue(wifi);
        var prev = wifi ? prevData.wifi : prevData.wired;
        var conStr = wifi ? 'Wi-Fi' : 'Wired';
        var checkResult = true;

        if (!checkIPAddress(user.ip)) {
            invalidTxt += "<br>" + suffix + " " + getLanguageText(languageTable, "IP Address");
            $('#ip' + suffix).val(prev.ip);
            checkResult = false;
        }

        if (!checkIPAddress(user.gateway)) {
            invalidTxt += "<br>" + suffix + " " + getLanguageText(languageTable, "Gateway");
            $('#gateway' + suffix).val(prev.gateway);
            checkResult = false;
        }

        if (!checkNetMask(user.netMask)) {
            invalidTxt += "<br>" + suffix + " " + getLanguageText(languageTable, "Subnet Mask");
            $('#netMask' + suffix).val(prev.netMask);
            checkResult = false;
        }

        if (!checkIPAddress(user.dns)) {
            invalidTxt += "<br>" + suffix + " " + getLanguageText(languageTable, "DNS Server");
            $('#dns' + suffix).val(prev.dns);
            checkResult = false;
        }

        return checkResult;
    }

    var needReconnection = false;

    function isNetworkSettingChanged(prev, user) {
        var isDhcpChanged = prev.dhcp != user.dhcp;
        var isIpChanged = prev.ip != user.ip;
        var isNetMaskChanged = prev.netMask != user.netMask;
        var isGatewayChanged = prev.gateway != user.gateway;
        var isDnsChanged = prev.dns != user.dns;
        return isDhcpChanged || isIpChanged || isNetMaskChanged || isGatewayChanged || isDnsChanged;
    }

    function changeNetworkSettings(wifi) {
        var user = getUserValue(wifi);
        var prev = wifi ? prevData.wifi : prevData.wired;

        if (!isNetworkSettingChanged(prev, user)) {
            console.log("network settings is not changed");
            return;
        }

        if (user.dhcp) {
            user.dns = '';
        }

        setNetworkDNS(user.dns, prev.ssid);
        setNetworkStatus(user.dhcp, user.ip, user.netMask, user.gateway, prev.ssid);

        needReconnection |= (!prev.dhcp && user.dhcp) || (prev.ip != user.ip);

        changePrevData(wifi, user);
    }

    $('#apply').click(function () {
        var signageName = $("#signageName").val();
        var checkResult = true;

        if (!env.supportLedSignage) {
            if (signageName.length <= 0 || signageName.replace(/^ */g, '').length <= 0) {
                showWarningModal(getLanguageText(languageTable, "Input Signage Name please."));
                $('#signageName').val(prevData.signageName);
                return;
            }
        }

        if (!isSelectIpv6 && prevData.wired.connected && !checkValid(false)) {
            checkResult = false;
        }

        if (!isSelectIpv6Wifi && prevData.wifi.connected && !checkValid(true)) {
            checkResult = false;
        }

        if (!checkResult) {
            showWarningModal("<strong>" + getLanguageText(languageTable, "Invalid value") + "</strong>" + invalidTxt);
            invalidTxt = '';
            return;
        }

        if (!confirm(getLanguageText(languageTable, "Are you sure to change Settings?"))) {
            return;
        }

        if (!env.supportLedSignage) {
            setSignageName($("#signageName").val());
        }

        needReconnection = false;

        if (!isSelectIpv6 && prevData.wired.connected) {
            changeNetworkSettings(false);
        }

        if (!isSelectIpv6Wifi && prevData.wifi.connected) {
            changeNetworkSettings(true);
        }

        setTimeout(getNetworkData, 1000);

        if (needReconnection) {
            showWarning(getLanguageText(languageTable, "You changed IP address or DHCP status. Please reconnect to changed IP address"));
        }
    });

    $('#wifiTitle').click(function () {
        showWarningModal(getLanguageText(languageTable, "To choose an AP, please use Settings App. with a remote controller."));
        return;
    });

    getNetworkData();

    $('#pageTitle').html(getLanguageText(languageTable, "Network"));

    if (locale == "ar-SA") {
        changeRtl();
    }
}

function changeRtl() {
    $(".col-md-9.one_line").css("float", "right");
    $(".col-md-3.one_line").css("float", "right");
    $('#ip').attr("dir", "ltr").css("text-align", "right");
}

function drawFilterSelect(id) {
    var dropDown = createDropDown(id, 'IPv4', function (selected) {
        var filterVal = "";
        var wifiFilterVal = "";
        if (isSupportIpv6) {
            filterVal = $("#filterDrop").html().indexOf("IPv6") >= 0 ? "IPv6" : "IPv4";
        }
        if (isSupportIpv6Wifi) {
            wifiFilterVal = $("#wifiFilterDrop").html().indexOf("IPv6") >= 0 ? "IPv6" : "IPv4";
        }

        console.log (filterVal, wifiFilterVal);
        var newPath = path + "?filter=" + filterVal + "&wifiFilter=" + wifiFilterVal;
        window.location.replace(newPath);
    });

    addDropDownOption(dropDown, "IPv4", "IPv4");
    addDropDownOption(dropDown, "IPv6", "IPv6");

    $("#" + id).append(dropDown);
}

function getIsSupportIpv6 (wifi, data) {
    var ipv6Data = wifi ? data.wifi : data.wired;
    var ipv6Type = jQuery.type(ipv6Data.ipv6);

    if (ipv6Data.state === "disconnected" || ipv6Type === 'undefined') {
        return false;
    }

    return true;
}

function initIPv6() {
    var urlQuery = queryString();
    var filter = urlQuery.filter ? urlQuery.filter : "IPv4";
    var wifiFilter = urlQuery.wifiFilter ? urlQuery.wifiFilter : "IPv4";

    console.log("filter = " + filter + ", wifiFilter = " + wifiFilter);

    isSelectIpv6 = filter === "IPv6" ? true : false;
    isSelectIpv6Wifi = wifiFilter === "IPv6" ? true : false;
    drawFilterSelect("filter");
    drawFilterSelect("wifiFilter");
    setDropButtonText('filter', filter);
    setDropButtonText('wifiFilter', wifiFilter);
}

function changeIPv6View(wifi) {
    var which = wifi ? 'wifi' : '';
    if ((wifi && isSelectIpv6Wifi) || (!wifi && isSelectIpv6)) {
        $('#dhcp' + which + 'Low').hide();
        $('#netMask' + which + '_low').hide();

        $('#netPrefix' + which).attr("disabled", true);
    }

    if ((wifi && !isSelectIpv6Wifi) || (!wifi && !isSelectIpv6)) {
        $('#netPrefix' + which + '_low').hide();
    }
}

function getDnsInfo(wifi, data) {
    if (!data) {
        return "";
    }
    var dns = "";
    var repeat = true;
    var i = 1;
    while (repeat) {
        var key = "dns" + i;
        if (data[key]) {
            if ((wifi && isSelectIpv6Wifi) || (!wifi && isSelectIpv6)) {
                if (data[key].indexOf(":") >= 0) {
                    dns = data[key];
                    repeat = false;
                }
            } else {
                if (data[key].indexOf(".") >= 0) {
                    dns = data[key];
                    repeat = false;
                }
            }
        } else {
            repeat = false;
        }

        i++;
    }
    return dns;
}
