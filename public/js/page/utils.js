function getFailOverPriorityInfo(callback) {
    // Check whether failOverPriorityInfo already has value of failover priority.
    // And Check whether value of failover priority exist in input list.
    var failOverPriorityInfo = [];
    getInputList(function (inputListMsg) {
        getFailOverPriority(function (failOverPriorityMsg) {
            for (var i = 0; i < inputListMsg.length; i++) {
                var failOverPriority = failOverPriorityMsg["failoverPriority" + (i + 1)];
                if (!checkSameFailOverPriority(failOverPriorityInfo, failOverPriority)) {
                    for (var j = 0; j < inputListMsg.length; j++) {
                        if (failOverPriority === inputListMsg[j].settingKey) {
                            failOverPriorityInfo[i] = inputListMsg[j];
                            break;
                        }
                    }
                }
            }
            callback(failOverPriorityInfo);
        });
    });
}

function checkSameFailOverPriority(failOverPriorityInfo, failOverPriority) {
    for (var i in failOverPriorityInfo) {
        if (failOverPriorityInfo[i].settingKey === failOverPriority) {
            console.log("checkSameFailOverPriority() true");
            return true;
        }
    }
    return false;
}