var fanData = {
	OL1data: [],
	CL1data: [],
	PU1data: [],
	OL2data: [],
	CL2data: [],
};

var labels = [];
var color = [
	"#4572a7", "#aa4643", "#89a54e", "#71588f", "#4198af", "#db843d",
	"#93a9cf", "#d19392", "#b9cd96", "#a99bbd", "#88c8da", "#f3b07a",
	"#0d62f5", "#ec1f1c", "#a1ec20", "#6e1ce2", "#1eb6e0", "#f3740c"
];
var fanChart;

function createChart(table) {
	for (var i = 0; i != table.length; ++i) {
		labels.push(table[i].temp)
	}

	var data = {
		labels: labels,
		datasets: []
	};

	idMap.id.forEach(function (id, idx) {
		if (fanTypes.indexOf(id.substr(0, id.length -1)) < 0) {
			return;
		}
		data.datasets.push({
			label: id + ' Fan',
			fillColor: "rgba(255,255,255,0.2)",
			strokeColor: color[idx],
			pointColor: color[idx],
			pointStrokeColor: "#fff",
			pointHighlightFill: "#fff",
			pointHighlightStroke: color[2],
			data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
		});
	});

	var ctx = document.getElementById("myChart").getContext("2d");
	fanChart = new Chart(ctx).Line(data, {
		scaleOverride: true,
		scaleStartValue: 0,
		scaleSteps: 20,
		scaleStepWidth: 5,
		responsive: true,
		bezierCurve: false,
		animationSteps: 5
	});

	fanChart.chart.canvas.onclick = function(evt) {
		var points = fanChart.getPointsAtEvent(evt);
		if (!points || !points.length) {
			return;
		}
		var label = points[0].label;
		$('.tempSel').val(Number(label)).change();
	}

	idMap.id.forEach(function(id) {
		if (fanTypes.indexOf(id.substr(0, id.length -1)) < 0) {
			return;
		}
		for (var i = 0; i != labels.length; ++i) {
			$('#' + idMap[id] + 'temp').append($('<option>',
				{ value: labels[i], text: String(labels[i]) }));
		}
	});
}

function initFanTest() {
    $("#startFan").click(function() {
		$("#fanDiv").show();
		$("#startFan").hide();
		$("#stopFan").show();
		$('.testVal').html('0');

		idMap.id.forEach(function(id) {
			if (fanTypes.indexOf(id.substr(0, id.length -1)) < 0) {
				$('#' + idMap[id] + 'testControl').hide();
				return;
			}
			checkRangeUI(idMap[id] + 'test', 0, 255);
		});

		stopFanControl();
	});

	$("#stopFan").click(function() {
		$("#fanDiv").hide();
		$("#startFan").show();
		$("#stopFan").hide();
		$('[id$=test]').each(function (index, element) {
			$(element).val(0);
		});
		restartFanControl();
	});

	$("#fanDiv").hide();
	$("#stopFan").hide();

	$('[id$=test]').on('input', function(e) {
		var prefix = $(this).attr('id').substr(0, 3);
		var tag = '#' + prefix + 'test';
		var duty = $(tag).val();
		$(tag + 'Val').html(duty);
		checkRangeUI(e.target.id, 0, 255);
	}).change(function(e) {
		var prefix = $(this).attr('id').substr(0, 3);
		var tag = '#' + prefix + 'test';
		var duty = $(tag).val();
		var id = "openLoop";
		if (prefix.substr(0, 2) === 'CL') {
			id = "closedLoop";
		} else if (prefix.substr(0, 2) === "PU") {
			id = "power"
		}
		checkRangeUI(e.target.id, 0, 255);
		var micomId = parseInt(prefix.substr(2, 1)) - 1;
		setFanDuty(id, duty, micomId);
	});

}

function initFanPage() {
	idMap.id.forEach(function(id, idx) {
		if (fanTypes.indexOf(id.substr(0, id.length -1)) < 0) {
			$('#' + idMap[id] + 'control').hide();
			return;
		}
		$('#' + idMap[id] + 'header').css('color', color[idx]);
	});

	$('.tempSel').change(function() {
		var idx = Number($(this).val()) / 5 - 1;
		var prefix = $(this).attr('id').substr(0, 3);
		var data = fanData[prefix + 'data'];
		if (!data || data.length == 0) {
			return;
		}
		$('#' + prefix + 'speed').val(data[idx].level);
		checkRangeUI(prefix + 'speed', 0, 100);
		$('#' + prefix + 'speedVal').html(data[idx].level);
	});

	$('[id$=speed]').on('input', function(e) {
		$('#' + $(this).attr('id') + 'Val').html("" + $(this).val());
		checkRangeUI(e.target.id, 0, 100);
	}).change(function(e) {
		var prefix = $(this).attr('id').substr(0, 3);
		var idx = Number($('#' + prefix + 'temp').val()) / 5 - 1;

		var data = fanData[prefix + 'data'];
		var datasetsIdx = 0;
		if (prefix === 'OL2') {
			datasetsIdx = 1;
		} else if (prefix === 'CL1') {
			if (env.fanControl.numOfFanIndex !== 2) {
				datasetsIdx = 1;
			} else {
				datasetsIdx = 2;
			}
		} else if (prefix === 'CL2') {
			datasetsIdx = 3;
		} else if (prefix === 'PU1') {
			if (env.fanControl.numOfFanIndex !== 2) {
				datasetsIdx = 4;
			} else {
				datasetsIdx = 2;
			}
		}

		var prevVal = data[idx].level;

		data[idx].level = Number($(this).val());
		fanChart.datasets[datasetsIdx].points[idx].value = data[idx].level;

		var propotion = data[idx].level / prevVal;
		var curVal = data[idx].level;

		if (propotion < 1) {
			for (var i = idx - 1; i >= 0; --i) {
				if (data[i].level <= data[i + 1].level) {
					continue;
				}

				var newVal = curVal;
				data[i].level = newVal;
				fanChart.datasets[datasetsIdx].points[i].value = newVal;
			}
		}

		if (propotion > 1) {
			for (var i = idx + 1; i != data.length; ++i) {
				if (data[i].level >= data[i - 1].level) {
					continue;
				}

				var newVal = curVal;
				data[i].level = newVal;
				fanChart.datasets[datasetsIdx].points[i].value = newVal;
			}
		}

		fanChart.update();
		checkRangeUI(e.target.id, 0, 100);
	});

	var chartCreated = false;
	function applyTableValue(fanKind) {
		if (fanKind == idMap.id.length) {
			fanChart.update();
			return;
		}

		var fanID = idMap.id[fanKind];

		function initEachFan(msg) {
			if (!chartCreated) {
				createChart(msg.table);
				chartCreated = true;
			}

			var code = idMap[fanID];
			fanData[code + 'data'] = msg.table;
			for (var i = 0; i != msg.table.length; ++i) {
				fanChart.datasets[fanKind].points[i].value = fanData[code + 'data'][i].level;
			}

			$('#' + code + 'speed').val(fanData[code + 'data'][0].level);
			$('#' + code + 'speedVal').html("" + fanData[code + 'data'][0].level);
			checkRangeUI(code + 'speed', 0, 100);
		}

		getFanCtlTable(fanID.substr(0, fanID.length -1), parseInt(idMap[fanID].substr(2, 1)) - 1, function(msg) {
			if (msg.returnValue === true && msg.table.length > 0) {
				initEachFan(msg);
			}

			applyTableValue(fanKind + 1);
		});
	}

	applyTableValue(0);
}

function addFanEventListener() {
	socket.on("restartFan", function() {
		showWarning("Automatic Fan control is restarted", 10 * 1000);
        $("#fanDiv").hide();
        $("#startFan").show();
        $("#stopFan").hide();
	});

	window.onunload = function() {
		restartFanControl();
	}

	$('#refresh').click(function() {
		refreshSensor();
	});

	$('#apply').click(function() {
		idMap.id.forEach(function(id) {
			if (fanTypes.indexOf(id.substr(0, id.length -1)) < 0) {
				return;
			}
			fanData[idMap[id] + 'data'].forEach(function(val) {
				var micomId = parseInt(id.substr(id.length -1, 1)) - 1;
				setFanCtlTable(id.substr(0, id.length -1), val, micomId);
			});
		});
	});

	$('#revert').click(function() {
		location.reload();
	});

	$('#reset').click(function() {
		resetFanCtlTable();
		location.reload();
	});
}
