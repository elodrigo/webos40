var captureTimeInterval = "";
var deviceInfo = {
    "usb": undefined,
    "internal": undefined,
}
var inputList = [];

function init() {
    getDeviceInfo();
    drawInterval();
    initFailOver();

    $('#pageTitle').html(getLanguageText(languageTable, "Fail Over"));

    $("#apply").click(function () {
        setFailOver();
    });

    $("#btnFailover").change(function () {
        $("#failOverPriorityauto").click();

        if ($(this).is(':checked')) {
            $("#failOverTable").css('display', 'table');
            $("#advancedSetting").css('display', 'table');
        } else {
            $("#failOverTable").css('display', 'none');
            $("#failOverPriorityTable").css('display', 'none');
            $("#advancedSetting").css('display', 'none');
        }
    });

    $("input[name=failOverPriority]").change(function () {
        if ($("input[name=failOverPriority]:checked").val() == 'auto') {
            $("#failOverPriorityInfo").hide();
        } else {
            $("#failOverPriorityInfo").show();
        }
    });

    $("#btnBackup").change(function () {
        updateBackupViaStorage();
    });

    $("input[name=playViaStorage]").change(function () {
        updateBackupViaStorageOption();
    });

    $(".file_upload :file").change(function () {
        var file = this.files[0];
        if (file) {
            $("#fileName").html(filteringXSS(file.name));
        }
    })

    $('input[type="submit"]').on('click', function (evt) {
        evt.preventDefault();
        var file = document.getElementById('upload').files[0];

        if (!file) {
            showWarning(getLanguageText(languageTable, "Please choose a file to upload"), 5 * 1000);
            return;
        } else if (env.oledModel && !isVideoFile(file.name)) {
            showWarning(getLanguageText(languageTable, "Please choose a video file to upload"), 5 * 1000);
            return;
        } else if (!isImageFile(file.name) && !isVideoFile(file.name)) {
            showWarning(getLanguageText(languageTable, "Select media file"), 5 * 1000);
            return;
        }

        var targetDir = deviceInfo["internal"].deviceUri + "/.failover/";
        var backupViaStorageFile = targetDir + filteringXSS(file.name);

        deleteFailover(function (msg) {
            setSystemSettings("commercial", {backupViaStorageFile: backupViaStorageFile});
            uploadFile(file, targetDir);
        });
    });

    if (env.oledModel) {
        $(".file_upload input[type='file']").attr("accept", "video/*");
    } else {
        $(".file_upload input[type='file']").attr("accept", "video/*,image/*");
    }

    if (locale == "ar-SA") {
        changeRtl();
    }
}

function getDeviceInfo() {
    getListDevices(function (msg) {
        var devices = msg.devices;

        devices.forEach(function (device) {
            if (device.deviceType == "usb") {
                deviceInfo["usb"] = device;
            } else if (device.deviceType == "internal signage") {
                deviceInfo["internal"] = device;
                setBackupfileInfo();
            }
        });
    });
}


function uploadFile(file, targetDir) {
    var formData = new FormData();
    var newname = filteringXSS(file.name); //filter for XSS

    console.log("targetDir::" + targetDir);
    formData.append('dir', targetDir);
    formData.append('newname', newname);
    formData.append('failOver', file);

    if (!file) {
        showWarning(getLanguageText(languageTable, "Please choose a file to upload"), 5 * 1000);
        return;
    }

    overlayControl(true, getLanguageText(languageTable, "Uploading") + "<br>" + newname);
    $("#progress").show();
    $(".upload_disable").prop("disabled", true);
    $("button").prop("disabled", true);

    var xhr = new XMLHttpRequest();

    xhr.open('post', '/upload', true);

    xhr.upload.onprogress = function (e) {
        if (e.lengthComputable) {
            var per = Math.round((e.loaded / e.total) * 100)
            $('#uploadProgress').html(per + "%");
            $('#uploadProgress').css('width', per + '%');
            $('#overlay_progress').html(per + "%");
        }
    };

    xhr.upload.onload = function (e) {
        setTimeout(function () {
            overlayControl(false);
            setBackupfileInfo();
        }, 2000);
    }

    xhr.onerror = function (e) {
        overlayControl(false);
        alert(getLanguageText(languageTable, "An error occurred while submitting the form."));
        console.log('An error occurred while submitting the form. Maybe your file is too big');
    };

    xhr.onload = function () {
        console.log(this.statusText);
    };

    xhr.send(formData);

}

function updateBackupViaStorage() {
    var isOn = $("#btnBackup").is(':checked');
    console.log("updateBackupViaStorage() " + isOn);
    if (isOn) {
        var option = "auto";
        $("#advancedSettingTable").css('display', 'table');
        if (env.failover.backupViaStorage.auto.visible) {
            option = "auto"
        } else if (env.failover.backupViaStorage.manual.visible) {
            option = "manual"
        } else if (env.failover.backupViaStorage.supersign.visible) {
            option = "supersign"
        } else if (env.failover.backupViaStorage.siapp.visible) {
            option = "siapp"
        }

        if ($("#playViaStorage" + option).is(':checked')) {
            updateBackupViaStorageOption();
        } else {
            $("#playViaStorage" + option).click();
        }
    } else {
        $("#playViaStorageauto").attr("checked", true);
        $("#advancedSettingTable").css('display', 'none');
        $("#intervalTable").css('display', 'none');
        $("#BackupImage").css('display', 'none');
    }
}

function updateBackupViaStorageOption() {
    var option = $("input[name=playViaStorage]:checked").val();
    if (option == 'auto') {
        option = "auto";
        $("#BackupImage").css('display', 'none');
        $("#intervalTable").css('display', 'table');
    } else if (option == 'manual') {
        option = "manual";
        $("#BackupImage").css('display', 'table');
        $("#intervalTable").css('display', 'none');
    } else if (option == 'supersign') {
        option = "supersign";
        $("#BackupImage").css('display', 'none');
        $("#intervalTable").css('display', 'none');
    } else if (option == 'siapp') {
        option = "siapp";
        $("#BackupImage").css('display', 'none');
        $("#intervalTable").css('display', 'none');
    }

    console.log("updateBackupViaStorageOption() " + option);
}

function drawInterval() {
    var dropDown = createDropDown('intervalSelect', '-', function (selected) {
        captureTimeInterval = selected;
    });

    if (locale == "ar-SA") {
        dropDown.find(".dropdown-menu").css("right", 0);
    }

    addDropDownOption(dropDown, "30", "30 " + getLanguageText(languageTable, "min"));
    addDropDownOption(dropDown, "60", "1 " + getLanguageText(languageTable, "hour"));
    addDropDownOption(dropDown, "120", "2 " + getLanguageText(languageTable, "hour"));
    addDropDownOption(dropDown, "180", "3 " + getLanguageText(languageTable, "hour"));

    if (locale == "ar-SA") {
        dropDown.find(".dropdown-menu").css("right", 0);
    }

    $('#intervalSelect').append(dropDown);

    getBackupViaStorage(function (msg) {
        captureTimeInterval = msg.backupViaStorageInterval;
        setDropButtonText('intervalSelect', msg.backupViaStorageInterval);

        if (msg.backupViaStorage == "off") {
            $("#btnBackup").prop("checked", false);
            $("#btnBackup").change();
        } else {
            $("#btnBackup").prop("checked", true);
            $("#btnBackup").change();

            $("#advancedSettingTable").css('display', 'table');
            if ($("#playViaStorage" + msg.backupViaStorage).is(':checked')) {
                updateBackupViaStorageOption();
            } else {
                $("#playViaStorage" + msg.backupViaStorage).click();
            }
        }
    });
}

function setAdvancedSetting(callback) {
    var backupViaStorage = "";
    var settings = undefined;

    $("#btnBackup").prop("checked") ? backupViaStorage = $("input[name=playViaStorage]:checked").val() : backupViaStorage = "off";

    if (backupViaStorage == "off") {
        settings = {
            backupViaStorage: backupViaStorage
        }
    } else if (backupViaStorage == "auto") {
        settings = {
            backupViaStorage: backupViaStorage,
            backupViaStorageInterval: captureTimeInterval
        }
    } else {
        settings = {
            backupViaStorage: backupViaStorage
        }
    }

    setSystemSettings("commercial", settings, callback)
}

function setBackupfileInfo() {
    getSystemSettings("commercial", ["backupViaStorageFile"], function (msg) {

        var backupViaStorageFile = msg.backupViaStorageFile;
        getBackupfileName(deviceInfo["internal"].deviceUri + "/.failover/", function (msg) {
            var fileName = msg[0];
            if (backupViaStorageFile != '' && msg[0]) {
                var filter = {
                    video: ["avi", "mp4", "wmv", 'divx', 'asf', 'm4v', 'mov',
                        '3gp', '3g2', 'mkv', 'ts', 'trp', 'tp', 'mts',
                        'mpg', 'mpeg', 'dat', 'vob'],
                    image: ["png", "bmp", "jpg", 'jpeg', 'jpe']
                };

                $("#failoverImageName").html(fileName);

                var ext = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();

                if (filter["video"].indexOf(ext) >= 0) {
                    $("#failoverImagetype").attr("src", "/images/ic_record.png");
                } else if (filter["image"].indexOf(ext) >= 0) {
                    $("#failoverImagetype").attr("src", "/images/ic_photo.png");
                }

                getThumbnail("failOver", fileName, function (msg) {
                    $("#failoverImageName").html(fileName);
                    if (msg.result === true) {
                        var time = new Date().getTime();
                        $("#failoverImage").attr("src", "/failoverimage?time=" + time);
                    } else {
                        $("#failoverImage").attr("src", "/images/screenshot_noimage_small.png");
                    }
                    $(".upload_disable").prop("disabled", false);
                    $("button").prop("disabled", false);
                });
            } else {
                $("#failoverImage").attr("src", "/images/screenshot_noimage_small.png");
                $("#failoverImageName").html(getLanguageText(languageTable, "There's No Backup file"));
            }
        });
    });
}

function showFailOverProritySettingModal() {
    console.log("showFailOverProritySettingModal()");
    $("#failOverPrioritySettingModal .modal-body").html("");
    drawFailOverPriority();
}

function setFailOver() {
    var applyResult = true;
    var failover = ""
    var settings = undefined;

    $("#btnFailover").prop("checked") ? failover = $("input[name=failOverPriority]:checked").val() : failover = "off";

    if (failover == "manual") {
        settings = {
            failover: failover,

        }

        for (var i = 0; i < inputList.length; i++) {
            var property = "failoverPriority" + (i + 1);
            settings[property] = inputList[i].settingKey;
        }
    } else {
        settings = {
            failover: failover
        };
    }

    setSystemSettings("commercial", settings, function (m) {
        if (!m.returnValue) {
            applyResult = false;
        }
    });

    if (failover != "off") {
        setAdvancedSetting(function (m) {
            if (!m.returnValue) {
                applyResult = false;
            }
            showReturnValue(applyResult, "", "failover");
        });
    } else {
        showReturnValue(applyResult, "", "failover");
    }
}

function initFailOver() {
    $("#failOverPrioritySettingModal").css({"padding-top": (window.innerHeight * 0.25) + "px"});

    getFailOverPriorityInfo(function (failOverPriorityInfo) {
        inputList = failOverPriorityInfo;
        updateFailOverPriorityInfo();
        drawFailOverPriority();
    });

    getFailOver(function (msg) {
        if (msg.failover == "off") {
            $("#btnFailover").prop("checked", false);
            $("#btnFailover").change();
        } else {
            $("#btnFailover").prop("checked", true);
            $("#failOverPriority" + msg.failover).attr("checked", true);
            $("input[name=failOverPriority]").change();
        }
        $("#rootContainer").show();
    });
}

function drawFailOverPriority() {
    var html = "";
    var float = locale === "ar-SA" ? "left" : "right";

    for (var i = 1; i <= inputList.length; i++) {
        html += "<h4 id='failOverPriority" + i + "'>" + "\n";
        html += "   <div class='priority_title'>" + getLanguageText(languageTable, "Priority") + " " + i + "</div>" + "\n";
        html += "   <div class='priority_item' style='font-weight: bold;'>" + inputList[i - 1].name + "</div>" + "\n";
        html += "   <div class='btn_priority btn_priority_up mobile' style='float: " + float + "' " +
            "onclick=setPriorityUp('failOverPriority" + i + "')></div>" + "\n";
        html += "   <div class='btn_priority btn_priority_down mobile' style='float: " + float + "' " +
            "onclick=setPriorityDown('failOverPriority" + i + "')></div>" + "\n";
        html += "</h4>" + "\n";
    }

    $("#failOverPrioritySettingModal .modal-body").append(html);
    if (locale === "ar-SA") {
        $("#failOverPrioritySettingModal .modal-body h4").css("padding-right", "20px")
    }

}

function updateFailOverPriorityInfo() {
    var failOverPriorityInfo = "";
    for (var i = 0; i < inputList.length; i++) {
        failOverPriorityInfo += (i + 1) + ". " + inputList[i].name + " ";
    }
    $("#failOverPriorityInfo span").text(failOverPriorityInfo);
}

function setPriorityUp(id) {
    var priority = id.charAt(id.length - 1);
    if (priority > 1) {
        var curText = $("#failOverPrioritySettingModal #" + id + " .priority_item").text();
        var upText = $("#failOverPrioritySettingModal #" + (id.substring(0, id.length - 1) + (parseInt(priority) - 1)) + " .priority_item").text();
        $("#failOverPrioritySettingModal #" + id + " .priority_item").text(upText);
        $("#failOverPrioritySettingModal #" + (id.substring(0, id.length - 1) + (parseInt(priority) - 1)) + " .priority_item").text(curText);
    }
}

function setPriorityDown(id) {
    var priority = id.charAt(id.length - 1);
    if (priority < inputList.length) {
        var curText = $("#failOverPrioritySettingModal #" + id + " .priority_item").text();
        var downText = $("#failOverPrioritySettingModal #" + (id.substring(0, id.length - 1) + (parseInt(priority) + 1)) + " .priority_item").text();
        $("#failOverPrioritySettingModal #" + id + " .priority_item").text(downText);
        $("#failOverPrioritySettingModal #" + (id.substring(0, id.length - 1) + (parseInt(priority) + 1)) + " .priority_item").text(curText);
    }
}

function savePriority() {
    var newInputList = [];
    for (var i = 0; i < inputList.length; i++) {
        for (var j = 0; j < inputList.length; j++) {
            if ($("#failOverPrioritySettingModal #failOverPriority" + (i + 1) + " .priority_item").text() === inputList[j].name) {
                newInputList[i] = inputList[j];
                break;
            }
        }
    }
    inputList = newInputList;

    updateFailOverPriorityInfo();
}

function changeRtl() {
    $("#failOverPrioritySettingModal").attr("dir", "rtl");
    $(".failover_menu_mobile").css({"margin-left": "0px", "margin-right": "-70px"});
    $(".advanced_setting_menu").css({"padding-left": "0px", "padding-right": "114px"});
    $(".advanced_setting_menu_mobile").css({"margin-left": "0px", "margin-right": "-70px"});
    $("#failOverPriorityInfo span").attr("dir", "ltr");
}
