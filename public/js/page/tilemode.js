function getData() {

    getTileModeValue(function (msg) {
        $("#tileMode").html(getLanguageText(languageTable, toCapitalize(msg.tileMode)));
        $("#row").html(msg.tileRow);
        $("#column").html(msg.tileCol);
        $("#tileID").html(msg.tileId);
        $("#naturalMode").html(getLanguageText(languageTable, toCapitalize(msg.naturalMode)));
        $("#naturalSize").html(msg.tileNaturalSize);

        if (msg.tileMode == "off")
            $("#tileModeViewSpace").css('display', 'none');

        drawTileMode(msg.tileRow, msg.tileCol, msg.tileId);
    });

}

function drawTileMode(row, column, tileID) {

    var divs = "";
    var width = 0;
    var height = 0;

    if ((row < 6) && (column < 6)) {

        if (row > column) {
            height = 186 / row;
            width = (height / 2) * 3;
        } else {
            width = 280 / column;
            height = (width / 3) * 2;
        }

        for (var i = 0; i < row; i++) {
            divs += "<tr>"
            for (var j = 0; j < column; j++) {
                console.log("tileID::" + tileID + ", X::" + ( (i * column) + j + 1 ));
                divs += "<td style='border:2px solid " + ( tileID == ( (i * column) + j + 1 ) ? "red" : "black") + ";width:" + width + "px;height:" + height + "px; text-align: center;'>" + ( (i * column) + j + 1 ) + "</td>";
            }
            divs += "</tr>"
        }
        ;
    } else {
        divs += "<td style='border:2px solid black;width:280px;height:186px; text-align: center;'  valign=middle>" + row + " X " + column + "</td>";
    }

    $("#tileModeView").append(divs);

}

$(document).ready(function () {
    getData();
    $('#pageTitle').html(getLanguageText(languageTable, "Tile Mode"));
});
