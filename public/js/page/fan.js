function toRpmString(rpm) {
    if (rpm < 0) {
        return "<font color=red>NG";
    }
    return rpm;
}

function refreshSensor() {
    $('.alert').remove();
    updateFanStatus();
    updateTemperature();
}

function updateFanStatus() {
    getFanStatus(function (res) {
        $(".table-fan .fan_name").each(function () {
            var text = $(this).text();
            if (text === "Normal Fan" || text === "PSU Fan") {
                text = text.replace(" Fan", "");
                $(this).text(getLanguageText(languageTable, text) + " " + getLanguageText(languageTable, "Fan"));
            } else {
                $(this).text(getLanguageText(languageTable, text));
            }
        });

        var types = res.group;
        types.forEach(function (type) {
            var fan = res[type];
            fan.status.forEach(function (status, index) {
                var rpm = ' - '
                var selector = '#' + type + (index + 1) + 'rpm';
                if (status) {
                    rpm = fan.rpm[index];
                    $(selector).css('color', '');
                } else {
                    rpm = getLanguageText(languageTable, 'NG');
                    $(selector).css('color', 'red');
                }
                $(selector).text(rpm);
            });
        });
    });
}

function updateTemperature() {
    getTemp(function (res) {
        var types = res.group ? res.group : [];
        types.forEach(function (type) {
            var sensor = res[type];
            var temperature = sensor.celsius;

            readTempType(function (tempType) {
                if (!tempType.isCelsius) {
                    $('#' + type + 'Temp').html(Math.floor((temperature) * 1.8 + 32) + '°F');
                } else {
                    $('#' + type + 'Temp').html(Number(temperature) + '°C');
                }
            });
        });
    });
}
