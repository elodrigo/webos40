var currentVer = "";
var updatePercent = 0;
var updateStarted = false;

var updateMsg = "";

function init(env) {
    updateMsg = getLanguageText(languageTable, "Please Keep this Signage on.") + "<br>";
    updateMsg += getLanguageText(languageTable, "The POWER remote controller key will not work during the update.") + "<br>";
    updateMsg += getLanguageText(languageTable, "Do not reboot in Device page.");

    var fileName = undefined;
    addConfirmModal('deleteModal', getLanguageText(languageTable, "Delete?"), '', function () {
        deleteFile("update", fileName, function () {
            window.location.reload(true);
        });
    });

    socket.on('swupdate', function (msg) {
        var micomProgress = -1;
        if (!updateStarted) {
            getMicomUpdateProgress(function (msg) {
                if (micomProgress === -1) {
                    $('.progress-bar').removeClass("active progress-bar-striped");
                    $('#update-prog').hide();
                    $('#micom-prog').show();
                }

                micomProgress = msg.progress;

                $('#micomProgressTitle').html(getLanguageText(languageTable, 'Micom Progress').replace('Micom', msg.type));
                $('#micomProgress').html(msg.progress + "%");
                $('#micomProgress').css('width', msg.progress + '%');
                $('#overlay_progress').html(msg.type + " " + getLanguageText(languageTable, "Update") + " : " + msg.progress + "%");
            });
        }

        updateStarted = true;

        if (updatePercent > 0 && msg == 0) {
            overlayControl(false);
            showWarning(getLanguageText(languageTable, "Fail to update. Check xxx file please.").replace(/xxx/gi, fileName));
            return;
        }

        if (updatePercent > 0 && updatePercent == msg) {
            return;
        }

        updatePercent = msg;

        $('#updateProgress').html(msg + "%");
        $('#overlay_progress').html(msg + "%");
        $('#updateProgress').css('width', msg + '%');

        if (msg == 100) {
            var updateCompletedMsg = getLanguageText(languageTable, "Update completed. Signage is rebooting now.") + "<br>" + getLanguageText(languageTable, "Please login again.");
            overlayControl(false);
            overlayControl(true, updateCompletedMsg);

            setTimeout(function () {
                setSWUpdateStatus(false);
                window.location.replace("/login");
            }, 2 * 60 * 1000);
        }
    });

    addConfirmModal('updateModal', getLanguageText(languageTable, "Update?"), '', function () {
        $('#update-prog').show();
        $('.progress-bar').addClass("active progress-bar-striped");

        overlayControl(true, getLanguageText(languageTable, "S/W Updating") + "......<br>" + updateMsg);

        var micomUpdate = $('#micomUpdate').prop('checked');

        updatePercent = 0;
        updateStarted = false;

        swupdate(fileName, micomUpdate, function (m) {
            if (m.returnValue) {
                setSWUpdateStatus(true);
            }
        });

        setTimeout(function () {
            getUpdateStatus(function (msg) {
                if (msg.status != 'in progress' && !updateStarted) {
                    overlayControl(false);
                    showWarning(getLanguageText(languageTable, "Cannot start S/W update. Check xxx file please.").replace(/xxx/gi, fileName));
                }
            });
        }, 10 * 1000);
    });

    if (env.supportMicomFanControl) {
        addConfirmModal('micomUpdateModal', getLanguageText(languageTable, "Fan Micom Update?"), '', function () {
            overlayControl(true, getLanguageText(languageTable, "Fan Micom S/W Updating") + "......<br>" + updateMsg);
            fanMicomUpdate(fileName, function (msg) {
                var warningMsg = "";
                if (msg.returnValue) {
                    warningMsg = getLanguageText(languageTable, "Update fan micom with xxx done").replace(/xxx/gi, fileName);
                } else {
                    warningMsg = getLanguageText(languageTable, "Failed to update fan micom with xxx. Check the file please").replace(/xxx/gi, fileName);
                }
                showWarning(warningMsg, 10 * 1000);
                overlayControl(false);
            });
        });

        env.uploadedFiles.forEach(function (file) {
            var ext = file.substr(file.length - 3, 3).toLowerCase();
            var button = $(".update[name='" + file + "']");
            if (ext === 'txt') {
                button.prop('disabled', false);
                button.removeClass('update');
                button.addClass('fanMicomUpdate');
            }
        });

        $('.fanMicomUpdate').click(function () {
            fileName = $(this).attr('name');
            $('#micomUpdateModalContent').html(getLanguageText(languageTable, "Update fan micom with this S/W Update file?") + "<br>" + fileName);
            $('#micomUpdateModalContent').css('word-break', 'break-all');
            $('#micomUpdateModal').modal();
        });
    }

    getBasicInfo(function (msg) {
        currentVer = msg.firmwareVersion;
    });

    listUpdates(function (msg) {
        if (!msg || !msg.returnValue) {
            return;
        }

        msg.fileinfo.forEach(function (file) {
            var button = $(".update[name='" + file.name + "']");
            var newVersion = file.version.toString(16);
            newVersion = '0' + newVersion.substring(0, 1) + '.' + newVersion.substring(1, 3) + '.' + newVersion.substring(3, 5);
            button.prop('id', newVersion);
            button.prop('disabled', false);
            $('.file-info[name="' + file.name + '"]').append('(' + newVersion + ')');

            var curVerInteger = parseInt(currentVer.replace(/[.]/g, ""));
            var newVerInteger = parseInt(newVersion.replace(/[.]/g, ""));

            if (isNaN(curVerInteger)) {
                curVerInteger = 0;
            }

            if (curVerInteger == newVerInteger) {
                $('.file-list[name="' + file.name + '"]').css("background-color", "#c6342f");
                $('.file-list[name="' + file.name + '"]').css("color", "white");
                $('.img-button-update[name="' + file.name + '"]').css("visibility", "hidden");
            } else if (curVerInteger > newVerInteger) {
                $('.file-list[name="' + file.name + '"]').css("background-color", "#bfb7b7");
                $('.file-list[name="' + file.name + '"]').css("color", "white");
            }
        });
    });

    $(".filebox :file").on("change", function () {
        var file = document.getElementById('upload').files[0];
        var fileName = filteringXSS(file.name);

        // check extension of file
        var ext = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();
        if (supportExt.indexOf(ext) < 0) {
            showWarning(getLanguageText(languageTable, "Unsupported File type."), 5 * 1000);
            resetInputFile($(this));
            return;
        }

        // check maxsize
        if (file.size > env.maxSize) {
            showWarning(getLanguageText(languageTable, "Flash memory is not enough."), 5 * 1000);
            resetInputFile($(this));
            return;
        }

        $('#fileName').html(fileName);
        $('#upload-submit').css("display", "inline-block");
        $('#upload-label').css("display", "none");
    });

    $(".update").click(function () {
        fileName = $(this).attr('name');
        var newVer = $(this).attr('id');
        if (newVer === undefined) {
            newVer = '-';
        }
        var modalMsg = getLanguageText(languageTable, "Do you want to install this S/W Update file?") + "<br>";
        modalMsg += "(" + getLanguageText(languageTable, "Newer Ver.") + ") " + newVer + "<br>";
        modalMsg += "(" + getLanguageText(languageTable, "Current Ver.") + ") " + currentVer + "<br>";
        $('#updateModalContent').html(modalMsg);
        $('#updateModal').modal();
    });

    $(".delete").click(function () {
        fileName = $(this).attr('name');
        $('#deleteModalContent').html(getLanguageText(languageTable, "Do you want to delete this S/W Update file?") + "<br>" + fileName);
        $('#deleteModalContent').css('word-break', 'break-all');
        $('#deleteModal').modal();
    });

    function uploadFile(file) {
        var formData = new FormData();
        var newname = filteringXSS(file.name); //filter for XSS

        formData.append('target', 'update');
        formData.append('newname', newname);
        formData.append('update', file);

        $('#upload-prog').show();
        overlayControl(true, getLanguageText(languageTable, "Uploading") + "\n" + newname);
        document.onkeydown = function (e) {
            return false;
        }

        $('input').prop('disabled', true);
        $('button').prop('disabled', true);

        var xhr = new XMLHttpRequest();

        xhr.open('post', '/upload', true);
        //xhr.setRequestHeader("Content-type","multipart/form-data");

        xhr.upload.onprogress = function (e) {
            if (e.lengthComputable) {
                var per = Math.round((e.loaded / e.total) * 100)
                $('#uploadProgress').html(per + "%");
                $('#uploadProgress').css('width', per + '%');
                $('#overlay_progress').html(per + "%");
            }
        };

        xhr.upload.onload = function (e) {
            setTimeout(function () {
                overlayControl(false);
                window.location.reload(true);
            }, 2000);
        }

        xhr.onerror = function (e) {
            overlayControl(false);
            showWarning(getLanguageText(languageTable, "An error occurred while submitting the form."), 5 * 1000);
        };

        xhr.onload = function () {
            console.log(this.statusText);
        };

        xhr.send(formData);
    }

    $('input[type="submit"]').on('click', function (evt) {
        evt.preventDefault();
        var file = document.getElementById('upload').files[0];

        if (!file) {
            showWarning(getLanguageText(languageTable, "Please choose a file to upload"), 5 * 1000);
            return;
        }

        var fileName = filteringXSS(file.name);
        if (env.uploadedFiles.indexOf(fileName) >= 0) {
            var modalTitle = getLanguageText(languageTable, "Overwrite?");
            var modalMsg = getLanguageText(languageTable, "Overwrite xxx?").replace(/xxx/gi, fileName);
            addConfirmModal("overwrite", modalTitle, modalMsg,
                function () {
                    setTimeout(function () {
                        uploadFile(file);
                    }, 500);
                    $('#overwrite').close();
                    $('#overwrite').remove();
                }, function () {
                    resetInputFile($('.filebox :file'));
                }
            );
            $('#overwrite').modal();
            return;
        }

        uploadFile(file);
    });

    $('#upload-prog').hide();
    $('#update-prog').hide();
    $('#micom-prog').hide();

    $('#pageTitle').html(getLanguageText(languageTable, "S/W Update"));

    if (locale == "ar-SA") {
        changeRtl();
    }
}

function resetInputFile(elm) {
    if (getInternetExplorerVersion() > -1) {
        elm.attr('type', 'radio');
        elm.attr('type', 'file');
    } else {
        elm.val("");
    }

    $('#fileName').html(getLanguageText(languageTable, 'Select a file to upload'));
    $('#upload-label').css('display', "inline-block");
    $('#upload-submit').css('display', "none");
}

function changeRtl() {
    $(".table>tbody>tr>th").css({"text-align": "start"});
    $(".table-update-file .file-info").attr("dir", "ltr");
    $(".table-update-file .file-info").css("text-align", "end");
}
