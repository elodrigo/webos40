function callGroupAPI(command, data, event, callback) {
	if (event && callback) {
		var id = Math.round(Math.random() * 1000000);
		socket.on(event + id, function(msg) {
			callback(msg);
			socket.removeAllListeners(event + id);
		});
		data.eventID = id;
	}
	data.command = command;
	socket.emit("group", data);
}

function createSecureKey(word, callback) {
	callGroupAPI('createSecureKey', {
		word: word
	}, 'secureKey', callback);
}

function getSecureKey(callback) {
	callGroupAPI('getSecureKey', {}, 'secureKey', callback);
}

function setSecureKey(key) {
	callGroupAPI('setSecureKey', {
		key: key
	});
}

function getPeers(callback) {
	callGroupAPI('getPeers', {}, 'peer', callback);
}

function addPeer(https, ip, port) {
	callGroupAPI('addPeer', {
		peer: {
			https: https === true,
			ip: ip,
			port: Number(port)
		}
	});
}

function removePeer(index) {
	callGroupAPI('removePeer', {
		index: index
	});
}

function getPeerIncidents(index, callback) {
	callGroupAPI('getPeerIncidents', {
		index: index
	}, 'peer', callback);
}

function getPeerScreen(index, callback) {
	callGroupAPI('getPeerScreen', {
		index: index
	}, 'peer', callback);
}

function getPeerStatus(index, callback) {
	callGroupAPI('getPeerStatus', {
		index: index
	}, 'peer', callback);
}
