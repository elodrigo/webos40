/**
 * Created by byungjin.park on 17. 5. 8.
 */
var MAX_LOGIN_TIMES = 5;
var WAITING_TIME_RESTRICTED = 5 * 60 * 1000; // 5min
var synth = window.speechSynthesis;

$(document).ready(function () {
    applyLanguageText();
    changeRtlDir();

    $.ajax({
        type: "get",
        url: "/getLoginStatus",
        data: {},
        success: function (data) {
            console.log(data)
            if (/^fail/.test(data) || /^restricted/.test(data)) {
                setLoginTryInfoExpires(data);
            }
        }
    });

    window.onload = function () {
        setActiveForTouch();
        $("#btnLogin").on("click", onClickedLogin);
    }

    $('.btn-login-refresh').on('click', function () {
        refreshCaptcha();
    });

    $('#login-error-ok').on('click', function () {
        loginErrorOK();
    });

    $('.btn-voice').on('click', function () {
        $.ajax({
            type: "get",
            url: "/getCaptchaString",
            data: {},
            success: function (data) {
                speakCaptcha(data);
            }
        });
    });

});

/*$(window).load(function () {
    setActiveForTouch();
    $("#btnLogin").on("click", onClickedLogin);
});*/

function isSupportBrowser() {
    return !!synth;
}

function speakCaptcha(captchaText) {
    if (isSupportBrowser()) {
        var language = window.navigator.userLanguage || window.navigator.language;
        synth.cancel();
        const speechMsg = new SpeechSynthesisUtterance();
        speechMsg.rate = 0.7;
        speechMsg.pitch = 1;
        speechMsg.lang = language;
        speechMsg.text = captchaText;
        synth.speak(speechMsg);
    } else {
        alert("Speech to text is not supported in this browser.");
        return;
    }
}


function setActiveForTouch() {
    $(".btn_default").on("touchstart", function (event) {
        $(this).addClass("active");
    });
    $(".btn_default").on("touchend", function (event) {
        $(this).removeClass("active");
    });
}

function onClickedLogin(event) {
    var pinPassword = $(".input_pin").val();

    if (pinPassword.length == 0) {
        $("#txtCategoryPin").css("color", "#ff0000");
        $("#txtCategoryCaptcha").css("color", "#a6a6a6");
        return;
    }

    var captcha = $(".input_captcha").val();

    if (!env.powerOnlyMode && (!captcha || captcha.length == 0)) {
        $("#txtCategoryPin").css("color", "#a6a6a6");
        $("#txtCategoryCaptcha").css("color", "#ff0000");
        return;
    }

    $("#txtCategoryPin").css("color", "#a6a6a6");
    $("#txtCategoryCaptcha").css("color", "#a6a6a6");

    event.preventDefault();
    requestLogin(pinPassword, captcha);
}

var tryTime = 0;
var remain = 0;
var restrictInterval = undefined;

function setLoginTryInfoExpires(data) {
    var arr = data.split(':');
    tryTime = Number(arr[1]);
    remain = Number(arr[2]);
    if (tryTime >= MAX_LOGIN_TIMES && remain > 0) {
        $('#login-error-modal').addClass('show');
    }
    updateRestrictedMsg($("#txtErrorMsg"), tryTime, remain);

    if (arr[0] == 'fail') {
        $('#login-error-modal').addClass('show');
        return;
    }

    if (restrictInterval == undefined) {
        restrictInterval = setInterval(function() {
            --remain;
            updateRestrictedMsg($("#txtErrorMsg"), tryTime, remain);
            if (remain == 0) {
                clearInterval(restrictInterval);
                restrictInterval = undefined;
            }
        }, 1000);
    }
};

function requestLogin(pinPassword, captcha) {
    $.ajax({
        type: "post",
        url: "/loginLed",
        data: {password: pinPassword, captcha: captcha},
        success: function (data) {
        console.log("🚀 ~ file: signage_login.js ~ line 145 ~ requestLogin ~ data", data)
            if (/^fail/.test(data)) {
                refreshPinPasword();
                refreshCaptcha();
                setLoginTryInfoExpires(data);
                return;
            } else if (/^restricted/.test(data)) {
                refreshPinPasword();
                refreshCaptcha();
                setLoginTryInfoExpires(data);
                return;
            } else {
                if (data === "first_login") {
                    showFirstLoginPopup();
                } else if (data === "success") {
                    location.replace("/ledSignage/language");
                    return;
                }
            }
        }
    });
}

function showFirstLoginPopup() {
console.log("🚀 ~ file: signage_login.js ~ line 169 ~ showFirstLoginPopup ~ showFirstLoginPopup", 'showFirstLoginPopup')
    
    var callback = function () {
        location.replace("/ledSignage/signage_change_pwd");
    };
    var message = getLanguageText(languageTable, "For your security, we recommend to change default password. Would you like to change your password now?");
    $('#login-failed-text').text(message);
    $('#login-error-ok').click(callback);
    $('#login-error-modal').addClass('show');
    // $("#popupFirstLogin").css({"padding-top": (window.innerHeight * 0.25) + "px"});
    // $("#popupFirstLogin .ok_btn").html($(".ok_btn").html().toUpperCase());
    // $("#popupFirstLogin .ok_btn").click(callback);
    // $("#popupFirstLogin").modal();
}

function applyLanguageText() {
    $(".btn_bottom_menu .txt_cancel").text($(".btn_bottom_menu .txt_cancel").text().toUpperCase());
    $(".btn_bottom_menu .txt_login").text($(".btn_bottom_menu .txt_login").text().toUpperCase());
}

function refreshPinPasword() {
    $(".input_pin").val("");
}

function refreshCaptcha() {
    $(".img_captcha img").attr("src", "/request/captchapng?timestamp=" + new Date().getTime());
    $(".input_captcha").val("");
}

function updateRestrictedMsg(node, loginTryTimes, remainTime) {
    var msg = "";
    if (loginTryTimes >= MAX_LOGIN_TIMES && remainTime > 0) {
        var min = Math.floor(remainTime / 60);
        var sec = Math.floor(remainTime - (min * 60));

        if (min < 10)
            min = "0" + min;
        if (sec < 10)
            sec = "0" + sec;

        msg = getLanguageText(languageTable, "You have failed to login 5 times. Please try again after 05:00.");
        if (/05:00/.test(msg)) {
            msg = msg.replace("05:00", min + ":" + sec);
        } else if (/5:00/.test(msg)) {
            msg = msg.replace("5:00", min + ":" + sec);
        } else if (/05.00/.test(msg)) {
            msg = msg.replace("05.00", min + ":" + sec);
        }
        $('#login-failed-text').text(msg);

    } else if (loginTryTimes == 1) {
        msg = getLanguageText(languageTable, "You have failed to login 1/5 time.");
        $('#login-failed-text').text(msg);
    } else if (loginTryTimes > 1 && loginTryTimes < MAX_LOGIN_TIMES) {
        msg = getLanguageText(languageTable, "You have failed to login 1/5 time.");
        msg = msg.replace("1", loginTryTimes);
        $('#login-failed-text').text(msg);
    } else {
        msg = "&nbsp;";
        refreshCaptcha();
    }

    node.html(msg);
}

function loginErrorOK() {
    $('#login-error-modal').removeClass('show');
}