document.addEventListener("DOMContentLoaded", function () {
  $("head").append(
    '<link rel="stylesheet" type="text/css" href="/assets/css/control_led_w4/popup_network_ping.css">'
  );
  // $('head').append('<link rel="stylesheet" type="text/css" href="/assets/css/control_led_w4/popup_control_input.css">');

  createCurrentMenu(1, "network");

  addConfirmModals(
    "",
    "auto-modal",
    getLanguageText(languageTable, "Automatically Assign IP Address"),
    `${getLanguageText(
      languageTable,
      "Are you sure to use Wired DHCP?"
    )} <br> ${getLanguageText(
      languageTable,
      "You may not be able to reconnect to web pages if you cannot find out new IP address."
    )}`,
    false,
    function () {
      // _closePopup('auto-modal');
    },
    function () {
      auto.prop("checked", false);
      auto.attr("aria-checked", false);
      // _closePopup('auto-modal');
    }
  );

  addAlertModals(
    "alert-info-modal",
    "IP 주소 또는 DHCP 상태를 변경했습니다. <br> 변경된 IP 주소로 다시 연결하세요."
  );

  var networkAlertContainer = document.getElementById("network-warning-modal");
  var networkAlertFocusTrap = focusTrap.createFocusTrap(
    "network-warning-modal",
    {
      onActivate: function () {
        networkAlertContainer.classList.add("is-active");
      },
      onDeactivate: function () {
        networkAlertContainer.classList.remove("is-active");
      },
      escapeDeactivates: false,
    }
  );

  addAlertModals(
    "network-warning-modal",
    getLanguageText(languageTable, "Input Signage Name please."),
    function () {
      networkAlertFocusTrap.deactivate();
    }
  );

  var prevData = {
    wired: {},
    wifi: {},
  };
  var invalidTxt = "";
  var isSupportIpv6 = false;
  var isSupportIpv6Wifi = false;
  var isSelectIpv6 = false;
  var isSelectIpv6Wifi = false;
  var path = location.protocol + "//" + location.host + location.pathname;
  var isNameChanged = false;

  var ipv4 = $("#ipv4");
  var ipv6 = $("#ipv6");
  var auto = $("#dhcp");
  var address = $("#network-ip");
  var subnet = $("#network-subnet");
  var gateway = $("#network-gateway");
  var dns = $("#network-dns");
  var btn = $("#btn-ip-confirm");

  ipv4.on("change", function () {
    var isChecked = $(this).prop("checked");
    if (isChecked) {
      auto.prop("disabled", false);
      address.prop("disabled", false);
      subnet.prop("disabled", false);
      gateway.prop("disabled", false);
      dns.prop("disabled", false);
      btn.prop("disabled", false);
    }
  });

  ipv6.on("change", function () {
    var isChecked = $(this).prop("checked");
    if (isChecked) {
      auto.prop("disabled", true);
      address.prop("disabled", true);
      subnet.prop("disabled", true);
      gateway.prop("disabled", true);
      dns.prop("disabled", true);
      btn.prop("disabled", true);
    }
  });

  ipv4.on("click", function () {
    $(this).attr("checked", true);
    ipv6.prop("checked", false);
    ipv6.attr("checked", false);
  });

  ipv6.on("click", function () {
    $(this).attr("checked", true);
    ipv4.prop("checked", false);
    ipv4.attr("checked", false);
  });

  auto.on("change", function () {
    var isChecked = auto.prop("checked");
    auto.attr("aria-checked", isChecked);
    if (!isChecked) {
      address.prop("readonly", false);
      address.removeClass("readonly");
      subnet.prop("readonly", false);
      subnet.removeClass("readonly");
      gateway.prop("readonly", false);
      gateway.removeClass("readonly");
      dns.prop("readonly", false);
      dns.removeClass("readonly");
    } else {
      showMyModal("auto-modal");
      address.prop("readonly", true);
      address.removeClass("readonly");
      subnet.prop("readonly", true);
      subnet.removeClass("readonly");
      gateway.prop("readonly", true);
      gateway.removeClass("readonly");
      dns.prop("readonly", true);
      dns.removeClass("readonly");
    }
  });

  //   $("#btn-ip-confirm").on("click", function () {
  //     var addressLen = address.val().length;
  //     var subnetLen = subnet.val().length;
  //     var gatewayLen = gateway.val().length;
  //     var dnsLen = dns.val().length;

  //     if (addressLen > 0 && subnetLen > 0 && gatewayLen > 0 && dnsLen > 0) {
  //       showMyModal(
  //         "alert-info-modal",
  //         "IP 주소 또는 DHCP 상태를 변경했습니다. <br> 변경된 IP 주소로 다시 연결하세요."
  //       );
  //     } else {
  //       var text = `${getLanguageText(languageTable, "Wired")}<br> ${
  //         addressLen < 1
  //           ? getLanguageText(languageTable, "IP Address") + "<br>"
  //           : ""
  //       } ${
  //         subnetLen < 1
  //           ? getLanguageText(languageTable, "Subnet Mask") + "<br>"
  //           : ""
  //       }
  //                         ${
  //                           gatewayLen < 1
  //                             ? getLanguageText(languageTable, "Gateway") + "<br>"
  //                             : ""
  //                         } ${
  //         dnsLen < 1 ? getLanguageText(languageTable, "DNS Server") + "<br>" : ""
  //       } 를 입력해주세요.`;
  //       showMyModal("alert-info-modal", text);
  //     }
  //   });

  //   ipv4.prop("checked", true);

  $("#dhcp-switch-label").on("click", function () {
    showMyModal("dhcp-switch-modal");
  });

  $("#btn-ping-test").on("click", function () {
    showMyModal("ping-address-popup");
  });

  var dhcpContent = `${getLanguageText(
    languageTable,
    "Are you sure to use Wired DHCP?"
  )}<br/>${getLanguageText(
    languageTable,
    "You may not be able to reconnect to web pages if you cannot find out new IP address."
  )}`;
  addConfirmModals(
    "#dhcp-switch-modal-btn",
    "dhcp-switch-modal",
    getLanguageText(languageTable, "information message"),
    dhcpContent,
    false,
    function () {
      var dhcp = $("#dhcp");
      var checked = dhcp.prop("checked");
      dhcp.prop("checked", !checked);
      dhcp.attr("aria-checked", !checked);
      controlDhcp(false);
    },
    function () {
      // undo checking
      var dhcp = $("#dhcp");
      dhcp.prop("checked", false);
      dhcp.attr("aria-checked", false);
    }
  );

  function controlDhcp(wifi) {
    var suffix = wifi ? "wifi" : "";
    var prev = wifi ? prevData.wifi : prevData.wired;

    var checked = $("#dhcp" + suffix).attr("aria-checked");
    var isIpv6 = (wifi && isSelectIpv6Wifi) || (!wifi && isSelectIpv6);
    var ip = $("#network-ip" + suffix);
    var netMask = $("#network-subnet" + suffix);
    var gateway = $("#network-gateway" + suffix);
    var dns = $("#network-dns" + suffix);
    // $(ip).prop('disabled', checked || isIpv6);
    // $(netMask).prop('disabled', checked || isIpv6);
    // $(gateway).prop('disabled', checked || isIpv6);
    // $(dns).prop('disabled', checked || isIpv6);

    if (checked === "true" || isIpv6) {
      $(ip).addClass("readonly");
      $(ip).prop("readonly", true);
      $(netMask).addClass("readonly");
      $(netMask).prop("readonly", true);
      $(gateway).addClass("readonly");
      $(gateway).prop("readonly", true);
      $(dns).addClass("readonly");
      $(dns).prop("readonly", true);

      $(ip).val(prev.ip);
      $(netMask).val(prev.netMask);
      $(gateway).val(prev.gateway);
      $(dns).val(prev.dns);
    } else {
      $(ip).removeClass("readonly");
      $(ip).prop("readonly", false);
      $(netMask).removeClass("readonly");
      $(netMask).prop("readonly", false);
      $(gateway).removeClass("readonly");
      $(gateway).prop("readonly", false);
      $(dns).removeClass("readonly");
      $(dns).prop("readonly", false);
    }
  }

  getNetworkData();

  function getDnsInfo(wifi, data) {
    if (!data) {
      return "";
    }
    var dns = "";
    var repeat = true;
    var i = 1;
    while (repeat) {
      var key = "dns" + i;
      if (data[key]) {
        if ((wifi && isSelectIpv6Wifi) || (!wifi && isSelectIpv6)) {
          if (data[key].indexOf(":") >= 0) {
            dns = data[key];
            repeat = false;
          }
        } else {
          if (data[key].indexOf(".") >= 0) {
            dns = data[key];
            repeat = false;
          }
        }
      } else {
        repeat = false;
      }

      i++;
    }
    return dns;
  }

  function getIsSupportIpv6(wifi, data) {
    var ipv6Data = wifi ? data.wifi : data.wired;
    var ipv6Type = jQuery.type(ipv6Data.ipv6);

    return !(ipv6Data.state === "disconnected" || ipv6Type === "undefined");
  }

  function changeIPv6View(wifi) {
    var which = wifi ? "wifi" : "";
    if ((wifi && isSelectIpv6Wifi) || (!wifi && isSelectIpv6)) {
      $("#dhcp" + which + "Low").hide();
      $("#network-subnet" + which + "_low").hide();

      $("#netPrefix" + which).attr("disabled", true);
    }

    if ((wifi && !isSelectIpv6Wifi) || (!wifi && !isSelectIpv6)) {
      $("#netPrefix" + which + "_low").hide();
    }
  }

  function savePrevData(wifi, data) {
    var ipv6;
    var prev = wifi ? prevData.wifi : prevData.wired;
    data = wifi ? data.wifi : data.wired;

    prev.ip = data.ipAddress;
    prev.gateway = data.gateway;
    prev.netMask = data.netmask;
    prev.dhcp = data.method === "dhcp";
    prev.dns = getDnsInfo(wifi, data);
    prev.connected = data.state === "connected";

    if (wifi && isSelectIpv6Wifi) {
      ipv6 = data.ipv6;
      prev.gateway = ipv6.gateway;
      prev.ip = ipv6.ipAddress;
    }

    if (!wifi && isSelectIpv6) {
      ipv6 = data.ipv6;
      prev.gateway = ipv6.gateway;
      prev.ip = ipv6.ipAddress;
    }

    if (wifi) {
      prev.ssid = data.ssid;
    }
  }

  function setPanelValue(wifi, newData) {
    var data = wifi ? newData.wifi : newData.wired;
    var which = wifi ? "wifi" : "";

    if ((wifi && !isSelectIpv6Wifi) || (!wifi && !isSelectIpv6)) {
      $("#network-subnet" + which).val(data.netmask);
    }

    $("#network-ip" + which).val(data.ipAddress);
    $("#network-gateway" + which).val(data.gateway);
    $("#dhcp" + which).attr("checked", data.method === "dhcp");
    $("#dhcp" + which).change();
    $("#network-dns" + which).val(getDnsInfo(wifi, data));
    $("#network-mac" + which).val(data.macAddress);
  }

  function getNetworkData() {
    getNetworkStatus(function (ret) {
      isSupportIpv6 = getIsSupportIpv6(false, ret);
      isSupportIpv6Wifi = getIsSupportIpv6(true, ret);
      isSupportIpv6 ? (isSelectIpv6 = isSelectIpv6) : (isSelectIpv6 = false);
      isSupportIpv6Wifi
        ? (isSelectIpv6Wifi = isSelectIpv6Wifi)
        : (isSelectIpv6Wifi = false);

      $("#wiredConnection").html(
        ret.wired.state === "connected"
          ? getLanguageText(languageTable, "Connected")
          : getLanguageText(languageTable, "Disconnected")
      );
      $("#wifiConnection").html(
        ret.wifi.state === "connected"
          ? ret.wifi.ssid
          : getLanguageText(languageTable, "Disconnected")
      );

      // if (!isSupportIpv6) {
      //     $('#filter').hide();
      // }
      // if (!isSupportIpv6Wifi) {
      //     $('#wifiFilter').hide();
      // }
      if (!isSupportIpv6) {
        $('#ipv4').prop('checked', true);
      } else {
        $('#ipv6').prop('checked', false);
      }

      changeIPv6View(false);
      changeIPv6View(true);

      if (ret.wired.state !== "connected") {
        $("#wiredPanel").hide();
      } else {
        savePrevData(false, ret);
        setPanelValue(false, ret);
      }

      if (ret.wifi.state !== "connected") {
        $("#wifiPanel").hide();
      } else {
        savePrevData(true, ret);
        setPanelValue(true, ret);
      }
    });
  }

  //   saving

  addConfirmModals(
    "#apply",
    "apply-modal",
    getLanguageText(languageTable, "information message"),
    getLanguageText(languageTable, "Are you sure to change Settings?"),
    false,
    function () {
      saveConfirm();
    }
  );

  $("#apply").on("click", function () {
    showMyModal(
      "apply-modal",
      getLanguageText(languageTable, "Are you sure to change Settings?"),
      "안내메시지"
    );
  });

  function getUserValue(wifi) {
    var suffix = wifi ? "wifi" : "";

    var dhcp = $("#dhcp" + suffix).prop("checked");
    var ip = $("#network-ip" + suffix).val();
    var netMask = $("#network-subnet" + suffix).val();
    var gateway = $("#network-gateway" + suffix).val();
    var dns = $("#network-dns" + suffix).val();

    return {
      dhcp: dhcp,
      ip: ip,
      netMask: netMask,
      gateway: gateway,
      dns: dns,
    };
  }

  function checkIPAddress(ip) {
    var numbers = ip.split(".");

    if (numbers.length !== 4) {
      return false;
    }

    for (var i = 0; i !== 4; ++i) {
      if (!$.isNumeric(numbers[i])) {
        return false;
      }
      var n = parseInt(numbers[i]);

      if (n < 0 || n > 255) {
        return false;
      }
    }

    return true;
  }

  function checkNetMask(netMask) {
    var regExp = new RegExp(
      "^(((128|192|224|240|248|252|254)\\.0\\.0\\.0)|(255\\.(0|128|192|224|240|248|252|254)\\.0\\.0)|(255\\.255\\.(0|128|192|224|240|248|252|254)\\.0)|(255\\.255\\.255\\.(0|128|192|224|240|248|252|254|255)))$"
    );
    return regExp.test(netMask);
  }

  function checkDHCP(className) {
    var elem = $(`.${className}`);
    var suffix = elem.attr("id").indexOf("wifi") > 0 ? "wifi" : "";

    if (elem.prop("checked")) {
      var msg =
        getLanguageText(languageTable, "Are you sure to use Wired DHCP?") +
        "\n";
      if (suffix) {
        msg =
          getLanguageText(languageTable, "Are you sure to use Wi-Fi DHCP?") +
          "\n";
      }
      msg += getLanguageText(
        languageTable,
        "You may not be able to reconnect to web pages if you cannot find out new IP address."
      );
      var ret = confirm(msg);
      console.log(ret);
      $("#dhcp" + suffix).attr("checked", ret);
    }
  }

  function checkValid(wifi) {
    var suffix = wifi ? "wifi" : "";
    var user = getUserValue(wifi);
    var prev = wifi ? prevData.wifi : prevData.wired;
    // var conStr = wifi ? 'Wi-Fi' : 'Wired';
    var checkResult = true;

    if (!checkIPAddress(user.ip)) {
      invalidTxt +=
        "<br>" + suffix + " " + getLanguageText(languageTable, "IP Address");
      $("#network-ip" + suffix).val(prev.ip);
      checkResult = false;
    }

    if (!checkIPAddress(user.gateway)) {
      invalidTxt +=
        "<br>" + suffix + " " + getLanguageText(languageTable, "Gateway");
      $("#network-gateway" + suffix).val(prev.gateway);
      checkResult = false;
    }

    if (!checkNetMask(user.netMask)) {
      invalidTxt +=
        "<br>" + suffix + " " + getLanguageText(languageTable, "Subnet Mask");
      $("#network-subnet" + suffix).val(prev.netMask);
      checkResult = false;
    }

    if (!checkIPAddress(user.dns)) {
      invalidTxt +=
        "<br>" + suffix + " " + getLanguageText(languageTable, "DNS Server");
      $("#network-dns" + suffix).val(prev.dns);
      checkResult = false;
    }

    return checkResult;
  }

  var needReconnection = false;

  function isNetworkSettingChanged(prev, user) {
    var isDhcpChanged = prev.dhcp !== user.dhcp;
    var isIpChanged = prev.ip !== user.ip;
    var isNetMaskChanged = prev.netMask !== user.netMask;
    var isGatewayChanged = prev.gateway !== user.gateway;
    var isDnsChanged = prev.dns !== user.dns;
    return (
      isDhcpChanged ||
      isIpChanged ||
      isNetMaskChanged ||
      isGatewayChanged ||
      isDnsChanged
    );
  }

  function changePrevData(wifi, data) {
    var prev = wifi ? prevData.wifi : prevData.wired;
    prev.dhcp = data.dhcp;
    prev.ip = data.ip;
    prev.netMask = data.netMask;
    prev.gateway = data.gateway;
    prev.dns = data.dns;
  }

  function changeNetworkSettings(wifi) {
    var user = getUserValue(wifi);
    var prev = wifi ? prevData.wifi : prevData.wired;

    if (!isNetworkSettingChanged(prev, user)) {
      console.log("network settings is not changed");
      return;
    }

    if (user.dhcp) {
      user.dns = "";
    }
    console.log(
      "🚀 ~ file: network.js ~ line 550 ~ changeNetworkSettings ~ user",
      user
    );

    setNetworkDNS(user.dns, prev.ssid);
    setNetworkStatus(user.dhcp, user.ip, user.netMask, user.gateway, prev.ssid);

    needReconnection |= (!prev.dhcp && user.dhcp) || prev.ip !== user.ip;

    changePrevData(wifi, user);
  }

  function saveConfirm() {
    var checkResult = true;

    if (!isSelectIpv6 && prevData.wired.connected && !checkValid(false)) {
      checkResult = false;
    }

    if (!isSelectIpv6Wifi && prevData.wifi.connected && !checkValid(true)) {
      checkResult = false;
    }
    if (!checkResult) {
      showMyModal(
        "network-warning-modal",
        getLanguageText(languageTable, "Invalid value")
      );
      console.log("🚀 ~ ", invalidTxt);
      invalidTxt = "";
      networkAlertFocusTrap.activate();
      return;
    }

    needReconnection = false;

    if (!isSelectIpv6 && prevData.wired.connected) {
      changeNetworkSettings(false);
    }

    if (!isSelectIpv6Wifi && prevData.wifi.connected) {
      changeNetworkSettings(true);
    }

    setTimeout(getNetworkData, 1000);

    if (needReconnection) {
      showMyModal(
        "network-warning-modal",
        getLanguageText(
          languageTable,
          "You changed IP address or DHCP status. Please reconnect to changed IP address"
        )
      );
      networkAlertFocusTrap.activate();
    }
  }
});
