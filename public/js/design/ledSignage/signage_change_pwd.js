var socket = io();

document.addEventListener("DOMContentLoaded", function() {
    initSocket();

    $('#btn-cancel-pwd').on('click', function () {
        _changePWDCancelClick();
    });

    $('#btn-confirm-pwd').on('click', function () {
        _changePWDConfirmClick();
    });

    addAlertModals('pwd-alert-modal', getLanguageText(languageTable, "You have failed to login 5 times. Please try again after 05:00."));

    if (locale === "ar-SA") {
        changeRtl();
    }
});

function _changePWDCancelClick() {
    location.href = '/ledSignage/system'
}

function _changePWDConfirmClick() {
    var currentPin = $('#input-password');
    var currentPinVal = currentPin.val();
    var newPin = $('#input-new-password');
    var newPinVal = newPin.val();
    var confirmPin = $('#input-confirm-password');
    var confirmPinVal = confirmPin.val();

    var isPassed = 0;

    if (newPinVal.length < 8) {
        // $(".txt_error_msg").text(getLanguageText(languageTable, "Invalid New password. Password must be at least 8 characters."));
        newPin.attr('aria-invalid', "true");
        showMyModal('pwd-alert-modal', getLanguageText(languageTable, "Invalid New password. Password must be at least 8 characters."));
    } else if (!passwdCheck(newPinVal)) {
        // $(".txt_error_msg").text(getLanguageText(languageTable, "Invalid New password. You must use a mixture of mixed-case letters, numbers and special characters."));
        newPin.attr('aria-invalid', "true");
        showMyModal('pwd-alert-modal', getLanguageText(languageTable, "Invalid New password. You must use a mixture of mixed-case letters, numbers and special characters."));
    } else {
        newPin.attr('aria-invalid', "false");
        isPassed += 1;
    }

    if (newPinVal !== confirmPinVal) {
        // $(".txt_error_msg").html(getLanguageText(languageTable, "Validation Error.") + "<br>" + getLanguageText(languageTable, "New Password and Confirm Password do not match."));
        confirmPin.attr('aria-invalid', "true");
        showMyModal('pwd-alert-modal', getLanguageText(languageTable, "Validation Error.") + "<br>" + getLanguageText(languageTable, "New Password and Confirm Password do not match."));
    } else {
        confirmPin.attr('aria-invalid', "false");
        isPassed += 1;
    }

    if (currentPinVal.length < 8) {
        currentPin.attr('aria-invalid', "true");
    } else {
        currentPin.attr('aria-invalid', "false");
        isPassed += 1;
    }

    if (isPassed >= 3) {
        newPin.attr('aria-invalid', "false");
        confirmPin.attr('aria-invalid', "false");
        currentPin.attr('aria-invalid', "false");
        socket.emit("changePassword", {
            currentPasswd: currentPinVal,
            newPasswd: newPinVal
        });
    }
}

function changePassword() {
    console.log("changePassword clicked");
    currentPin = $("#currentPin .input_pin").val();
    newPin = $("#newPin .input_pin").val();
    confirmPin = $("#confirmPin .input_pin").val();


}

function passwdCheck(newPasswd) {
    return /[\@\#\$\%\^\&\*\(\)\_\+\!]/.test(newPasswd) && /[a-z]/.test(newPasswd) && /[0-9]/.test(newPasswd) && /[A-Z]/.test(newPasswd);
}

function initSocket() {
    socket.on('connect', function (socket) {
        console.log('client Connected!');
    });

    socket.on('disconnect', function (socket) {
        console.log('client Disconnected!');
    });

    socket.on("changePassword", function (msg) {
        if (msg === "success") {
            showMyModal('pwd-alert-modal', getLanguageText(languageTable, "Your Password has been changed."));
            window.location = '/ledSignage/language'
        } else if (msg === "fail_same") {
            // $(".txt_error_msg").text(getLanguageText(languageTable, "You've entered the same password as the current one. Please enter a different password."));
            showMyModal('pwd-alert-modal', getLanguageText(languageTable, "You've entered the same password as the current one. Please enter a different password."));
        } else if (msg === "fail_incorrect") {
            // $(".txt_error_msg").text(getLanguageText(languageTable, "Incorrect Password entered. Please try again."));
            showMyModal('pwd-alert-modal', getLanguageText(languageTable, "Incorrect Password entered. Please try again."));
        }
    });
}