var updateStarted = false;
var updatePercent = 0;

document.addEventListener("DOMContentLoaded", function() {
    $('head').append('<link rel="stylesheet" type="text/css" href="/assets/css/control_led_w4/control_led_w4.css">');
    $('#ROOT_CONTAINER').addClass('dashboard');
    
    createCurrentMenu(4, "system");

    addAlertModals('system-alert-modal', getLanguageText(languageTable, "Unsupported File Type."), undefined, function () {
        window.location.reload();
    });

    addProgressModal('system-progress-modal', getLanguageText(languageTable, "Uploading"));
    addProgressModal('micom-progress-modal', getLanguageText(languageTable, 'Micom Progress'));

    $('#btn-change-pwd').on('click', function () {
        _changePWDClick();
    });

    $('#micom-switch').on('change', function () {
        var isChecked = $(this).prop('checked');
        $(this).attr('aria-checked', isChecked);
    });

    $('#btn-upload').on('click', function () {
        document.getElementById('upload-file').click();
    });

    addConfirmModals('', 'system-update-modal', getLanguageText(languageTable, "S/W will update. Do you want to continue?"), '', false, function () {

        _closePopup('system-update-modal');
        showMyModal('system-progress-modal', getLanguageText(languageTable, "S/W Updating"+ "...."), updateMsg);

        var micomUpdate = $('#micom-switch').prop('checked');

        updatePercent = 0;
        updateStarted = false;

        swupdate(fileName, micomUpdate, function (m) {
            if (m.returnValue) {
                setSWUpdateStatus(true);
            }
        });

        setTimeout(function () {
            getUpdateStatus(function (msg) {
                if (msg.status !== 'in progress' && !updateStarted) {
                    _closePopup('system-progress-modal');
                    showMyModal('system-alert-modal', getLanguageText(languageTable, "Cannot start S/W update. Check xxx file please.").replace(/xxx/gi, fileName));
                }
            });
        }, 10 * 1000);

    });

    $(".update").on('click',function () {
        fileName = $(this).attr('name');
        var newVer = $(this).attr('id');
        if (newVer === undefined) {
            newVer = '-';
        }
        var modalMsg = getLanguageText(languageTable, "Do you want to install this S/W Update file?") + "<br>";
        modalMsg += "(" + getLanguageText(languageTable, "Newer Ver.") + ") " + newVer + "<br>";
        if (env.supportCinemaModel && newVer.substr(0,2) === "00") {
            modalMsg += "(" + getLanguageText(languageTable, "Current Ver.") + ") " + fpgaVer + "<br>";
        } else {
            modalMsg += "(" + getLanguageText(languageTable, "Current Ver.") + ") " + currentVer + "<br>";
        }
        showMyModal('system-update-modal', modalMsg);
    });

    addConfirmModals('', 'system-delete-modal', getLanguageText(languageTable, "Delete the file"), '', false, function () {
        deleteFile("update", fileName, function () {
            if ($('#micom-switch').prop('checked')) {
                location.href = "/ledSignage/system?micom=on";
            } else {
                location.href = "/ledSignage/system";
            }
        });
    });

    $(".btn-system-delete").on('click',function () {
        fileName = $(this).attr('name');
        showMyModal('system-delete-modal', getLanguageText(languageTable, "Do you want to delete this S/W Update file?") + "<br>" + fileName);
    });

    if (locale === "ar-SA") {
        changeRtl();
    }

});

function initFactoryReset() {
    addConfirmModals('#btn-factory-reset', 'factory-reset-modal', getLanguageText(languageTable, "information message"), getLanguageText(languageTable, "All settings will be reset. Do you want to continue?"), false, function () {
        displayLoading(true);

        setTimeout(doFactoryDefault, 1000);

        setTimeout(function () {
            displayLoading(false);
            window.location.replace("/login");
        }, 60 * 1000);
    });
}

function initReboot() {
    addConfirmModals('#btn-restart', 'restart-modal', getLanguageText(languageTable, "information message"), getLanguageText(languageTable, "System controller will restart. Do you want to continue?"), false, function () {
        displayLoading(true);

        rebootScreen('commerReset');

        setTimeout(function () {
            displayLoading(false);
            window.location.replace("/login");
        }, 60 * 1000);
    });

}

function uploadStart() {
    var file = document.getElementById('upload-file').files[0];

    if (!file) {
        showMyModal('system-alert-modal', getLanguageText(languageTable, "Please choose a file to upload"));
        return;
    }

    var fileName = file.name;
    if (env.uploadedFiles.indexOf(fileName) >= 0) {
        $('#system-overwrite-modal').remove();
        var modalTitle = getLanguageText(languageTable, "Overwrite?");
        var modalMsg = getLanguageText(languageTable, "Overwrite xxx?").replace(/xxx/gi, fileName);
        addConfirmModals('', "system-overwrite-modal", modalTitle, modalMsg, false, function () {
            setTimeout(function () {
                uploadAfterCheckLogin(file);
            }, 500);
            //$('#overwrite').close();
            return;
        }, function() {
            window.location.reload();
            return;
        });
        showMyModal('#system-overwrite-modal');
        return;
    }
    uploadAfterCheckLogin(file);
}

function uploadAfterCheckLogin(file) {
    checkLogin(function(ret) {
        if (!ret.login) {
            console.log('session expired');
            window.location.replace("/login");
            return;
        }
        uploadFile(file);
    });
}

function checkLogin(callback) {
    jQuery.ajax({
        type:"GET",
        url:"/checkLoginLed",
        dataType:"JSON",
        success : function(data) {
            callback(data);
        }, complete : function(data) {
            console.log(data);
        }, error : function(xhr, status, error) {
            console.log(status);
        }
    });
}

function enableUserInput(enable) {
    document.onkeydown = function (e) {
        return enable;
    }

    $('input').prop('disabled', !enable);
    $('button').prop('disabled', !enable);

}

function uploadFile(file) {
    var formData = new FormData();
    var maxSize = env.maxSize;
    formData.append('update', file);
    formData.append('target', 'update');

    console.log(file.size);
    if (file.size > maxSize) {
        showMyModal('system-alert-modal', getLanguageText(languageTable, 'Flash memory is not enough.'));
        return;
    }

    showMyModal('system-progress-modal', getLanguageText(languageTable, "Uploading"), file.name);
    // $('#upload-prog').show();
    // overlayControlLed(true, getLanguageText(languageTable, "Uploading") + "\n" + file.name);
    enableUserInput(false);

    var xhr = new XMLHttpRequest();

    xhr.open('post', '/upload', true);
    //xhr.setRequestHeader("Content-type","multipart/form-data");

    xhr.upload.onprogress = function (e) {
        if (e.lengthComputable) {
            var per = Math.round((e.loaded / e.total) * 100)
            updateProgressModal('system-progress-modal', per);
        }
    };

    var reloadTimer = undefined;
    xhr.upload.onload = function (e) {
        reloadTimer = setTimeout(function () {
            _closePopup('system-progress-modal');
            if ($('#micom-switch').prop('checked')) {
                location.href = "/ledSignage/system?micom=on";
            } else {
                location.href = "/ledSignage/system";
            }
        }, 2000);
    }

    xhr.onerror = function (e) {
        _closePopup('system-progress-modal');
        showMyModal('system-alert-modal', getLanguageText(languageTable, "An error occurred while submitting the form."));
    };

    xhr.onload = function () {
        if (this.status === 406) {
            if (reloadTimer) {
                clearTimeout(reloadTimer);
            }
            _closePopup('system-progress-modal');
            enableUserInput(true);
            document.getElementById('upload-file').value = '';
            showMyModal('system-alert-modal', getLanguageText(languageTable, "Unsupported File type."));
        }
    };

    xhr.send(formData);
}

function _changePWDClick() {
    location.href = "/ledSignage/signage_change_pwd";
}

function addUpdateHandler() {
    socket.on('swupdate', function (msg) {
        var micomProgress = -1;
        if (!updateStarted) {
            getMicomUpdateProgress(function (msg) {
                if (micomProgress === -1) {
                    _closePopup('system-progress-modal');
                    showMyModal('micom-progress-modal');
                }

                micomProgress = msg.progress;

                $('#micom-progress-modal .field-label').html(getLanguageText(languageTable, 'Micom Progress').replace('Micom', msg.type));
                updateProgressModal('micom-progress-modal', msg.progress);
            });
        }

        updateStarted = true;

        if (updatePercent > 0 && msg === 0) {
            _closePopup('micom-progress-modal');
            _closePopup('system-progress-modal');
            showMyModal('system-alert-modal', getLanguageText(languageTable, "Fail to update. Check xxx file please.").replace(/xxx/gi, fileName));
            return;
        }

        if (updatePercent > 0 && updatePercent === msg) {
            return;
        }

        updatePercent = msg;

        showMyModal('system-progress-modal');
        updateProgressModal('system-progress-modal', msg);

        if (msg === 100) {
            var updateCompletedMsg = getLanguageText(languageTable, "S/W Update succeeded");
            _closePopup('micom-progress-modal');
            _closePopup('system-progress-modal');
            showMyModal('system-alert-modal', updateCompletedMsg);

            setTimeout(function () {
                setSWUpdateStatus(false);
                window.location.replace("/login");
            }, 2 * 60 * 1000);
        }
    });
}

function addInfoToUpdateList() {
    listUpdates(function (msg) {
        if (!msg || !msg.returnValue) {
            return;
        }

        msg.fileinfo.forEach(function (file) {
            var button = $(".update[name='" + file.name + "']");
            var newVersion = file.version.toString(16);
            var versionLength = newVersion.length;
            var fpgaChk = env.supportCinemaModel && versionLength === 5;
            if (fpgaChk) {
                newVersion = '00.0' + newVersion.substring(0, 1) + '.' + newVersion.substring(1, 3);
            } else {
                newVersion = '0' + newVersion.substring(0, 1) + '.' + newVersion.substring(1, 3) + '.' + newVersion.substring(3, 5);
            }
            button.prop('id', newVersion);
            button.prop('disabled', false);

            var btnExpand = $(".btn-update-expand[name='" + file.name + "']");
            btnExpand.prop('id', newVersion);
            btnExpand.prop('disabled', false);

            $('.file-info[name="' + file.name + '"]').append('(' + newVersion + ')');

            if (!fpgaChk) {
                var curVerInteger = parseInt(currentVer.replace(/[.]/g, ""));
                var newVerInteger = parseInt(newVersion.replace(/[.]/g, ""));

                if (isNaN(curVerInteger)) {
                    curVerInteger = 0;
                }

                if (curVerInteger === newVerInteger) {
                    $('.file-list[name="' + file.name + '"]').css("background-color", "#c6342f");
                    $('.file-list[name="' + file.name + '"]').css("color", "white");
                    $('.btn-update[name="' + file.name + '"]').css("visibility", "hidden");
                } else if (curVerInteger > newVerInteger) {
                    $('.file-list[name="' + file.name + '"]').css("background-color", "#bfb7b7");
                    $('.file-list[name="' + file.name + '"]').css("color", "white");
                }
            }
        });
    });
}
