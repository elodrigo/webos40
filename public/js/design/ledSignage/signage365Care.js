var installPopUpTimer;
var updatePopupTimer;
var signature ='';
var downloadUrl ='';
var appVersion='00.00.00';
var accountName='UnKnown';
var accountNo='000000'

document.addEventListener("DOMContentLoaded", function() {
    $('head').append('<link rel="stylesheet" type="text/css" href="/assets/css/control_led_w4/control_led_w4.css">');

    createCurrentMenu(3, "signage365Care");

    addConfirmModals('#btn-reset', 'care-reset-modal', getLanguageText(languageTable, "information message"),
        `${getLanguageText(languageTable, "{apptitle} will be deleted.").replace('{apptitle}', getLanguageText(languageTable,'LG ConnectedCare app'))}<br> ${getLanguageText(languageTable, "Do you want to proceed?")}`, false, function () {
            appReset();
        });

    addAlertModals('care-alert-modal', 'Error');

    $('#btn-update-check').on('click', function () {
        displayLoading(true);

        get365CareServiceMode( function(mode) {
            appDownloadURL( mode.commer365CareServiceMode, function(ret) {
                if (ret == "ERROR" || ret == "TIME") {
                    console.log('got error');
                }

                if (ret == "ERROR") {
                    // $('[name="networkError"]').show()
                    showMyModal('care-alert-modal', 'networkError');
                } else if (ret == "TIME") {
                    getUseNetworkTime( function(ret) {
                        if (!ret.useNetworkTime) {
                            showMyModal('care-alert-modal', 'appTimeZoneError');
                            // $('[name="appTimeZoneError"]').show();
                        } else {
                            showMyModal('care-alert-modal', 'networkError');
                            // $('[name="networkError"]').show();
                        }
                    });
                } else {
                    var newVersion = ret.downloadUrl.split('/');
                    if (newVersion[5] != appVersion) {
                        $('#appVersion').val(getLanguageText(languageTable, 'Ver') + '.' + appVersion + ' (' + getLanguageText(languageTable, 'Ver') + '.' + newVersion[5] + ')');
                        signature = ret.signature;
                        downloadUrl = ret.downloadUrl;
                    }
                }
                displayLoading(false);
            });
        });

    });

    $('#appEnable').on('change', function () {
        var isChecked = $(this).prop('checked');
        $(this).attr('aria-checked', isChecked);
    });

    $('#app-title').text(getLanguageText(languageTable, "Enable or disable {apptitle} service on this device").replace('{apptitle}', getLanguageText(languageTable,'LG ConnectedCare app')));

    if (locale === "ar-SA") {
        changeRtl();
    }
});

function appReset() {
    displayLoading(true);
    care365Disable( function() {
        remove365CareApp( function() {
            setSystemSettings('commercial', {
                care365Enable: "off",
                signage365CareAccountNumber: "",
                signage365CareAccountName: ""
            }, function() {
                setTimeout( function() {
                    _closePopup('care-reset-modal');
                    location.reload();
                }, 3000);
            });
        });
    });
}

function getContents365Care() {
    getListApps( function(ret) {
        for (app in ret.apps) {
            if (ret.apps[app].id === "com.webos.app.commercial.care365") {
                appVersion = ret.apps[app].version;
                $('#appVersion').val(getLanguageText(languageTable, 'Ver') + '.' + appVersion);
                // $('#365CareNotInstall').hide();
                // $('#365CareInstalled').show();
                break;
            }
        }
    });

    socket.on('serverStatus', function(ret) {
        if (ret.serverStatus === "Waiting") {
            $('#serverStatus').val(getLanguageText(languageTable, "Waiting for Approval"));
        } else {
            $('#serverStatus').val(getLanguageText(languageTable, ret.serverStatus));
        }
    });
}

function getSystemSetting365Care() {
    getSystemSettings('commercial', ['signage365CareAccountNumber', 'signage365CareAccountName','care365Enable'], function(ret) {
        accountName = ret.signage365CareAccountName;
        accountNo = ret.signage365CareAccountNumber;
        var accountText = `${getLanguageText(languageTable, "Number")} : ${accountNo} / ${getLanguageText(languageTable, "Name")} : ${accountName}`;
        $('#id-account').val(accountText);

        var care365Enable = ret.care365Enable == "on" ? true : false;
        $('#appEnable').prop('checked', care365Enable);

        isFirstBoot( function(ret) {
            if (ret && care365Enable) {
                getServerStatusCare365Subscribe( function (ret) {
                });
            }
        });

        if (!$('#appEnable').prop('checked')) {
            $('#serverStatus').val(getLanguageText(languageTable, "Not Connected"));
        } else {
            getServerStatusCare365( function(ret) {
                if (ret.serverStatus === "Waiting") {
                    $('#serverStatus').val(getLanguageText(languageTable, "Waiting for Approval"));
                } else {
                    $('#serverStatus').val(getLanguageText(languageTable, ret.serverStatus));
                }
            });
        }
    });
}

function change365Enable() {
    $('#appEnable').on('change', function() {
        displayLoading(true);
        if($('#appEnable').prop('checked')) {
            care365Enable( function() {
                setSystemSettings('commercial', {
                    care365Enable: "on"
                }, function() {
                    getServerStatusCare365Subscribe( function (ret) {
                    });
                    displayLoading(false);
                });
            });
        } else {
            care365Disable( function() {
                setSystemSettings('commercial', {
                    care365Enable: "off"
                }, function() {
                    $('#serverStatus').val(getLanguageText(languageTable, "Not Connected"));
                    displayLoading(false);
                });
            });
        }
    });
}

