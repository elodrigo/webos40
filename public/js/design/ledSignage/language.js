var languageData = {
  cs: "icon_langue_czech",
  da: "icon_langue_denmark",
  de: "icon_langue_deutsch",
  en: "icon_langue_us",
  es: "icon_langue_spain",
  el: "icon_langue_greece",
  fr: "icon_langue_france",
  it: "icon_langue_italy",
  nl: "icon_langue_netherland",
  nb: "icon_langue_norway",
  "pt-PT": "icon_langue_portugal",
  "pt-BR": "icon_langue_brazil",
  ru: "icon_langue_russia",
  fi: "icon_langue_finland",
  sv: "icon_langue_svenska",
  tr: "icon_langue_turkey",
  "ko-KR": "icon_langue_korea",
  "zh-Hans": "icon_langue_china",
  ja: "icon_langue_japan",
  "zh-Hant": "icon_langue_hongkong",
  ar: "icon_langue_arabic",
  ko: "icon_langue_korea",
};

// var langRet = [];
// var myLang;

document.addEventListener("DOMContentLoaded", function () {
  $("head").append(
    '<link rel="stylesheet" type="text/css" href="/assets/css/control_led_w4/control_led_w4_language.css">'
  );

  createCurrentMenu(0, "language");

  drawLanguageSelect();

  makeKeyDownDropdown("lang-dropdown");

  $("#languageDone").click(function () {
    setMenuLanguage($(".btn-dropdown").data("value"));
  });

  $('.btn-dropdown').on('click', function(e){
    e.preventDefault();
    var $this = $(this);
    var $parent = $this.closest('.dropdown');
    var $parentTable = $this.closest('table').not('.no-scroll, has-not-header');
    var test = $(this).is(':focus')
  
    $('.btn-dropdown').not($this).attr('aria-expanded', 'false');
    // console.log($this.attr('aria-expanded'))
    if($this.attr('aria-expanded') === 'false') {
      // preventScroll();
      if($parentTable.length > 0) {
        var scrollTop = $(document).scrollTop();
        var offsetTop = $parent.offset().top;
        var offsetLeft = $parent.offset().left;
        var parentWidth = $parent.outerWidth();
        var parentHeight = $parent.outerHeight();
  
        $this.next('.dropdown-menu').css({
          top:offsetTop + parentHeight - scrollTop,
          left:offsetLeft,
          width:parentWidth,
        });
      }
      $this.attr('aria-expanded', 'true');
    } else {
      $this.attr('aria-expanded', 'false');
    }
  });
});


// $(".lists").animate({scrollTop: $("#option-0112")[0].offsetTop})

function drawLanguageSelect() {
  $.getJSON("/resources/languageTable.json", function (table) {
    // langRet = table.languages;
    // console.log(table.languages)

    getMenuLanguage(function (ret) {
      // myLang = ret;

      var ul = drawChildren(table.languages);

      var dropdownMenu = $(".dropdown-menu");
      $(dropdownMenu).html(ul);

      var lis = $(ul).children("li");
      var myIndex = 0;
      var languageCode = ret.substr(0, 2);
      lis.each(function (i) {
        if (lis[i].dataset.value === languageCode) {
          myIndex = i;
        }
        $(this).on("click", function () {
          changeLanguageLabel(i);
          changeLanguageAria(i);
          selectedIndex = i;
          //   setMenuLanguage(lis[i].dataset.value);
        });
      });

      changeLanguageLabel(myIndex);
      changeLanguageAria(myIndex);
    });
  });
}

function drawChildren(table) {
  //   var caretClass = getCaretClass();
  var ul = document.createElement("ul");
  ul.classList.add("lists");
  var selected = true;
  for (var i = 0; i < table.length; i++) {
    var top = table[i];
    var textVal = top.name;
    var dataVal = top.languageCode;
    var src = languageData[dataVal];
    // var myId = "option-lang-" + i;
    var readableI = `${i}`.length === 1 ? `0${i}` : `${i}`;
    var liID = `option-01${readableI}`;
    var item = `<li class="list" role="option" id="${liID}" data-value="${dataVal}" aria-selected="${selected}" tabindex="-1">
                  <img src="../../assets/images/${src}.svg" alt="language" />
                    <font style="vertical-align: inherit;">
                        ${textVal}
                    </font>
                 </li>`;
    $(ul).append(item);
    selected = false;
  }
  return ul;
}

function changeLanguageLabel(index) {
  var lis = $(".dropdown-menu ul").children("li");
  var labelHTML = $(lis[index]).html();
  var topA = $(".dropdown.type-c").children("button");

  topA[0].innerHTML = labelHTML;
}

function changeLanguageAria(index, deactivate) {
  var lis = $(".dropdown-menu ul").children("li");
  var readableI = `${index}`.length === 1 ? `0${index}` : `${index}`;
  var ariaId = `option-01${readableI}`;

  var label = $(".btn-dropdown");
  label.attr("aria-activedescendent", ariaId);
  label.attr("data-value", lis[index].dataset.value);

  for (var i = 0; i < lis.length; i++) {
    if (i === index && !deactivate) {
      $(lis[i]).attr("aria-selected", "true");
    } else {
      $(lis[i]).attr("aria-selected", "false");
    }
  }
}
