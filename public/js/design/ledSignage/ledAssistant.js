document.addEventListener("DOMContentLoaded", function () {
  $("head").append(
    '<link rel="stylesheet" type="text/css" href="/assets/css/control_led_w4/control_led_w4.css">'
  );

  createCurrentMenu(2, "ledAssistant");

  var statusTxt = {
    connected: "Connected",
    notConnected: "Not Connected",
    notApproval: "Not Approval",
  };

  var prevData = {
    enable: false,
    ip: "",
    ipv6: "",
    port: 0,
    ipType: "ipv4",
    status: "",
  };

  addAlertModals(
    "alert-assistant-modal",
    "IP 주소 또는 DHCP 상태를 변경했습니다. <br> 변경된 IP 주소로 다시 연결하세요."
  );

  var activate = $("#serverEnable");
  var ipv4 = $("#ipv4");
  var ipv6 = $("#ipv6");

  ipv4.on("click", function () {
    $(this).attr("checked", true);
    $(this).attr("aria-checked", true);
    ipv6.prop("checked", false);
    ipv6.attr("checked", false);
    ipv6.attr("aria-checked", true);
    $("#ipv4Row").show();
    $("#ipv6Row").hide();
  });

  ipv6.on("click", function () {
    $(this).attr("checked", true);
    $(this).attr("aria-checked", true);
    ipv4.prop("checked", false);
    ipv4.attr("checked", false);
    ipv4.attr("aria-checked", false);
    $("#ipv6Row").show();
    $("#ipv4Row").hide();
  });

  activate.on("click", function () {
    var isChecked = $(this).prop("checked");
    $(this).attr("aria-checked", isChecked);
  });

  $("#serverIPv6").on("input", function () {
    if ($("#serverIPv6").val().length > 20) {
      $("#serverIPv6").prop("size", $("#serverIPv6").val().length);
    }
  });

  initLedAssistant(true);

  function loadLedAssistantInfo(all) {
    getLedAssisantInfo(function (ret) {
      if (all) {
        if (ret.linkServerEnable === "on") {
          activate.attr("checked", true);
          activate.attr("aria-checked", true);
        }
        ret.linkServerIpType === "ipv6"
          ? ipv6.trigger("click")
          : ipv4.trigger("click");

        $("#serverIP").val(ret.linkServerIP);
        $("#serverIPv6").val(ret.linkServerIPv6);
        $("#serverPort").val(ret.linkServerPort);

        if (ret.linkServerIpType === "ipv4") {
          $("#ipv4Row").show();
          $("#ipv6Row").hide();
        } else {
          $("#ipv6Row").show();
          $("#ipv4Row").hide();
        }

        prevData.enable = ret.linkServerEnable;
        prevData.ipType = ret.linkServerIpType;
        prevData.ip = ret.linkServerIP;
        prevData.ipv6 = ret.linkServerIPv6;
        prevData.port = ret.linkServerPort;

        if (prevData.ipv6.length > 20) {
          $("#serverIPv6").prop("size", prevData.ipv6.length);
        }
      }

      $("#serverStatus").val(
        getLanguageText(languageTable, statusTxt[ret.linkServerStatus])
      );
      prevData.status = ret.linkServerStatus;
    });
  }

  function initLedAssistant() {
    loadLedAssistantInfo(true);

    setInterval(function () {
      loadLedAssistantInfo(false);
    }, 5 * 1000);

    $("#apply").click(function () {
      var enable = $("#serverEnable").prop("checked") ? "on" : "off";
      var ip = $("#serverIP").val();
      var ipv6 = $("#serverIPv6").val();
      var port = $("#serverPort").val();
      var ipType = $("#ipv6").prop("checked") ? "ipv6" : "ipv4";

      if (!checkIP(ip, false)) {
        showMyModal("alert-assistant-modal", "Error IP.");
        $("#serverIP").val(prevData.ip);
        return;
      }

      if (!checkIP(ipv6, true)) {
        showMyModal("alert-assistant-modal", "Error IPv6.");
        $("#serverIPv6").val(prevData.ipv6);
        return;
      }

      var portNum = Number(port);

      if (isNaN(portNum) || portNum < 0 || portNum > 65535) {
        showMyModal("alert-assistant-modal", "Error Port.");
        $("#serverPort").val(prevData.port);
        return;
      }

      setLedAssistantInfo(enable, ip, portNum, ipType, ipv6, function (ret) {
        loadLedAssistantInfo();
        if (ret.returnValue) {
            showMyModal("alert-assistant-modal", "적용되었습니다.");
        }
      });
    });

    $("#serverIPv6").on("input", function () {
      if ($("#serverIPv6").val().length > 20) {
        $("#serverIPv6").prop("size", $("#serverIPv6").val().length);
      }
    });
  }

  // // addConfirmModals('alert-assistant-modal', '안내메세지', 'IP 주소 또는 DHCP 상태를 변경했습니다. <br> 변경된 IP 주소로 다시 연결하세요.', true);
  // addAlertModals('alert-assistant-modal', 'IP 주소 또는 DHCP 상태를 변경했습니다. <br> 변경된 IP 주소로 다시 연결하세요.');

  // confirm.on('click', function () {
  //     var isEnabled = enabling.prop('checked');
  //     if (!isEnabled) {
  //         showMyModal('alert-assistant-modal', '적용되었습니다.');
  //     } else {
  //         var isIPv6 = ipv6.prop('checked');
  //         var ipVal = address.val();
  //         var textIP = ``;
  //         if (!isIPv6) {
  //             var isIpRight = checkIP(ipVal);
  //             var ipLen = ipVal.length;

  //             if (ipLen > 0) {
  //                 textIP = `${isIpRight ? '' : 'IP 주소를 잘못 입력했습니다.<br>'}`;
  //             } else {
  //                 textIP = `IP 주소를 입력해 주세요.<br>`;
  //             }
  //         } else {
  //             var isIpRight = checkIP(ipVal, true);
  //             var ipLen = ipVal.length;
  //             if (ipLen > 0) {
  //                 textIP = `${isIpRight ? '' : 'IP 주소를 잘못 입력했습니다.<br>'}`;
  //             } else {
  //                 textIP = `IP 주소를 입력해 주세요.<br>`;
  //             }
  //         }

  //         var portNum = port.val();
  //         var isPort = checkPort(portNum);
  //         var portLen = portNum.length;
  //         var textPort = ``;
  //         if (portLen > 0) {
  //             textPort = `${isPort ? '' : '포트 번호를 잘못 입력했습니다.<br>'}`;
  //         } else {
  //             textPort = `포트 번호를 입력해주세요.<br>`;
  //         }

  //         if (textIP.length + textPort.length < 3) {
  //             showMyModal('alert-assistant-modal', '적용되었습니다.');
  //         } else {
  //             showMyModal('alert-assistant-modal', `${textIP.length > 2 ? textIP : ''}${textPort.length > 2 ? textPort : ''}`);
  //         }

  //     }
  // });
});
