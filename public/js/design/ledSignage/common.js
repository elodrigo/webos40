var GnbURL = [
  {
    url: "language",
    title: getLanguageText(languageTable, "Language"),
    menuClass: "menu-language",
  },
  {
    url: "network",
    title: getLanguageText(languageTable, "Network Settings"),
    menuClass: "menu-network",
  },
  {
    url: "ledAssistant",
    title: getLanguageText(languageTable, "LED Assistant Settings"),
    menuClass: "menu-assistan",
  },
  {
    url: "signage365Care",
    title: getLanguageText(languageTable, "LG ConnectedCare App"),
    menuClass: "menu-lgc",
  },
  {
    url: "system",
    title: getLanguageText(languageTable, "System"),
    menuClass: "menu-settings",
  },
];

var regExps = {
  ipv4: new RegExp(
    "^([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$"
  ),
  subnetMask: new RegExp(
    "^(((128|192|224|240|248|252|254)\\.0\\.0\\.0)|(255\\.(0|128|192|224|240|248|252|254)\\.0\\.0)|(255\\.255\\.(0|128|192|224|240|248|252|254)\\.0)|(255\\.255\\.255\\.(0|128|192|224|240|248|252|254|255)))$"
  ),
  ipv6: new RegExp(
    "^(([0-9a-fA-F]{1,4}:){7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|::(ffff(:0{1,4}){0,1}:){0,1}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))$"
  ),
  subnetPrefixLength: new RegExp("^([1-9]|[1-9][0-9]|1[0-1][0-9]|12[0-7])$"),
};

//   initialize state
let state = {};

function initializeNewState (stateName, initialStateValues) {
    state[stateName] = new Map(Object.entries(initialStateValues));
}

function getState(stateName) {
    return state[stateName];
}

document.addEventListener("DOMContentLoaded", function () {
  // $('head').append('<link rel="stylesheet" type="text/css" href="/assets/css/control_led_w4/control_led_w4_common.css">');

  window.onload = function () {
    // createCurrentMenu();
    addMenuEvent();
  };
});

function createCurrentMenu(index, myUrl) {
  var gnb = $(".parent-menu");
  gnb.each(function (index) {
    $(this).removeClass("focus");
    var btn = $(this).children('button');
    var myName = $(btn[0]).attr("name");
    if (myName === myUrl) {
      $(this).addClass("focus");
      $(this).children("button").attr("aria-selected", "true");
      var langTitle = GnbURL.find(function (item) {return item.url === myName});
      var titleStr = langTitle.title + ' | LG Electronics Signage';
      $('head > title').text(titleStr);
    }
  });
}

function addMenuEvent() {
  var btnMenu = $(".btn-main-menu");
  btnMenu.each(function (index) {
    var myBtn = ($(this)[0])
    var myUrl = $(myBtn).attr('name');
    $(this).on("click", function () {
      location.href = "/ledSignage/" + myUrl;
    });
  });
}

function getCaretClass() {
  var caretClass = "caret" + (isMobile ? " mobile" : "");
  // if (locale === "ar-SA") {
  //     caretClass = caretClass + " arabic";
  // }
  return caretClass;
}

function createDropdownChildren(valueList, textMap, ariaId) {
  var caretClass = getCaretClass();
  var ul = document.createElement('ul');
  ul.classList.add('lists');
  $(ul).prop('tabindex', "-1");
  $(ul).prop('role', 'listbox');
  $(ul).prop('aria-labelledby', ariaId);
  var selected = true;
  for (var i=0; i < valueList.length; i++) {
    var top = valueList[i];
    var textVal = textMap[top];
    if (!textVal) {
      textVal = textMap.get(top);
    }
    var myId = ariaId ? 'option-' + ariaId + '-' + i : undefined;
    var item;

    /*if (myId) {
        item = `<li class="list" ><a href="javascript:void(0);" class="${caretClass}"
                    role="option" aria-selected="${selected}" data-value="${top}" id="${myId}" tabindex="-1">${textVal}</a></li>`
    } else {
        item = `<li class="list"><a href="javascript:void(0);" class="${caretClass}"
                    role="option" aria-selected="${selected}" data-value="${top}" tabindex="-1">${textVal}</a></li>`
    }*/

    if (myId) {
      item = `<li class="list ${caretClass}" role="option" aria-selected="${selected}" data-value="${top}" 
                    id="${myId}" tabindex="-1">${textVal}</li>`
    } else {
      item = `<li class="list ${caretClass}" role="option" aria-selected="${selected}" data-value="${top}" 
                    tabindex="-1">${textVal}</li>`
    }

    $(ul).append(item);
    selected = false;
  }
  return ul;
}

function changeDropdownLabel(id, text, ariaId) {
  var element = $('#' + id);
  var newBtn = `<button type="button" id="field-capture-btn" class="btn-dropdown" aria-haspopup="listbox" 
                aria-labelledby="label-${id} ${id}-btn" aria-activedescendent="option-0101" aria-expanded="false">text</button>`
  var label = $(element).children('button');
  if (label.length > 0) {
    $(label).html(text);
    $(label).prop('id', id + '-btn');
    $(label).prop('aria-labelledby', `"label-${id} ${id}-btn"`);
    $(label).prop('aria-expanded', "false");
  } else {
    var button = $(element).children('a');
    $(button).html(text);
  }
}

function changeDropdownAria(id, index, deactive) {
  var element = $('#' + id);
  var lists = $(element).find('li');
  var label = $(element).children('button');

  var ariaId = 'option-' + id + '-' + index;

  if (label.length > 0) {
    $(label).attr('aria-activedescendent', ariaId)
  } else {
    var button = $(element).children('a');
    $(button).attr('aria-activedescendent', ariaId)
  }

  for (var i = 0; i < lists.length; i++) {
    if (i === index && !deactive) {
      $(lists[i]).attr('aria-selected', "true");
    } else {
      $(lists[i]).attr('aria-selected', "false");
    }
  }
}

function makeKeyDownDropdown(dropdown) {
  $(`#${dropdown}`).on('keydown', function (evt) {
    if (evt.key !== 'Tab') {
      evt.preventDefault();
      // evt.stopPropagation();
      const lis = $(this).find('li');

      // let myIndex = lis.index(lis.filter(':focus'));
      var myIndex = 0;
      lis.each(function (i) {
        var a = $(this).attr('aria-selected');
        if (a == 'true') {
          myIndex = i;
        }
      })

      if (evt.key === "ArrowUp" && myIndex > 0) {
        myIndex--
      }
      if (evt.key === "ArrowDown" && myIndex < lis.length - 1) {
        myIndex++
      }
      if (!~myIndex) {
        myIndex = 0;
      }

      if (evt.key === "Enter" || evt.key === " ") {
        const label = $(this).children('button');
        if (label.attr('aria-expanded') == 'true') {
          lis.eq(myIndex).trigger('click');
          return;
        } else {
          $(label).attr('aria-expanded', true)
          return;
        }

      }

      lis.eq(myIndex).trigger('focus');
      lis.each(function (j) {
        if (j === myIndex) {
          $(this).attr('aria-selected', 'true');
        } else {
          $(this).attr('aria-selected', 'false');
        }
      })
      if (myIndex >= 0) {
        const myId = lis.eq(myIndex).prop('id');
        const label = $(this).children('button');
        $(label).attr('aria-activedescendent', myId);
      }
    }

  });
}

function _closePopup(id) {
  var popup = $(`#${id}`);
  $(popup).removeClass("show");
}

function addConfirmModals(
  btnId,
  id,
  title,
  content,
  isOneBtn,
  callback,
  callbackCancel,
  btnCallback
) {
  function createBtn() {
    if (isOneBtn) {
      return `<div class="button-container">
              <button type="button" id="${id}-cancel" class="btn btn-yes btn-primary">${getLanguageText(
        languageTable,
        "OK"
      )}</button>
            </div>`;
    } else {
      return `<div class="button-container">
              <button type="button" id="${id}-cancel" class="btn btn-cancel">${getLanguageText(
        languageTable,
        "NO"
      )}</button>
              <button type="button" id="${id}-ok" class="btn btn-yes btn-primary">${getLanguageText(
        languageTable,
        "YES"
      )}</button>
            </div>`;
    }
  }

  var html = `<div id="${id}" class="popup-wrapper">
      <div class="dim">&nbsp;</div>
      <div class="popup" role="dialog" aria-labelledby="${id}-label" aria-modal="true">
        <div class="popup-container">
          <div class="popup-header">
            <div id="${id}-label">
              <font style="vertical-align: inherit;">
                ${title}
              </font>
            </div>
          </div>
          <div class="popup-content">
            <div class="text-content" role="alert">
              <P id="${id}-text-content" class="alet-text">${content}</P>
            </div>
          </div>
          <div class="popup-footer">
            ${createBtn()}
          </div>
        </div>
      </div>
    </div>`;


  var modal = $(html);
  $("body").append(modal);

  var myContainer = document.getElementById(id);
  var myFocusTrap = focusTrap.createFocusTrap(`#${id}`, {
    onActivate: function () {
      myContainer.classList.add("is-active");
    },
    onDeactivate: function () {
      myContainer.classList.remove("is-active");
    },
    escapeDeactivates: false,
  });

  $(`#${id}-cancel`).on("click", function () {
    myFocusTrap.deactivate();
    _closePopup(id);
    if (callbackCancel) {
      callbackCancel();
    }
  });

  $(`#${id}-ok`).on("click", function () {
    myFocusTrap.deactivate();
    _closePopup(id);
    if (callback) {
      callback();
    }
  });

  var newBtnId =
    btnId[0] === "#" || btnId[0] === "." ? btnId.slice(1, btnId.length) : btnId;

  $(btnId).on("click", function () {
    if (btnCallback) {
      btnCallback($(this));
    }
    togglePopup(newBtnId, id);
    myFocusTrap.activate();
  });

  $(`#${id}`).on("cssClassChanged", function () {
    myFocusTrap.activate();
  });
}

function addAlertModals(id, content, callback) {
  var html = `<div id="${id}" class="popup-wrapper">
      <div class="dim">&nbsp;</div>
      <div class="popup popup-alert" role="dialog" aria-labelledby="popup-label" aria-modal="true">
        <div class="popup-container">
          <h1 id="popup-label" class="ir">안내 팝업</h1>
          <div class="popup-content">
            <div class="alet-text">
              ${content}
            </div>
          </div>
          <div class="popup-footer">
            <div class="button-container">
              <button type="button" id="${id}-ok" class="btn btn-yes btn-primary">${getLanguageText(
    languageTable,
    "OK"
  )}</button>
            </div>
          </div>
        </div>
      </div>
    </div>`;

  var modal = $(html);
  $("body").append(modal);

  var myContainer = document.getElementById(id);
  var myFocusTrap = focusTrap.createFocusTrap(`#${id}`, {
    onActivate: function () {
      myContainer.classList.add("is-active");
    },
    onDeactivate: function () {
      myContainer.classList.remove("is-active");
    },
    escapeDeactivates: false,
  });

  $(`#${id}-ok`).on("click", function () {
    myFocusTrap.deactivate();
    if (callback) {
      callback();
    }
    _closePopup(id);
  });

  $(`#${id}`).on("cssClassChanged", function (e) {
    // e.stopPropagation();
    myFocusTrap.activate();
  });
}

function addProgressModal(id, content, title) {
  var contentText = ``;

  var html = `<div id="${id}" class="popup-wrapper">
            <div class="dim">&nbsp;</div>
            <div class="popup popup-sw-ing" role="dialog" aria-labelledby="popup-label" aria-modal="true">
                <div class="popup-container">
                  <div class="popup-header">
                    <h1 id="${id}-popup-label">${
    title ? title : getLanguageText(languageTable, "S/W Update")
  }</h1>
                  </div>
                  <div class="popup-content">
                    <div class="field field-update">
                      <h1 class="field-label">${
                        content
                          ? content
                          : getLanguageText(languageTable, "Update Progress")
                      }</h1>
                      <div class="field-form">
                        <div class="update-bar"><div id="${id}-state-bar" style="width:34%;" class="state-bar"></div></div>
                        <div id="${id}-percent" class="update-percent">34<span>%</span></div>
                      </div>
                    </div>
                    <div class="sw-content">
                      ${getLanguageText(
                        languageTable,
                        "Please Keep this Signage on."
                      )} ${getLanguageText(
    languageTable,
    "The POWER remote controller key will not work during the update."
  )} <br>
                      ${getLanguageText(
                        languageTable,
                        "Do not reboot in Device page."
                      )}
                      <button id="${id}-btn-hidden"></button>
                    </div>
                  </div>
                </div>
            </div>
           </div>`;

  var modal = $(html);
  $("body").append(modal);

  const myContainer = document.getElementById(id);
  const myFocusTrap = focusTrap.createFocusTrap(`#${id}`, {
    onActivate: function () {
      myContainer.classList.add("is-active");
    },
    onDeactivate: function () {
      myContainer.classList.remove("is-active");
    },
    initialFocus: function () {
      document.getElementById(`${id}-btn-hidden`);
    },
    escapeDeactivates: false,
  });

  $(`#${id}`).on("cssClassChanged", function (e) {
    var isShow = $(this).hasClass("show");
    // e.stopPropagation();
    if (isShow) {
      myFocusTrap.activate();
    } else {
      myFocusTrap.deactivate();
    }
  });
}

function updateProgressModal(id, per) {
  $(`#${id}-state-bar`).css("width", per + "%");
  $(`#${id}-percent`).html(per + "<span>%</span>");
}

function showMyModal(id, content, title) {
  var m = $(`#${id}`);
  if (title) {
    $(m).find("#popup-label").html(title);
  }

  if (content) {
    $(m).find(".alet-text").html(content);
  }
  $(m).addClass("show");
  $(m).trigger("cssClassChanged");
}

function checkIP(ip, isV6) {
  if (isV6) {
    return regExps.ipv6.test(ip);
  }
  return regExps.ipv4.test(ip);
}

function checkNetMask(netMask) {
  return regExps.subnetMask.test(netMask);
}

function checkNetPrefix(netPrefix) {
  return regExps.subnetPrefixLength.test(netPrefix);
}

function isRtlDir() {
  return localeInfo.locales.UI === "ar-SA";
}

function changeRtlDir() {
  console.log(localeInfo);
  if (isRtlDir()) {
    $(".txt_error_msg").attr("dir", "rtl");
    $("#pingStatTitle").attr("dir", "rtl");
    $("#pingStatBody").attr("dir", "rtl");
    $("#pingTimeBody").attr("dir", "rtl");
    $("#gridSystemModalLabel").attr("dir", "rtl");
    $("span").attr("dir", "rtl");
    // $('li').attr('dir', 'rtl');
    $("h3").css("text-align", "left");
    $("h4").css("text-align", "left");
    $("table tr").css("text-align", "left");
    $("#currentPin").attr("dir", "ltr");
  }
}

function checkPort(port) {
  var portNum = Number(port);

  return !(isNaN(portNum) || portNum < 0 || portNum > 65535);
}

function displayLoading(show) {
  var loading = $('.loading');
  if (show) {
    $(loading).show();
  } else {
    $(loading).hide();
  }
}

function changeRtl() {
  $('#ROOT_CONTAINER').addClass('lang-rtl');
}