document.addEventListener("DOMContentLoaded", function() {
    // init UI change
    $('head').append('<link rel="stylesheet" type="text/css" href="/assets/css/control_manager/control_display_and_sound.css">');
    $('.content-container').addClass('wide-padding');
    var menu2 = $('#menu-2-2');
    menu2.removeClass('active');
    menu2.css("display", "none");
    var menu1 = $('#menu-2-1');
    menu1.addClass('active');
    menu1.css("display", "block");
    $('.lnb-box').css("display", "block");
    $('#ROOT_CONTAINER').removeClass('dashboard');

    getData();

    displayLoading(false);

    $('.side-logo').css('top', '35px');
});

function getData() {

    getTileModeValue(function (msg) {
        var {tileRow: tileRow, tileCol: tileCol, tileId: tileId, tileNaturalSize: tileNaturalSize} = msg;
        $("#network-mode").val(getLanguageText(languageTable, toCapitalize(msg.tileMode)));
        $("#network-row").val(tileRow);
        $("#network-col").val(tileCol);
        $("#network-id").val(tileId);
        $("#network-natural").val(getLanguageText(languageTable, toCapitalize(msg.naturalMode)));
        $("#network-size").val(tileNaturalSize);

        if (msg.tileMode === "off") {
            $("#tileModeViewSpace").css('display', 'none');
        }

        drawTileMode(tileRow, tileCol, tileId);
    });

}

function drawTileMode(row, column, tileID) {
    var box = $('#tileModeView');
    var boxClass = '';
    var gridItems = '';

    if ((row < 6) && (column < 6)) {
        if (column < 5) {
            boxClass = 'tile-row0' + String(column);
        } else if (column === 5) {
            boxClass = '';
        } else {
            boxClass = 'tile-row' + String(column);
        }

        for (var m=0; m < row; m++) {
            for (var n=0; n < column; n++) {
                gridItems += `<span class="grid-item ${( tileID == ( (m * column) + n + 1 ) ? 'on' : '')}">${(m * column) + n + 1}</span>`;
            }
        }

    } else {
        boxClass = 'tile-row15';
        gridItems += `<span class="grid-item">${row + 'x' + column}</span>`;
    }

    box.addClass(boxClass);

    box.html(gridItems);
}