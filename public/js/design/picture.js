var currentValue = {};
var backlightValues = [5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100];
// var backlightStrings = ['5%', '10%', '15%', '20%', '25%', '30%', '35%', '40%', '45%',
//     '50%', '55%', '60%', '65%', '70%', '75%', '80%', '85%', '90%', '95%', '100%'
// ];
var backlightMap = {
    5: '5%', 10: '10%', 15: '15%', 20: '20%', 25: '25%', 30: '30%', 35: '35%', 40: '40%', 45: '45%',
    50: '50%', 55: '55%', 60: '60%', 65: '65%', 70: '70%', 75: '75%', 80: '80%', 85: '85%', 90: '90%', 95: '95%', 100: '100%'
}

var pictureModeNameMap = {
    sports: getLanguageText(languageTable, 'Transportation'),
    game: getLanguageText(languageTable, 'Education'),
    vivid: getLanguageText(languageTable, 'Mall/QSR'),
    cinema: getLanguageText(languageTable, 'Gov./Corp.'),
    govCorp: getLanguageText(languageTable, 'Gov./Corp.'),
    normal: getLanguageText(languageTable, 'General'),
    eco: getLanguageText(languageTable, 'APS'),
    photo: getLanguageText(languageTable, 'Photo'),
    expert1: getLanguageText(languageTable, 'Expert'),
    expert2: getLanguageText(languageTable, 'Calibration'),
    dicom: getLanguageText(languageTable, 'DICOM'),
    hdrVivid: getLanguageText(languageTable, 'HDR Vivid'),
    hdrBright: getLanguageText(languageTable, 'HDR Bright'),
    hdrStandard: getLanguageText(languageTable, 'HDR Standard'), // i18n It will display picture mode value list
    hdrEffect: getLanguageText(languageTable, 'HDR Effect'),
    hdrExternal: getLanguageText(languageTable, 'HDR Standard'), //it is amazon only spec
    dolbyHdrDark: getLanguageText(languageTable, 'Movie Dark'),
    dolbyHdrBright: getLanguageText(languageTable, 'Movie Bright'),
    dolbyHdrVivid: getLanguageText(languageTable, 'Vivid'),
    dolbyHdrDarkAmazon: getLanguageText(languageTable, 'Movie Dark') //it is amazon only spec
};

var energySavingValList = ['auto', 'off', 'min', 'med', 'max', 'screen_off'];

var energySavingNameMap = {
    auto: getLanguageText(languageTable, 'Auto'),
    off: getLanguageText(languageTable, 'Off'),
    min: getLanguageText(languageTable, 'Minimum'),
    med: getLanguageText(languageTable, 'Medium'),
    max: getLanguageText(languageTable, 'Maximum'),
    screen_off: getLanguageText(languageTable, 'Screen Off')
};

var rotationValList = ['off', '90', '180', '270'];

var rotationNameMap = {
    'off': getLanguageText(languageTable, 'Off'),
    '90': '90°',
    '180': '180°',
    '270': '270°'
}

var aspectValList = ['original', 'full'];

var aspectNameMap = {
    'original': getLanguageText(languageTable, 'Original'),
    'full': getLanguageText(languageTable, 'Full')
}

document.addEventListener("DOMContentLoaded", function() {
});

function setPortVal(msg) {
    if (!msg) {
        return;
    }

    currentValue = msg;
    var {osdPortraitMode: portrait, contentRotation: rotation, rotationAspectRatio: aspect} = msg;

    var portraitIndex = rotationValList.findIndex(function (item) {return item === portrait});
    var portraitText = rotationNameMap[portrait];
    var rotationIndex = rotationValList.findIndex(function (item) {return item === rotation});
    var rotationText = rotationNameMap[rotation];
    var aspectIndex = aspectValList.findIndex(function (item) {return item === aspect});
    var aspectText = aspectNameMap[aspect];

    changeDropdownLabel("portrait-dropdown", portraitText);
    changeDropdownAria("portrait-dropdown", portraitIndex);
    changeDropdownLabel("rotation-dropdown", rotationText);
    changeDropdownAria("rotation-dropdown", rotationIndex);
    changeDropdownLabel("aspect-ratio-dropdown", aspectText);
    changeDropdownAria("aspect-ratio-dropdown", aspectIndex);

    setPortraitStatus(portraitIndex);
    setRotationStatus(rotation);

    disableDropButton('aspect-ratio-dropdown', msg.deactiveMenus.indexOf('rotationAspectRatio') >= 0);
}

function getPictureVal() {
    getPictureMode(function (msg) {
        var modified = msg.pictureSettingModified;
        var mode = msg.pictureMode;

        changeDropdownLabel('picture-mode-dropdown', pictureModeNameMap[mode]);
        var myIndex = env.pictureModeList.findIndex(function (item) {return item === mode});
        changeDropdownAria('picture-mode-dropdown', myIndex);
        $('#modified').html(mode !== 'expert1' && modified[mode] ? '(' + getLanguageText(languageTable, 'User') + ')' : '');
    });
}

function pictureCallback() {
    getPictureDBVal(['energySaving'], function (msg) {
        var value = msg.energySaving;
        setVisibleMinMaxTable(value);
        getPowerState(function (msg) {
            if (msg.state === 'Screen Off') {
                value = 'screen_off';
            }
            changeDropdownLabel("energy-saving-dropdown", energySavingNameMap[value]);
            var myIndex = energySavingValList.findIndex(function (item) {return item === value});
            changeDropdownAria("energy-saving-dropdown", myIndex);
        });
    });

    getPictureVal();
}

function setPortraitMode() {
    setPortrait(currentValue);
}

function setPortraitStatus(index) {
    var rotates = $('.rotates');
    for (var i=0; i < rotates.length; i++) {
        if (i === index) {
            $(rotates[i]).children('input').prop("checked", true);
        } else {
            $(rotates[i]).children('input').prop("checked", false);
        }
    }
}

function setRotationStatus(degree) {
    var myDegree;
    if (degree === "off") {
        myDegree = 0;
    } else {
        myDegree = degree;
    }
    $('#rotate-img').css({'transform': `rotate(${myDegree}deg)`});
}

function drawPictureModeSelect() {
    var pictureDropdown = $('#picture-mode-dropdown');
    var dropdownMenu = $(pictureDropdown).children(".dropdown-menu");
    var ul = createDropdownChildren(env.pictureModeList, pictureModeNameMap, 'picture-mode-dropdown');

    var lis = $(ul).children("li");
    lis.each(function (i) {
       $(this).on('click', function () {
           var selected = env.pictureModeList[i];
           changeDropdownLabel("picture-mode-dropdown", pictureModeNameMap[selected]);
           changeDropdownAria("picture-mode-dropdown", i);
           setPictureMode(selected);
           pictureCallback();
           showSuccess('picture-setting-success');
       });
    });

    $(dropdownMenu).html(ul);
}

function drawEnergySavingSelect() {
    var energyDropdown = $('#energy-saving-dropdown');
    var dropdownMenu = $(energyDropdown).children(".dropdown-menu");
    var ul = createDropdownChildren(energySavingValList, energySavingNameMap, 'energy-saving-dropdown');

    var lis = $(ul).children('li');
    lis.each(function (i) {
        $(this).on('click', function () {
            var selected = energySavingValList[i];
            // 테스트 위한-
            // 아래 대부분을 작동 안되게 쭉 코멘트
            /*setPictureDBVal({
                energySavingModified: true,
                energySaving: selected
            });
            if (selected !== "screen_off") {
                turnOnScreen();
            }
            setVisibleMinMaxTable(selected);
            // device.js api
            // 이 부분에서 btnScreen 화면이란 항목에 변화. As-Is html에만 있음
            getPowerState(getPowerStateResponse);*/
            changeDropdownLabel("energy-saving-dropdown", energySavingNameMap[selected]);
            changeDropdownAria("energy-saving-dropdown", i);
            showSuccess('picture-setting-success');
        });
    });

    /*for (var i=0; i < energySavingValList.length; i++) {
        $(lis[i]).on('click', function () {
            var selected = energySavingValList[i];
            setPictureDBVal({
                energySavingModified: true,
                energySaving: selected
            });
            if (selected !== "screen_off") {
                turnOnScreen();
            }
            setVisibleMinMaxTable(selected);
            // device.js api
            // 이 부분에서 btnScreen 화면이란 항목에 변화. As-Is html에만 있음
            getPowerState(getPowerStateResponse);
            changeDropdownLabel("energy-saving-dropdown", energySavingNameMap[selected]);
            changeDropdownAria("energy-saving-dropdown", i);
        });
    }*/

    $(dropdownMenu).html(ul);
}

function drawPortraitSelect() {
    var portraitDropdown = $('#portrait-dropdown');
    var dropdownMenu = $(portraitDropdown).children(".dropdown-menu");

    var ul = createDropdownChildren(rotationValList, rotationNameMap, 'portrait-dropdown');

    var lis = $(ul).children('li');
    lis.each(function (i) {
        $(this).on('click', function () {
            var selected = rotationValList[i];
            setPortraitStatus(i);
            changeDropdownLabel("portrait-dropdown", rotationNameMap[selected]);
            changeDropdownAria("portrait-dropdown", i);
            currentValue.osdPortraitMode = selected;
            setPortraitMode();
            showSuccess('rotation-success');
        });
    });

    $(dropdownMenu).html(ul);
}

function drawRotationSelect() {
    var rotationDropdown = $('#rotation-dropdown');
    var dropdownMenu = $(rotationDropdown).children(".dropdown-menu");

    var ul = createDropdownChildren(rotationValList, rotationNameMap, 'rotation-dropdown');

    var lis = $(ul).children('li');
    lis.each(function (i) {
        var selected = rotationValList[i];
        $(this).on('click', function () {
            setRotationStatus(selected);
            changeDropdownLabel("rotation-dropdown", rotationNameMap[selected]);
            changeDropdownAria("rotation-dropdown", i);
            currentValue.contentRotation = selected;
            setPortraitMode();
            showSuccess('rotation-success');
        });
    });

    $(dropdownMenu).html(ul);
}

function drawAspectRatioSelect() {
    var aspectRatioDropdown = $('#aspect-ratio-dropdown');
    var dropdownMenu = $(aspectRatioDropdown).children(".dropdown-menu");

    var ul = createDropdownChildren(aspectValList, aspectNameMap, 'aspect-ratio-dropdown');

    var lis = $(ul).children('li');
    lis.each(function (i) {
        var selected = aspectValList[i];
        $(this).on('click', function () {
            changeDropdownLabel("aspect-ratio-dropdown", aspectNameMap[selected]);
            changeDropdownAria("aspect-ratio-dropdown", i);
            currentValue.rotationAspectRatio = selected;
            setPortraitMode();
            showSuccess('rotation-success');
        });
    });

    $(dropdownMenu).html(ul);
}

function drawMinBacklightSelect(maxBacklight, minBacklight) {
    var minDropdown = $('#min-backlight-dropdown');
    var dropdownMenu = minDropdown.children(".dropdown-menu");
    var newList = [];
    for (var num of backlightValues) {
        if (num < maxBacklight) {
            newList.push(num);
        }
    }

    var ul = createDropdownChildren(newList, backlightMap, 'min-backlight-dropdown');

    var lis = $(ul).children("li");
    lis.each(function (i) {
        $(this).on('click', function () {
            var selected = newList[i];
            changeDropdownLabel('min-backlight-dropdown', backlightMap[selected]);
            changeDropdownAria('min-backlight-dropdown', i);
            setMinMaxBacklight('minBacklight', selected);
        });
    });

    $(dropdownMenu).html(ul);
}

function drawMaxBacklightSelect(maxBacklight, minBacklight) {
    var maxDropdown = $('#max-backlight-dropdown');
    var dropdownMenu = maxDropdown.children(".dropdown-menu");
    var newList = [];
    for (var num of backlightValues) {
        if (num > minBacklight) {
            newList.push(num);
        }
    }

    var ul = createDropdownChildren(newList, backlightMap, 'max-backlight-dropdown');

    var lis = $(ul).children("li");
    lis.each(function (i) {
        $(this).on('click', function () {
            var selected = newList[i];
            changeDropdownLabel('max-backlight-dropdown', backlightMap[selected]);
            changeDropdownAria('max-backlight-dropdown', i);
            setMinMaxBacklight('maxBacklight', selected);
        });
    });

    $(dropdownMenu).html(ul);
}

function setVisibleMinMaxTable(energySaving) {
    $('#max-backlight').css('display', (energySaving === 'auto') ? 'flex' : 'none');
    $('#min-backlight').css('display', (energySaving === 'auto') ? 'flex' : 'none');
}

function minMaxCallback() {
    getBacklight(function (msg) {
        var backlight = msg.settings;
        drawMaxBacklightSelect(backlight.maxBacklight, backlight.minBacklight);
        drawMinBacklightSelect(backlight.maxBacklight, backlight.minBacklight);

        changeDropdownLabel('min-backlight-dropdown', backlightMap[backlight.minBacklight]);
        var myIndex2 = backlightValues.findIndex(function (item) {return item === backlight.minBacklight});
        changeDropdownAria('min-backlight-dropdown', myIndex2);

        changeDropdownLabel('max-backlight-dropdown', backlightMap[backlight.maxBacklight]);
        var myIndex = backlightValues.findIndex(function (item) {return item === backlight.maxBacklight});
        changeDropdownAria('max-backlight-dropdown', myIndex);
    });
}

function setMinMaxBacklight(id, val) {
    setBacklight(id, val, function () {
        minMaxCallback();
    });
}

function initPicture() {
    drawPictureModeSelect();
    drawEnergySavingSelect();
    drawPortraitSelect();
    drawRotationSelect();
    drawAspectRatioSelect();

    makeKeyDownDropdown('picture-mode-dropdown');
    makeKeyDownDropdown('energy-saving-dropdown');
    makeKeyDownDropdown('portrait-dropdown');
    makeKeyDownDropdown('rotation-dropdown');
    makeKeyDownDropdown('aspect-ratio-dropdown');

    minMaxCallback();

    getPictureVal();
    pictureCallback();
    getPortrait(setPortVal);

    getForegroundAppInfo(function (msg) {
        if ((msg.appId.indexOf("dsmp") > 0) || (msg.appId.indexOf("browser") > 0)) {
            console.log('app id here:', msg.appId)
            // 테스트 위한- 기존에 관련 로직
            // disableDropButton('portrait-dropdown', true);
            // disableDropButton('rotation-dropdown', true);
            // disableDropButton('aspect-ratio-dropdown', true);
        }
    });

    getRecentsAppInfo(function (msg) {
        var {recentsAppList: recentsAppList} = msg;
        recentsAppList.forEach(function (appinfo) {
            if (appinfo.indexOf("browser") > 0) {
                console.log('app id here:', appinfo)
                // 테스트 위한- 기존에 관련 로직
                // disableDropButton('portrait-dropdown', true);
            }
        });
    });

    /*var rotateTypes = $('[name=rotate-type]');
    rotateTypes.on('click', function () {
        var myVal = $(this).prop('id');
        rotateTypes.each(function (i, elem) {
            var elemVal = $(elem).prop('id');
            if (myVal === elemVal) {
                $(this).attr("checked", true);
            } else {
                $(this).attr("checked", false);
            }
        });
    });*/
}