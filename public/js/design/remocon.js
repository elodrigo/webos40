var interval = undefined;
var sizeClicked = 254;
var height = 0;

document.addEventListener("DOMContentLoaded", function() {
    // init UI change
    var head = $('head');
    head.append('<link rel="stylesheet" type="text/css" href="/assets/css/control_manager/virtical_controller.css">');
    $('#ROOT_CONTAINER').removeClass('dashboard');

    $("#play").on("click", function () {
       if (interval) {
           clearInterval(interval);
       }
       refresh();
    });

    $("button.remo").on("click",function () {
        var command = $(this).val();
        // 테스트 위한-
        // 종료 버튼을 누를경우 작동되지 않게 리턴시킨다
        if (command === "exit") {
            return;
        }
        getOSDLockMode(function (msg) {
            if (msg.settings.enableOsdVisibility === "off") {
                if ((command === "menu") || (command === "home") || (command === "inputKey")) {
                    return;
                } else {
                    sendRemocon(command);
                    refresh();
                }
            } else {
                sendRemocon(command);
                refresh();
            }
        });
    });

    var captureSize = {
        "1": {
            class: "col-mo-4",
            height: 154
        },
        "2": {
            class: "col-mo-6",
            height: 254
        },
        "3": {
            class: "col-mo-12",
            height: 354
        }
    };

    var scr = $('#scr');

    $("button.size").on("click", function () {
        displayLoading(true);
        // scr.trigger("click");
        if (interval) {
            clearInterval(interval)
        }

        var size = $(this).val();
        sizeClicked = captureSize[size].height;
        reloadCapture();
        displayLoading(false);
        // $("#scr-wrap").height(captureSize[size].height);
        // var parDiv = $('#scr-wrap').parent();
        // parDiv.removeClass('col-mo-4 col-mo-6 col-mo-12');
        // parDiv.addClass(captureSize[size].class);
    });

    scr.on("click", function () {
        refresh();
    });

    var width = document.body.clientWidth;
    if (width <= 378) {
        $("#small").trigger("click");
    } else {
        $("#middle").trigger("click");
    }
    $("button.play").trigger("click");

    var keyCodeMap = {};
    keyCodeMap["8"] = "backspace";
    keyCodeMap["13"] = "enter";
    keyCodeMap["32"] = "space";
    keyCodeMap["190"] = ".";
    keyCodeMap["37"] = "left";
    keyCodeMap["38"] = "up";
    keyCodeMap["39"] = "right";
    keyCodeMap["40"] = "down";

    showRotationWarning();

    $("img, .arrow, .remo").on('focus', function (e) {
        e.preventDefault();
    });

    if ((navigator.appName === 'Netscape' && navigator.userAgent.search('Trident') !== -1) || (navigator.userAgent.toLowerCase().indexOf("msie") !== -1)) {
        $('.arrow').attr('onfocus', "blur()");
    }

    getOSDLockMode(function (msg) {
        if (msg.settings.enableOsdVisibility === "off") {
            $("button[value*=menu]").css("visibility", "hidden");
            $("button[value*=home]").css("visibility", "hidden");
            $("button[value*=inputKey]").css("visibility", "hidden");
        }
    });

    reloadCapture();
    refresh();

    displayLoading(false);

    $('.side-logo').css('top', '35px');
});

function showRemote(index) {
    var direction = ["up", "right", "down", "left", "ok"];

    sendRemocon(direction[index - 1]);
    refresh();
}

function reloadCapture() {
    // var h = Math.ceil($('#scr-wrap').height());
    var h = sizeClicked;

    if (height === 0) {
        height = isMobile ? 214 : 254;
    } else if (Math.abs(h - height) > 10) {
        height = h;
    }

    getCapture(height, function (m) {
        // $("#scr-wrap").height(sizeClicked);
        $("#scr").attr('src', m);
    });
}

function refresh() {
    var count = 0;
    if (interval) {
        clearInterval(interval)
    }
    $('#play').prop('disabled', true);
    interval = setInterval(function () {
        if (count % 10 === 0) {
            showRotationWarning();
        }
        reloadCapture();
        ++count;
        if (count >= 20) {
            $('#play').prop('disabled', false);
            clearInterval(interval);
            count = 0;
        }
    }, 1000);
}