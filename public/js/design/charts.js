var myEnv = {};
var chartPage = 1;
var chartPageM = 1;
var defaultHistorySize = isMobile ? 30 : 50;
var defaultHistorySizeM = 30;
var pageSet = 10;
var pageSetM = 3;
var start = 0;
var end = 50;
var startM = 0;
var endM = 30;

var tempSensors = [];
var fans = [];
var color = [
    "#4572a7", "#aa4643", "#89a54e", "#71588f", "#4198af", "#db843d",
    "#93a9cf", "#d19392", "#b9cd96", "#a99bbd", "#88c8da", "#f3b07a",
    "#0d62f5", "#ec1f1c", "#a1ec20", "#6e1ce2", "#1eb6e0", "#f3740c"
];
var pointStyles = [
    'circle',
    'triangle',
    'star',
    'rect',
    'cross',
    'dash',
    'line',
    'rectRounded',
    'crossRot',
    'rectRot',
];

var totalHistorySize = 0;
var totalPage = 1;

var MAX_QUEUE_SIZE = 10000;

var isCelsius = true;

document.addEventListener("DOMContentLoaded", function() {
    // init UI change
    var head = $('head');
    head.append('<link rel="stylesheet" type="text/css" href="/assets/css/control_manager/control_chart.css">');
    head.append('<link rel="stylesheet" type="text/css" href="/assets/css/control_manager/my_control_common.css">');
    $('.content-container').addClass('wide-padding');
    var menu1 = $('#menu-2-1');
    menu1.removeClass('active');
    menu1.css("display", "none");
    var menu2 = $('#menu-2-2');
    menu2.addClass('active');
    menu2.css("display", "block");
    $('.lnb-box').css("display", "block");
    $('#ROOT_CONTAINER').removeClass('dashboard');

    // drawPagination();

    $('#chart-refresh-btn').on('click', function () {
        window.location.reload();
    });

    displayLoading(false);
});

function init(_env, _chartPage) {
    myEnv = _env;
    chartPage = _chartPage;
    chartPageM = _chartPage;

    if (chartPage < 1) {
        chartPage = 1;
    }
    if (chartPageM < 1) {
        chartPageM = 1;
    }

    start = (chartPage - 1) * defaultHistorySize;
    startM = (chartPageM - 1) * defaultHistorySizeM;
    end = start + defaultHistorySize;
    endM = startM + defaultHistorySizeM;
    tempSensors = env.curTempSensor;

    initChart();
}

function initChart() {
    if (env.isFactoryMenu !== undefined) {
        return;
    }

    var f = $('#fahrenheit');
    var c = $('#celsius');

    readTempType(function (tempType) {
        // console.log("isCelsius = " + tempType.isCelsius);
        if (tempType.isCelsius) {
            f.addClass("color-gary");
            c.removeClass("color-gary");
        } else {
            f.removeClass("color-gary");
            c.addClass("color-gary");
        }
        isCelsius = tempType.isCelsius;
        startTemperatureChart(tempSensors);
    });

    f.on('click', function () {
        f.removeClass("color-gary");
        c.addClass("color-gary");
        writeTempType(false);
        window.location.reload();
    });

    c.on('click', function () {
        f.addClass("color-gary");
        c.removeClass("color-gary");
        writeTempType(true);
        window.location.reload();
    });

    if (env.supportFan) {
        fans = env.fanControl.fans;
        startFanChart();
    }

    if (env.supportBacklight) {
        startBacklightChart();
    }

    if (env.supportBrighness) {
        startBrightnessChart();
    }

    if (env.supportAcCurrent) {
        startAcCurrentChart();
    }

    if (env.supportSolarVoltage) {
        startSolarChart();
    }

    if (locale == "ar-SA") {
        changeRtl();
    }
}

function toTimeStr(date) {
    var timeStr = date.toTimeString();
    timeStr = timeStr.substr(0, timeStr.indexOf(" "));
    timeStr = timeStr.substr(0, timeStr.lastIndexOf(":"));
    return timeStr;
}

function createLabels(length, interval) {
    var beforeLogNums = 0;
    if ((totalPage - chartPage -1) >= 0) {
        beforeLogNums = totalHistorySize % defaultHistorySize + (totalPage - chartPage - 1) * defaultHistorySize;
    }

    var labels = [];
    var bootTime = env.bootTime.split(' ').splice(0, 5).join(' ');
    var bootDate = new Date(bootTime);
    var tempDate = new Date();
    interval = interval * 1000;
    for (var i = 0; i !== length; ++i) {
        tempDate.setTime(bootDate.getTime() + interval * (i - 1 + beforeLogNums));
        var timeStr = toTimeStr(tempDate);
        labels.push(timeStr);
    }

    return labels;
}

function drawChartReflowTable(id, chartData, unitTitle, tableCaption) {
    // var ul = document.createElement('ul');
    // ul.classList.add('table-body');
    var tableBody = '';

    for (const dataset of chartData.datasets) {

        // var tbody = document.createElement('tbody');
        var rows = '';
        for (var i=0; i < dataset.data.length; i++) {
            var row = `<tr>
                            <td>${chartData.labels[i]}</td>
                            <td>${dataset.data[i]}${dataset.labelUnit ? dataset.labelUnit : ''}</td>
                        </tr>`;

            // $(tbody).append(row);
            rows += row;
        }

        var tableRow = `<li class="table-row">
                              <div class="summary-data">
                                <div class="row">
                                  <div class="table-cell center">
                                    <button type="button" role="listbox" class="btn btn-expand" aria-expanded="false">${dataset.label}</button>
                                  </div>
                                </div>
                              </div>
                              <div class="all-data-box">
                                <div class="row-table">
                                  <table>
                                    <caption>${tableCaption}</caption>
                                    <thead>
                                      <tr>
                                        <th>${getLanguageText(languageTable, "Hours")}</th>
                                        <th>${unitTitle}</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      ${rows}
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </li>`

        // $(ul).append(tableRow);
        tableBody += tableRow;
    }

    $(`#${id}`).html(tableBody);

    $(`#${id} .summary-data`).on('click', function () {
        var parent = $(this).parent();
        var isExpanded = $(parent).hasClass('expand');
        if (isExpanded) {
            $(parent).removeClass('expand');
        } else {
            $(parent).addClass('expand');
        }
    });

}

function drawTempTable(id, chartData) {
    function makeCols() {
        var colHtml = '';

        for (var label of chartData.labels) {
            colHtml += '<col style="width: 120px;">';
        }
        return colHtml;
    }

    function makeLabels() {
        var labelHtml = '';

        for (var label of chartData.labels) {
            labelHtml += `<th scope="col"><div class="text-block">${label}</div></th>`
        }
        return labelHtml;
    }

    function makeTbody() {
        var tbodyHtml = '';

        for (var trData of chartData.datasets) {
            var tr = '<tr>';
            var th = `<th scope="row" colspan="2">${trData.label}</th>`;
            tr += th;

            for (var tdData of trData.data) {
                var td = `<td>${tdData}${trData.labelUnit ? trData.labelUnit : ''}</td>`
                tr += td
            }

            tr += '</tr>';
            tbodyHtml += tr;
        }

        return tbodyHtml;
    }

    var html = `
            <caption>Temperature Sensor (Celsius, Fahrenheit) Data</caption>
            <colgroup>
                <col style="width: 77px;">
                <col style="width: 77px;">
                
                ${makeCols()}
            </colgroup>
            
            <thead>
                <tr>
                    <th scope="col">Section</th>
                    <th scope="col">${getLanguageText(languageTable, "Time")}</th>
                   
                    ${makeLabels()}
                </tr>
            </thead>

            <tbody>
                ${makeTbody()}
            </tbody>
        `;

    $(`#${id}`).html(html);
}

function drawTempChart(history, interval) {
    if (!history || history.length === 0) {
        showMyWarning(getLanguageText(languageTable, "Temperature history is not yet collected"));
        return;
    }

    var refHistory = undefined;
    for (var i = 0; i !== history.length; ++i) {
        if (!history[i]) {
            continue;
        }
        if (history[i].length === 0) {
            alert(getLanguageText(languageTable, "Data is not collected now. wait for maximum 60 seconds").replace("60", interval));
            return;
        }
        refHistory = history[i];
        break;
    }

    for (var i = 0; i !== history.length; ++i) {
        if (history[i]) {
            history[i].reverse();
        }
    }

    var min = 987;
    var max = -987;

    var errorSensors = '';
    for (var i = 0; i !== tempSensors.length; ++i) {
        var error = false;
        for (var j = 0; history[i] && j !== history[i].length; ++j) {

            if (!isCelsius) {
                history[i][j] = (history[i][j] * 1.8) + 32;
            }

            if (history[i][j] < -40) {
                history[i][j] = -40;
                if (!(totalHistorySize < MAX_QUEUE_SIZE && chartPage === totalPage && j === 0)) {
                    error = true;
                }
            }
            history[i][j] = Number(history[i][j].toFixed(1));

            min = history[i][j] < min ? history[i][j] : min;
            max = history[i][j] > max ? history[i][j] : max;
        }
        if (error) {
            errorSensors += getLanguageText(languageTable, tempSensors[i].name) + ' ';
        }
    }

    if (errorSensors) {
        var warningMsg = getLanguageText(languageTable, "Check Temperature sensors. -40 means errors when reading Temperature sensors") + '\u202A (' + errorSensors + ')\u202C';
        showMyWarning(warningMsg);
    }

    var labels = createLabels(refHistory.length, interval);
    var data = {
        labels: labels,
        datasets: []
    };

    var colorIndex = 0;
    for (var i = 0; i !== tempSensors.length; ++i) {
        if (!history[i]) {
            continue;
        }

        data.datasets.push({
            label: tempSensors[i].name,
            data: history[i],
            borderWidth: 1,
            borderColor: color[colorIndex],
            backgroundColor: color[colorIndex],
            labelUnit: isCelsius ? '℃' : '℉',
            pointStyle: pointStyles[i],
            pointRadius: 6,
        });
        ++colorIndex;
    }

    if (min >= 5) {
        min = 0;
    } else if (min > -40) {
        min = Math.floor(min / 5) * 5 - 5;
    }

    var stepWidth = 10;
    var steps = (max - min) / stepWidth + 1;

    var opt = {
        maintainAspectRatio: false,
        responsive: true,
        layout: {
            padding: 0
        },
        scales: {
            yAxes: [
                {
                    gridLines: {
                        display: false
                    },
                    ticks: {
                        min: min,
                        stepSize: stepWidth
                    }
                }
            ]
        },
        plugins: {
            legend: {
                position: 'bottom',
            },
            title: {
                display: false,
                text: 'Chart.js Line Chart'
            },
            tooltip: {
                usePointStyle: true,
            }
        }
    };

    // console.log(data)
    var ctx1 = document.getElementById('myChart1').getContext('2d');
    var myChart1 = new Chart(ctx1, {type: 'line', data: data, options: opt});

    // draw table
    drawTempTable('chart-description-1', data);
    var captionTitle = `${getLanguageText(languageTable, "Temperature")} ${getLanguageText(languageTable, "Sensor")} ${getLanguageText(languageTable, "Charts")}`
    drawChartReflowTable('chart-reflow-1', data, getLanguageText(languageTable, "Temperature"), captionTitle);
}

function startTemperatureChart(tempSensors) {
    var index = 0;
    var tempHistory = [];

    function tempHistoryCallback(msg) {
        var {queueSize: queueSize} = msg;
        if (msg.returnValue) {
            tempHistory.push(msg.history);
        }
        if (index === tempSensors.length - 1) {
            totalHistorySize = queueSize;
            totalPage = parseInt(totalHistorySize) / defaultHistorySize + 1;
            drawPager();
            drawPagerM();
            drawTempChart(tempHistory, msg.interval);
            index = 0;
            return;
        }
        ++index;
        getTempHistory(tempSensors[index].id, start, end, tempHistoryCallback);
    }
    getTempHistory(tempSensors[index].id, start, end, tempHistoryCallback);
}

function startFanChart() {
    var fanHistory = {};
    var fanNums = 0;

    fans.forEach(function (fan) {
        fanNums = fanNums + fan.numberOfFans;
        fanHistory[fan.id] = [];
    });

    fans.forEach(function (fan) {
        for (var i = 0; i < fan.numberOfFans; i++) {
            getFanRpmHistory(fan.id, i, start, end, function (res) {
                fanHistory[fan.id].push(res);
                fanNums = fanNums - 1;

                if (fanNums === 0) {
                    drawFanRpmChart(fanHistory);
                }
            });
        }
    });
}

function drawFanRpmChart(data, numFans) {
    var firstFan = data[fans[0].id][0];

    if (!firstFan.returnValue) {
        showMyWarning(getLanguageText(languageTable, "Fan RPM history is not yet collected"));
        return;
    }

    var labels = createLabels(firstFan.history.length, firstFan.interval);
    var chartData = {
        labels: labels,
        datasets: []
    };

    var colorIndex = 0;
    var pointIndex = 0;
    fans.forEach(function (fan) {
        for (var i = 0; i < fan.numberOfFans; ++i) {
            var dataset = {
                label: getLanguageText(languageTable, fan.name.replace(/ /gi, "")) + (data[fan.id][i].num + 1),
                data: data[fan.id][i].history.reverse(),
                borderWidth: 1,
                borderColor: color[colorIndex],
                backgroundColor: color[colorIndex],
                labelUnit: '',
                pointStyle: pointStyles[pointIndex],
                pointRadius: 6,
            };
            ++colorIndex;
            ++pointIndex;
            chartData.datasets.push(dataset);
        }
    });

    var stepWidth = 1000 * 3;
    var maxRPM = 16000;

    var opt = {
        maintainAspectRatio: false,
        responsive: true,
        layout: {
            padding: 0
        },
        scales: {
            yAxes: [
                {
                    gridLines: {
                        display: false
                    },
                    ticks: {
                        min: 0,
                        max: maxRPM,
                        stepSize: stepWidth,
                    }
                }
            ]
        },
        plugins: {
            legend: {
                position: 'bottom',
            },
            title: {
                display: false,
                text: 'Chart.js Line Chart'
            },
            tooltip: {
                usePointStyle: true,
            }
        }
    };

    var ctx2 = document.getElementById('myChart2').getContext('2d');
    var myChart2 = new Chart(ctx2, {type: 'line', data: chartData, options: opt});

    // drawTable
    drawTempTable('chart-description-2', chartData);
    var captionTitle = `${getLanguageText(languageTable, "Fan RPM")} ${getLanguageText(languageTable, "Sensor")} ${getLanguageText(languageTable, "Charts")}`
    drawChartReflowTable('chart-reflow-2', chartData, getLanguageText(languageTable, "Fan RPM"), captionTitle);
}

function startBacklightChart() {
    getBacklightHistory(start, end, function (backMsg) {
        getEyeQSensorHistory(start, end, function (eyeqMsg) {
            drawBacklightChart(backMsg, eyeqMsg);
        });
    });
}

function drawBacklightChart(backlightData, eyeqData) {
    if (!backlightData || !backlightData.returnValue) {
        showMyWarning(getLanguageText(languageTable, "History is not yet collected"));
        return;
    }
    var labels = createLabels(backlightData.history.length, backlightData.interval);
    var chartData = {};
    chartData.labels = labels;
    chartData.datasets = [];

    var backlightSet = {
        label: getLanguageText(languageTable, "Backlight") + "(%)",
        data: backlightData.history.reverse(),
        borderWidth: 1,
        borderColor: color[0],
        backgroundColor: color[0],
        labelUnit: '%',
        pointStyle: 'circle',
        pointRadius: 6,
    };
    chartData.datasets.push(backlightSet);

    var eyeqSet = {
        label: getLanguageText(languageTable, "EyeQ sensor"),
        data: eyeqData.history.reverse(),
        borderWidth: 1,
        borderColor: color[1],
        backgroundColor: color[1],
        pointStyle: 'triangle',
        pointRadius: 6,
    };
    chartData.datasets.push(eyeqSet);

    var max = -999;
    backlightData.history.forEach(function (val) {
        if (val > max) {
            max = val;
        }
    });
    eyeqData.history.forEach(function (val) {
        if (val > max) {
            max = val;
        }
    });

    var opt = {
        maintainAspectRatio: false,
        responsive: true,
        layout: {
            padding: 0
        },
        scales: {
            yAxes: [
                {
                    gridLines: {
                        display: false
                    },
                    ticks: {
                        max: max,
                    }
                }
            ]
        },
        plugins: {
            legend: {
                position: 'bottom',
            },
            title: {
                display: false,
                text: 'Chart.js Line Chart'
            },
            tooltip: {
                usePointStyle: true,
            }
        }
    };

    // console.log(data)
    var ctx3 = document.getElementById('myChart3').getContext('2d');
    var myChart3 = new Chart(ctx3, {type: 'line', data: chartData, options: opt});

    // draw table
    drawTempTable('chart-description-3', chartData);
    var captionTitle = `${getLanguageText(languageTable, "Brightness")}/${getLanguageText(languageTable, "EyeQ sensor")} ${getLanguageText(languageTable, "Charts")}`
    drawChartReflowTable('chart-reflow-3', chartData, getLanguageText(languageTable, "Brightness")+'/'+getLanguageText(languageTable, "EyeQ sensor"), captionTitle);
}

function startBrightnessChart() {
    getBluMaintainHistory(start, end, function (res) {
        if (!checkHistoryData(res)) {
            return;
        }
        drawBrightnessChart(res);
    });
}

function drawBrightnessChart(res) {
    var labels = createLabels(res.history.length, res.interval);
    var chartData = {};
    chartData.labels = labels;
    chartData.datasets = [];

    for (var i = 0; i < res.history.length; i++) {
        if (res.history[i] < 0) {
            res.history[i] = 0;
        }
    }

    chartData.datasets.push({
        label: getLanguageText(languageTable, 'Brightness'),
        data: res.history.reverse(),
        borderWidth: 1,
        borderColor: color[0],
        backgroundColor: color[0],
        pointStyle: 'circle',
        pointRadius: 6,
    });

    var opt = {
        maintainAspectRatio: false,
        responsive: true,
        layout: {
            padding: 0
        },
        scales: {
            yAxes: [
                {
                    gridLines: {
                        display: false
                    },
                    ticks: {
                        min: 0,
                        stepSize: isMobile ? 1000 : 500,
                    }
                }
            ]
        },
        plugins: {
            legend: {
                position: 'bottom',
            },
            title: {
                display: false,
                text: 'Chart.js Line Chart'
            },
            tooltip: {
                usePointStyle: true,
            }
        }
    };

    var ctx4 = document.getElementById('myChart4').getContext('2d');
    var myChart4 = new Chart(ctx4, {type: 'line', data: chartData, options: opt});
    drawTempTable('chart-description-4', chartData);
    var captionTitle = `${getLanguageText(languageTable, "Brightness")} ${getLanguageText(languageTable, "Sensor")} ${getLanguageText(languageTable, "Charts")}`
    drawChartReflowTable('chart-reflow-4', chartData, getLanguageText(languageTable, "Brightness"), captionTitle);
}

function startAcCurrentChart() {
    getPowerCurrentHistory(start, end, function (res) {
        if (!checkHistoryData(res)) {
            return;
        }
        drawAcCurrentChart(res);
    });
}

function drawAcCurrentChart(res) {
    var labels = createLabels(res.history.length, res.interval);
    var chartData = {};
    chartData.labels = labels;
    chartData.datasets = [];

    for (var i = 0; i < res.history.length; i++) {
        if (res.history[i] < 0) {
            res.history[i] = 0;
        }
    }

    chartData.datasets.push({
        label: getLanguageText(languageTable, 'AC Current'),
        data: res.history.reverse(),
        borderWidth: 1,
        borderColor: color[0],
        backgroundColor: color[0],
        pointStyle: 'circle',
        pointRadius: 6,
    });

    var opt = {
        maintainAspectRatio: false,
        responsive: true,
        layout: {
            padding: 0
        },
        scales: {
            yAxes: [
                {
                    gridLines: {
                        display: false
                    },
                    ticks: {
                        min: 0,
                        stepSize: isMobile ? 6 : 3,
                    }
                }
            ]
        },
        plugins: {
            legend: {
                position: 'bottom',
            },
            title: {
                display: false,
                text: 'Chart.js Line Chart'
            },
            tooltip: {
                usePointStyle: true,
            }
        }
    };

    var ctx5 = document.getElementById('myChart5').getContext('2d');
    var myChart5 = new Chart(ctx5, {type: 'line', data: chartData, options: opt});
    drawTempTable('chart-description-5', chartData);
    var captionTitle = `${getLanguageText(languageTable, "AC Current")} ${getLanguageText(languageTable, "Sensor")} ${getLanguageText(languageTable, "Charts")}`
    drawChartReflowTable('chart-reflow-5', chartData, getLanguageText(languageTable, "AC Current"), captionTitle);
}

function startSolarChart() {
    getSolarHistory(start, end, function (res) {
        if (!checkHistoryData(res)) {
            return;
        }
        drawSolarChart(res);
    });
}

function drawSolarChart(res) {
    var labels = createLabels(res.history.length, res.interval);
    var chartData = {};
    chartData.labels = labels;
    chartData.datasets = [];

    for (var i = 0; i < res.history.length; i++) {
        if (res.history[i] < 0) {
            res.history[i] = 0;
        }
    }

    chartData.datasets.push({
        label: getLanguageText(languageTable, 'Solar') + ' (V)',
        data: res.history.reverse(),
        borderWidth: 1,
        borderColor: color[0],
        backgroundColor: color[0],
        pointStyle: 'circle',
        pointRadius: 6,
    });

    var opt = {
        maintainAspectRatio: false,
        responsive: true,
        layout: {
            padding: 0
        },
        scales: {
            yAxes: [
                {
                    gridLines: {
                        display: false
                    },
                    ticks: {
                        min: 0,
                        stepSize: 1,
                    }
                }
            ]
        },
        plugins: {
            legend: {
                position: 'bottom',
            },
            title: {
                display: false,
                text: 'Chart.js Line Chart'
            },
            tooltip: {
                usePointStyle: true,
            }
        }
    };

    var ctx6 = document.getElementById('myChart6').getContext('2d');
    var myChart6 = new Chart(ctx6, {type: 'line', data: chartData, options: opt});
    drawTempTable('chart-description-6', chartData);
    var captionTitle = `${getLanguageText(languageTable, "Solar")} ${getLanguageText(languageTable, "Sensor")} ${getLanguageText(languageTable, "Charts")}`
    drawChartReflowTable('chart-reflow-6', chartData, getLanguageText(languageTable, "Solar"), captionTitle);
}

function drawPager() {
    var maxPage = Math.ceil(totalHistorySize / defaultHistorySize);

    var startSet = (Math.ceil(chartPage / pageSet) - 1) * pageSet + 1;
    var endSet = startSet + pageSet - 1;
    if (endSet > maxPage) {
        endSet = maxPage;
    }

    var pageHtml = '';
    for (var i = startSet; i <= endSet; i++) {
        var li = `<a href="charts?page=${i}" role="button" data-num=${i} class="page page-${i} ${i === chartPage ? 'current' : ''}" 
                    aria-label="${i} page." aria-pressed="${i === chartPage ? 'true' : 'false'}"><span>${i}</span></a>`;
        pageHtml += li;
    }
    var pagers = $('.pager');
    var prevPagers = $('.go-prev');
    var nextPagers = $('.go-next');
    var beforePagers = $('.go-before');
    var afterPagers = $('.go-after');
    $(pagers[0]).html(pageHtml);

    var prev2One = chartPage - 1;
    if (prev2One >= 1) {
        $(prevPagers[0]).attr('href', "charts?page=" + prev2One);
    }

    var next2One = chartPage + 1;
    if (next2One <= maxPage) {
        $(nextPagers[0]).attr('href', "charts?page=" + next2One);
    }

    $(beforePagers[0]).attr('href', "charts?page=1");
    $(afterPagers[0]).attr('href', "charts?page=" + maxPage);

}

function drawPagerM() {
    var maxPage = Math.ceil(totalHistorySize / defaultHistorySize);

    var startSet = (Math.ceil(chartPageM / pageSetM) - 1) * pageSetM + 1;
    var endSet = startSet + pageSetM - 1;
    if (endSet > maxPage) {
        endSet = maxPage;
    }

    var pageHtml = '';
    for (var i = startSet; i <= endSet; i++) {
        var li = `<a href="charts?page=${i}" role="button" data-num=${i} class="page page-${i} ${i === chartPageM ? 'current' : ''}" 
                    aria-label="${i} page." aria-pressed="${i === chartPageM ? 'true' : 'false'}"><span>${i}</span></a>`;
        pageHtml += li;
    }
    var pagers = $('.pager');
    var prevPagers = $('.go-prev');
    var nextPagers = $('.go-next');
    var beforePagers = $('.go-before');
    var afterPagers = $('.go-after');
    $(pagers[1]).html(pageHtml);

    var prev2One = chartPageM - 1;
    if (prev2One >= 1) {
        $(prevPagers[1]).attr('href', "charts?page=" + prev2One);
    }

    var next2One = chartPageM + 1;
    if (next2One <= maxPage) {
        $(nextPagers[1]).attr('href', "charts?page=" + next2One);
    }

    $(beforePagers[1]).attr('href', "charts?page=1");
    $(afterPagers[1]).attr('href', "charts?page=" + maxPage);

}

function checkHistoryData(data) {
    var result = true;
    if (!data || !data.returnValue) {
        showMyWarning(getLanguageText(languageTable, 'History is not yet collected'));
        result = false;
    }

    return result;
}
