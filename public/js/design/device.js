var envirment = {};
// var wordsList = {};
var init = false;
var pictureMode = undefined;

var rebootContainer = document.getElementById('loading-contents');
var rebootFocusTrap =  focusTrap.createFocusTrap('#loading-contents', {
    onActivate: function () {rebootContainer.classList.add('is-active')},
    onDeactivate: function() {rebootContainer.classList.remove('is-active')},
    escapeDeactivates: false,
});

document.addEventListener("DOMContentLoaded", function() {
    // init UI change
    var head = $('head');
    head.append('<link rel="stylesheet" type="text/css" href="/assets/css/control_manager/control_display_and_sound.css">');
    head.append('<link rel="stylesheet" type="text/css" href="/assets/css/control_manager/my_control_common.css">');
    $('.content-container').addClass('wide-padding');
    var menu2 = $('#menu-2-2');
    menu2.removeClass('active');
    menu2.css("display", "none");
    var menu1 = $('#menu-2-1');
    menu1.addClass('active');
    menu1.css("display", "block");
    $('.lnb-box').css("display", "block");
    $('#ROOT_CONTAINER').removeClass('dashboard');

    $('#main-title').text(getLanguageText(languageTable, wordsList.deviceTitle));
    $('#menu-2-1-1 a').text(getLanguageText(languageTable, wordsList.deviceTitle));

    displayLoading(false);
});


function addListener() {
    init = true;
    $("#muted").on('change',function () {
        var isChecked = $("#muted").prop('checked');
        $("#muted").attr('aria-checked', isChecked);
        setMuted($("#muted").prop('checked'));
    });

    $("#field-slide-backlight02").on('change',function () {
        $('#muted').prop('checked', false);
        $("#volumn-val").html($(this).val());
        setMuted(false);
        setVolume($(this).val());
    }).on('input', function () {
        $("#volumn-val").html($(this).val());
        checkRangeUI('field-slide-backlight02', 0, 100);
        showSuccess('sound-control-success');
    });
}

function backlightCallback(powerState) {
    var enableBacklightVal = false;
    getPictureDBVal(['backlight', 'energySaving', 'pictureMode'], function (msg) {
        getEasyBrightnessMode(function (result) {
            var {easyBrightnessMode: easyBrightnessMode} = result;
            var isScreenOn = powerState;
            if (powerState === undefined) {
                isScreenOn = true;
            }
            var isOnEnergySaving = !((msg.energySaving === "off") || (msg.energySaving === "min") || (msg.energySaving === "med"));
            var isOnEasyBrightnessMode = easyBrightnessMode === "on";

            if (!isOnEnergySaving && !isOnEasyBrightnessMode && isScreenOn) {
                enableBacklightVal = true;
            }
            $('#field-slide-backlight').val(msg.backlight);

            checkRangeUI('field-slide-backlight', 0, 100);

            if (!enableBacklightVal) {
                disableBacklight();
            } else {
                enableBacklight();
                $('#backlightVal').html((msg.backlight + "%"));
            }

            pictureMode = msg.pictureMode;

            if (powerState && msg.energySaving === "screen_off") {
                setTimeout(function () {
                    backlightCallback(powerState);
                }, 150);
            }
        });
    });
}

function disableBacklight() {
    var backlight = $('#field-slide-backlight');
    backlight.prop('disabled', true);
    backlight.css('cursor', 'not-allowed');
    $('#backlightVal').html(getLanguageText(languageTable, "Energy Saving"));
    backlight.css('background', '#f1f1f1');
    var bar = $('#field-slide-bar');
    bar.removeClass('redslider');
    bar.addClass('greyslider');
    // $('#contrast').prop('disabled', true);
    // $('#contrastVal').html(getLanguageText(languageTable, "Energy Saving"));
}

function enableBacklight() {
    var backlight = $('#field-slide-backlight');
    backlight.prop('disabled', false);
    backlight.css('cursor', 'pointer');
    var bar = $('#field-slide-bar');
    bar.removeClass('greyslider');
    bar.addClass('redslider');
    // var contrast = $('#contrast');
    // $(contrast).prop('disabled', false);
    // $('#contrastVal').html($(contrast).val());
}

function setHdmi(msg) {
    if (!msg) {
        return;
    }
    $("input:radio[name=hdmi]:input[value='" + msg + "']").val(msg).attr("checked", true);
}

function drawDPMSelect() {
    var displayName = {
        'off': getLanguageText(languageTable, "Off"),
        '10sec': getLanguageText(languageTable, "10 sec"),
        '1min': "1 " + getLanguageText(languageTable, "min"),
        '3min': "3 " + getLanguageText(languageTable, "min"),
        '5min': "5 " + getLanguageText(languageTable, "min"),
        '10min': "10 " + getLanguageText(languageTable, "min")
    };

    var dpmDropdown = $('#dpm-dropdown');
    var dropdownMenu = $(dpmDropdown).children(".dropdown-menu");
    var DPMValList = [];
    var itemList = envirment.dpmMode.itemList;
    for (var top of itemList) {
        if (top.value) {
            DPMValList.push(top.value);
        }
    }

    var ul = createDropdownChildren(DPMValList, displayName, 'dpm-dropdown');

    if (locale === "ar-SA") {
        dpmDropdown.find(".dropdown-menu").css("right", 0);
    }

    var lis = $(ul).children('li');
    lis.each(function (i) {
        var item = itemList[i];
        if (item.visible) {
            $(this).on('click', function () {
                setDPM(item.value);
                changeDropdownLabel("dpm-dropdown", displayName[item.value]);
                changeDropdownAria("dpm-dropdown", i);
                showSuccess('power-off-success');
            });
        }
    })

    $(dropdownMenu).html(ul);

    getDPM(function (msg) {
        var index = itemList.findIndex(function (item) {return item.value === msg.dpmMode});
        changeDropdownLabel("dpm-dropdown", displayName[msg.dpmMode]);
        changeDropdownAria("dpm-dropdown", index);
    });
}

function drawPmModeSelect() {
    if (!envirment.pmMode.visible) {
        return;
    }
    var displayName = {
        "powerOff": getLanguageText(languageTable, "Power Off(Default)"),
        "sustainAspectRatio": getLanguageText(languageTable, "Sustain Aspect Ratio"),
        "screenOff": getLanguageText(languageTable, "Screen Off"),
        "screenOffAlways": getLanguageText(languageTable, "Screen Off Always"),
        "screenOffBacklight": getLanguageText(languageTable, "Screen Off & Backlight On"),
        "networkReady": getLanguageText(languageTable, "Network Ready")
    };

    var pmDropdown = $('#pm-mode-dropdown');
    var dropdownMenu = $(pmDropdown).children(".dropdown-menu");
    var PMModeValList = [];
    var itemList = envirment.pmMode.itemList;

    for (var top of itemList) {
        if (top.value) {
            PMModeValList.push(top.value);
        }
    }

    var ul = createDropdownChildren(PMModeValList, displayName, 'pm-mode-dropdown');

    if (locale === "ar-SA") {
        pmDropdown.find(".dropdown-menu").css("right", 0);
    }

    var lis = $(ul).children('li');
    lis.each(function (i) {
        var item = itemList[i];

        if (item.visible) {
            if (item.value === "sustainAspectRatio" || item.value === "screenOff" || item.value === "networkReady") {
                $(this).on('click', function () {
                    // 테스트 위한-
                    // 처음에 전원끄기 기본을 다른것으로 바꾸고 선택시 아래가 실행되지 않게 코멘트
                    // setPmMode(item.value);
                    changeDropdownLabel("pm-mode-dropdown", displayName[item.value]);
                    changeDropdownAria("pm-mode-dropdown", i);
                    showSuccess('power-off-success');
                });
            } else {
                $(this).addClass("disabled");
                $(this).on('click', function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                });
            }
        }
    })

    $(dropdownMenu).html(ul);

    getPmMode(function (msg) {
        var index = itemList.findIndex(function (item) {return item.value === msg.pmMode});
        changeDropdownLabel("pm-mode-dropdown", displayName[msg.pmMode]);
        changeDropdownAria("pm-mode-dropdown", index);
    });
}

function initMyDevice(env, wList) {
    envirment = env;
    wordsList = wList;

    if (env.supportScreenOnOff === true) {
        $("#btnScreen").on('click', function () {
            if ($("#btnScreen").is(":checked")) {
                turnOnScreen();
                backlightCallback(true);
            } else {
                turnOffScreen();
                backlightCallback(false);
            }
        });
    }

    $('#field-slide-backlight').on('change',function () {
        var setting = {
            backlight: Number($(this).val())
        };
        setting.pictureSettingModified = {};
        setting.pictureSettingModified[pictureMode] = true;
        setPictureDBVal(setting, "field-slide-backlight");
        $('#backlightVal').html($(this).val() + "%");
        showSuccess('backlight-success');
    }).on('input', function () {
        $('#backlightVal').html($(this).val() + "%");

        checkRangeUI('field-slide-backlight', 0, 100);
    });

    getVolume(function (ret) {
        $("#field-slide-backlight02").val(ret.volume);

        checkRangeUI('field-slide-backlight02', 0, 100);

        $("#volumn-val").html(ret.volume);
        $('#muted').prop('checked', ret.muted).change();

        if (!init) {
            addListener();
        }
    });

    getInputList(function (msg) {
        var analogInputList = ["RGB", "COMPONENT", "AV"];
        var analogInput = [];
        var digitalInput = [];
        for (var i = 0; i !== msg.length; ++i) {
            var isAnalogInput = false;
            for (var j = 0; j < analogInputList.length; j++) {
                if (msg[i].name === analogInputList[j]) {
                    isAnalogInput = true;
                    break;
                }
            }

            if (isAnalogInput) {
                analogInput.unshift(msg[i]);
            } else {
                digitalInput.push(msg[i]);
            }
        }

        var rtl = locale === "ar-SA" ? 'dir=rtl' : '';
        for (var i2 = 0; i2 !== digitalInput.length; ++i2) {
            var radio = `<div class="radio"><input type="radio" value="${digitalInput[i2].id}" name="hdmi" 
                        id="${digitalInput[i2].id}${rtl}"><label for="${digitalInput[i2].id}">${digitalInput[i2].name}</label></div>`

            $(radio).appendTo('#inputList');
        }

        for (var i3 = 0; i3 !== analogInput.length; ++i3) {
            var radio2 = `<div class="radio"><input type="radio" value="${analogInput[i3].id}" name="hdmi" 
                      id="${analogInput[i3].id}${rtl}"><label for="${analogInput[i3].id}">${analogInput[i3].name}</label></div>`

            $(radio2).appendTo('#inputList');
        }

        var hdmi = $('[name=hdmi]');
        hdmi.on('click',function () {
            for (var i = 0; i !== msg.length; ++i) {
                if ($(this).val() === msg[i].id) {
                    setInputSouce(msg[i].appId, function (msg) {
                        var span = $('#input-source-title span');
                        $(span[0]).html(getLanguageText(languageTable, "Success"));
                    });
                }
            }
            var myVal = $(this).val()
            hdmi.each(function (j, elem) {
                if (myVal === $(elem).val()) {
                    $(this).attr("checked", true);
                } else {
                    $(this).attr("checked", false);
                }
            });
            showSuccess('select-input-success');
        });

        getHdmiInput(setHdmi);
    });

    getPowerState(getPowerStateResponse);

    drawPmModeSelect();
    makeKeyDownDropdown("pm-mode-dropdown");

    drawDPMSelect();
    makeKeyDownDropdown("dpm-dropdown");
    addConfirmModals('#rebootModal-btn', 'rebootModal', getLanguageText(languageTable, "Reboot?"), getLanguageText(languageTable, "Are you sure to reboot?"),
        false,function () {
            // 테스트 위한-
            // 테스트를 위해 아래를 코멘트 시킴
            // rebootScreen('commerReset');
            var newContents = `<p>${getLanguageText(languageTable, "Rebooting")}</p><button></button>`;
            $('#loading-contents').html(newContents);
            displayLoading(true);
            rebootFocusTrap.activate();
            setTimeout(function () {
                rebootFocusTrap.deactivate();
                displayLoading(false);
                $('#loading-contents').html('');
                window.location.replace("/login");
            }, 60 * 1000);
        });

    if (locale === "ar-SA") {
        changeRtl();
    }
}

function getPowerStateResponse(msg) {
    var powerState = msg.state;
    if (powerState === "Screen Off") {
        if (env.supportScreenOnOff) {
            $("#btnScreen").attr("checked", "false");
        }
        backlightCallback(false);
    } else {
        if (env.supportScreenOnOff) {
            $("#btnScreen").attr("checked", "true");
        }
        backlightCallback(true);
    }
}
