var isPortrait = false;
var isContentRotated = false;
var isMobile = false;

var topNavbarWidth = 0;
var locale = "en-US";

// getIsMobile();
changeLanguage();

// document.addEventListener("DOMContentLoaded", function(event) {
// var isMobile = false;
// var locale = "en-US";
var GnbURL = [{
    url: 'dashboard',
    title: 'Dashboard',
    menuClass: 'menu-dashboard'
}, {
    url: 'device',
    title: 'Device Control',
    menuClass: 'menu-devicecontrol',
    subMenuId: 'menu-2-1-1',
}, {
    url: 'remocon',
    title: 'Virtual Controller',
    menuClass: 'menu-vitualcontrol'
}, {
    url: 'charts',
    title: 'Information',
    menuClass: 'menu-information',
    subMenuId: 'menu-2-2-1',
}, {
    url: 'media',
    title: 'Media Library',
    menuClass: 'menu-medialibrary',
}, {
    url: 'contentManager',
    title: 'Content Manager',
    menuClass: 'menu-contentManager',
}, {
    url: 'groupManager',
    title: 'Group Manager',
    menuClass: 'menu-groupManager',
}];

var LnbURL = [{
    url: 'device',
    title: 'Display & Sound',
    menuId: 'menu-2-1-1',
    parentMenuClass: 'menu-devicecontrol',
}, {
    url: 'time',
    title: 'Time & Date',
    menuId: 'menu-2-1-2',
    parentMenuClass: 'menu-devicecontrol',
}, {
    url: 'network',
    title: 'Network',
    menuId: 'menu-2-1-3',
    parentMenuClass: 'menu-devicecontrol',
}, {
    url: 'tileMode',
    title: 'Tile Mode',
    menuId: 'menu-2-1-4',
    parentMenuClass: 'menu-devicecontrol',
}, {
    url: 'failOver',
    title: 'Fail Over',
    menuId: 'menu-2-1-5',
    parentMenuClass: 'menu-devicecontrol',
}, {
    url: 'playViaUrl',
    title: 'Play via URL',
    menuId: 'menu-2-1-6',
    parentMenuClass: 'menu-devicecontrol',
}, {
    url: 'update',
    title: 'S/W Update',
    menuId: 'menu-2-1-7',
    parentMenuClass: 'menu-devicecontrol',
}, {
    url: 'checkScreen',
    title: 'Screen Fault Detection',
    menuId: 'menu-2-1-8',
    parentMenuClass: 'menu-devicecontrol',
}, {
    url: 'door',
    title: 'Door Monitor',
    menuId: 'menu-2-1-9',
    parentMenuClass: 'menu-devicecontrol',
}, {
    url: 'charts',
    title: 'Charts',
    menuId: 'menu-2-2-1',
    parentMenuClass: 'menu-information',
}, {
    url: 'fan',
    title: 'Fan',
    menuId: 'menu-2-2-2',
    parentMenuClass: 'menu-information',
}, {
    url: 'logFile',
    title: 'Log',
    menuId: 'menu-2-2-3',
    parentMenuClass: 'menu-information',
}, {
    url: 'system',
    title: 'System Information',
    menuId: 'menu-2-2-4',
    parentMenuClass: 'menu-information',
}];

var myWeekDays = {
    Sun: getLanguageText(languageTable, "Sunday"),
    Mon: getLanguageText(languageTable, "Monday"),
    Tue: getLanguageText(languageTable, "Tuesday"),
    Wed: getLanguageText(languageTable, "Wednesday"),
    Thu: getLanguageText(languageTable, "Thursday"),
    Fri: getLanguageText(languageTable, "Friday"),
    Sat: getLanguageText(languageTable, "Saturday")
};
var myMonths = {
    Jan: getLanguageText(languageTable, "January"),
    Feb: getLanguageText(languageTable, "February"),
    Mar: getLanguageText(languageTable, "March"),
    Apr: getLanguageText(languageTable, "April"),
    May: getLanguageText(languageTable, "May"),
    Jun: getLanguageText(languageTable, "June"),
    Jul: getLanguageText(languageTable, "July"),
    Aug: getLanguageText(languageTable, "August"),
    Sep: getLanguageText(languageTable, "September"),
    Oct: getLanguageText(languageTable, "October"),
    Nov: getLanguageText(languageTable, "November"),
    Dec: getLanguageText(languageTable, "December")
};

document.addEventListener("DOMContentLoaded", function() {

    window.onload = function () {
        createCurrentMenu();
        addMenuEvent();
        createBreadcrumb();
        getMenuLanguage(function (ret) {
            locale = ret;
            if (ret === 'ar-SA') {
                changeRtl();
            }
        });

        // 테스트 위한-
        /*$('.btn-main-menu').hover(function () {
            var name = $(this)[0].innerHTML;
            console.log('in:', name);
        }, function () {
            // console.log('out:', $(this));
        });*/

        $('.btn-dropdown').on('click', function(e){
            e.preventDefault();
            var $this = $(this);
            var $parent = $this.closest('.dropdown');
            var $parentTable = $this.closest('table').not('.no-scroll, has-not-header');
            var test = $(this).is(':focus')

            const lis = $(this).siblings('.dropdown-menu').find('li');
            lis.each(function (n) {
                var cur = $(this).attr('data-cur');
                if (cur == 'true') {
                    $(this).css('background-color', '#cccccc');
                } else {
                    $(this).css('background-color', '');
                }
            });

            $('.btn-dropdown').not($this).attr('aria-expanded', 'false');
            // console.log($this.attr('aria-expanded'))
            if($this.attr('aria-expanded') === 'false') {
                // preventScroll();
                if($parentTable.length > 0) {
                    var scrollTop = $(document).scrollTop();
                    var offsetTop = $parent.offset().top;
                    var offsetLeft = $parent.offset().left;
                    var parentWidth = $parent.outerWidth();
                    var parentHeight = $parent.outerHeight();

                    $this.next('.dropdown-menu').css({
                        top:offsetTop + parentHeight - scrollTop,
                        left:offsetLeft,
                        width:parentWidth,
                    });
                }
                $this.attr('aria-expanded', 'true');
            } else {
                $this.attr('aria-expanded', 'false');
            }
        });
    }

});

function createCurrentMenu() {
    var path = $(location).attr('pathname');
    var pathList = path.split('/');

    if (pathList.length >= 2 && pathList[1] !== "") {
        var pathOne = pathList[1];
        var gnbMenu = GnbURL.find(function (item) {return pathOne === item.url});
        if (gnbMenu) {
            var parent = $(`.${gnbMenu.menuClass}`);
            $(parent).addClass('focus');
            $(parent).attr('aria-current', "page");
            $(parent).children('button').attr('aria-selected', "true");
            if (gnbMenu.subMenuId) {
                var myChild = $(`#${gnbMenu.subMenuId}`);
                $(myChild).addClass('active');
                $(myChild).children('a').attr('aria-current', "page");
            }
        } else {
            var child = LnbURL.find(function (item2) {return pathOne === item2.url});
            if (child) {
                var myChild2 = $(`#${child.menuId}`);
                $(myChild2).addClass('active');
                $(myChild2).children('a').attr('aria-current', "page");
                var myParent = $(`.${child.parentMenuClass}`);
                $(myParent).addClass('focus');
                $(myParent).children('button').attr('aria-selected', "true");
            }
        }

    } else {
        var parentMenu = $('.parent-menu');
        var childMenu = $('.grand-child-menu');
        $(parentMenu[0]).addClass('focus');
        $(parentMenu[0]).attr('aria-current', "page");
        $(parentMenu[0]).children('button').attr('aria-selected', "true");
        $(childMenu[0]).addClass('active');
    }
}

function createBreadcrumb() {
    var path = $(location).attr('pathname');
    var pathList = path.split('/');
    var breadcrumb = $('.breadcrumb');
    breadcrumb.empty();

    if (pathList.length > 1) {
        var pathOne = pathList[1];
        var myIndex = GnbURL.findIndex(function (item) {return item.url === pathOne});

        if (myIndex > 0) {
            var thisGnb = GnbURL[myIndex];
            var langTitle = getLanguageText(languageTable, thisGnb.title);
            // $('#main-title').text(langTitle);
            // var span1 = `<span class="bread">${langTitle}</span>`
            // breadcrumb.append(span1);
            var span0 = `<span class="bread">${getLanguageText(languageTable, "Control Manager")}</span>`
            breadcrumb.append(span0);

            var titleStr = '';

            if (thisGnb.subMenuId) {
                $('#bread-head').removeClass('btn-lnb');
                var lnbMenu = LnbURL.find(function (item2) {return item2.menuId === thisGnb.subMenuId});
                var langSubMenu = getLanguageText(languageTable, lnbMenu.title);
                // var span2 = `<span class="bread">${langSubMenu}</span>`;
                // breadcrumb.append(span2);
                var span1 = `<span class="bread">${langTitle}</span>`
                breadcrumb.append(span1);
                $('#main-title').text(langSubMenu);
                titleStr += langSubMenu + ' | LG Electronics Signage > ' + getLanguageText(languageTable, "Control Manager") + ' > ' + langTitle;
            } else {
                $('#bread-head').addClass('btn-lnb');
                $('#main-title').text(langTitle);
                titleStr += langTitle + ' | LG Electronics Signage > ' + getLanguageText(languageTable, "Control Manager");
            }

            $('head > title').text(titleStr);

        } else if (myIndex === 0) {
            $('#bread-head').html(`<h2>${getLanguageText(languageTable, "Dashboard")}</h2>`);
            var titleStr2 = getLanguageText(languageTable, "Dashboard") + ' | LG Electronics Signage > ' + getLanguageText(languageTable, "Control Manager");
            $('head > title').text(titleStr2);
        } else {
            var thisLnb = LnbURL.find(function (item3) {return item3.url === pathOne});
            if (thisLnb) {
                if (thisLnb.parentMenuClass) {
                    var myParent = GnbURL.find(function (item4) {return item4.menuClass === thisLnb.parentMenuClass});
                    var parentLang = getLanguageText(languageTable, myParent.title);
                    var span5 = `<span class="bread">${getLanguageText(languageTable, "Control Manager")}</span>`
                    breadcrumb.append(span5);
                    var span4 = `<span class="bread">${parentLang}</span>`;
                    breadcrumb.append(span4);
                }
                var langTitle2 = getLanguageText(languageTable, thisLnb.title);
                // var span3 = `<span class="bread">${langTitle2}</span>`;
                // breadcrumb.append(span3);
                $('#main-title').text(langTitle2);

                var titleStr3 = langTitle2 + ' | LG Electronics Signage > ' + getLanguageText(languageTable, "Control Manager" + ' > ' +  parentLang);
                $('head > title').text(titleStr3);
            }

        }
    }
}

function addMenuEvent() {
    var btnMenu = $('.btn-main-menu');
    btnMenu.each(function (index) {
        $(this).on("click", function () {
            location.href = "/" + GnbURL[index].url;
        });
    });
}

function showRotationWarning() {
    $("#screenshotStatus").html('');
    getPortrait(function (msg) {
        getOSDPortrait(function (msgOSD) {
            getAspectRatio(function (rotationAspectRatio) {
                isPortrait = msgOSD.screenRotation !== "off";
                isContentRotated = msg.contentRotation !== 'off';

                var text = "";
                if (isPortrait) {
                    console.log("osd portraited!");
                    text += getLanguageText(languageTable, "Screen Rotation is ON") + "(" + msgOSD.screenRotation + "°)<br>";
                }

                if (isContentRotated) {
                    text += getLanguageText(languageTable, "External Input Rotation is ON") +
                        "(" + getLanguageText(languageTable, msg.contentRotation) + "°, " +
                        getLanguageText(languageTable, toCapitalize(rotationAspectRatio)) + ")";
                }

                if (isPortrait || isContentRotated) {
                    $("#screenshotStatus").html(text);
                }
            });
        });
    });
}

function getCaretClass() {
    var caretClass = 'caret' + (isMobile ? ' mobile' : '');
    if (locale === "ar-SA") {
        caretClass = caretClass + " arabic";
    }
    return caretClass
}

function createDropdownChildren(valueList, textMap, ariaId) {
    var caretClass = getCaretClass();
    var ul = document.createElement('ul');
    ul.classList.add('lists');
    $(ul).prop('tabindex', "-1");
    $(ul).attr('role', 'listbox');
    $(ul).prop('aria-labelledby', ariaId);
    var selected = true;
    for (var i=0; i < valueList.length; i++) {
        var top = valueList[i];
        var textVal = textMap[top];
        if (!textVal) {
            textVal = textMap.get(top);
        }
        var myId = ariaId ? 'option-' + ariaId + '-' + i : undefined;
        var item;

        /*if (myId) {
            item = `<li class="list" ><a href="javascript:void(0);" class="${caretClass}" 
                        role="option" aria-selected="${selected}" data-value="${top}" id="${myId}" tabindex="-1">${textVal}</a></li>`
        } else {
            item = `<li class="list"><a href="javascript:void(0);" class="${caretClass}" 
                        role="option" aria-selected="${selected}" data-value="${top}" tabindex="-1">${textVal}</a></li>`
        }*/

        if (myId) {
            item = `<li class="list ${caretClass}" role="option" data-selected="${selected}" data-cur="${selected}" data-value="${top}" 
                    id="${myId}" tabindex="-1">${textVal}</li>`
        } else {
            item = `<li class="list ${caretClass}" role="option" data-selected="${selected}" data-cur="${selected}" data-value="${top}" 
                    tabindex="-1">${textVal}</li>`
        }

        $(ul).append(item);
        selected = false;
    }
    return ul;
}

function changeDropdownLabel(id, text, ariaId) {
    var element = $('#' + id);
    var newBtn = `<button type="button" id="field-capture-btn" class="btn-dropdown" aria-haspopup="listbox" 
                aria-labelledby="label-${id} ${id}-btn" aria-activedescendent="option-0101" aria-expanded="false">text</button>`
    var label = $(element).children('button');
    if (label.length > 0) {
        $(label).html(text);
        $(label).prop('id', id + '-btn');
        // $(label).attr('aria-labelledby', `label-${id} ${text}`);
        $(label).prop('aria-expanded', "false");
    } else {
        var button = $(element).children('a');
        $(button).html(text);
    }
}

function changeDropdownAria(id, index, deactive) {
    var element = $('#' + id);
    var lists = $(element).find('li');
    var label = $(element).children('button');

    var ariaId = 'option-' + id + '-' + index;
    var labelled = `label-${id} ${ariaId}`

    if (label.length > 0) {
        $(label).attr('aria-activedescendent', ariaId);
        $(label).attr('aria-labelledby', labelled);
    } else {
        var button = $(element).children('a');
        $(button).attr('aria-activedescendent', ariaId)
    }

    for (var i = 0; i < lists.length; i++) {
        if (i === index && !deactive) {
            $(lists[i]).attr('data-selected', "true");
            $(lists[i]).attr('data-cur', "true");
        } else {
            $(lists[i]).attr('data-selected', "false");
            $(lists[i]).attr('data-cur', "false");
        }
    }
}

function makeKeyDownDropdown(dropdown) {
    $(`#${dropdown}`).on('keydown', function (evt) {
        if (evt.key !== 'Tab') {
            evt.preventDefault();
            // evt.stopPropagation();
            const lis = $(this).find('li');

            lis.unbind('mouseenter mouseleave');

            // let myIndex = lis.index(lis.filter(':focus'));
            var myIndex = 0;
            lis.each(function (i) {
                var a = $(this).attr('data-selected');
                if (a == 'true') {
                    myIndex = i;
                }
            });

            if (evt.key === "ArrowUp" && myIndex > 0) {
                myIndex--
            }
            if (evt.key === "ArrowDown" && myIndex < lis.length - 1) {
                myIndex++
            }
            if (!~myIndex) {
                myIndex = 0;
            }

            if (evt.key === "Enter" || evt.key === " ") {
                const label = $(this).children('button');
                if (label.attr('aria-expanded') == 'true') {
                    lis.eq(myIndex).trigger('click');
                    lis.eq(myIndex).css('background-color', '#cccccc');
                    return;
                } else {
                    lis.each(function (n) {
                        var cur = $(this).attr('data-cur');
                        if (cur == 'true') {
                            $(this).css('background-color', '#cccccc');
                        } else {
                            $(this).css('background-color', '');
                        }
                    });
                    $(label).attr('aria-expanded', true)
                    return;
                }
            }

            lis.eq(myIndex).trigger('focus');
            lis.each(function (j) {
                if (j === myIndex) {
                    if ($(this).attr('data-cur') != 'true') {
                        $(this).css('background-color', '#efefef');
                    }
                    $(this).attr('data-selected', 'true');
                } else if (j !== myIndex && $(this).attr('data-cur') == 'true') {
                    $(this).attr('data-selected', 'false');
                    $(this).css('background-color', '#cccccc');
                } else {
                    $(this).attr('data-selected', 'false');
                    $(this).css('background-color', '');
                }
            });
            if (myIndex >= 0) {
                const myId = lis.eq(myIndex).prop('id');
                const label = $(this).children('button');
                $(label).attr('aria-activedescendent', myId);
            }

            $(this).children('button').trigger('focus');

            lis.hover(function () {
            }, function () {
                lis.each(function (l) {
                    var selected = $(this).attr('data-selected');
                    var cur = $(this).attr('data-cur');
                    if (selected == 'true' && cur == 'false') {
                        $(this).css('background-color', '');
                    }

                });
                lis.unbind('mouseenter mouseleave');
            })
        }

    });
}

function disableDropButton(id, disabled) {
    $(`#${id}`).children('button').prop('disabled', disabled);
}

function _openPopup(id) {
    $(`#${id}`).addClass('show');
}

function _closePopup(id) {
    $(`#${id}`).removeClass('show');
}

function addConfirmModals(btnId, id, title, content, isOneBtn, callback, callbackCancel, btnCallback) {

    function createBtn() {
        if (isOneBtn) {
            return `<div class="button-container">
              <button type="button" id="${id}-cancel" class="btn btn-yes btn-primary">${getLanguageText(languageTable, "OK")}</button>
            </div>`;
        } else {
            return `<div class="button-container">
              <button type="button" id="${id}-cancel" class="btn btn-cancel">${getLanguageText(languageTable, "NO")}</button>
              <button type="button" id="${id}-ok" class="btn btn-yes btn-primary">${getLanguageText(languageTable, "YES")}</button>
            </div>`;
        }
    }

    var html = `<div id="${id}" class="popup-wrapper">
      <div class="dim">&nbsp;</div>
      <div class="popup" role="dialog" aria-labelledby="popup-label" aria-modal="true">
        <div class="popup-container">
          <div class="popup-header">
            <h1 id="popup-label">${title}</h1>
          </div>
          <div class="popup-content">
            <div class="text-content" role="alert">
              <P id="${id}-text-content" class="alet-text">${content}</P>
            </div>
          </div>
          <div class="popup-footer">
            ${createBtn()}
          </div>
        </div>
      </div>
    </div>`

    var modal = $(html);
    $('body').append(modal);

    var myContainer = document.getElementById(id);
    var myFocusTrap = focusTrap.createFocusTrap(`#${id}`, {
        onActivate: function () {myContainer.classList.add('is-active')},
        onDeactivate: function () {myContainer.classList.remove('is-active')},
        escapeDeactivates: false,
    });

    $(`#${id}-cancel`).on('click', function () {
        myFocusTrap.deactivate();
        _closePopup(id);
        if (callbackCancel) {
            callbackCancel();
        }
    });

    $(`#${id}-ok`).on('click', function () {
        myFocusTrap.deactivate();
        _closePopup(id);
        if (callback) {
            callback();
        }
    });

    var newBtnId = btnId[0] === '#' || btnId[0] === '.' ? btnId.slice(1, btnId.length) : btnId;

    $(btnId).on('click', function () {
        if (btnCallback) {
            btnCallback($(this));
        }
        togglePopup(newBtnId, id);
        myFocusTrap.activate();
    });

    $(`#${id}`).on('cssClassChanged', function () {
        myFocusTrap.activate();
    });
}

function addAlertModals(id, content, callback) {

    var html = `<div id="${id}" class="popup-wrapper">
      <div class="dim">&nbsp;</div>
      <div class="popup popup-alert" role="dialog" aria-labelledby="popup-label" aria-modal="true">
        <div class="popup-container">
          <h1 id="popup-label" class="ir">${getLanguageText(languageTable, "information message")}</h1>
          <div class="popup-content">
            <div class="alet-text">
              ${content}
            </div>
          </div>
          <div class="popup-footer">
            <div class="button-container">
              <button type="button" id="${id}-ok" class="btn btn-yes btn-primary">${getLanguageText(languageTable, "OK")}</button>
            </div>
          </div>
        </div>
      </div>
    </div>`

    var modal = $(html);
    $('body').append(modal);

    var myContainer = document.getElementById(id);
    var myFocusTrap = focusTrap.createFocusTrap(`#${id}`, {
        onActivate: function () {myContainer.classList.add('is-active')},
        onDeactivate: function () {myContainer.classList.remove('is-active')},
        escapeDeactivates: false,
    });

    $(`#${id}-ok`).on('click', function () {
        myFocusTrap.deactivate();
        if (callback) {
            callback();
        }
        _closePopup(id);
    });

    $(`#${id}`).on('cssClassChanged', function (e) {
        // e.stopPropagation();
       myFocusTrap.activate();
    });
}

function showMyModal(id, content, title ) {
    var m = $(`#${id}`);
    if (title) {
        $(m).find('#popup-label').html(title);
    }

    if (content) {
        $(m).find('.alet-text').html(content);
    }
    $(m).addClass('show');
    $(m).trigger('cssClassChanged');
}

function enableDropdownClickable(id, isEnable) {
    var dropdownChild = $(`#${id}`).children('.btn-dropdown');

    if (isEnable) {
        dropdownChild.on('click', function () {
            var parentTable = dropdownChild.closest('table').not('.no-scroll, has-not-header');
            // preventScroll();
            if(parentTable.length > 0) {
                var scrollTop = $(document).scrollTop();
                var offsetTop = $parent.offset().top;
                var offsetLeft = $parent.offset().left;
                var parentWidth = $parent.outerWidth();
                var parentHeight = $parent.outerHeight();

                dropdownChild.next('.dropdown-menu').css({
                    top:offsetTop + parentHeight - scrollTop,
                    left:offsetLeft,
                    width:parentWidth,
                });
            }
            dropdownChild.attr('aria-selected', 'true');
        });
    } else {
        dropdownChild.on('click', function () {
            dropdownChild.attr('aria-selected', 'false');
        });
    }
}

function getIsMobile() {
    isMobile = document.location.pathname.indexOf('mobile') > 0;
}

function toCapitalize(text) {
    return text.replace(/\b[a-z]/g, function (f) {
        return f.toUpperCase();
    });
}

function isImageFile(fileName) {
    if (!fileName) {
        return false;
    }

    var imageFileExtList = ["png", "bmp", "jpg", 'jpeg', 'jpe'];
    var ext = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();
    return imageFileExtList.indexOf(ext) !== -1;
}

function isVideoFile(fileName) {
    if (!fileName) {
        return false;
    }

    var videoFileExtList = [
        "mp4", "mpeg", "avi", "tp", "mkv", "mov", "mpg", "asf", "wmv", "m4v", "3gp", "3g2", "trp", "ts", "mts", "dat", "vob"];
    var ext = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();
    return videoFileExtList.indexOf(ext) !== -1;
}


function getLanguageText(languageTable, text) {
    var newText = text + "";

    for (var i = languageTable.length - 1; i >= 0; i--) {
        if (languageTable[i][newText]) {
            newText = languageTable[i][newText];
        }
    }
    return newText;
}

function capitalizeFirstLetter(str) {
    return str.substring(0, 1).toUpperCase() + str.substring(1, str.length).toLowerCase();
}

/*function setRollingText() {
    setTimeout(function () {
        $(".txt_rolling span").each(function () {
            if ($(this).children("marquee").length > 0) {
                $(this).css("overflow", "hidden");
                var html = $(this).children("marquee").html();
                $(this).html(html);
            }
        });

        $(".txt_rolling").each(function () {
            $(this).children("span").unbind("mouseenter mouseleave");
            var width = $(this).width();
            var childWidth = $(this).children("span").width();
            if (childWidth > width) {
                $(this).children("span").hover(function () {
                    console.log("mouseover");
                    $(this).css("overflow", "visible");
                    var html = "<marquee>" + $(this).html() + "</marquee>";
                    $(this).html(html);
                }, function () {
                    console.log("mouseout");
                    $(this).css("overflow", "hidden");
                    var html = $(this).children("marquee").html();
                    $(this).html(html);
                })
            }
        });
    }, 100);
}*/

function changeLanguage() {
    var localeFormatter = {
        'es': 'es-ES',
        'ko': 'ko-KR',
        'ar': 'ar-SA',
        'zh-HK': 'zh-Hant-HK',
        'zh-TW': 'zh-Hant-TW',
        'zh-CN': 'zh-Hans-CN'
    };
    // locale = navigator.language || navigator.browerLanguage;
    locale = navigator.language;
    if (localeFormatter[locale]) {
        locale = localeFormatter[locale];
    }

    console.log("locale: " + locale);
    $.ajax({
        type: "post",
        data: {locale: locale},
        url: "/setLanguage",
        success: function (data) {
            // console.log(data);
            if (data === "changed") {
                location.reload();

            }
        },
        error: function (req, status, err) {
            console.log("error");
        }
    });
}

function queryString () {
    var query_string = {};
    var query = window.location.search.substring(1);
    var vars = query.split("&");

    for (var i=0; i<vars.length; i++) {
        var pair = vars[i].split("=");
        // If first entry with this name
        if (typeof query_string[pair[0]] === "undefined") {
            query_string[pair[0]] = decodeURIComponent(pair[1]);
            // If second entry with this name
        } else if (typeof query_string[pair[0]] === "string") {
            query_string[pair[0]] = [query_string[pair[0]], decodeURIComponent(pair[1])];
            // If third or later entry with this name
        } else {
            query_string[pair[0]].push(decodeURIComponent(pair[1]));

        }
    }
    return query_string;
}

function getInternetExplorerVersion()
{
    var re;
    var ua;
    var rv = -1;
    if (navigator.appName === 'Microsoft Internet Explorer')
    {
        ua = navigator.userAgent;
        re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
        if (re.exec(ua) != null)
            rv = parseFloat( RegExp.$1 );
    }
    else if (navigator.appName === 'Netscape')
    {
        ua = navigator.userAgent;
        re = new RegExp("Trident/.*rv:([0-9]{1,}[\.0-9]{0,})");
        if (re.exec(ua) != null)
            rv = parseFloat( RegExp.$1 );
    }
    return rv;
}

function filteringXSS(name) {
    // [LGPSVE-499]
    return name.replace(/\<|\>|\"|\'|\%|\;|\&|\+/g, "");
}

function drawPagination(callback) {
    var pagination = $('.pagination');

    for (var top of pagination) {
        var goBefore = $(top).children('.go-before')[0];
        var goPrev = $(top).children('.go-prev')[0];
        var goNext = $(top).children('.go-next')[0];
        var goAfter = $(top).children('.go-after')[0];
        var pager = $(top).children('.pager')[0];
        var pages = $(pager).children('.page');

        for (var i = 0; i < pages.length; i++) {
            $(pages[i]).on('click', function () {
                paginationClicker(pages, i);
                if (callback) {
                    callback(i);
                }
            });
        }
        $(goPrev).on('click', function () {
            goPrevClicker(pages);
        });
        $(goNext).on('click', function () {
            goNextClicker(pages);
        });
    }
}

function paginationClicker(pages, index) {
    for (var i = 0; i < pages.length; i++) {
        if (index === i) {
            $(pages[i]).addClass('current');
            $(pages[i]).attr("aria-pressed", "true");
        } else {
            $(pages[i]).removeClass('current');
            $(pages[i]).attr("aria-pressed", "false");
        }
    }
}

function goPrevClicker(pages) {
    for (var i = 0; i < pages.length; i++) {
        if ($(pages[i]).hasClass('current') && i > 0) {
            $(pages[i]).removeClass('current');
            $(pages[i]).attr('aria-pressed', 'false');
            $(pages[i - 1]).addClass('current');
            $(pages[i - 1]).attr('aria-pressed', 'true');
            break;
        }
    }
}

function goNextClicker(pages) {
    for (var i = 0; i < pages.length; i++) {
        if ($(pages[i]).hasClass('current') && i < pages.length) {
            $(pages[i]).removeClass('current');
            $(pages[i]).attr('aria-pressed', 'false');
            $(pages[i+1]).addClass('current');
            $(pages[i+1]).attr('aria-pressed', 'true');
            break;
        }
    }
}

function drawCompleteModal(name) {
    var message1 = getLanguageText(languageTable, "Update completed. Signage is rebooting now.");
    var message2 = getLanguageText(languageTable, "Please login again.");

    var html = `<div id="popup-${name}-complete" class="popup-wrapper">
                <div class="dim">&nbsp;</div>
                <div class="popup popup-sw-complate" role="dialog" aria-labelledby="popup-label" aria-modal="true">
                    <div class="popup-container">
                        <div class="popup-content" id="${name}-popup-label-complete">
                            <div class="sw-content">
                              ${message1} <br>
                              ${message2}
                            </div>
                        </div>
                    </div>
                </div>
             </div>`;

    var modal = $(html);
    $('body').append(modal);
}

function drawProgressModal(name) {
    var contentText = `${getLanguageText(languageTable, "Please Keep this Signage on.")} ${getLanguageText(languageTable, "The POWER remote controller key will not work during the update.")} <br>
                      ${getLanguageText(languageTable, "Do not reboot in Device page.")}`

    var html = `<div id="popup-${name}" class="popup-wrapper">
            <div class="dim">&nbsp;</div>
            <div class="popup popup-sw-ing" role="dialog" aria-labelledby="popup-label" aria-modal="true">
                <div class="popup-container">
                  <div class="popup-header">
                    <h1 id="${name}-popup-label">${getLanguageText(languageTable, "S/W Update")}</h1>
                  </div>
                  <div class="popup-content">
                    <div class="field field-update">
                      <h1 id="${name}-field-label" class="field-label">${getLanguageText(languageTable, "Update Progress")}</h1>
                      <div class="field-form">
                        <div class="update-bar"><div id="${name}-state-bar" style="width:34%;" class="state-bar"></div></div>
                        <div id="${name}-percent" class="update-percent">34<span>%</span></div>
                      </div>
                    </div>
                    <button></button>
                    <div id="${name}-content" class="sw-content">
                      ${contentText}
                    </div>      
                  </div>
                </div>
            </div>
           </div>`;

    var modal = $(html);
    $('body').append(modal);

    var myContainer = document.getElementById(`popup-${name}`);
    var myFocusTrap = focusTrap.createFocusTrap(`#popup-${name}`, {
        onActivate: function () {myContainer.classList.add('is-active')},
        onDeactivate: function () {myContainer.classList.remove('is-active')},
        escapeDeactivates: false,
    });

    $(`#popup-${name}`).on('cssClassChanged', function (e) {
        // e.stopPropagation();
        myFocusTrap.activate();
    });
}

function moveHere(url) {
    window.location = url;
}

function fileThis(id) {
    document.getElementById(id).click();
}

function displayLoading(show) {
    var loading = $('.loading');
    if (show) {
        $(loading).show();
    } else {
        $(loading).hide();
    }
}

function showMyWarning(msg, timeout) {
    $('#sensor-error-field p').text(msg);

    if (timeout) {
        setTimeout(function() {
            $('#sensor-error-field p').text('');
        }, timeout);
    }
}

function showSuccess(id) {
    $('#' + id).css('display', 'inline-block');
    setTimeout(function () {
        $('#' + id).css('display', 'none');
    }, 2000);
}

function changeRtl() {
    $('#ROOT_CONTAINER').addClass('lang-rtl');
}