var captureTimeInterval = "";
var deviceInfo = {
    "usb": undefined,
    "internal": undefined,
}
var inputList = [];

var failPriorityContainer = document.getElementById('failOverPrioritySettingModal');
var failPriorityFocusTrap =  focusTrap.createFocusTrap('failOverPrioritySettingModal', {
    onActivate: function () {failPriorityContainer.classList.add('is-active')},
    onDeactivate: function () {failPriorityContainer.classList.remove('is-active')},
    escapeDeactivates: false,
});
var failAlertContainer = document.getElementById('fail-warning-modal');
var failAlertFocusTrap =  focusTrap.createFocusTrap('fail-warning-modal', {
    onActivate: function () {failAlertContainer.classList.add('is-active')},
    onDeactivate: function () {failAlertContainer.classList.remove('is-active')},
    escapeDeactivates: false,
});

document.addEventListener("DOMContentLoaded", function() {
    // init UI change
    var head = $('head');
    head.append('<link rel="stylesheet" type="text/css" href="/assets/css/control_manager/control_input.css">');
    head.append('<link rel="stylesheet" type="text/css" href="/assets/css/control_manager/popup_control_input.css">');
    head.append('<link rel="stylesheet" type="text/css" href="/assets/css/control_manager/my_control_common.css">');

    $('.content-container').addClass('wide-padding');
    var menu2 = $('#menu-2-2');
    menu2.removeClass('active');
    menu2.css("display", "none");
    var menu1 = $('#menu-2-1');
    menu1.addClass('active');
    menu1.css("display", "block");
    $('.lnb-box').css("display", "block");
    $('#ROOT_CONTAINER').removeClass('dashboard');
    addAlertModals('fail-warning-modal', getLanguageText(languageTable, "Please choose a file to upload"), function () {
        failAlertFocusTrap.deactivate();
    });

    init();

    var prior = $('[name=failOverPriority]');
    prior.on('click', function () {
        var myVal = $(this).prop('id');
        prior.each(function (i, elem) {
            var elemVal = $(elem).prop('id');
            if (myVal === elemVal) {
                $(this).attr("checked", true);
            } else {
                $(this).attr("checked", false);
            }
        });
    });

    var storage = $('[name=playViaStorage]');
    storage.on('click', function () {
        var myVal = $(this).prop('id');
        storage.each(function (i, elem) {
            var elemVal = $(elem).prop('id');
            if (myVal === elemVal) {
                $(this).attr("checked", true);
            } else {
                $(this).attr("checked", false);
            }
        });
    });

    displayLoading(false);

    $('.side-logo').css('top', '35px');
});

function getDeviceInfo() {
    getListDevices(function (msg) {
        var {devices: devices} = msg;

        devices.forEach(function (device) {
            var {deviceType: deviceType} = device;
            if (deviceType === "usb") {
                deviceInfo["usb"] = device;
            } else if (deviceType === "internal signage") {
                deviceInfo["internal"] = device;
                setBackupfileInfo();
            }
        });
    });
}

function uploadFile(file, targetDir) {
    var formData = new FormData();
    var newname = filteringXSS(file.name); //filter for XSS

    // console.log("targetDir::" + targetDir);
    formData.append('dir', targetDir);
    formData.append('newname', newname);
    formData.append('failOver', file);

    if (!file) {
        showMyModal('fail-warning-modal', getLanguageText(languageTable, "Please choose a file to upload"));
        failAlertFocusTrap.activate();
        // showWarning(getLanguageText(languageTable, "Please choose a file to upload"), 5 * 1000);
        return;
    }

    // overlayControl(true, getLanguageText(languageTable, "Uploading") + "<br>" + newname);
    $("#progress").show();
    $(".upload_disable").prop("disabled", true);
    $("button").prop("disabled", true);

    var xhr = new XMLHttpRequest();

    xhr.open('post', '/upload', true);

    xhr.upload.onprogress = function (e) {
        if (e.lengthComputable) {
            var per = Math.round((e.loaded / e.total) * 100);
            // $('#uploadProgress').html(per + "%");
            // $('#uploadProgress').css('width', per + '%');
            // $('#overlay_progress').html(per + "%");
        }
    };

    xhr.upload.onload = function (e) {
        setTimeout(function () {
            // overlayControl(false);
            setBackupfileInfo();
        }, 2000);
    }

    xhr.onerror = function (e) {
        // overlayControl(false);
        alert(getLanguageText(languageTable, "An error occurred while submitting the form."));
        console.log('An error occurred while submitting the form. Maybe your file is too big');
    };

    xhr.onload = function () {
        console.log(this.statusText);
    };

    xhr.send(formData);
}

function updateBackupViaStorage() {
    var isOn = $("#btnBackup").is(':checked');
    // console.log("updateBackupViaStorage() " + isOn);
    if (isOn) {
        var option = "auto";
        $("#advancedSettingTable").css('display', 'block');
        if (env.failover.backupViaStorage.auto.visible) {
            option = "auto"
        } else if (env.failover.backupViaStorage.manual.visible) {
            option = "manual"
        } else if (env.failover.backupViaStorage.supersign.visible) {
            option = "supersign"
        } else if (env.failover.backupViaStorage.siapp.visible) {
            option = "siapp"
        }

        if ($("#playViaStorage" + option).is(':checked')) {
            updateBackupViaStorageOption();
        } else {
            $("#playViaStorage" + option).click();
        }
    } else {
        $("#playViaStorageauto").attr("checked", true);
        $("#advancedSettingTable").css('display', 'none');
        $("#intervalTable").css('display', 'none');
        $("#BackupImage").css('display', 'none');
    }
}

function updateBackupViaStorageOption() {
    var option = $("input[name=playViaStorage]:checked").val();
    if (option === 'auto') {
        option = "auto";
        $("#BackupImage").css('display', 'none');
        $("#intervalTable").css('display', 'block');
    } else if (option === 'manual') {
        option = "manual";
        $("#BackupImage").css('display', 'block');
        $("#intervalTable").css('display', 'none');
    } else if (option === 'supersign') {
        option = "supersign";
        $("#BackupImage").css('display', 'none');
        $("#intervalTable").css('display', 'none');
    } else if (option === 'siapp') {
        option = "siapp";
        $("#BackupImage").css('display', 'none');
        $("#intervalTable").css('display', 'none');
    }

    // console.log("updateBackupViaStorageOption() " + option);
}

function drawInterval() {
    var displayName = {
        "30": "30 " + getLanguageText(languageTable, "min"),
        "60": "1 " + getLanguageText(languageTable, "hour"),
        "120": "2 " + getLanguageText(languageTable, "hour"),
        "180": "3 " + getLanguageText(languageTable, "hour")
    }
    var intervalValList = ["30", "60", "120", "180"];

    var intervalDropdown = $('#interval-dropdown');
    var dropdownMenu = $(intervalDropdown).children(".dropdown-menu");

    var ul = createDropdownChildren(intervalValList, displayName, 'interval-dropdown');

    var lis = $(ul).children('li');
    lis.each(function (i) {
        var selected = intervalValList[i];
        $(this).on('click', function () {
            changeDropdownLabel("interval-dropdown", displayName[selected]);
            changeDropdownAria("interval-dropdown", i);
        });
    });

    /*for (var i = 0; i< intervalValList.length; i++) {
        var selected = intervalValList[i];
        $(lis[i]).on('click', function () {
            changeDropdownLabel("interval-dropdown", displayName[selected]);
            changeDropdownAria("interval-dropdown", i);
        });
    }*/

    $(dropdownMenu).html(ul);

    getBackupViaStorage(function (msg) {
        captureTimeInterval = msg.backupViaStorageInterval;
        var mySelected =  displayName[msg.backupViaStorageInterval];
        var myIndex = intervalValList.findIndex(function (item) {return item === msg.backupViaStorageInterval});
        changeDropdownLabel("interval-dropdown", mySelected);
        changeDropdownAria("interval-dropdown", myIndex);

        var btnBackup = $("#btnBackup");
        if (msg.backupViaStorage === "off") {
            btnBackup.prop("checked", false);
            btnBackup.trigger('change');

            $("#advancedSettingTable").css('display', 'none');
        } else {
            btnBackup.prop("checked", true);
            btnBackup.trigger('change');

            $("#advancedSettingTable").css('display', 'block');
            var playViaStorage = $("#playViaStorage" + msg.backupViaStorage);
            if (playViaStorage.is(':checked')) {
                updateBackupViaStorageOption();
            } else {
                playViaStorage.trigger('click');
            }
        }
    });
}

function setAdvancedSetting(callback) {
    var backupViaStorage = "";
    var settings = undefined;

    $("#btnBackup").prop("checked") ? backupViaStorage = $("input[name=playViaStorage]:checked").val() : backupViaStorage = "off";

    if (backupViaStorage === "off") {
        settings = {
            backupViaStorage: backupViaStorage
        }
    } else if (backupViaStorage === "auto") {
        settings = {
            backupViaStorage: backupViaStorage,
            backupViaStorageInterval: captureTimeInterval
        }
    } else {
        settings = {
            backupViaStorage: backupViaStorage
        }
    }

    setSystemSettings("commercial", settings, callback)
}

function setBackupfileInfo() {
    getSystemSettings("commercial", ["backupViaStorageFile"], function (msg) {

        var backupViaStorageFile = msg.backupViaStorageFile;
        getBackupfileName(deviceInfo["internal"].deviceUri + "/.failover/", function (msg) {
            var fileName = msg[0];
            if (backupViaStorageFile !== '' && msg[0]) {
                var filter = {
                    video: ["avi", "mp4", "wmv", 'divx', 'asf', 'm4v', 'mov',
                        '3gp', '3g2', 'mkv', 'ts', 'trp', 'tp', 'mts',
                        'mpg', 'mpeg', 'dat', 'vob'],
                    image: ["png", "bmp", "jpg", 'jpeg', 'jpe']
                };

                $("#failoverImageName").html(fileName);

                var ext = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();

                if (filter["video"].indexOf(ext) >= 0) {
                    $("#failoverImagetype").attr("src", "/images/ic_record.png");
                } else if (filter["image"].indexOf(ext) >= 0) {
                    $("#failoverImagetype").attr("src", "/images/ic_photo.png");
                }

                getThumbnail("failOver", fileName, function (msg) {
                    $("#failoverImageName").html(fileName);
                    if (msg.result === true) {
                        var time = new Date().getTime();
                        $("#failoverImage").attr("src", "/failoverimage?time=" + time);
                    } else {
                        $("#failoverImage").attr("src", "/images/screenshot_noimage_small.png");
                    }
                    $(".upload_disable").prop("disabled", false);
                    $("button").prop("disabled", false);
                });
            } else {
                $("#failoverImage").attr("src", "/images/screenshot_noimage_small.png");
                $("#failoverImageName").html(getLanguageText(languageTable, "There's No Backup file"));
            }
        });
    });
}

function updateFailOverPriorityInfo() {
    var ol = $('#failOverPriorityInfo ol');
    ol.empty();
    for (var i = 0; i < inputList.length; i++) {
        var item = `<li><div class="field-label">${inputList[i].name}</div></li>`
        $(ol).append(item);
    }
}

function drawFailOverPriority() {

    function createOl() {
        var ol = document.createElement('ol');
        ol.classList.add('field-list');
        var lis = '';
        for (var i = 1; i <= inputList.length; i++) {
            var item = `<li id="failOverPriority${i}"><div class="priority_item field-label">${inputList[i-1].name}</div>
                        <div class="up-down-btns">
                          <button type="button" class="btn-up" title="Item up"
                          onclick="setPriorityUp('failOverPriority${i}')" ${i===1 && `disabled`}></button>
                          <button type="button" class="btn-down" title="Item down" 
                          onclick="setPriorityDown('failOverPriority${i}')" ${i===inputList.length && `disabled`}></button>
                        </div>
                    </li>`
            lis += item;
        }
        return lis;
    }

    var html = `<div id="failOverPrioritySettingModal" class="popup-wrapper">
    <div class="dim">&nbsp;</div>
    <div class="popup popup-information" role="dialog" aria-labelledby="popup-label" aria-modal="true">
        <div class="popup-container">
            <div class="popup-header">
                <h1 id="popup-label">${getLanguageText(languageTable, "Input Priority Setting")}</h1>
                <button></button>
            </div>
            <div class="popup-content">
                <ol class="field-list">
                    ${createOl()}
                </ol>
            </div>
        </div>
        <div class="popup-footer">
            <div class="button-container">
                <div class="button-container">
                    <button type="button" id="failOver-cancel" class="btn btn-cancel" onclick="_clickFailCancel()">${getLanguageText(languageTable, "Cancel")}</button>
                    <button type="button" id="failOver-ok" class="btn btn-yes btn-primary" onclick="_clickFailOK()">${getLanguageText(languageTable, "Save")}</button>
                </div>
            </div>
        </div>
    </div>
    </div>`

    var modal = $(html);
    $('body').append(modal);
}

function _clickFailCancel() {
    failPriorityFocusTrap.deactivate();
    _closePopup('failOverPrioritySettingModal');
}

function _clickFailOK() {
    failPriorityFocusTrap.deactivate();
    savePriority();
    _closePopup('failOverPrioritySettingModal');
}

function setFailOver() {
    var applyResult = true;
    var failover = "";
    var settings = undefined;

    $("#btnFailover").prop("checked") ? failover = $("input[name=failOverPriority]:checked").val() : failover = "off";

    if (failover === "manual") {
        settings = {
            failover: failover,

        }

        for (var i = 0; i < inputList.length; i++) {
            var property = "failoverPriority" + (i + 1);
            settings[property] = inputList[i].settingKey;
        }
    } else {
        settings = {
            failover: failover
        };
    }

    setSystemSettings("commercial", settings, function (m) {
        if (!m.returnValue) {
            applyResult = false;
        }
    });

    if (failover !== "off") {
        setAdvancedSetting(function (m) {
            if (!m.returnValue) {
                applyResult = false;
            }
            showReturnValue(applyResult, "", "failover");
        });
    } else {
        showReturnValue(applyResult, "", "failover");
    }
}

function initFailOver() {
    // $("#failOverPrioritySettingModal").css({"padding-top": (window.innerHeight * 0.25) + "px"});

    getFailOverPriorityInfo(function (failOverPriorityInfo) {
        inputList = failOverPriorityInfo;
        updateFailOverPriorityInfo();
        drawFailOverPriority();
    });

    getFailOver(function (msg) {
        var btnFailOver = $("#btnFailover");
        if (msg.failover === "off") {
            btnFailOver.prop("checked", false);
            btnFailOver.trigger('change');
        } else {
            btnFailOver.prop("checked", true);
            $("#failOverPriority" + msg.failover).attr("checked", true);
            $("input[name=failOverPriority]").change();
        }
        // $("#rootContainer").show();
    });
}

// function fileThis() {
//     document.getElementById('file-find').click();
// }

function init() {
    getDeviceInfo();
    drawInterval();
    makeKeyDownDropdown("interval-dropdown");
    initFailOver();

    $('#failover-setting-btn').on('click', function () {
        togglePopup('failover-setting-btn', 'failOverPrioritySettingModal');
        failPriorityFocusTrap.activate();
    });

    $('#apply').on('click', function () {
        setFailOver();
        showSuccess('basic-setting-success');
    });

    $('#btnFailover').on('change', function () {
        var isChecked = $(this).prop('checked');
        $(this).attr('aria-checked', isChecked);
        $('#failOverPriorityauto').trigger('click');

        if ($(this).is(':checked')) {
            $("#failOverTable").css('display', 'block');
            $("#advancedSetting").css('display', 'block');
        } else {
            $("#failOverTable").css('display', 'none');
            $("#failOverPriorityTable").css('display', 'none');
            $("#advancedSetting").css('display', 'none');
        }
    });

    $("input[name=failOverPriority]").on('change', function () {
        if ($("input[name=failOverPriority]:checked").val() === 'auto') {
            $("#failOverPriorityInfo").hide();
        } else {
            $("#failOverPriorityInfo").show();
        }
    });

    $("#btnBackup").on('change',function () {
        var isChecked = $(this).prop('checked');
        $(this).attr('aria-checked', isChecked);
        updateBackupViaStorage();
    });

    $("input[name=playViaStorage]").on('change',function () {
        updateBackupViaStorageOption();
    });

    $('.file-area :file').on('change', function () {
        var file = this.files[0];
        if (file) {
            $("#fileName").html(filteringXSS(file.name));
        } else {
            $('#fileName').html('');
        }
    });

    $('#upload-btn').on('click', function () {
        var file = document.getElementById('file-find').files[0];

        if (!file) {
            showMyModal('fail-warning-modal', getLanguageText(languageTable, "Please choose a file to upload"));
            failAlertFocusTrap.activate();
            return;
        } else if (env.oledModel && !isVideoFile(file.name)) {
            showMyModal('fail-warning-modal', getLanguageText(languageTable, "Please choose a video file to upload"));
            failAlertFocusTrap.activate();
            return;
        } else if (!isImageFile(file.name) && !isVideoFile(file.name)) {
            showMyModal('fail-warning-modal', getLanguageText(languageTable, "Select media file"));
            failAlertFocusTrap.activate();
            return;
        }

        var targetDir = deviceInfo["internal"].deviceUri + "/.failover/";
        var backupViaStorageFile = targetDir + filteringXSS(file.name);

        deleteFailover(function (msg) {
            setSystemSettings("commercial", {backupViaStorageFile: backupViaStorageFile});
            uploadFile(file, targetDir);
        });
    });

    if (env.oledModel) {
        $(".file_area input[type='file']").attr("accept", "video/*");
    } else {
        $(".file_area input[type='file']").attr("accept", "video/*,image/*");
    }

    if (locale === "ar-SA") {
        changeRtl();
    }
}

function setPriorityUp(id) {
    var priority = id.charAt(id.length - 1);
    if (priority > 1) {
        var curText = $("#failOverPrioritySettingModal #" + id + " .priority_item").text();
        var upText = $("#failOverPrioritySettingModal #" + (id.substring(0, id.length - 1) + (parseInt(priority) - 1)) + " .priority_item").text();
        $("#failOverPrioritySettingModal #" + id + " .priority_item").text(upText);
        $("#failOverPrioritySettingModal #" + (id.substring(0, id.length - 1) + (parseInt(priority) - 1)) + " .priority_item").text(curText);
    }
}

function setPriorityDown(id) {
    var priority = id.charAt(id.length - 1);
    if (priority < inputList.length) {
        var curText = $("#failOverPrioritySettingModal #" + id + " .priority_item").text();
        var downText = $("#failOverPrioritySettingModal #" + (id.substring(0, id.length - 1) + (parseInt(priority) + 1)) + " .priority_item").text();
        $("#failOverPrioritySettingModal #" + id + " .priority_item").text(downText);
        $("#failOverPrioritySettingModal #" + (id.substring(0, id.length - 1) + (parseInt(priority) + 1)) + " .priority_item").text(curText);
    }
}

function savePriority() {
    var newInputList = [];
    for (var i = 0; i < inputList.length; i++) {
        for (var j = 0; j < inputList.length; j++) {
            if ($("#failOverPrioritySettingModal #failOverPriority" + (i + 1) + " .priority_item").text() === inputList[j].name) {
                newInputList[i] = inputList[j];
                break;
            }
        }
    }
    inputList = newInputList;

    updateFailOverPriorityInfo();
}

// function changeRtl() {
//     $("#failOverPrioritySettingModal").attr("dir", "rtl");
//     $(".failover_menu_mobile").css({"margin-left": "0px", "margin-right": "-70px"});
//     $(".advanced_setting_menu").css({"padding-left": "0px", "padding-right": "114px"});
//     $(".advanced_setting_menu_mobile").css({"margin-left": "0px", "margin-right": "-70px"});
//     $("#failOverPriorityInfo span").attr("dir", "ltr");
// }
