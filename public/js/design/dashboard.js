var currentTime;
var fullRefreshTimer;
var captureRefreshTimer;
var criticalTemp = 85;
var warningTemp = criticalTemp - 3;
var unitStatus = {
    ok: 0,
    ng: 0,
    off: 0
};

function setCurTimeDash() {
    getCurrentTime(function(msg) {
        getMenuLanguage(function (ret) {
            currentTime = msg.current;
            var dateArr = currentTime.split(' ');
            var weekDay = myWeekDays[dateArr[0]];
            var year = `${dateArr[3]}${ret === 'en-US' ? '' : getLanguageText(languageTable, "year")}`;
            var month = `${myMonths[dateArr[1]]}`;
            var day = `${dateArr[2]}${ret === 'en-US' ? '' : getLanguageText(languageTable, "Days")}`;
            var timeArr = dateArr[4].split(':');

            var span = `<span>${year} ${month} ${day} <strong>${weekDay}</strong></span>`;

            $('#current-date').html(span);
            $('.hour').text(timeArr[0]);
            $('.min').text(timeArr[1]);
            $('.sec').text(timeArr[2]);
            $('#uptime').html(msg.uptime);
            getDowntimeIncidentDash();
        });
    });
}

function setScreenDash() {
    var height = document.getElementById('screen').naturalHeight;
    getCapture(height, function (m) {
        $('#screen').attr('src', m);
    });
}

function updateTemperature() {
    getTemp(function (res) {
        var tempStatus = true;
        var types = res.group ? res.group : [];
        types.forEach(function (type) {
            var {celsius: temperature, status} = res[type];

            readTempType(function (tempType) {
                if (!tempType.isCelsius) {
                    var tempStr = Math.floor((temperature) * 1.8 + 32) + '<span>°F</span>';
                    $(`#${type}Temp`).html(tempStr);
                    $('#dash-temp-type').html('°F');
                } else {
                    var tempStr = temperature + '°C'
                    $(`#${type}Temp`).html(tempStr);
                    $('#dash-temp-type').html('<span>°C</span>');
                }

                if (!status || temperature >= warningTemp) {
                    tempStatus = false;
                }
            });

            var tempBar = $(`#${type}-temp-bar`);
            tempBar.removeClass("green-bar mint-bar blue-bar");
            tempBar.addClass(tempStatus ? "mint-bar" : "blue-bar");
        });

    });
}

function getSignalInfo() {
    function setSignalStatusAttr() {
        if (isMobile) {
            $('#signal-status').attr('class', 'status mobile');
        } else {
            $('#signal-status').attr('class', 'status');
        }
    }

    function checkExternalInput(foregroundAppInfo, inputList) {
        var isExternalInput = false;
        for (var i=0; i<inputList.length; i++) {
            if (foregroundAppInfo.appId === inputList[i].appId) {
                isExternalInput = true;
                break;
            }
        }
        // console.log("checkExternalInput() isExternalInput: " + isExternalInput);
        return isExternalInput;
    }

    function getIsPcMode(videoInfo, hdmiPcMode, currInput) {
        var {timingMode: timingMode} = videoInfo;
        var {hdmi1: hdmi1, hdmi2: hdmi2, hdmi3: hdmi3, hdmi4: hdmi4} = hdmiPcMode;
        var isPcMode;
        if (timingMode === "PC") {
            isPcMode = true;
        } else if (timingMode === "TV") {
            isPcMode = false;
        } else {
            isPcMode = (hdmi1 !== undefined && hdmi1 && currInput === "HDMI_1")
                || (hdmi2 !== undefined && hdmi2 && currInput === "HDMI_2")
                || (hdmi3 !== undefined && hdmi3 && currInput === "HDMI_3")
                || (hdmi4 !== undefined && hdmi4 && currInput === "HDMI_4");
        }
        return isPcMode;
    }

    function getScanType(videoInfo) {
        var {scanType: scanTypeInfo} = videoInfo;
        var scanType = "";
        if (scanTypeInfo === "VIDEO_PROGRESSIVE") {
            scanType = "p";
        } else if (scanTypeInfo === "VIDEO_INTERLACED") {
            scanType = "i";
        }
        return scanType;
    }

    getForegroundAppInfo(function (foregroundAppInfo) {
        getInputList(function (inputList) {
            if (!checkExternalInput(foregroundAppInfo, inputList)) {
                $('#signal').html(capitalizeFirstLetter(getLanguageText(languageTable, "NOT AVAILABLE")));
                setBackground($('#signalCard'), true);
                $("#signalCard .tail").hide();
                return
            }

           isNoSignal(function (msg) {
               var {noSignal: noSig} = msg;
               setSignalStatusAttr();

               getHdmiInput(function (currInput) {
                   var isNoSignal = !msg || noSig;
                    if (isNoSignal) {
                        $('#signal').html(getLanguageText(languageTable, "No Signal"));
                        setBackground($('#signalCard'), false);
                        setInputName(currInput);
                        return
                    }

                    getVideoSize(function (videoInfo) {
                        if (!videoInfo || !videoInfo.returnValue || (videoInfo.width === 0 && videoInfo.height === 0)) {
                            $('#signal').html(capitalizeFirstLetter(getLanguageText(languageTable, "NOT AVAILABLE")));
                            setBackground($('#signalCard'), true);
                            $("#signalCard .tail").hide();
                            return
                        }

                        getHdmiPcMode(function (hdmiPcMode) {
                            var isPcMode = getIsPcMode(videoInfo, hdmiPcMode, currInput);
                            var scanType = getScanType(videoInfo);

                            var generalResolution = videoInfo.height + scanType;
                            var pcModeResolution = videoInfo.width + " X " + videoInfo.height;
                            var text = isPcMode ? pcModeResolution : generalResolution;

                            $('#signal').html(text);
                            setBackground($('#signalCard'), true);
                            setInputName(currInput);

                            getVideoStillStatus(function (msg) {
                                if (msg.state === "stalled") {
                                    $('#signal').html(getLanguageText(languageTable, "Stalled Image"));
                                    $('#stall').html(getLanguageText(languageTable, "over {period} min.").replace('{period}', msg["period(minutes)"]));
                                    setBackground($('#signalCard'), false);
                                }
                            })
                        });
                    });
               });
           })
        });
    })
}

function getSWInfoDash() {
    getBasicInfo(function (msg) {
        var {firmwareVersion: firmwareVersion} = msg;
        $('#firmware').html(firmwareVersion);
    });
}

function getSignageNameDash() {
    getSignageName(function (ret) {
        $("#signageName").html(ret);
        setBackground($('#signageNameCard'), true);
    });
}

function getIdDash() {
    getSetID(function (msg) {
        $('#setID').html(msg.setId);
        setBackground($('#setIDCard'), true);
    })
}

function getDPMDash() {
    getDPM(function (msg) {
        $("#DPM").html((msg.dpmMode === "off") ? getLanguageText(languageTable, "Off") : getLanguageText(languageTable, "On"));
        var dpmInfo = (msg.dpmMode === "off") ? "" : msg.dpmMode + ". off";
        if (dpmInfo !== "") {
            if (msg.dpmMode === '10sec') {
                dpmInfo = getLanguageText(languageTable, "10 sec") + ". " + getLanguageText(languageTable, "Off");
            } else {
                dpmInfo = msg.dpmMode.substring(0, msg.dpmMode.length - 3) + getLanguageText(languageTable, "min") + ". " + getLanguageText(languageTable, "Off");
            }
        }
        $("#DPMmode").html(dpmInfo);
        setBackground($('#DPMCard'), true);
    })
}

function getNetworkStatusDash() {
    getNetworkStatus(function (msg) {
        var {isInternetConnectionAvailable: isInternetConnectionAvail} = msg;
        var connection = msg.wired.state === 'connected' ? msg.wired : msg.wifi;
        $('#ip').html(connection.state === 'connected' ? connection.ipAddress : getLanguageText(languageTable, "Not Connected"));
        $('#wan').html(isInternetConnectionAvail ? getLanguageText(languageTable, "Internet available") : getLanguageText(languageTable, "Internet Not available"));
        setBackground($('#networkCard'), connection.state === 'connected');
    });
}

function getPlayViaUrlDash() {
    getPlayViaUrl(function (msg) {
        var playViaUrlMode = msg.playViaUrlMode;
        $("#URLStatus").html((playViaUrlMode === "off") ? getLanguageText(languageTable, "Off") : getLanguageText(languageTable, "On"));

        if (playViaUrlMode === "on") {
            var playViaUrl = msg.playViaUrl;
            if (!playViaUrl) {
                $("#URL").html(getLanguageText(languageTable, "Check your URL input"));
            } else if (playViaUrl.length > 20) {
                $("#URL").html(playViaUrl.substr(0, 20) + "...")
            } else {
                $("#URL").html(playViaUrl);
            }
        } else {
            $("#URL").html("");
        }
        setBackground($('#URLCard'), true);
    });
}

function getCheckScreenOnDash() {
    if (env.checkscreen.support) {
        getCheckScreenOn(function (m1) {
            if (m1 === 'off') {
                $('#checkScreen').html(getLanguageText(languageTable, "Off"));
                setBackground($('#screenCard'), true);
                return;
            }
            getCheckScreenColor(function (msg) {
                var {colorValid: colorValid} = msg;
                $('#checkScreen').html(colorValid ? getLanguageText(languageTable, "OK") : "Not OK");
                setBackground($('#screenCard'), colorValid);
            });
        });
    }
}

function getPanelErrorOutDash() {
    if (env.supportPanelErrorOut) {
        getPanelErrorOut(function (msg) {
            var {isPanelBLUOk: isPanelBLUOk} = msg;
            var isNG = !isPanelBLUOk;
            $('#BLU').html(isNG ? "Not OK" : getLanguageText(languageTable, "OK"));
            setBackground($('#BLUCard'), !isNG);
        });
    }
}

function updateTileModeInfoDash() {
    getTileModeValue(function (msg) {
        var {tileRow: tileRow, tileCol: tileCol, tileId: tileId} = msg;
        var tileMode = (msg.tileMode === "off") ? "Off" : "On";
        $("#tileMode").html(getLanguageText(languageTable, tileMode));
        setBackground($('#tileModeCard'), true);
        if (tileMode === "On") {
            $("#tileModeSettings").html("(" + tileRow + "x" + tileCol + ", " + getLanguageText(languageTable, "Tile ID") + ": " + tileId + ")");
        } else {
            $("#tileModeSettings").html("");
        }
    });
}

function updateFailOverInfoDash() {
    getFailOver(function (msg) {
        // Change first letter  to upper case
        var displayInfo = msg.failover.substring(0, 1).toUpperCase() + msg.failover.substring(1, msg.failover.length);
        $("#failOver").html(getLanguageText(languageTable, displayInfo));

        if (msg.failover === "off") {
            $("#failOverPriority").html("");
        } else {
            getFailOverPriorityInfo(function (failOverPriorityInfo) {
                var html = "";

                if (failOverPriorityInfo.length === 1) {
                    html = "(1 " + failOverPriorityInfo[0].name + ")";
                } else {
                    if (msg.failover === "auto") {
                        var priority = [];
                        for (var i = 0; i < failOverPriorityInfo.length; i++) {
                            if (failOverPriorityInfo[i].id === "HDMI_1") {
                                priority[0] = failOverPriorityInfo[i].name;
                            } else if (failOverPriorityInfo[i].id === "HDMI_2") {
                                priority[1] = failOverPriorityInfo[i].name;
                            }
                        }
                        html = "(1 " + priority[0];
                        html += " > 2 " + priority[1];
                    } else if (msg.failover === "manual") {
                        html = "(1 " + failOverPriorityInfo[0].name;
                        html += " > 2 " + failOverPriorityInfo[1].name;
                    }

                    if (failOverPriorityInfo.length === 2) {
                        html += ")";
                    } else {
                        html += "..)";
                    }
                }
                $("#failOverPriority").html(html);
                if ($('#failOverPriority').width() > $('#failOverCard').width()) {
                    setTimeout(function () {
                        var priority = $('#failOverPriority')
                        var html = "<marquee>" + priority.html() + "</marquee>";
                        priority.html(html);

                    }, 1000);
                }
            })
        }
        setBackground($('#failOverCard'), true);
    });
}

function updateFanStatusDash() {
    getFanStatus(function (res) {
        if (!res.returnValue || !res.support) {
            return;
        }

        var isOK = true;
        var types = res.group;
        types.some(function (type) {
            var fan = res[type];
            fan.status.some(function (status) {
                isOK = status
                return !isOK;
            });
            return !isOK;
        });

        $('#fan').html(isOK ? getLanguageText(languageTable, 'OK') : "Not OK");
        setBackground($('#fanCard'), isOK);
    });
}

function getDowntimeIncidentDash() {
    getDowntimeIncident(function (msg) {
        var {incident: incs} = msg;
        var downloadLog = $('#downloadLog');
        if (!msg.returnValue) {
            downloadLog.data('url', '')
            // downloadLog.removeAttr('href');
            return;
        }
        $('#incidentTable > tbody').empty();

        incs.forEach(function (inc) {
            if (inc.state !== 'NG') {
                addIncident(inc);
            }
        });
        incs.forEach(function (inc) {
            if (inc.state === 'NG') {
                addIncident(inc);
            }
        });
        // downloadLog.attr('href', '/downloadIncidents?name=downloadIncidents.csv');
        downloadLog.data('url', '/downloadIncidents?name=downloadIncidents.csv');

        drawIncidentReflowTable(incs);
    });
}

function getPsuStatusDash() {
    getSDMInfo(function (msg) {
        var {PSUStatus: PSUStatusMsg} = msg;
        var PSUStatus = PSUStatusMsg;
        var checkResult = true;

        if (PSUStatus) {
            for (var i = 0; i < PSUStatus.length; i++) {
                for (var variable in PSUStatus[i]) {
                    if (PSUStatus[i].hasOwnProperty(variable)) {
                        if (!PSUStatus[i][variable]) {
                            checkResult = false;
                            break;
                        }
                    }
                }
                if (!checkResult) {
                    break;
                }
            }

            $('#PSU').html(checkResult ? 'OK' : 'Not OK');
            setBackground($('#PSUCard'), checkResult);
        } else {
            setBackground($('#PSUCard'), false);
        }
    });
}

function showDoorStatus(doors) {
    if (!doors) {
        return;
    }

    var opened = false;

    for (var i = 0; i !== doors.length; ++i) {
        if (doors[i]) {
            opened = true;
            break;
        }
    }

    $('#door').html(opened ? getLanguageText(languageTable, "Opened") : getLanguageText(languageTable, "Closed"));
    setBackground($('#doorCard'), !opened);
}

function getBrightness() {
    getBluMaintain(function (res) {
        if (!res.returnValue || !res.status) {
            $('#brightness').text("Not OK");
            setBackground($('#brighnessCard'), false);
        } else {
            $('#brightness').text(res.bluMaintain + ' cd/m2');
            setBackground($('#brighnessCard'), true);
        }
    })
}

function getAcCurrent() {
    getPowerCurrent(function (res) {
        if (!res.returnValue || !res.status) {
            $('#acCurrent').text("Not OK");
            setBackground($('#acCurrentCard'), false);
        } else {
            $('#acCurrent').text(res.powerCurrent + ' A');
            setBackground($('#acCurrentCard'), true);
        }
    })
}

function getSolarVoltage() {
    getSolar(function (res) {
        if (!res.returnValue || !res.status) {
            $('#solarVoltage').text("Not OK");
            setBackground($('#solarVoltageCard'), false);
        } else {
            $('#solarVoltage').text(res.solar + ' mV');
            setBackground($('#solarVoltageCard'), true);
        }
    });
}

function updateTempFontSize() {
    // var temperature = $('.temperature');
    // var sensorCount = temperature.length;

    // if (sensorCount >= 5) {
        // temperature.css("font-size", 100 / sensorCount);
        // $('.sensor-name').css("font-size", 100 / sensorCount);
    // }
}

function setBackground(card, isOK) {
    if (isOK) {
        card.removeClass('bg-red');
        card.addClass('bg-gray');
    } else {
        card.removeClass('bg-gray');
        card.addClass('bg-red');
    }
}

function addIncident(inc) {
    var {sensor: sensor} = inc;
    currentTime = currentTime.replace(" ", "T");
    var startTime = inc.startTime.replace(" ", "T");

    var isNG = inc.state === 'NG';
    var duration = inc.duration;

    if (duration === 0 && inc.endTime.length > 0 && inc.startTime.length > 0) {
        duration = (new Date(currentTime)).getTime() - (new Date(startTime)).getTime();
        duration = Math.round(duration / 1000);
    }

    if (duration < 0) {
        duration = 0;
    }

    if (!isMobile) {
        var arabicAttr = '';
        if (locale === "ar-SA") {
            arabicAttr = 'dir="ltr" style="text-align: right"';
        }

        $('#incidentTable > tbody:last').prepend(
            '<tr>'
            //+ '<td>' + inc.category + '</td>'
            + '<td ' + arabicAttr + '>' + inc.category + "(" + sensor + ')</td>'
            + '<td>' + inc.event + '</td>'
            + '<td' + (isNG ? ' class="color-point"' : "") + '>' + (isNG ? "Not OK" : inc.state) + '</td>'
            + '<td>' + inc.startTime + '</td>'
            + '<td>' + inc.endTime + '</td>'
            + '<td>' + secToTime(duration) + '</td></tr>');
    } else {
        $('#incidentTable > tbody:last').prepend(
            '<tr>'
            + '<td style="font-size:10px">' + inc.category + '</td>'
            + '<td style="font-size:10px">' + sensor
            + '<br>' + inc.event + '</td>'
            + '<td style="font-size:10px">' + getMobileTimeFormat(inc.startTime)
            + '<br>' + getMobileTimeFormat(inc.endTime) + '</td>'
            + '<td style="font-size:10px"' + (isNG ? ' class="color-point"' : "") + '>' + (isNG ? "Not OK" : inc.state)
            + '<br>' + secToTime(duration) + '</td></tr>');
    }
}

function drawIncidentReflowTable(incs) {
    var rows = '';

    for (var inc of incs) {
        var {category, sensor, event, state, startTime, endTime, duration} = inc;
        var tableRow = `<li class="table-row">
                <div class="summary-data">
                    <div class="row">
                        <div class="table-cell center">${category}(${sensor})</div>
                        <div class="table-cell">
                            <button type="button" role="listbox" class="btn btn-expand" aria-expanded="false">${event}</button>
                        </div>
                    </div>                 
                </div>
                <div class="all-data-box">
                    <ul class="all-data">
                        <li>
                            <span class="field-label">${getLanguageText(languageTable, 'Status')}</span>
                            <span class="field-content">${state}</span>
                        </li>
                        <li>
                            <span class="field-label">${getLanguageText(languageTable, 'Start')}</span>
                            <span class="field-content">${startTime}</span>
                        </li>
                        <li>
                            <span class="field-label">${getLanguageText(languageTable, 'End')}</span>
                            <span class="field-content">${endTime}</span>
                        </li>
                        <li>
                            <span class="field-label">${getLanguageText(languageTable, 'Duration')}</span>
                            <span class="field-content">${duration <= 0 ? '-' : duration}</span>
                        </li>
                    </ul>
                </div>
            </li>`;
        rows += tableRow
    }

    var incident = $('#incident-reflow-table-body');
    incident.html(rows);

    var btn = incident.find('button');
    $(btn).on('click', function () {
        var myRow = $(this).parents('li');
        var isExpanded = $(myRow[0]).hasClass('expand');
        if (isExpanded) {
            $(myRow[0]).removeClass('expand');
            $(this).attr('aria-expanded', "false");
        } else {
            $(myRow[0]).addClass('expand');
            $(this).attr('aria-expanded', "true");
        }
    });
}

function secToTime(sec) {
    if (sec === 0) {
        return "-"
    }
    var hour = Math.floor(sec / 60 / 60);
    var min = Math.floor(sec / 60) % 60;
    sec = sec % 60;

    return "" + hour + "H " + min + "M " + sec + "S";
}

function getAllData() {
    setCurTimeDash();
    getSWInfoDash();
    getNetworkStatusDash();

    // sensor data start
    getSignalInfo();
    updateTemperature();
    getCheckScreenOnDash();
    getPanelErrorOutDash();
    getSignageNameDash();
    getIdDash();
    getDPMDash();
    getPlayViaUrlDash();
    updateFanStatusDash();
    // sensor data end

    if (env.supportPsuStatus) {
        getPsuStatusDash();
    }

    updateTileModeInfoDash();
    updateFailOverInfoDash();

    if (env.supportBrighness) {
        getBrightness();
    }
    if (env.supportAcCurrent) {
        getAcCurrent();
    }
    if (env.supportSolarVoltage) {
        getSolarVoltage();
    }
    $('#screen').trigger("click");
}

function getCookie(key) {
    var replace = new RegExp("(?:(?:^|.*;\\s*)" + key + "\\s*\\=\\s*([^;]*).*$)|^.*$");
    return document.cookie.replace(replace, "$1");
}

function getExpireDate() {
    var curDay = new Date();
    curDay = new Date(curDay.getTime() + 24 * 60 * 60 * 1000);
    return ';expires=' + curDay.toUTCString() + ';';
}

function setInputName(currInput) {
    $('#stall').html('');
    getInputList(function (allInput) {
        allInput.forEach(function (input) {
            if (currInput === input.id) {
                $('#signalCard .tail').show();
                $('#stall').html("(" + input.name + ")");
            }
        });
    });
}

function initDash() {
    criticalTemp = env.criticalTemp? env.criticalTemp : criticalTemp;

    // init UI change
    var head = $('head');
    head.append('<link rel="stylesheet" type="text/css" href="/assets/css/control_manager/control_dashboard.css">');
    head.append('<link rel="stylesheet" type="text/css" href="/assets/css/control_manager/my_control_common.css">');
    $('.lnb-box').css("display", "none");
    $('.content-container').removeClass('wide-padding');
    $('#ROOT_CONTAINER').addClass('dashboard');

    // getIsMobile();

    getAllData();

    updateTempFontSize();

    if (env.supportDoor) {
        socket.on('door', function (msg) {
            showDoorStatus(msg.status);
        });
        getDoorStatus(function (msg) {
            showDoorStatus(msg.status);
        });
    }

    $("#screen").on('click', function () {
        showRotationWarning();
        var height = document.getElementById('screen').naturalHeight;
        getCapture(height, function (m) {
            $('#screen').attr('src', m);
        });

        getPowerState(function (msg) {
            if (msg.state === 'Screen Off') {
                $('#screen').hide();
            } else {
                $('#screen').show();
            }
        });
    });

    var dataRefresh = $('#dataRefresh');
    dataRefresh.on('click', function () {
        var checked = $(this).prop('checked');
        $(this).attr('aria-checked', checked);
        if (checked) {
            fullRefreshTimer = setInterval(function () {
                getAllData();
                $('#screen').trigger("click");
            }, 5 * 60 * 1000);
            document.cookie = 'dataRefresh=1' + getExpireDate() + ';secure';
        } else {
            if (fullRefreshTimer) {
                clearTimeout(fullRefreshTimer);
            }
            document.cookie = 'dataRefresh=;secure';
        }
    });

    var captureRefresh = $('#captureRefresh');
    captureRefresh.on('click',function () {
        var checked = $(this).prop('checked');
        $(this).attr('aria-checked', checked);
        if (checked) {
            captureRefreshTimer = setInterval(function () {
                $('#screen').trigger('click');
            }, 10 * 1000);
            document.cookie = 'captureRefresh=1' + getExpireDate() + ';secure';
        } else {
            if (captureRefreshTimer) {
                clearTimeout(captureRefreshTimer);
            }
            document.cookie = 'captureRefresh=;secure';
        }
    });

    $('#btn-capture-refresh').on('click', function () {
        $('#screen').trigger('click');
    });

    $('#downloadLog').on('click', function () {
        window.location = $(this).data('url');
    })

    $('#btn-refresh-get-all').on('click', function () {
        getAllData();
    });

    displayLoading(false);

    if (getCookie('dataRefresh')) {
        dataRefresh.trigger("click");
        dataRefresh.prop('checked', true);
    }

    if (getCookie('captureRefresh')) {
        captureRefresh.trigger("click");
        captureRefresh.prop('checked', true);
    }

    $('#screen').trigger("click");

    // if (locale === "ar-SA") {
    //     changeRtl();
    // }

}