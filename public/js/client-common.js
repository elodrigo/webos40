var socket = io();

function createReturn(isOK, msg, id) {
	var ret = "<span id='" + id + "'>";
	ret += "<font color='" + (isOK ? "green" : "red") + "'>" + msg;
	ret += " " + (isOK ? getLanguageText(languageTable, "Success") : getLanguageText(languageTable, "Fail")) + "</span>";

	return ret;
}

function createAlert(isOK, msg, id) {
	var ret = "<div id='" + id + "' class='alert ";
	ret += isOK ? "alert-success": "alert-danger";
	ret += "'>";
	ret += '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
	ret += "<strong>" + (isOK ? getLanguageText(languageTable, "Success"): getLanguageText(languageTable, "Fail")) + "</strong>  ";
	ret += msg;
	ret += "</div>";

	return ret;
}

function createWarning(msg, id) {
	var ret = "<div id='" + id + "' class='alert alert-warning' style='color:red; font-weight:bold'>";
	ret += '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
	ret += msg;
	ret += "</div>";

	return ret;
}

function showAlert(isOK, msg, from) {
	var alertHtml = "a";
	var id = "alert" + Math.round(Math.random() * 100000);

	alertHtml = createAlert(isOK, from + ":" + msg, id);

	$("#alertMessage").prepend(alertHtml);

	setTimeout(function() {
		$("#" + id).remove();
	},
	5000);
}

function showReturnValue(isOK, msg, from) {
	var id = "alert" + Math.round(Math.random() * 100000);
	var alertHtml = createReturn(isOK, msg, id);

	var retSpanId = "#" + from + "Return";

	$(retSpanId).empty();
	$(retSpanId).prepend(alertHtml);

	setTimeout(function() {
		$("#" + id).remove();
	},
	1000);
}

function addMaskModal() {
    var title = capitalizeFirstLetter(getLanguageText(languageTable, "Masking Result"));
    var html
        = '<div class="modal fade" tabindex="-1" role="dialog" id="maskingModal">'
        + '<div class="modal-dialog">'
            + '<div class="modal-content">'
                + '<div class="modal-header">'
                    + '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
                    + '<h4 class="modal-title" id="gridSystemModalLabel">' + title + '</h4>'
                + '</div>'
                + '<div class="modal-body">'
                    + '<span id="maskingModalContent"></span>'
                    + '<br><br>'
                    + '<button class="btn" data-dismiss="modal">OK</button>'
                + '</div>'
            + '</div>'
        + '</div>'
    + '</div>';

    var modal = $(html);
    $('body').append(modal);
}

function addWarningModal() {
    var title = capitalizeFirstLetter(getLanguageText(languageTable, "warning"));
    var html
        = '<div class="modal fade" tabindex="-1" role="dialog" id="warningModal">'
            + '<div class="modal-dialog">'
                + '<div class="modal-content">'
                    + '<div class="modal-header">'
                        + '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
                        + '<h4 class="modal-title" id="gridSystemModalLabel">' + title + '</h4>'
                    + '</div>'
                    + '<div class="modal-body">'
                        + '<span id="warningModalContent"></span>'
                        + '<br><br>'
                        + '<button class="btn" data-dismiss="modal">OK</button>'
                    + '</div>'
                + '</div>'
            + '</div>'
        + '</div>';

    var modal = $(html);
    $('body').append(modal);
}

function addWarningModalWithCb() {
    var title = capitalizeFirstLetter(getLanguageText(languageTable, "warning"));
    var html
        = '<div class="modal fade" tabindex="-1" role="dialog" id="warningModalWithCb">'
            + '<div class="modal-dialog">'
                + '<div class="modal-content">'
                    + '<div class="modal-header">'
                        + '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
                        + '<h4 class="modal-title" id="gridSystemModalLabel">' + title + '</h4>'
                    + '</div>'
                    + '<div class="modal-body">'
                        + '<span id="warningModalContentWithCb"></span>'
                        + '<br><br>'
                        + '<button class="btn" data-dismiss="modal" id="ModalCancelWithCb">OK</button>'
                    + '</div>'
                + '</div>'
            + '</div>'
        + '</div>';

    var modal = $(html);
    $('body').append(modal);

    $('#ModalCancelWithCb').click(function() {
        window.location.reload();
    });
}

// addWarningModal();
// addWarningModalWithCb();
// addMaskModal();

function showMaskModal(msg) {
	$('#maskingModalContent').html(msg);
	$('#maskingModal').modal();
}

function showWarningModal(msg, timeout) {
	$('#warningModalContent').html(msg);
	$('#warningModal').modal();
	if (timeout) {
		setTimeout(function() {
			$('#warningModal').modal('hide');
		}, timeout);
	}
}

function showWarningModalWithCb(msg, timeout, cb) {
	$('#warningModalContentWithCb').html(msg);
	$('#warningModalWithCb').modal();
	if (timeout) {
		setTimeout(function() {
			$('#warningModalWithCb').modal('hide');
            cb();
		}, timeout);
	}
}

function showWarning(msg, timeout) {
	var id = "alert" + Math.round(Math.random() * 100000);
	warningHtml = createWarning(msg, id);

	$("#warningMessage").append(warningHtml);
	$('html,body').scrollTop(0);

	if (timeout) {
		setTimeout(function() {
			$("#" + id).remove();
		}, timeout);
	}
}

var multiResponseHandler = {
	results: [],
	handleMultiResponse: function (msg) {
		if (msg.from !== 'network') {
			return false;
		}

		var ALL_RECEIVED_TIME = 1000;
		if (this.results.length === 0) {
			setTimeout(function () {
				this.displayResult(msg);
			}.bind(this), ALL_RECEIVED_TIME);
		}

		this.setResultDate(msg);
		return true;
	},
	displayResult: function(msg) {
		var result = true;
		for (var i = 0; i < this.results.length; i++) {
			if (!this.results[i]) {
				result = false;
				break;
			}
		}
		msg.result = result;
		showResult(msg);
		this.results = [];
	},
	setResultDate: function (msg) {
		if (msg.result === true) {
			this.results.push(true);
		} else if (msg.result === false) {
			this.results.push(false);
		} else if (msg.result instanceof Object) {
			if (msg.result.returnValue == true) {
				this.results.push(true);
			} else if (msg.result.returnValue == false) {
				this.results.push(false);
			}
		}
	}
}

function showResult(msg) {
	var showFn = showReturnValue;
	if (!msg.from) {
		showFn = showAlert;
		msg.from = "";
	}

	if (msg.result == false) {
		showFn(false, "", msg.from);
	} else if (msg.result.returnValue === false) {
		showFn(false, msg.reason, msg.from);
	} else {
		showFn(true, "", msg.from);
	}
}

socket.on('error', function(msg) {
	console.log('Log-in session expired. Please log in again');
	//showWarning('Log-in session expired. Please log in again');
});

socket.on('return', function(msg) {
	if (multiResponseHandler.handleMultiResponse(msg)) {
		return;
	}

	if (!msg.result) {
		return;
	}

	showResult(msg);
});

socket.on('locale', function(locale) {
    location.reload();
});
