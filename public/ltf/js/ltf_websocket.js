var socket = io();

function callLedAPI(command, data, event, callback) {
	if (event && callback) {
		var id = Math.round(Math.random() * 1000000);
		socket.on(event + id, function(msg) {
			callback(msg);
			socket.removeAllListeners(event + id);
		});
		data.eventID = id;
	}
	data.command = command;
	socket.emit("led", data);
}

function ltf_call_websocket(testcase_number, callback) {
	var command = ltf_command[testcase_number] ;
	var data = ltf_parameter[testcase_number];
	var event = "imb";
	console.log(command);
	callLedAPI(command, data, event, callback); 
}
