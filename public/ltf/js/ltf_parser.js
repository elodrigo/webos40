var ltf_command = new Array();
var ltf_parameter = new Array();
var ltf_expected_result = new Array();
var ltf_max_parameter_number = 5;

function testcases_parsing(testcases) {
    let file = testcases;
    let string ="";
  
    if (testcases) {
            let contents = testcases; 
            let rows = contents.split(/\r?\n/); 

            rows.forEach(function(part, i, rowArr){
                if (rowArr[i]){
                    rowArr[i] = rowArr[i].split(" ");
                    rowArr[i].forEach(function(part2, j, valArr){
                        string = valArr[j];
//                        if(!string.indexOf('/')){
//                        	;
//                        }
                        
                        if(!string.indexOf('>')){
                        	ltf_command[i] = valArr[j].slice(1);
                        	//console.log(ltf_command[i]);
                        }
                        else if(!string.indexOf('{')){
                        	//ltf_parameter[i] = valArr[j];
                        	var object;
                        	ltf_parameter[i] = JSON.parse(valArr[j]);
                        	object = ltf_parameter[i];
                        	
                        	object = Object.keys(ltf_parameter[i]);
                        	
                        //	console.log(object);
                        //	console.log();
                      
                        }
                        else if(!string.indexOf('$')){
                        	ltf_expected_result[i] = valArr[j].slice(1);
                        	//console.log(ltf_expected_result[i]);
                        }
                        
                    });    
                }
            });

    }
}


function trims(in_str) {
        var trim_value = in_str;
        if (trim_value === "") {
            return trim_value;
        }
        while (String(trim_value).charAt(0) === " " ||
                  String(trim_value).charAt(0) === "\n" || 
                  String(trim_value).charAt(0) === "\t" || 
                  String(trim_value).charAt(0) === "\r") {
            trim_value = trim_value.substring(
                    1, trim_value.length);
        }
        while (String(trim_value).charAt(trim_value.length - 1) === " " ||
                  String(trim_value).charAt(trim_value.length - 1) === "\n" ||
                  String(trim_value).charAt(trim_value.length - 1) === "\t" ||
                  String(trim_value).charAt(trim_value.length - 1) === "\r") {
            trim_value = trim_value.substring(
                    0, trim_value.length - 1);
        }
        return trim_value;
    };


function model_list_parser() {

	this.model_list = new Array();
	this.testcase_file_path_list = new Array();
	
	this.parse_model_list_file = function(raw_data){
		var row_delimiter = 
		    (raw_data.indexOf('\r\n') > -1) ? '\r\n' :
		    (raw_data.indexOf('\r') > -1) ? '\r' : '\n';
        var lines = raw_data.split(row_delimiter);
        for (var i = 0; i < lines.length; i++) {
        	var line = trims(lines[i]);
        	if (line === "") {
        		continue;
        	}
        	var item = line.split("=");
        	var model_name = item[0];
        	var testcase_file_path = item[1];
        	this.model_list[this.model_list.length] = model_name;
        	this.testcase_file_path_list[model_name] = testcase_file_path;
        }
	}
	
	this.get_model_list = function(){
		return this.model_list;
	}
	
	this.get_testcase_file_path = function(model_name){
		return this.testcase_file_path_list[model_name];
	}
}
