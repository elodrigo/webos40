var ltf_timer_interval_id = -1;
var TIMER_TASK_LIST = [];

function startTimer() {
	if (ltf_timer_interval_id != -1) {
		ltf_clearInterval("startTimer", ltf_timer_interval_id);
		ltf_timer_interval_id = -1;
	}
	ltf_timer_interval_id = ltf_setInterval("startTimer", update_time_display, 1000);
}
 
function stopTimer() {
	if (ltf_timer_interval_id !== -1) {
		clearInterval(ltf_timer_interval_id);
		ltf_timer_interval_id = -1;
	}
}

function restartTimer() {
	for (var id in TIMER_TASK_LIST) {
		console.log("id ===> " + id);
		var obj = TIMER_TASK_LIST[id];
		if (obj === null) {
			continue;
		}
		ltf_clearInterval("restartTimer", id);
		var new_id = ltf_setInterval("restartTimer", obj.task, obj.interval);
		console.log("Re-Mapping Timer " + id + " ---> " + new_id);
		TIMER_TASK_LIST[new_id] = null;
		obj.id = new_id;
		TIMER_TASK_LIST[id] = obj;
	}
	stopTimer();
	startTimer();
}

function update_time_display () {
    var curtime = new Date();
    var time = "";
	time = String(curtime);
    $('#date').html(time.substring(0,25));
    //console.log(time);
}

function ltf_setInterval(message, task, interval_in_ms) {
	var interval_id = setInterval(task, interval_in_ms);
	//console.log("\n------> setInterval : " + message + " : " + interval_id);
	TIMER_TASK_LIST[interval_id] = { "id":interval_id, "task":task, "interval":interval_in_ms };
	return interval_id;
}

function ltf_clearInterval(message, interval_id) {
	var obj = TIMER_TASK_LIST[interval_id];
	
	if (interval_id !== obj.id) {
		console.log("\n------> clearInterval : " + message + " : " + interval_id + " -- " + obj.id);
	}
	if (obj.id !== -1) {
		clearInterval(obj.id);
	}
	TIMER_TASK_LIST[interval_id] = null;
}