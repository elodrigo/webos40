var current_item = 'li_horizontal_1';
var list_index = 1;
var horizontal_index = 1;
var number_of_horizontal_tabs = 5;
var page_number = 1;
var number_of_pages = 0;
var total_count = 0;
var passed_count = 0;
var failed_count = 0;
var total_log = "";
var passed_log = "";
var failed_log = "";
var event_log = "";
var remaining_count = 0;
var current_page = 1;
var ELEMENTS_PER_PAGE = 12;
var current_testcase_index = 1;
var current_result_screen = "total_result";
var is_run_all = false;
var is_running = false;
var is_manual_window_on = false;
var manual_window_selection = "yes";
var result_area;
var url;
var current_screen = "li_horizontal_2";
var themes = new Array("none","black-theme","grey-theme");
$("document").ready(function() {
    $(document).keydown(function(e) {
	var key_pressed = e.keyCode;
	if (key_pressed === 0x0193) { // RED
		window.location.href = "http://10.177.175.58:4000/procentric/application/browserware/index.html";
	}
	if(key_pressed != 38 && key_pressed != 40 && key_pressed != 37 &&
	    key_pressed != 39 && key_pressed != 13)
	    return false;	 
	e.preventDefault();
	if(is_manual_window_on){
	    switch (key_pressed) {
	        case 38: // keyup
		    case 40: //keydown
		    case 37: //keleft
		    case 39: //keyright
				if(manual_window_selection === "yes"){
					document.getElementById(manual_window_selection).style.background = 
                    "url(images/yes.png) no-repeat";
					manual_window_selection = "no";
					document.getElementById(manual_window_selection).style.background = 
                    "url(images/no-active.png) no-repeat";
				}else{
					document.getElementById(manual_window_selection).style.background = 
                    "url(images/no.png) no-repeat";
					manual_window_selection = "yes";
					document.getElementById(manual_window_selection).style.background = 
                    "url(images/yes-active.png) no-repeat";
				}
		        break;
			case 13: //enter
			    if(manual_window_selection === "yes"){
				    set_result(true);
				}else{
				    set_result(false);
				}
				break;
		}
	    
	}else{

		defocus_current_item();
		execute_key_event(key_pressed);
		focus_current_item();
	}		
    });
});

function set_stylesheet(title){
    var i;
	var style_object;
	style_sheets=[""];
    for(i=0; (style_object=document.getElementsByTagName("link")[i]); i++) {
        if(style_object.getAttribute("rel") ==
		"alternate stylesheet" && style_object.getAttribute("title")) {
            style_object.disabled = true;
            style_sheets.push(style_object);
            if(style_object.getAttribute("title") === title)
                style_object.disabled = false;
        }
    }
    return "";
}

function choose_style(style_title){
	if (document.getElementById){
		set_stylesheet(style_title);
	}
}

function execute_key_event(key_code){
	var ul_menu = document.getElementById('ul_elem' + current_page);
    var l = ul_menu.getElementsByTagName('li').length;
	var result = document.getElementsByClassName("col-1")[1];
	var scroll_top = result.scrollTop;
    var div_height = result.offsetHeight;
    var scroll_height = result.scrollHeight;

	switch (key_code) {
		case 38: // keyup
			if(current_item === 'li_horizontal_1'){
				if(document.getElementById('next').innerHTML != ""){
                    current_item = 'next';
                }else if (document.getElementById('prev').innerHTML != ""){
                    current_item = 'prev';
                }else{
                    current_item = 'li_' + l;
					list_index = l;
                }
			}else if(current_item === 'next' || current_item === 'prev'){
                list_index = ((current_page - 1) * ELEMENTS_PER_PAGE ) + l;
                current_item = 'li_'+ list_index;
            }else if(current_item === 'li_'+ list_index){
                if(list_index === 
                    ((current_page - 1) * ELEMENTS_PER_PAGE ) + 1){
					horizontal_index = 1;
                    current_item = 'li_horizontal_1';
                }
                else{
                    list_index = list_index - 1;				
                    current_item = 'li_'+ list_index;
                }

            }else if(current_item === 'li_horizontal_' + horizontal_index){
                current_item = 'result_area';
            }else if(current_item === 'back'){
                current_item = 'result_area';
            }else if(current_item === 'result_area'){
                if (scroll_top - div_height <= 0) {
					horizontal_index = number_of_horizontal_tabs;
                    current_item = 'li_horizontal_' + horizontal_index;
                    result.scrollTop = 0;
                } else {
				    result.scrollTop -= 100;
                }
			}
            break;
		case 40: //keydown
			if(current_item === 'li_horizontal_1'){
                list_index = ((current_page - 1) * ELEMENTS_PER_PAGE ) + 1;
                current_item = 'li_' + list_index;
            }else if(current_item === 'li_'+ list_index){
                if(list_index - ((current_page - 1) * ELEMENTS_PER_PAGE) != l){
                    list_index = list_index + 1;				
                    current_item = 'li_'+ list_index;
                }
                else if(document.getElementById('next').innerHTML != ""){
                    current_item = 'next';
                }else if (document.getElementById('prev').innerHTML != ""){
                    current_item = 'prev';
                }else{
					list_index = 1;
                    current_item = 'li_' + list_index;
                }
            }else if(current_item === 'next' || current_item === 'prev'){
                list_index = 1;
				horizontal_index = 1;
                current_item = 'li_horizontal_1';
            }else if(current_item === 'back'){
                horizontal_index = number_of_horizontal_tabs;
                current_item = 'li_horizontal_' + horizontal_index;
            }else if(current_item === 'li_horizontal_' + horizontal_index){
                current_item = 'result_area';
            }else if(current_item === 'result_area'){
                if (scroll_top + div_height >= scroll_height) {
					current_item = 'back';
                    result.scrollTop = scroll_height;
                } else {
				    result.scrollTop += 100;
                }
			}
            break;
		case 37: //keleft
			if(current_item === 'li_horizontal_'+ horizontal_index){
                if(horizontal_index === 1){
                    horizontal_index = number_of_horizontal_tabs;
                }else{
                    horizontal_index = horizontal_index - 1;
                }
                current_item = 'li_horizontal_'+ horizontal_index;
            }else if(current_item === 'li_'+ list_index){
                if(document.getElementById('prev').innerHTML != ""){
                    current_item = 'prev';
                }else if (document.getElementById('next').innerHTML != ""){
                    current_item = 'next';
                }else{
					horizontal_index = 1;
                    current_item = 'li_horizontal_1';
                }
            }else if(current_item === 'prev'){
                current_item = 'back';
            }else if(current_item === 'next'){
                if(document.getElementById('prev').innerHTML != ""){
                    current_item = 'prev';
                }else{
                    current_item = 'back';
                }
            }else if(current_item === 'back'){
                if(document.getElementById('next').innerHTML != ""){
                    current_item = 'next';
                }else if (document.getElementById('prev').innerHTML != ""){
                    current_item = 'prev';
                }else{
					horizontal_index = 1;
                    current_item = 'li_horizontal_1';
                }
            }else if(current_item === 'result_area'){
				horizontal_index = 1;
                current_item = 'li_horizontal_1';
			}
            break;
		case 39: //keyright
			if(current_item === 'li_horizontal_'+ horizontal_index){
                if(horizontal_index === number_of_horizontal_tabs){
                    horizontal_index = 1;
                }else{
                    horizontal_index = horizontal_index + 1;
                }
                current_item = 'li_horizontal_'+ horizontal_index;
            }else if(current_item === 'li_'+ list_index){		
                if(document.getElementById('next').innerHTML != ""){
                    current_item = 'next';
                }else if (document.getElementById('prev').innerHTML != ""){
                    current_item = 'prev';
                }else{
				    horizontal_index = 1;
                }
            }else if(current_item === 'next'){
                current_item = 'back';
            }else if(current_item === 'prev'){
                if(document.getElementById('next').innerHTML != ""){
                    current_item = 'next';
                }else{
                    current_item = 'back';
                }
            }else if(current_item === 'back'){
                if(document.getElementById('prev').innerHTML != ""){
                    current_item = 'prev';
                }else if (document.getElementById('next').innerHTML != ""){
                    current_item = 'next';
                }else{
					horizontal_index = 1;
                    current_item = 'li_horizontal_1';
                }
            }else if(current_item === 'result_area'){
				current_item = 'back';
			}
            break;
		case 13: //enter
			if(current_item === 'back'){
                window.location = "init.html";
            }else if(current_item === 'next'){
                page_number++;
                display(page_number);
				if(document.getElementById('next').innerHTML != "")
                    current_item = 'next';
				else
				    current_item = 'prev';
            }else if(current_item === 'prev'){
                page_number--;
                display(page_number);
				if(document.getElementById('prev').innerHTML != "")
                    current_item = 'prev';
				else
				    current_item = 'next';
            }else if(current_item === 'li_horizontal_1'){
				execute_testcases();
			}else if(current_item === 'li_'+ list_index){
				execute_testcase(list_index);
			}else if(current_item === 'li_horizontal_2'){
			    show_result_field("total_result");
			}else if(current_item === 'li_horizontal_3'){
			    show_result_field("passed_result");
			}else if(current_item === 'li_horizontal_4'){
			    show_result_field("failed_result");
			}else if(current_item === 'li_horizontal_5'){
                show_result_field("event_result");
			    //change_theme();
			}else if(current_item === 'li_horizontal_6'){
			    monitor_task_instance.print_report();
            }
			break;
	}
}
function defocus_current_item() {
	if(current_item === 'li_horizontal_' + horizontal_index){
		if(current_item === current_screen){
			document.getElementById(current_item).setAttribute('class','h_tab_selected');
		}else{
			document.getElementById(current_item).setAttribute('class','h_tab');
		}
	}else if(current_item === 'li_' + list_index){
		document.getElementById(current_item).setAttribute('class','list_bg');
	}else if(current_item === 'next' || current_item === 'prev'){
        document.getElementById(current_item).
        style.backgroundColor = "";
	}else if(current_item === 'back'){
        document.getElementById(current_item).style.background = 
        "url(images/back.png) no-repeat right bottom";
	}else if(current_item === 'result_area'){
	    document.getElementsByClassName("col-1")[1].style.border = "";
	}
}
function focus_current_item() {
	if(current_item === 'li_horizontal_' + horizontal_index){
		document.getElementById(current_item).setAttribute('class','h_tab_focus');
	}else if(current_item === 'li_' + list_index){
		document.getElementById(current_item).setAttribute('class','list_bg_focus');
		if(!is_run_all)
			show_description(list_index);
	}else if(current_item === 'next' || current_item === 'prev'){
        document.getElementById(current_item).
        style.backgroundColor = "gold";
	}else if(current_item === 'back'){
        document.getElementById(current_item).style.background = 
        "url(images/back-active.png) no-repeat right bottom";
	}else if(current_item === 'result_area'){
        document.getElementsByClassName("col-1")[1].style.border =
		"2px solid blue";
	}
}

var sub_index;
var testcase_list = new Array();
var util = new utils();
var prefix = new prefixes();
function read_testcase_file(){
    next_sel_tab = "hoz";	
    var xmlhttp;
    if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    url = new String(window.location);
    url = unescape(url);
    var temp = url.split("#");
    var index = temp[1];
    var processing_file_lines_ = new Array();
    sub_sel = index;
    processing_line_ = 0;
    xmlhttp.onreadystatechange=function(){
        if (xmlhttp.readyState === 4 ){ //&& xmlhttp.status==200
            var all_text=xmlhttp.responseText;
            process_file_text(all_text);			
        }
    }
    xmlhttp.open("GET", index,true);
    xmlhttp.send();
}
function read_constant_file(){	

    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    var processing_file_lines_ = new Array();
    processing_line_ = 0;
    xmlhttp.onreadystatechange=function(){
        if (xmlhttp.readyState === 4 ){ //&& xmlhttp.status==200
            var all_text = xmlhttp.responseText;
            process_constants(all_text);			
        }
    }
    xmlhttp.open("GET", "testcase/" + "hdstl_html_define.csv",true);
    xmlhttp.send();
}
var testcase_parser = new testcase_parser();
var variable_parser;
function process_constants(all_text){
    variable_parser = new variable_parser();
    variable_parser.processing_task(all_text);
    read_testcase_file();
}
function process_file_text(all_text){
    variable_parser.processing_task(all_text);
    testcase_parser.parse_testcase(all_text);
    populate_testcase();
    display(current_page);

    document.getElementById("li_horizontal_1").
        setAttribute("onClick","execute_testcases()");
}

function clear_all_testcase_result_image() {
     for(var i = 1; i <= testcase_list.length; i++){
         var image_id = "img_" + i;
         document.getElementById(image_id).src="images/status_not_tested.png";
    }
}

function execute_testcases(){
	if(is_run_all || is_running) {
		return;
    }
	is_run_all = true;
	show_result_field('total_result');
	clear_result_fields();
    clear_all_testcase_result_image();   
	display(1);
    monitor_task_instance.run_all_testcases();
}
function execute_testcase(testcase_index){
    if(is_run_all || is_running) {
		return;
    }
	is_running = true;
    show_result_field('total_result');
    clear_result_fields();
    current_testcase_index = testcase_index;
    monitor_task_instance.run_testcase(testcase_list[testcase_index - 1]);
}

function clear_result_fields(){
    current_testcase_index = 1;
    $("#passed").html("PASSED");
    $("#failed").html("FAILED");
    $("#total").html("TOTAL");
    $("#not_tested").html("NOT_TESTED" + testcase_list.length);
    $("#event_tab").html("EVENT");
	$("#total_result").html("");
	$("#passed_result").html("");
	$("#failed_result").html("");
    $("#event_result").html("");
	document.getElementById("print").style.display = 'none';
	number_of_horizontal_tabs = 5;
	total_count = 0;
    passed_count = 0;
    failed_count = 0;
	total_log = "";
    passed_log = "";
    failed_log = "";
    //event_log = "";
    remaining_count = testcase_list.length;
}

function populate_testcase(){
    remaining_count = testcase_list.length;
    for(var i = 1; i <= testcase_list.length; i++){
        var l  = testcase_list.length < page_number * ELEMENTS_PER_PAGE ? 
                testcase_list.length : page_number * ELEMENTS_PER_PAGE;
        //var start = page_number === 1 ? 1 : 
            //(page_number-1) * ELEMENTS_PER_PAGE + 1;
        var left = testcase_list.length < page_number * ELEMENTS_PER_PAGE ?
                l%ELEMENTS_PER_PAGE : ELEMENTS_PER_PAGE;
        //var ul_elm = document.getElementById("ul_menu");
        var span_list = document.getElementById("list");
        var ul_elm = document.createElement('ul');
        var ul_id = 'ul_elem' + page_number;
        ul_elm.setAttribute('id', ul_id);
        ul_elm.innerHTML ="";
        span_list.appendChild(ul_elm);
        for( j = 1; j <= left; j++){
            var li_elm = document.createElement('li');
            li_elm.setAttribute('id', 'li_' + i);
            ul_elm.appendChild(li_elm);

            var link = document.createElement('a');
            link.setAttribute('href', url);
            link.setAttribute('id', 'a_' + i);
			link.setAttribute("onClick","execute_testcase(" + i + ")");
			link.setAttribute("OnMouseOver","testcase_mouse_over(" + i + ")");
            li_elm.appendChild(link);
            var strong = document.createElement('strong');
            var image_id = "img_" + i;
            strong.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;' + i +')' +
                testcase_list[i-1].get_title().substring(0,50) + 
                '<img  src="images/status_not_tested.png" id = ' + 
                image_id +
                ' width="20" height="20" align="right" style="margin: 2px 2px">';
            link.appendChild(strong);
            i++;
        }
        document.getElementById(ul_id).style.display = 'none';
        i--;
        ++page_number;
    }
    number_of_pages = page_number - 1;
    page_number = 1;
    document.getElementById('li_horizontal_1').style.background = 
        "url(images/m-act.png) no-repeat";
    var temp = url.split("#");

    var index = temp[1];
    $('#heading').html("<h4>TestCase List : " + index.substring(9) + "</h4>");
    $('#version').html("<b> Version : " + VERSION + "</b>");
    $('#not_tested').html("Not Tested : " + remaining_count);
	create_result_text_feilds();
}
function display(new_page_number){
	if(new_page_number > number_of_pages)
		return false;
	page_number = new_page_number;
    document.getElementById('ul_elem'+current_page).style.display = 'none';
    document.getElementById('ul_elem'+page_number).style.display = '';
    current_page = page_number;
    $("#prev").html("");	
    $("#next").html("");
    
    if(current_page != 1){
        var prev = document.createElement('a');
        prev_index = parseInt(sub_index) - 1;
        prev.setAttribute("href", "javascript:void(0);");
        prev.setAttribute("name", "prev");
        var pn = page_number - 1;	
        prev.setAttribute("onClick","display(" + pn + ")");
        prev.innerHTML = " << ";
        document.getElementById('prev').appendChild(prev);

    }
    if(current_page != number_of_pages){
        var next = document.createElement('a');
        next_index = parseInt(sub_index) + 1;
        next.setAttribute("href", "javascript:void(0);");
        next.setAttribute("name", "next");
        var pn = page_number + 1;
        next.setAttribute("onClick","display(" + pn + ")");
        next.innerHTML = ">>";
        document.getElementById('next').appendChild(next);
    } 
	if(current_item.charAt(0) === 'l' && current_item.charAt(3) != 'h')
		{
			document.getElementById(current_item).style.background = 
			"url(images/list-bg.png) repeat-x";
			horizontal_index = 1;
			current_item = 'li_horizontal_1';
			document.getElementById(current_item).style.background = 
			"url(images/m-act.png) no-repeat";
		}
}
function change_img(index,is_passed){
     var image_id = "image_" + index;
    if(is_passed){
        document.getElementById(image_id).src="images/status_passed.png";
//        $("#passed").html("");
//        $("#passed").html("Passed : " + (++passed_count));
    }else{
        document.getElementById(image_id).src="images/status_failed.png";
        $("#failed").html("");
        $("#failed").html("Failed : " + (++failed_count));
    }
//    $("#total").html("");
//    $("#total").html("Total : " + (++total_count));
//    $("#not_tested").html("");
//    $("#not_tested").html("Not Tested : " + (--remaining_count));
}
function testcase_mouse_over(index){
    if(is_run_all){
		return;
	}else{
	    show_description(index);
	}
}
function show_description(index){
	var testcase = testcase_list[index - 1];
	var name = testcase.get_title();
    var list_of_command_element_list = testcase.get_list_of_command_string();

	var commands ="";
	for(var i =0; i<list_of_command_element_list.length; i ++){
		commands += list_of_command_element_list[i] + "<br/>";
	}
    $("#description").html("<b>" + name + "</b><br/>" + commands);
}

function create_result_text_feilds(){
	result_area = document.getElementById("result");
	var total_result = document.createElement('span');
	total_result.setAttribute('id', 'total_result');
	var passed_result = document.createElement('span');
	passed_result.setAttribute('id', 'passed_result');
	var failed_result = document.createElement('span');
	failed_result.setAttribute('id', 'failed_result');	
    var event_result = document.createElement('span');
    event_result.setAttribute('id', 'event_result');
	var current_result = document.createElement('span');
	current_result.setAttribute('id', 'current_result');	
	result_area.appendChild(total_result);
	result_area.appendChild(passed_result);
	result_area.appendChild(failed_result);
    result_area.appendChild(event_result);
	result_area.appendChild(current_result);
	document.getElementById('total_result').style.display = 'none';
	document.getElementById('passed_result').style.display = 'none';
	document.getElementById('failed_result').style.display = 'none';
    document.getElementById('event_result').style.display = 'none';
	document.getElementById('current_result').style.display = 'none';
	document.getElementById('print').style.display = 'none';
	document.getElementById("total").setAttribute("onClick","show_result_field" + "('total_result')");
	document.getElementById("passed").setAttribute("onClick","show_result_field" + "('passed_result')");
	document.getElementById("failed").setAttribute("onClick","show_result_field" + "('failed_result')");
    document.getElementById("event_tab").setAttribute("onClick","show_result_field" + "('event_result')");
	document.getElementById("print").setAttribute("onClick", "monitor_task_instance.print_report()");
	//document.getElementById("text_colour").setAttribute("onClick", "change_theme()");
	
	init_event_listener();
}
var theme = 1;
function change_theme(){
	choose_style(themes[theme]);
	theme++;
	if(theme === themes.length)
		theme = 0;	
}
function update_command_name(command_name,is_passed){
    var message = "";
    message = "<br/>  <b>&nbsp;&nbsp;&nbsp;&nbsp;" +command_name + "</b>";
	total_log += message ;
	document.getElementById("current_result").innerHTML += message ;
	if(is_passed)
	    passed_log += message ;
	else
	    failed_log += message ;
}

function update_testcase_name(testcase_name,is_passed){
    var message = "";
    message = "<b><br/>" + testcase_name + "</b>";
	total_log += message ;
	$("#current_result").html(message);
	if(is_passed)
	    passed_log += message ;
	else
	    failed_log += message ;
}

function show_message(input_message){
    var message = "";
    message = "<b><br/>" + input_message + "</b>";
    total_log += message ;
    $("#current_result").html(message);
    passed_log += message ;
    failed_log += message ;
}

function update_result_field(result_description,is_passed){
    var message = "";
    message = "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
    result_description + "<br/>" ;
	total_log += message ;
	document.getElementById("current_result").innerHTML += message ;
	if(is_passed)
	    passed_log += message ;
	else
	    failed_log += message ;
}

function update_event_result_field(result_description){
    var message = "";
    message = "[EVENT] " + result_description + "<br/>";
    total_log += message ;
    document.getElementById("current_result").innerHTML += message ;
    event_log = message + event_log;
    show_result_field("total_result");
}

function show_result_field(result_field){
	document.getElementById(current_result_screen).style.display = 'none';
	change_tab_highlight(current_result_screen,false);
	if(is_run_all){
		//document.getElementById("current_result").style.display = '';
		current_result_screen = "current_result";
	}
	else{
	    if(result_field === "total_result"){
		    $("#" + result_field).html(total_log);
		} else if(result_field === "passed_result"){
			$("#" + result_field).html(passed_log);
		} else if(result_field === "failed_result"){
			$("#" + result_field).html(failed_log);
        } else if(result_field === "event_result"){
            $("#" + result_field).html(event_log);
        } 
		document.getElementById(result_field).style.display = '';
		current_result_screen = result_field;
	}
	change_tab_highlight(current_result_screen,true);
}
function change_tab_highlight(display_field,is_active){
    if(display_field === "total_result"){
        current_screen = "li_horizontal_2";
	}else if(display_field === "current_result"){
	    current_screen = "li_horizontal_2";
	}else if(display_field === "passed_result"){
	    current_screen = "li_horizontal_3";
	}else if(display_field === "failed_result"){
	    current_screen = "li_horizontal_4";
	}else if(display_field === "event_result") {
	    current_screen = "li_horizontal_5";
//	}else{
//	    current_screen = "li_horizontal_4";
	}
	if(is_active){
		document.getElementById(current_screen).setAttribute('class','h_tab_selected');
	}else{
		document.getElementById(current_screen).setAttribute('class','h_tab');
	}
}

restartTimer();