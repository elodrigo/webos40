var	current_selection = 1;
$("document").ready(function() {
	$(document).keydown(function(e) {
		var ul_menu = document.getElementById('ul_menu');
		var l = ul_menu.getElementsByTagName('li').length;

		document.getElementById('li_' + current_selection).style.background = 
		    "url(images/list-bg.png) repeat-x";
		switch (e.keyCode) {
			case 38: // keyup
				if(current_selection === 1){
					current_selection = l;
				}else{
					current_selection = current_selection - 1;
				}
				break;
			case 40: //keydown
				if(current_selection === l){
					current_selection = 1;
				}else{
					current_selection = current_selection + 1;
				}	
				break;
			case 13: //enter
				{
					window.location = "testcase_list.html#" + 
					    model_list_parser_.get_testcase_file_path(
					            model_array[current_selection-1]) ;
				}
		}
		document.getElementById('li_' + current_selection).style.background=
		    "url(images/list-bg-hover.png) repeat-x";
	});		
});
var lines;
function read_model_list_file(){
	var xmlhttp;

	if (window.XMLHttpRequest) {//code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		try {
			xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {
			xmlhttp = false;
			}
		}

	}
	xmlhttp.onreadystatechange = function(){
		if (xmlhttp.readyState === 4 ){ //&& xmlhttp.status==200
			var raw_text = xmlhttp.responseText;
			process_model_list(raw_text);
			//lines = raw_text.split("\n");
		}
	}
	xmlhttp.open("GET","htaf_testcases.csv",true);
	xmlhttp.send();
}

var model_array = new Array();
var model_list_parser_;
function process_model_list(raw_text){
	model_list_parser_ = new model_list_parser();
	model_list_parser_.parse_model_list_file(raw_text);
	model_array = model_list_parser_.get_model_list();
	//display_model_list();
	console.log(model_array);
}
