/*
 * 
 */
var testcases;
var total_testcases_number = 0;
document.getElementById('select_file').addEventListener('change', load_client_file, false);

function load_server_file(filePath) {
	  var result = null;
	  var xmlhttp = new XMLHttpRequest();
	  xmlhttp.open("GET", filePath, false);
	  xmlhttp.send();
	  if (xmlhttp.status==200) {
	    result = xmlhttp.responseText;
	  }
	  return result;
}

function load_server_usb_file(filePath) {
	  var result = null;
	  console.log(filePath);
	  var xmlhttp = new XMLHttpRequest();
	  xmlhttp.open("GET", filePath, false);
	  xmlhttp.onreadystatechange = function ()
	    {
	        if(xmlhttp.readyState === 4)
	        {
	            if(xmlhttp.status === 200 || xmlhttp.status == 0)
	            {
	                var allText = xmlhttp.responseText;
	            }
	        }
	    }
	  
	  xmlhttp.send();
	  if (xmlhttp.status==200) {
	    result = xmlhttp.responseText;
	  }
	  return result;
}

//it is not working properly on chrmoe 
function load_client_file(evt){

	var files = evt.target.files;

    for (var i = 0, f; f = files[i]; i++)
    {

        var reader = new FileReader();
        reader.onload = (function(reader)
        {
            return function()
            {
                var contents = reader.result;
                var lines = contents.split('\n');

                console.log(contents);
                testcases = contents;
                update_init_table(testcases);
                testcases_parsing(testcases);
                run_all();
            }
        })(reader);

        reader.readAsText(f);
    }

}

/*
 * update_table format
 */
let delim = ",";
var comment = false;

function update_init_table(testcases) {
    let file = testcases;
    let string ="";
    
    if (testcases) {
	    let contents = testcases; 
	    let rows = contents.split(/\r?\n/); 
	
	    // Read the CSV 
	    let tbl = document.createElement("table");
	    rows.forEach(function(part, i, rowArr){
	        if (rowArr[i]){
	            rowArr[i] = rowArr[i].split(" ");
	            let tr = document.createElement("tr");
	            rowArr[i].forEach(function(part2, j, valArr){
	            	if(!valArr[j].indexOf('>')){
	                    let td = document.createElement("td");
	                    td.appendChild(document.createTextNode(valArr[j]));
	                    tr.appendChild(td);
	                    string = valArr[j];
	               	}

	            });
	        
	            if(!comment){
	                let td = document.createElement("td");
	                var image = document.createElement("img");
	                image.setAttribute("src", "images/status_not_tested.png");
	                image.setAttribute("id", "image_"+i);
	                image.setAttribute("width",20);
	                image.setAttribute("height",20);
	                td.appendChild(image);
	                tr.appendChild(td);
	                total_testcases_number = i;
	            }
	            
	            tr.setAttribute("align", "center")
	            tbl.appendChild(tr);
	        }
	    });
	    tbl.setAttribute("border", "2");
	
	    let main = document.getElementById("fileContents");
	    console.log(main);
	    main.parentNode.replaceChild(tbl, main);
	    
	    let total = document.getElementById("total");
	    total_testcases_number++;
	    total.innerHTML = "Total Testcases: " + total_testcases_number;
    }
}


/*
 * draw_output
 */
/*
function processData(csv) {
    var allTextLines = csv.split(/\r\n|\n/);
    var lines = [];
    while (allTextLines.length) {
        lines.push(allTextLines.shift().split(','));
    }
	console.log(lines);
	//drawOutput(lines);
}
*/
function errorHandler(evt) {
	if(evt.target.error.name == "NotReadableError") {
		alert("Canno't read file !");
	}
}

function drawOutput(lines){
	//Clear previous data
	document.getElementById("fileContents").innerHTML = "";
	var table = document.createElement("table");
	for (var i = 0; i < lines.length; i++) {
		var row = table.insertRow(-1);
		for (var j = 0; j < lines[i].length; j++) {
			var firstNameCell = row.insertCell(-1);
			firstNameCell.appendChild(document.createTextNode(lines[i][j]));
		}
		
	}
	document.getElementById("fileContents").appendChild(table);
}

