var interval_time_ = 1000;  // ms
var iteration_number_ = 1; 
var total_passed_number = 0;
var total_failed_number = 0;
var total_tested_number = 0;
var total_not_tested_number = 0;

﻿var max_time_out_count = 10000; // ms
var have_to_check_timeout = true;
var testcase_number = 0;
var testcase_tested = new Array();
var interval_id;
var ret;

//compare between JSON object
function equals_function(x, y) { 
	
	if (x === y) 
		return true;  
	if (!(x instanceof Object) || !(y instanceof Object)) 
		return false;
	if (x.constructor !== y.constructor) 
		return false;

	for (var p in x) { 
		if (!x.hasOwnProperty(p)) 
			continue; 
		if (!y.hasOwnProperty(p)) 
			return false; 
		if (x[p] === y[p]) 
			continue;
		if (typeof(x[p]) !== "object") 
			return false; 
		if (!equals_function(x[p], y[p])) 
			return false;
	}

	for (p in y) { 
		if (y.hasOwnProperty(p) && !x.hasOwnProperty(p)) 
			return false; 
	}
	
	return true;
}

//image change
function change_img(index,is_passed){
   var image_id = "image_" + index;
   if(is_passed){
       document.getElementById(image_id).src="images/status_passed.png";
   }else{
       document.getElementById(image_id).src="images/status_failed.png";
   }
}

function clear_img(){  
	for(var index =0; index < total_testcases_number; index++){
		var image_id = "image_" + index;
		document.getElementById(image_id).src="images/status_not_tested.png";
	}
}

// testcases call using websocket callback function
function test_case_call(testcase_number){
	var object;
	var array = new Array();
	total_tested_number++;
	
	ltf_call_websocket(testcase_number, function(ret) {        
		object = JSON.parse(ltf_expected_result[testcase_number]);
		console.log(ret);
		//console.log(JSON.stringify(ret));
		
		var result = equals_function(ret, object);
		console.log("> result : " + result);
		
		if(result){
			total_passed_number++;
			change_img(testcase_number, true);
		}
		else{
			total_failed_number++;
			change_img(testcase_number, false);
		}
		testcase_tested[testcase_number] = true;
		
		//screen update for test status
		total_not_tested_number = total_tested_number - total_passed_number - total_failed_number;
		let passed_number_ = document.getElementById("passed");
		let failed_number_ = document.getElementById("failed");
		let not_tested_number_ = document.getElementById("not_tested");
	
		passed_number_.innerHTML = "Passed : " + total_passed_number;	
		failed_number_.innerHTML = "Failed : " + total_failed_number;
		not_tested_number_.innerHTML = "Not Responsed : " + total_not_tested_number;  	
     });
}

function test_case_task() {
	test_case_call(testcase_number);
	testcase_number++;

    if(testcase_number >= ltf_command.length){
    	if(iteration_number_ - 1){
    		testcase_number = 0;
    		iteration_number_--;
    	}
    	else{
    		ltf_clearInterval(test_case_task, interval_id);
    		iteration_number_ = 0;
    	}
    	
    }
    if(testcase_number ==0)
    	setTimeout(function(){
    		clear_img();
    	},1000);
}

// excute testcase with interval time and iteraction number
function run_all(testcase) {
	interval_time_ = document.getElementById('_interval_time').value;
	iteration_number_ = document.getElementById('_iteration_number').value;
	interval_id = ltf_setInterval(test_case_task, test_case_task, interval_time_);
}

function interval_time(event){
	event = event || window.event;
	var keyID = (event.which) ? event.which : event.keyCode;
	if ( (keyID >= 48 && keyID <= 57) || (keyID >= 96 && keyID <= 105) || keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39 ){
		return true;
	} 
	else{
		interval_time_ = 1000;
		return false;
	}
}

function iteration_number(event){
	event = event || window.event;
	var keyID = (event.which) ? event.which : event.keyCode;
	if ( (keyID >= 48 && keyID <= 57) || (keyID >= 96 && keyID <= 105) || keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39 ){
		return true;
	} 
	else{
		iteration_number_ = 1;
		return false;
	}
}

function removeChar(event) {
	return;
	event = event || window.event;
	var keyID = (event.which) ? event.which : event.keyCode;
	if ( keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39 ) 
		return;
	else
		event.target.value = event.target.value.replace(/[^0-9]/g, "");
}